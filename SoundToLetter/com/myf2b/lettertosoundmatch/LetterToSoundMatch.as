﻿package com.myf2b.lettertosoundmatch 
{
	
	import com.avr.XmlLoader;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.Session;
	import com.myf2b.lettertosoundmatch.LetterToSoundMatchShell;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.utils.setTimeout;
	import com.myf2b.ui.*;
	
	import mx.events.MoveEvent;
		
	
	/**
	 * 
	 * @author haravallabhanjn
	 * 
	 */
	public class LetterToSoundMatch extends MovieClip {
		private var xmlLoader:XmlLoader;
		public var xml:XML;
		public static var presetsXML:XML;
		
		public static const BACK_TO_MAP:String = "back_to_map";
		public static var LETTER_TO_DEAL:String= "m";
		private var _letterXML:XMLList;
		public static var monsterLoaderArr = [];	
		public static var blueBoardLoaderArr = [];
		public static var blackLipMonsterArr = []
		
		public var blueBrd_ldr:Loader = new Loader();
		
		public static var blackLip_mc:MovieClip = new MovieClip();
		

		//public static var mainScreenProxy:LetterToSoundMatch;
		public static var gameIntroProxy:GameIntroScreen
		public static var gameSpaceProxy:GameSpace;
		public static var gameScreenProxy:GameScreen;
		public var loader:CoreLoader = new CoreLoader();
		
		public static var liploader:Loader = new Loader();
		
		
		public static var monsterMC:MovieClip;
		public static var _contentKey:String = "1";
		private var _gameXMLURL:String;
		
		public static var moduleSessionID:String;
		private var ths:*;
		private var stg:* ;
		private var getMonsterWidth:Number =0;
		private var getMonsterHeight:Number =0;
		public static  var monsterCoverMc:MovieClip;
		private var getMonsterWidth1:Number =0;
		private var getMonsterHeight1:Number = 0;
		private var letter_arr = [];
		private var instructionText:String;
		
		private var _sync : TextFieldSoundSync;
		private var maxWidth:Number = 250;
		private var maxHeight:Number = 250;
		
		/**
		 * 
		 * @param contentKey
		 * 
		 */
		
	public function LetterToSoundMatch(aThs,contentKey:String="1") {
		
			//trace(" LetterToSoundMatch ")
			ths = LetterToSoundMatchShell.isThis();
			stg = LetterToSoundMatchShell.isStage();			
		//	ths = aThs
		
			addEventListener(Event.ADDED,init);
			addEventListener(Event.REMOVED_FROM_STAGE,destroy);
			_contentKey = contentKey;
			moduleSessionID = Reporter.getNewModuleSessionID();//To DB		
			trace("Reporter = "+Reporter)
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
			//trace("Listen and Record destroy");
			//gameScreenProxy = null;
			//mainScreenProxy = null;
			//gameSpaceProxy = null;
			_contentKey = null;
			SoundMixer.stopAll();
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function init(e:Event):void {
			removeEventListener(Event.ADDED,init);
			stop();
			visible=false;
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else {
			//	_gameXMLURL = Core.filesURL+"/listen_and_record/"+_contentKey+"/game.xml";
			}
			
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/4/xml_feed", presetsXMLLoaded);//game.xml
		//	xmlLoader = new XmlLoader("game.xml", presetsXMLLoaded)
			
		}
		
		/**
		 * 
		 * 
		 */
		private function presetsXMLLoaded():void {
		//	presetsXML = XML(e.target.data);
			presetsXML = XML(xmlLoader.data);				
			setTimeout(showStuff,100);
			gotoAndStop(2);			
			gameIntroProxy = new GameIntroScreen();
			gameIntroProxy.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", endOfIntroScreen_Handler);	
			
		}
		
		private function endOfIntroScreen_Handler(e:Event):void 
		{
			LETTER_TO_DEAL = gameIntroProxy.selectedObject;
			gameScreenProxy = new GameScreen();		
			
			trace("chk deal== "+LETTER_TO_DEAL)
			loadXMLListForLetter();			
			loadBlackLipMonsterGraphic()
			loadMonsterGraphic();
			
		}
		
		
		
		 
		private function loadMonsterGraphic():void{
			loader.load(new URLRequest(_letterXML.monsterGraphic))					
			loader.cacheAsBitmap = true;			 
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,loadComplete);					 	 
		}
		
		private function loadComplete(e:Event):void{
			if(monsterLoaderArr == null){
				monsterLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			monsterLoaderArr[0] = new MovieClip()			
			monsterLoaderArr[0] =  (e.target as LoaderInfo).content
			monsterLoaderArr[0].x = 370;//_letterXML.monsterLocationX;
			monsterLoaderArr[0].y = 450;//_letterXML.monsterLocationY;
			
			var ratio:Number;//ratio
			ratio = monsterLoaderArr[0].height/monsterLoaderArr[0].width;//calculation ratio
			//trace(ratio)
			if (monsterLoaderArr[0].width>maxWidth) {
				monsterLoaderArr[0].width = maxWidth;
				monsterLoaderArr[0].height = Math.round(monsterLoaderArr[0].width*ratio);
			}

			if (monsterLoaderArr[0].height>maxHeight) {
				monsterLoaderArr[0].height = maxHeight;
				monsterLoaderArr[0].width = Math.round(monsterLoaderArr[0].height/ratio);
			}
			 			 
			
			ths.addChild(monsterLoaderArr[0])		 
			monsterLoaderArr[0].buttonMode = true;
			monsterLoaderArr[0].visible = false;
			getMonsterWidth = monsterLoaderArr[0].width;
			getMonsterHeight = monsterLoaderArr[0].height;		
			loadMonsterHandBlueBoard();	
							 
		}	
		private function loadBlackLipMonsterGraphic():void 
		{
			trace("loadBlackLipMonsterGraphic"+_letterXML.blackLipMonsterGraphic)
			var blackLipMonster_ldr:CoreLoader = new CoreLoader();
			blackLipMonster_ldr.load(new URLRequest(_letterXML.blackLipMonsterGraphic))					
			blackLipMonster_ldr.cacheAsBitmap = true;			 
			blackLipMonster_ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, blackLipMonster_LoadComplete);				
		}
		
		private function blackLipMonster_LoadComplete(e:Event):void 
		{			
			if(blackLipMonsterArr == null){
				blackLipMonsterArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			blackLipMonsterArr[0] = new MovieClip()			
			blackLipMonsterArr[0] =  (e.target as LoaderInfo).content
			blackLipMonsterArr[0].x = 370;//_letterXML.monsterLocationX;
			blackLipMonsterArr[0].y = 450;//_letterXML.monsterLocationY;			
			var ratio:Number;//ratio
			ratio = blackLipMonsterArr[0].height/blackLipMonsterArr[0].width;//calculation ratio
			//trace(ratio)
			if (blackLipMonsterArr[0].width>maxWidth) {
				blackLipMonsterArr[0].width = maxWidth;
				blackLipMonsterArr[0].height = Math.round(blackLipMonsterArr[0].width*ratio);
			}
			if (blackLipMonsterArr[0].height>maxHeight) {
				blackLipMonsterArr[0].height = maxHeight;
				blackLipMonsterArr[0].width = Math.round(blackLipMonsterArr[0].height/ratio);
			}
			 	 
			
			ths.addChild(blackLipMonsterArr[0])
		}
		

		
		
		
		private function loadMonsterHandBlueBoard():void 
		{			 
			trace("_letterXML.monsterLetterBoard= "+_letterXML.monsterLetterBoard)
		//	blueBrd_ldr.load(new URLRequest(presetsXML.variables.monsterHandBoardGraphic))			 
			blueBrd_ldr.load(new URLRequest(_letterXML.monsterLetterBoard))			 
			blueBrd_ldr.contentLoaderInfo.addEventListener(Event.COMPLETE,monsterBlueBoard_loadComplete);
			blueBrd_ldr.cacheAsBitmap = true;				
		}
		
		private function monsterBlueBoard_loadComplete(e:Event):void 
		{
			if(blueBoardLoaderArr == null){
				blueBoardLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			
			blueBoardLoaderArr[0] = new MovieClip();			
			blueBoardLoaderArr[0] =  (e.target as LoaderInfo).content
			blueBoardLoaderArr[0].x = 300//presetsXML.variables.monsterHandBoardLocationX;
			blueBoardLoaderArr[0].y = 390//presetsXML.variables.monsterHandBoardLocationY;				
			ths.addChild(blueBoardLoaderArr[0])		
			ths.setChildIndex(blueBoardLoaderArr[0], ths.numChildren-1)
			blueBoardLoaderArr[0].buttonMode = true;	
			//blueBoardLoaderArr[0].monsterHandBoard.gotoAndStop(LETTER_TO_DEAL)			
			blueBoardLoaderArr[0].visible = true;		
			
			getMonsterWidth1  = blueBoardLoaderArr[0].width
			getMonsterHeight1 = blueBoardLoaderArr[0].height			
			loadMonsterCover()		
		}
		
		
		
			//
		private function loadMonsterCover():void 
		{
			/////////////////////////////////////////////////////
			// Important for validation//
			////////////////////////////////////////////////////
			
			gameSpaceProxy = new GameSpace();				
			monsterCoverMc = new MovieClip();
			//monsterCoverMc.graphics.lineStyle(0,0xffffff);
			monsterCoverMc.graphics.beginFill(0xff00ff00,0);
			//trace("getMonsterHeight==" + getMonsterHeight +" chk="+getMonsterHeight1)
			//trace("getMonsterHeight==" + getMonsterWidth+" chhk=" +getMonsterWidth1)			
			monsterCoverMc.graphics.drawRect(0, 0, getMonsterWidth+65, getMonsterHeight+55);
			monsterCoverMc.graphics.endFill();
			monsterCoverMc.name = "monster_cover"
			 
			ths.addChild(monsterCoverMc);	
			
			monsterCoverMc.x = 300;//presetsXML.variables.monsterHandBoardLocationX;
			monsterCoverMc.y = 390;//presetsXML.variables.monsterHandBoardLocationY;
			monsterCoverMc.buttonMode = true;
			ths.setChildIndex(monsterCoverMc, ths.numChildren - 1);		
			
		//	monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)	
		
			gameSpaceProxy.addEventListener("ENABLE_LISTENERS", enable_Listners_Handler)
			gameSpaceProxy.addEventListener("DISABLE_LISTENERS", diable_Listners_Handler)
			
			gameSpaceProxy.dispatchEvent(new Event("INIT_GAME")) 
			gameScreenProxy.addEventListener("AUTO_PLAY_INSTRUCTION_SND_ENDS", endofAutoPlay_Handler)	
		 
			 

		}
		
		private function endofAutoPlay_Handler(e:Event):void {
			trace("AUTO_PLAY_INSTRUCTION_SND_ENDS")
			gameSpaceProxy.dispatchEvent(new Event("START_GAME"))			
		}
		
	 
		 
		
		
		
		private function onMonsterCover_Handler(e:MouseEvent):void 
		{
			 trace("  onMonsterCover_Handler ")
			e.currentTarget.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler);
			//gameSpaceProxy.monsterLipClicked(e.target.name)
			//playSoundInstructions();
		}
			 
		private function playSoundInstructions():void {			
			loader.removeEventListener(MouseEvent.CLICK,playSoundInstructions);
			var sound:CoreSound=new CoreSound(new URLRequest(_letterXML.letterSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {					 			
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 	monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
					if (gameSpaceProxy._isCorrectOrWrong == "wrong") {						
						gameSpaceProxy.dispatchEvent(new Event("LETTER_SOUND_COMPETE"))
					}
				}
			}
		}
		
		private function enable_Listners_Handler(e:Event):void {
			trace("enable_Listners_Handler")
			monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)			 
		}
		private function diable_Listners_Handler(e:Event):void 
		{
			//monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)	
		}
		
		
		
		private function loadXMLListForLetter():void {
			
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL);			
		}
		
		private function showStuff():void {
			visible=true;
		}
		
		public function backToGames():void {
			//trace("BackToGames called");
			ScreenManager.getInstance().pop();
			//dispatchEvent(new Event(BACK_TO_GAMES));
		}		 
	}
}



