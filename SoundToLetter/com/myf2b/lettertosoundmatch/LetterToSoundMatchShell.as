﻿package com.myf2b.lettertosoundmatch {
	

	/**
     * @company Footsteps 2 Brilliance
     * @author Hara J N
     */
 
    import com.myf2b.core.Core;
    import com.myf2b.core.ScreenManager;
    import com.myf2b.core.Session;
    import com.myf2b.lettertosoundmatch.GameScreen;
    import com.myf2b.lettertosoundmatch.GameSpace;
    import com.myf2b.lettertosoundmatch.LetterToSoundMatch;
	
    
    import flash.display.MovieClip;
    import flash.events.Event;
   
 	public class LetterToSoundMatchShell extends MovieClip {
		public static var mainScreenProxy:LetterToSoundMatch;
		//public static var gameScreenProxy:GameSpace;
		public static var ths:*
		public static var stg:*
		public function LetterToSoundMatchShell(id:String = "1")  {
            Core.useStaging = true;
			ths = this;
			stg = stage;
            Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			
			ScreenManager.getInstance().stage = stage;		 
			ScreenManager.getInstance().pushModule("com.myf2b.lettertosoundmatch.LetterToSoundMatch", ["135"]);	
			//Reporter.interaction( "bookshelf", String(_contentKey) , BookShelf._moduleSessionID , { action : "open", actionObject : "game", module : f2b_booksXML.item[i].@module , asset : assetKey } );
			
			mainScreenProxy = new LetterToSoundMatch(this, id)
			
				
				
			//gameScreenProxy = new GameSpace(this)
			
        }
			
		public static function isThis():*{
			return ths
		}
		public static function isStage():*{
			return stg
		}
		
	}
	
}
