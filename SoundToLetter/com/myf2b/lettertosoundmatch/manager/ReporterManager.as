﻿package com.myf2b.lettertosoundmatch.manager
{
	
	import com.myf2b.lettertosoundmatch.data.Config;
	import com.myf2b.lettertosoundmatch.utils.Random;
	import com.myf2b.core.Reporter;

	public class ReporterManager
	{
		public static function init():void{
			Config.GAME_SESSION_ID = Reporter.getNewModuleSessionID();
		}
		
		public static function interaction(actionObject:Object):void {
			Reporter.interaction(Config.GAME_MODULE_ID, Config.GAME_KEY, Config.GAME_SESSION_ID, actionObject);
		}
		
		public static function wordExposure(word:String):void {
			Reporter.wordExposure(Config.GAME_MODULE_ID, Config.GAME_KEY, Config.GAME_SESSION_ID, word);
		}
		
	}
	
}