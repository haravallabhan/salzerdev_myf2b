﻿package com.myf2b.lettertosoundmatch
{
	
	import com.greensock.TweenLite;
	import com.greensock.text.SplitTextField;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Session;
	import com.myf2b.lettertosoundmatch.manager.tween.Ease;
	import com.myf2b.lettertosoundmatch.manager.tween.TweenManager;
	import com.myf2b.lettertosoundmatch.manager.ReporterManager;
	import com.myf2b.lettertosoundmatch.manager.SoundManager;
	
	import com.myf2b.ui.*;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import fl.controls.UIScrollBar;
	import flash.text.TextFieldAutoSize;
	
	
	
	public class GameScreen extends MovieClip {
		private var speed:Number=1;
		private var groupNode;
		private var assetNode;
		private var _letterToDeal:String;
		private var level:Number=0;
		private var presetsXML:XML = LetterToSoundMatch.presetsXML;
		private var instructionText:String;
		public var monsterClip:MovieClip;
		private var USERPOPUP_TITLE = ""
		private var USERPOPUP_INFO = ""
		private var USERPOPUP_PARENT_INFO = ""
		private var IS_A_TEACHER:Boolean = false;
		private var USERPOPUP_SUBTITLE = ""
		private var push_arr = []
		private var instTxtX:Number = 518;
		private var instTxtY:Number = 30;
			
		var sb:UIScrollBar = new UIScrollBar();
		private var _sync : TextFieldSoundSync;

		private var ths:*;
		private var stg:*;
		var _letterXML:XMLList;
		
		private var userPopUp:userPopup;
		
		public function GameScreen() {
			
			ths = LetterToSoundMatchShell.isThis();
			stg = LetterToSoundMatchShell.isStage();
			
			_letterToDeal = LetterToSoundMatch.LETTER_TO_DEAL;
			  
			init();
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			
			
		}
		
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
			//trace("GameScreen destroy");
			//stopGame();
			
		}
		
	 
		
		
				
		private function init():void {
			trace("Game Screen init ")
			instructionText = presetsXML.variables.instructionTxt;
			 _letterXML = presetsXML.deal.(@letter == _letterToDeal);
			ths.topBar.cacheAsBitmap = true;			
//			var myPattern:RegExp = /#/g; 
//			instructionText = instructionText.replace(myPattern,_letterToDeal);
			
			ths.topBar.instructionTxt.multiline  = true;
			ths.topBar.instructionTxt.wordWrap  = true;
			ths.topBar.instructionTxt.autoSize = TextFieldAutoSize.LEFT;
			ths.topBar.instructionTxt.text= instructionText;
			_sync = Ui.EnableTextSync(ths.topBar.instructionTxt, presetsXML);
			
			
			//_sync.speakAndDo(playLetterAudio);	
			//push_arr = SplitTextField.split(ths.topBar.instructionTxt,SplitTextField.TYPE_CHARACTERS,ths.topBar)
			//var lastNum:Number = push_arr.length-1;
			
				
		 	/*for(var i=0;i<push_arr.length;i++){					
				var mc:MovieClip= new MovieClip();										
				ths.topBar.addChild(mc);			
				mc.addChild(push_arr[i])
				mc.x = instTxtX;
				mc.y = instTxtY;
			}*/
		 		 				
		//	var txtStr:String = push_arr[lastNum].text;	
			
			var mcX:int=(ths.topBar.instructionTxt.x+ths.topBar.instructionTxt.getLineMetrics(ths.topBar.instructionTxt.numLines-1).width)//+(ths.topBar.width+20);
			var mcY:int = (ths.topBar.instructionTxt.y + ths.topBar.instructionTxt.textHeight) //+ (ths.topBar.height + 10);
			
			var hLightDeal_mc = new dealLetter_mc();
			hLightDeal_mc._txt.text = _letterToDeal//txtStr;
			hLightDeal_mc._txt.mouseEnabled = false;			
			
			hLightDeal_mc.x = mcX + 15;// instTxtX + push_arr[lastNum].x;		
			if (ths.topBar.instructionTxt.numLines == 1) {
				hLightDeal_mc.y = mcY - 43;
			}else{
				hLightDeal_mc.y = mcY - 33;// instTxtY + push_arr[lastNum].y;	
			}
			hLightDeal_mc.buttonMode = true;			
			hLightDeal_mc.addEventListener(MouseEvent.CLICK, instructionMClick_Handler)			
		//	ths.topBar.addChild(hLightDeal_mc);	 			 
			
			ths.topBar.soundBtn.buttonMode = ths.topBar.backToMap.buttonMode = true;			
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);		
			//
				
			//User PopUp Tearcher or Parent//
			if(Session.getInstance().role == "Teacher"){
				USERPOPUP_TITLE = presetsXML.variables.popupForTeacherTitle 
				USERPOPUP_INFO =  presetsXML.variables.popupInfoForTeacher;
				trace("USERPOPUP_INFO-="+USERPOPUP_INFO)
				USERPOPUP_PARENT_INFO = presetsXML.variables.popupInfoForParent; 
				IS_A_TEACHER = true;
				
			}else if(Session.getInstance().role == "Parent"){
				USERPOPUP_TITLE = presetsXML.variables.popupForParentTitle 
				USERPOPUP_INFO =  presetsXML.variables.popupInfoForParent;				
				IS_A_TEACHER = false;
			}
			
			USERPOPUP_SUBTITLE = presetsXML.variables.popupSubTitle;
			//trace("USERPOPUP_SUBTITLE="+USERPOPUP_SUBTITLE)
			
			//User PopUp Tearcher or Parent//
		 
			ths.topBar.userIcon.buttonMode=true;
			ths.topBar.soundBtn.buttonMode=true;
			ths.topBar.langBtn.buttonMode=true;
			
			
			if(IS_A_TEACHER){
				ths.topBar.userIcon.teacherIcon.visible = true;
				ths.topBar.userIcon.parentIcon.visible = false;
			}else{
				ths.topBar.userIcon.teacherIcon.visible = false;
				ths.topBar.userIcon.parentIcon.visible = true;
			}
			
			if(Session.getInstance().language == "en"){
				////trace("tpbar= "+ths.topBar.langBtn.en)
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;
			}else if(Session.getInstance().language == "esp"){
				ths.topBar.langBtn.en.visible=false;
				ths.topBar.langBtn.esp.visible=true;				
			}
			
			ths.topBar.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			ths.topBar.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);
			// //Popup setting Language
			
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, onSpeaker);
		}
		private function changeLang(e : MouseEvent ):void {
			if(ths.topBar.langBtn.en.visible){
				ths.topBar.langBtn.en.visible=false;			
				ths.topBar.langBtn.esp.visible=true;
			}else{
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;
				
			}
			
		}
		
		private function showInfoForUser(e : MouseEvent ):void {
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
				userPopUp = new userPopup();
				ths.addChild(userPopUp)
				userPopUp.name = "userPopUp"
				userPopUp.userPopupClose.addEventListener(MouseEvent.CLICK, closePopup);
				userPopUp.userPopupTitle.text = USERPOPUP_TITLE;//Config.USERPOPUP_PARENT
				userPopUp.userPopupInfo.text = USERPOPUP_INFO;
				
			if(IS_A_TEACHER){
				userPopUp.popupDivider.visible=true;
				userPopUp.infoForParentTitle.visible = true;
				userPopUp.infoForParent.visible = true;
				userPopUp.infoForParentTitle.text = USERPOPUP_SUBTITLE;
				userPopUp.infoForParent.text = USERPOPUP_PARENT_INFO;
				trace("USERPOPUP_INFO="+USERPOPUP_INFO)
				assignScrollBar(userPopUp.userPopupInfo)
			}else{
				userPopUp.popupDivider.visible=false;
				userPopUp.infoForParentTitle.visible = false;
				userPopUp.infoForParent.visible = false;				
			}
			
			
			show();
		};
		
		private function onSpeaker(e:MouseEvent):void{
			//ReporterManager.interaction( { action: "click", actionObject: "gameInfoSpeaker" });
		//	SoundManager.play("gameInfo");
		}
		
		private function stopGame():void {
			removeAllInstances();
		}
		
		private function loadComplete():void{
			//trace("Load Complete");
		}
		
		public function startGame(node1,node2):void {
			//level=0;
			//groupNode=node1;
			//assetNode=node2;
			//visible=true;
			//startLevel();
			//levelOverScreen.setDetails(0,0,13,15,assetNode);
			//objectTimer.stop();
			
			
		}
		
		public function startLevel():void {
			//trace("start")
		}
		
		public function nextLevel():void {
			level++;
		}
		public function playAgain():void {
			
		}
		
		private function onObjectTimerComplete(e:TimerEvent):void {
			removeAllInstances();
		}
		
		private function removeAllInstances():void {
		}
		
		private function onObjectTimer(e:TimerEvent):void {
		}
		
		private function formatTime(val):String {
			return null;
		}
		
		private function soundClicked(e:MouseEvent):void {
			
			dispatchEvent(new Event("PLAY_INSTRUCTIONS_SOUND"))
			//ReporterManager.interaction( { action: "soundclick", actionObject: "letterToSoundSpeaker" } );
			//_sync1.speakAndDo(playLetterAudio);
			
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			//Reporter.interaction("sound_to_letter", ListenAndRecord._contentKey, ListenAndRecord.moduleSessionID);
			//playSndInstructions("instSound");
			_sync.speakAndDo(playLetterAudio);
		}
		
		
		public function autoPlayInstruction(e:Event):void {
			
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			_sync.speakAndDo(playLetterAudio1);
		}
		
		public function playLetterAudio1(e:Event):void {
			
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 //dispatchEvent(new Event("AUTO_PLAY_INSTRUCTION_SND_ENDS"))				 
			}
			
			
		}
		

		private function playLetterAudio(e : Event):void {
			dispatchEvent(new Event("PLAY_INSTRUCTIONS_SOUND_FINISHED"))
			trace("inst btn finished ")
			/*var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				
				
			}*/
		};	

		
		private function playSndInstructions(aParam:String):void {
			//trace("RECORDING INSTRUCTIONS");
			if(SoundMixer.areSoundsInaccessible() == false) {
				SoundMixer.stopAll();					
			}
			//ths.topBar.soundBtn.removeEventListener(MouseEvent.CLICK,soundClicked);
			var sound:CoreSound
			if (aParam == "instSound") {
				 
				sound=new CoreSound(new URLRequest(presetsXML.variables.instructionSound));
			}else if(aParam == "letterSound"){
				//_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == _randomLetters[_getCurrentLipValue]._letter)
				sound=new CoreSound(new URLRequest(_letterXML.letterSound));
			}
		
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
				function onSoundComplete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
					
				}
			}
		}
		
		public function show():void {
			TweenManager.tween(userPopUp, 0.5, {alpha:1, scaleX:1, scaleY:1}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		public function hide():void{
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
			TweenManager.tween(userPopUp, 0.5, {alpha:0, scaleX:0, scaleY:0}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
			
		}
		
		private function closePopup(e:MouseEvent):void {			
			hide();				
		}
		
		private function instructionMClick_Handler(e:MouseEvent):void {
			
			playSndInstructions("letterSound");
		}
		
		function getLastCharInString($s:String):String
		{
			return $s.substr($s.length-1,$s.length);
		}
		
		function assignScrollBar(tf:TextField):void {
			trace("textLen="+tf.text.length)
			sb = new UIScrollBar();
			sb.move(tf.x + tf.width, tf.y);
			sb.setSize(sb.width, tf.height);			
			
			sb.scrollTarget = tf;	
			if (tf.text.length>=420) {
				sb.visible = true;
			}else {
				sb.visible = false;
			}
			
			userPopUp.addChild(sb);            
		}
		
		
		
		
		
	}
	
}

