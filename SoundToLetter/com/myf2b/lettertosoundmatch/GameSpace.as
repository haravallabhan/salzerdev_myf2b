﻿package com.myf2b.lettertosoundmatch
{
	import com.avr.XmlLoader;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.lettertosoundmatch.LetterToSoundMatch;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import com.myf2b.ui.*;
	import flash.media.SoundLoaderContext;
	import flash.media.SoundTransform;
	
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	import flash.system.Security
	import fl.transitions.*;
	import fl.transitions.easing.*;
	import flash.filters.BitmapFilterQuality;
 
	
	public class GameSpace extends MovieClip
	{
	 			 
		private var _getCurrentItemValue:Number = 0;
		private var _shootCount:Number = 1;
		private var _letterXML:XMLList;
		private var drapLipLen:Number  = 3;
		private var totalLen:Number = 3;
		private var totalLetterLen:Number = 0;
		private var _dragAttempt:Number = 0;
		private var attemptCount:Number = 0;
		private var consAttempt:Number =-1; 
		private var resetLen:Number = 0
		private var introGameCnt:Number = 0 ;
		
		
		public var _isCorrectOrWrong:String = "";	
		public var isRoundInfor_str:String = "";
		private var letterDealOrOrderDeal:Boolean = false;
		 	
		private var isMonsterLipClicked:Boolean = false;
		private var isStart_Game:Boolean = false;
		private var isSndOverLap:Boolean = false;
		private var isInitMonsterEffect_bool:Boolean = false;
		private var isWhichSound_str:String = ""
		private var succSndCon_str:String = "";
		 
		
			
		private var _dragLipMc_arr = []
		private var _lip_AnimMc_Arr = []
		private var _highLightMcArr = [];
		private var _letterArr= [] //= ["o","m","a"]
		private var _isCorrectArr = [];
		private var _randomLetters = []		
		private var wrongMc_arr = []
		private var tweenArr = []
		private var randomArr = []

 
		private var xarr = [120,650,720]
		private var yarr = [720, 120, 350]
		
		private var blinkCnt = 0;
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB//0xCC3333;
		private var glow:GlowFilter = new GlowFilter();
		var glow_str:String;
		//
		private var btn_blinkCnt = 0;	 		
		
			
		private var _levelStatus:levelStatus;	
		private var _sync1 : TextFieldSoundSync;
		private var _sync2 : TextFieldSoundSync;
		
		private var ch:SoundChannel
		
		private var ths:*;
		private var stg:* ;
		private var dragCoverMcArr = [];
		private var mySound:Sound
		private var myChannel:SoundChannel
		private var cnt = 0
		private var isHitTest:Boolean = false;
		private var letterarr= [];
		private var orderarr = [];
		
		private	var getRandArr = []
		private	var getPrevArr = []
		//private var comb_arr = [012, 021, 102, 120, 201, 210];
		private var comb_arr = [021,  120, 201];
 	
		private var iVal = 0;
		private var prevNum:Number
		private var getNum:Number 
		private var roundCount:Number = 0;
		private var rand_item_arr = [];
		var levelEndMc:mc_levelEnd
		private var instructionText:String;
		private var isDragPlaying:Boolean = false;
		private var isStartUp:Boolean = false;
		private var ch1:SoundChannel
	
		 
		
		
		public function GameSpace()
		{
			//TODO: implement function
		//	super();
			Security.allowDomain("*");			
			ths = LetterToSoundMatchShell.isThis();
			stg = LetterToSoundMatchShell.isStage();	
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
			 
			//ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)
			
			 
			createHandlers()
		}
		
		 
		
		private function createHandlers():void 
		{
			addEventListener("INIT_GAME", initGame_Handler);
			addEventListener("START_GAME", StartGame_Handler);
			addEventListener("END_OF_MONSTER_GLOW", endMonsterGlow_Handler);
			 
			addEventListener("START_ROUNDS", startRounds_Handler);
			addEventListener("BELL_SOUND_FINISHED", bellSoundFinish_Handler);
			addEventListener("START_DRAG_EVENT", startDragEvent_Handler);
			//addEventListener("END_MONSTER_SOUND", endMonsterSound_Handler);
			addEventListener("END_LETTERBOARD_SOUND", endMonsterLetterBoardSound_Handler);
			addEventListener("END_DEAL_LETTER_SOUND", endDealSound_Handler);
			addEventListener("PLAY_CORRECT_SOUND", playCorrectSound_Handler);
			addEventListener("PLAY_WRONG_SOUND", playWrongSound_Handler);
			addEventListener("END_CORR_WNG_SOUND_FINISHED", endCorrOrWrongSound_Handler);
		}		
		
		private function initGame_Handler(e) {
			//trace("INIT_GAME")
			getAlphabetsOrOrder()
			getRandomCombination()
			LoadAnimationLips()			
			getLevelStatus(); 	
			dispatchEvent(new Event("START_GAME"))
		}
		
		private function StartGame_Handler(e:Event):void {	
			trace("START_GAME__________")	
			playSoundInstructions("monsterIntro_sound")
			//addEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)			
			//LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover1_Handler)			
		}
		
		private function onMonsterCover1_Handler(e:Event) {	
			trace(" e. target= " + e.target.name)
			 
			if (e.target.name == "monster_cover" || LetterToSoundMatch.blackLip_mc.no == "blacklip_mc") {																		
				LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover1_Handler)												
				blinkCnt = 0;								
				removeEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler);								
				glow.alpha = 0;										
				LetterToSoundMatch.monsterLoaderArr[0].filters = [glow];			
				dispatchEvent(new Event("END_OF_MONSTER_GLOW"))
				playSoundInstructions("monsterIntro_sound")
			}	
			
					
		}
		
		
		private function endMonsterGlow_Handler(e:Event):void {
			trace(" endMonsterGlow " +roundCount+" - "+_shootCount)
			if (roundCount == 0) {
				if (_shootCount > 1) {
					trace("chk sht cnt")
					createSuffle_Lips();
					LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, 	onMonsterCover_Handler)
					//LetterToSoundMatch.LipLoaderArr[0].addEventListener(MouseEvent.CLICK,   onMonsterCover_Handler)
				}
			}else {				
				createSuffle_Lips();
				
			}
			
			
		}
		 
		
		private function startRounds_Handler(e:Event):void {
			trace(" start Rounds ")
			startAnimation()		
			loadLetterDragClips()			
			createCrossMark()
		}
		
		 
		private function getAlphabetsOrOrder():void {
			totalLetterLen = LetterToSoundMatch.presetsXML.deal.length(); 
			for(var j=0;j<totalLetterLen;j++){			 
				//_randomLetters.push({_letter:LetterToSoundMatch.presetsXML.deal[j].@letter,_order:LetterToSoundMatch.presetsXML.deal[j].@order})
				letterarr[j] = LetterToSoundMatch.presetsXML.deal[j].@letter
				orderarr[j] = LetterToSoundMatch.presetsXML.deal[j].@order;
			}
		}
		
		private function loadLetterDragClips():void {			
			//var xList:XMLList = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL)/**/			
			for (var i = 0; i < drapLipLen; i++) {	
				var xList:XMLList = LetterToSoundMatch.presetsXML.deal.(@letter == randomArr[rand_item_arr[i]].letter) 
				var liploader:Loader = new Loader();					
				liploader.load(new URLRequest(xList.lipGraphic))					 
				liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoad_DragLip_Complte(i))					
			}
		}
		
		function onLoad_DragLip_Complte(i):Function {			
			return function(e:Event):void {
				if(_dragLipMc_arr == null){
					_dragLipMc_arr = new Array()
				}
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				//var image:DisplayObject = (e.target as LoaderInfo).content;	
				var mc:MovieClip = new MovieClip()
				ths.addChild(mc)
				mc = ((e.target as LoaderInfo).content) as MovieClip 					
				
				_dragLipMc_arr[i] = new MovieClip()
				ths.addChild(_dragLipMc_arr[i])
				_dragLipMc_arr[i].addChild(mc)
				_dragLipMc_arr[i].x = 120+(i*293);
				_dragLipMc_arr[i].y = 190;
				_dragLipMc_arr[i].posx = _dragLipMc_arr[i].x;
				_dragLipMc_arr[i].posy = _dragLipMc_arr[i].y;				
				_dragLipMc_arr[i].visible = false;
				
				if(_dragLipMc_arr[i].lipMc){
					_dragLipMc_arr[i].lipMc.stop()
				}	
				
			}	
			
		}	
		
		
		
		private function getLevelStatus(){
			_levelStatus = new levelStatus()
			ths.addChild(_levelStatus)
			_levelStatus.x = 510;
			_levelStatus.y = 730;
			var _levelInfoMc:mc_levelInfo = new mc_levelInfo()
			ths.addChild(_levelInfoMc)
			_levelInfoMc.x = -250;
			_levelInfoMc.y = 1200;					
		}
		
		private function createCrossMark():void 
		{
			for (var i = 0; i < 3;i++ ) {
				wrongMc_arr[i] = new wrong_mc()
				//ths.wrongContainer.addChild(wrongMc);
				ths.addChild(wrongMc_arr[i]);
				wrongMc_arr[i].name = "wrong_mc";
				wrongMc_arr[i].x = 160 + (i * 293);
				wrongMc_arr[i].y = 180;			
			}
			CrossMarkDisable()		
		}
		
		private function CrossMarkDisable():void {
			for (var i = 0; i < 3; i++ ) {
				wrongMc_arr[i].visible = false;
			}			
		}
		
		private function CrossMarkEnable():void {
			for (var i = 0; i < 3; i++ ) {
				wrongMc_arr[i].visible = true;
			}			
		}
		
		private function startAnimation():void {
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)
			ths.roundFly_mc.gotoAndPlay(2)
			ths.roundFly_mc.addFrameScript(5 - 1, depthFlyAnim)
			ths.roundFly_mc.addFrameScript(11 - 1, startFlyAnim)
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount))
		}
		private function depthFlyAnim() {
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)
			ths.roundFly_mc.addFrameScript(5 - 1, null)
		}
				
		private function startFlyAnim():void {
			ths.roundFly_mc.stop()
			ths.roundFly_mc.addFrameScript(11 - 1, null)		
			startBellSound()			
		}
		
		private function startBellSound():void {	
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2)
			playSoundInstructions("startBell")				
		}
		
		private function bellSoundFinish_Handler(e:Event):void {	
			//
			
			ths.roundFly_mc.play()
			ths.roundFly_mc.addFrameScript(20 - 1, stopFlyAnim)	
			//LetterToSoundMatch.LipLoaderArr[0].visible = true;	
			//trace(" bellSoundFinish_Handler ")
			
			
			 
		}
		
		public function LoadAnimationLips(){			
			//var xList:XMLList = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL)/**/			 
			for (var i = 0; i < drapLipLen; i++) {			
				var xList:XMLList = LetterToSoundMatch.presetsXML.deal.(@letter == randomArr[rand_item_arr[i]].letter)
				var liploader:Loader = new Loader();					
				liploader.load(new URLRequest(xList.lipGraphic))				 
				liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLipAnimation_Complte(i))					
			}		
		}
		
		function onLipAnimation_Complte(i):Function {
			return function(e:Event):void {
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				//var image:DisplayObject = (e.target as LoaderInfo).content;
				_lip_AnimMc_Arr[i] = new MovieClip()
				_lip_AnimMc_Arr[i] =  (e.target as LoaderInfo).content;				 
				//monsterCoverMc.graphics.lineStyle(0,0xffffff);				
				ths.addChild(_lip_AnimMc_Arr[i]);			 
				_lip_AnimMc_Arr[i].x = 120+(i*293)
				_lip_AnimMc_Arr[i].y = 190;
				_lip_AnimMc_Arr[i].posx = _lip_AnimMc_Arr[i].x;
				_lip_AnimMc_Arr[i].posy = _lip_AnimMc_Arr[i].y;	
				//_lip_AnimMc_Arr[i].lipMc.stop();
				if(_lip_AnimMc_Arr[i].lipMc){
					_lip_AnimMc_Arr[i].lipMc.stop()
				}
			}	 
		}
		
		private function stopFlyAnim():void 		
		{	//trace("Fly stop")			
			ths.roundFly_mc.addFrameScript(20 - 1, null)
			ths.roundFly_mc.stop()
			gameStarts()
		}
		private function gameStarts():void {		
			trace(" ___________Game Starts_____________"+roundCount +"\ "+_shootCount)
			//playSoundInstructions("startGame_letter_sound")
			if (roundCount == 0) {
				if (_shootCount == 1) {	
					trace("chk bool * ")					 
					createSuffle_Lips()								
				}else {		
				 				
					_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL);	
					var ch2:SoundChannel = new SoundChannel();
					var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
					ch2 = _sound.play();
					ch2.addEventListener(Event.SOUND_COMPLETE, onSound1Complete);					
				
					function onSound1Complete(e:Event):void {					 			
						e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSound1Complete);	
						trace("get innnersdf")
						endMonsterGlow_Handler(null)
					}
				}
			}else {				 
					 		
				_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL);	
				var ch3:SoundChannel = new SoundChannel();
				var _sound1:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
				ch3 = _sound1.play();
				ch3.addEventListener(Event.SOUND_COMPLETE, onSound2Complete);
				function onSound2Complete(e:Event):void {	
					trace("get next innnersdf")
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSound2Complete);				 
					endMonsterGlow_Handler(null)
				}
			}
			
			
			//
		}
		
		private function onMonsterCover_Handler(e:MouseEvent):void {	
			trace(" GETTT INNER")
			
		//	LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)			
			if (ch1) {			 
				ch1.stop()
			}
		 
			/*if (isStartUp == false) {
				isStartUp = true;
				playSoundInstructions("monsterIntro_sound")					 
			}else {	*/			 
				if (!isMonsterLipClicked) {
					playSoundInstructions("monster_sound")	
				}
							 
		//	}
			trace("isMonsterLipClicked = "+isMonsterLipClicked)
			if (isMonsterLipClicked) {
				isMonsterLipClicked = false;
				trace(" chk for handlers ")
				dispatchEvent(new Event("END_LETTERBOARD_SOUND"))
			}
			
			
		}
		
		private function onMonsterLip_Handler(e:MouseEvent):void {					 
		//	LetterToSoundMatch.LipLoaderArr[0].removeEventListener(MouseEvent.CLICK, onMonsterLip_Handler)			 
			playSoundInstructions("monster_sound")			 
		}
		
		
		private function startDragEvent_Handler(e:Event):void {
			//trace("START_DRAG_EVENT "+" _ SHUFFLING")			
			//addEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)			
			//createSuffle_Lips()		
			//LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
		}		
		
		private function onMonsterGlowEffect_Handler(e:Event):void {
			blinkCnt++
			if(blinkCnt %5==0){		
				glow.alpha = 1;			
				glowCnt++
				if (glowCnt == 3) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}
				LetterToSoundMatch.monsterLoaderArr[0].filters = [glow];
			} 
			if (blinkCnt == 50) {
				trace(" isInitMonsterEffect_bool= "+isInitMonsterEffect_bool)
				if (isInitMonsterEffect_bool == true) {
					blinkCnt = 0
					removeEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)
					glow.alpha = 0;		
					LetterToSoundMatch.monsterLoaderArr[0].filters = [glow];
					dispatchEvent(new Event("END_OF_MONSTER_GLOW"))
					trace(" dispaatcheed")
				}
				isInitMonsterEffect_bool = true;
			
			}			
		}		
		
		private function createSuffle_Lips() {
			var shuffleSnd_str:String = LetterToSoundMatch.presetsXML.variables.lipAnimationSound		 
			mySound = new Sound();
			myChannel = new SoundChannel();
			mySound.load(new URLRequest(shuffleSnd_str));
			myChannel = mySound.play();
			myChannel.addEventListener(Event.SOUND_COMPLETE, onLipAnim_SoundComp_Handler)	
			for(var i=0;i<drapLipLen;i++){
				tweenArr[i] = new Tween(_lip_AnimMc_Arr[i], "x", Regular.easeInOut, xarr[i], yarr[i], .2, true)
				tweenArr[i].addEventListener(TweenEvent.MOTION_FINISH, handleFinish);
			}
			LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, 	onMonsterCover_Handler)
			//LetterToSoundMatch.LipLoaderArr[0].addEventListener(MouseEvent.CLICK,   onMonsterCover_Handler)
		}		
		
		private function handleFinish(e:Event):void 
		{
			e.target.yoyo();	
		}
		private function onLipAnim_SoundComp_Handler(e:Event):void {
			//trace("Animation Ends/")				
			myChannel.removeEventListener(Event.SOUND_COMPLETE, onLipAnim_SoundComp_Handler)
						
			for(var i=0;i<drapLipLen;i++){
				tweenArr[i].stop()
				_lip_AnimMc_Arr[i].x = _lip_AnimMc_Arr[i].posx;
				_lip_AnimMc_Arr[i].y = _lip_AnimMc_Arr[i].posy;
				_lip_AnimMc_Arr[i].visible = false;				
			}
			startDragEvents()
				
		}
		
		private function startInstructionAudio() {
			//trace("End Of InstructionText Plays/")	
			
		}	

		
		
		private function getRandomCombination():void 
		{
			rand_item_arr = []
			iVal = randomNumber(0,2);
			if (prevNum == iVal){
				iVal = randomNumber(0, 2);				
				for(;prevNum==iVal;){
					iVal = randomNumber(0, 2);				
				}
				
				prevNum = iVal;
				
			}
			else{		
				prevNum = iVal;		
			}
			trace("prevNum*&* "+prevNum)
			
			getNum = comb_arr[iVal]
			
			
			var nxt3:Number = 	getNum%10
			getNum = 	Math.round(getNum/10)
			var nxt2:Number = 	getNum%10
			getNum = 	Math.round(getNum/10)
			var nxt1:Number = 	getNum%10
			getNum = 	Math.round(getNum/10)	
			////trace("nxt1= "+letterarr[nxt1])
			////trace("nxt2= "+letterarr[nxt2])
			////trace("nxt3= "+letterarr[nxt3])
			rand_item_arr.push(nxt1)
			rand_item_arr.push(nxt2)
			rand_item_arr.push(nxt3)
			trace("combination&=" + rand_item_arr)				
			randomArr = getRandomNoRepeat()				
		}
		
		
		
		
		
		///////////////////////////////////////////////////////////////////////////////////////////
		private function startDragEvents():void 
		{			
			
			
			////trace("rand_item_arr *&* "+rand_item_arr)					
			for (var j = 0; j < 3; j++ ) {
				
				_dragLipMc_arr[j].visible = true;
				_dragLipMc_arr[j].addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown_Handler);
				_dragLipMc_arr[j].addEventListener(MouseEvent.MOUSE_UP, onMouseUp_Handler);
				_dragLipMc_arr[j].addEventListener(MouseEvent.CLICK, onMouseClick_Handler);
				_dragLipMc_arr[j].no = j;				 
				_dragLipMc_arr[j].buttonMode = true;	
				
				_dragLipMc_arr[j].letterToDeal = randomArr[rand_item_arr[j]].letter;
				////trace("deal="+_dragLipMc_arr[j].letterToDeal )				 
				_dragLipMc_arr[j].posx = _dragLipMc_arr[j].x
			    _dragLipMc_arr[j].posy = _dragLipMc_arr[j].y
				//_dragLipMc_arr[j].mouseEnabled = true;
				//_dragLipMc_arr[j].mouseChildren = true;			
				
				if(_dragLipMc_arr[j].lipMc){
					_dragLipMc_arr[j].lipMc.stop()
				}
				
			}	
			 
		}
		
		
		
		private function onMouseClick_Handler(e:MouseEvent):void 
		{
			trace(" Monster Clicked..." )
			isMonsterLipClicked = true;			
			_getCurrentItemValue = e.currentTarget.no;	
			
			playSoundInstructions("deal_letter_sound")
			disableDragListeners()		
			//LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
			//LetterToSoundMatch.LipLoaderArr[0].removeEventListener(MouseEvent.CLICK, onMonsterLip_Handler)
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			var highlightMc:highlight_mc = new highlight_mc()
			ths.addChild(highlightMc);
			highlightMc.name = "hLight_mc";			
			highlightMc.x = _dragLipMc_arr[_getCurrentItemValue].posx; 
			highlightMc.y = _dragLipMc_arr[_getCurrentItemValue].posy;	 
			
			
			highlightMc.mouseEnabled = false;
			 
		}
		
		
		private function onMouseDown_Handler(e:MouseEvent):void 
		{
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			
			isMonsterLipClicked = false;	
			e.currentTarget.startDrag();
			ths.setChildIndex(e.currentTarget as MovieClip,ths.numChildren-1);		 
			_getCurrentItemValue = e.currentTarget.no;
			stg.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
			stg.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler); 	 
			e.currentTarget.addEventListener(MouseEvent.MOUSE_UP, onCurrentClip_MouseUp_Handler);
		}
		
		private function onMouseUp_Handler(e:MouseEvent):void {	
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			e.currentTarget.addEventListener(MouseEvent.MOUSE_UP, onMouseUp_Handler);
			e.currentTarget.stopDrag();	
			if(	_dragLipMc_arr[_getCurrentItemValue].hitTestObject(LetterToSoundMatch.monsterCoverMc))
			{					
				disableDragListeners()
				LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
			//	LetterToSoundMatch.LipLoaderArr[0].removeEventListener(MouseEvent.CLICK, onMonsterLip_Handler)
				_dragAttempt++;
				//trace("letterToDeal= " + _dragLipMc_arr[_getCurrentItemValue].letterToDeal +" isMonsterLipClicked&="+isMonsterLipClicked)
				
				if (_dragLipMc_arr[_getCurrentItemValue].letterToDeal == LetterToSoundMatch.LETTER_TO_DEAL && isMonsterLipClicked == false)
				{
					trace(" hitted-----------------------------------------------------------------------------")
					isHitTest = true
					var xmlList:XMLList = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL)	
				 
					_dragLipMc_arr[_getCurrentItemValue].visible = false;						 					 
					
					LetterToSoundMatch.blackLipMonsterArr[0].visible = false;
					 LetterToSoundMatch.monsterLoaderArr[0].visible = true;										 					
					var mc:MovieClip = _dragLipMc_arr[_getCurrentItemValue]					 
					stg.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
					stg.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler);				 
					_levelStatus.gotoAndStop(_shootCount+1);					
					_isCorrectOrWrong = "correct";
					attemptCount++;	
					 
					if(attemptCount == 1){
						_isCorrectArr.push(1)				
					}else if(attemptCount == 2){
						_isCorrectArr.push(2)
					}else if(attemptCount >= 3){
						_isCorrectArr.push(3)
					} 
					 
					for ( var j = 0; j < _isCorrectArr.length;j++){
						if(_isCorrectArr[j] == 1){
							_levelStatus["star"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;		
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt++
						}else if(_isCorrectArr[j] == 2){
							_levelStatus["secondtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}else if(_isCorrectArr[j] >= 3){
							_levelStatus["thirdtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}
					}
					 
					//playSuccessOrTryAgainSound("correct")
					playSoundInstructions("monster_Phonic_sound","succ_snd_Con")
				}else {
										 
					_isCorrectOrWrong = "wrong";
					_dragLipMc_arr[_getCurrentItemValue].x = _dragLipMc_arr[_getCurrentItemValue].posx;
					_dragLipMc_arr[_getCurrentItemValue].y = _dragLipMc_arr[_getCurrentItemValue].posy;
					_levelStatus.gotoAndStop(_shootCount+1);
					 
					_levelStatus["wrong"+_shootCount].visible = true;
					_levelStatus["star"+_shootCount].visible= false;
					_levelStatus["emptyIcon"+_shootCount].visible= false;
					_levelStatus["secondtry"+_shootCount].visible= false;
					_levelStatus["thirdtry"+_shootCount].visible= false;
					 
					wrongMc_arr[_getCurrentItemValue].visible = true;
					 
					ths.setChildIndex(wrongMc_arr[_getCurrentItemValue], ths.numChildren - 1);
					_dragLipMc_arr[_getCurrentItemValue].mouseEnabled = false;
					_dragLipMc_arr[_getCurrentItemValue].mouseChildren = false;						
					attemptCount++;			 
					 
					playSuccessOrTryAgainSound("wrong")
					
					LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
					//LetterToSoundMatch.LipLoaderArr[0].removeEventListener(MouseEvent.CLICK, onMonsterLip_Handler)
				//	playSoundInstructions("monster_letter_sound")
					//trace(" ")
				}
			}else {
				//trace("not hitted")					
				//disableDragListeners()
				 
			}
			 isMonsterLipClicked = false;
			 //trace("isHitTest *(* " + isHitTest)
			 if (isHitTest == false ) {
				 stg.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
				 stg.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler);
				 resetDragPositon()  
			 }
			
		}
		
		private function onStageMouseUp_Handler(e:MouseEvent):void {			 
			resetDragPositon() 	
		}
		
		private function onStageMouseLeave_Handler(e:Event):void {			 
			resetDragPositon() 			
		}
		
		private function onCurrentClip_MouseUp_Handler(e:MouseEvent):void {			 
			_dragLipMc_arr[_getCurrentItemValue].stopDrag();
		}
		
		private function resetDragPositon() {
			//trace("_getCurrentItemValue&=" + _getCurrentItemValue)
			 
			if(!myNewIsNaN(_getCurrentItemValue)){
				_dragLipMc_arr[_getCurrentItemValue].stopDrag();
				_dragLipMc_arr[_getCurrentItemValue].x = _dragLipMc_arr[_getCurrentItemValue].posx;
				_dragLipMc_arr[_getCurrentItemValue].y = _dragLipMc_arr[_getCurrentItemValue].posy;	
			}else {				 
				resetDragToPosition()
			}
		}
		
		private function resetDragToPosition():void {
			for (var i = 0; i < 3; i++ ) {
				_dragLipMc_arr[i].x = _dragLipMc_arr[i].posx;
			    _dragLipMc_arr[i].y = _dragLipMc_arr[i].posy;			
			}			
		}
		
		
		
		
		
		private function endDealSound_Handler(e:Event):void {
			trace("endDealSound_Handler")
			//isMonsterLipClicked = true;
			enableDragListeners()
			
			//trace("END_DEAL_LETTER_SOUND " )
			//LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
			//LetterToSoundMatch.LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterLip_Handler)
		}
		private function endMonsterLetterBoardSound_Handler(e:Event):void {
			trace("endMonsterLetterBoardSound_Handler ")			
			 playSoundInstructions("monster_sound")		 
		}
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		 
		
		private function enableDragListeners() {
			for (var i = 0; i < 3; i++ ) {
			//	if (_dragLipMc_arr[i]) {	
					trace( " enableDragListeners")
				_dragLipMc_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown_Handler);
				_dragLipMc_arr[i].addEventListener(MouseEvent.MOUSE_UP, onMouseUp_Handler);
				_dragLipMc_arr[i].addEventListener(MouseEvent.CLICK, onMouseClick_Handler);
				_dragLipMc_arr[i].buttonMode = true;
			//	}
			}
		}
		private function disableDragListeners() {
			for (var i = 0; i < 3;i++ ) {
				_dragLipMc_arr[i].removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown_Handler);
				_dragLipMc_arr[i].removeEventListener(MouseEvent.MOUSE_UP, onMouseUp_Handler);
				_dragLipMc_arr[i].removeEventListener(MouseEvent.CLICK, onMouseClick_Handler);
				_dragLipMc_arr[i].buttonMode = false;				
			}
		}
		 
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		private function playSoundInstructions(aStr:String,aSuccSndCon:String = ""):void {	
			 
			isWhichSound_str = aStr;
			succSndCon_str = aSuccSndCon
			var flyBell_str:String = ""
			var sound:CoreSound		
			var mybuffer:SoundLoaderContext = new SoundLoaderContext(1000,true);		 
			
			if (isWhichSound_str == "monsterIntro_sound") {
				_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL);				 
				sound = new CoreSound(new URLRequest(_letterXML.letterSound),mybuffer);
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true); 
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				trace(" 1 ")
			 }
			 if (isWhichSound_str == "monster_sound") {	 
				 
				_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL);				 
				sound = new CoreSound(new URLRequest(_letterXML.letterSound),mybuffer);
				
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true); 
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				trace(" 2 ")
			 }
			 if (isWhichSound_str == "monster_Phonic_sound") {	 
				 
				_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == LetterToSoundMatch.LETTER_TO_DEAL);				 
				sound = new CoreSound(new URLRequest(_letterXML.monsterHandBoardSound),mybuffer);
				
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true); 
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				trace(" monster_Phonic 2 ")
			 }
			
			 if (isWhichSound_str == "startBell") {				
				flyBell_str = LetterToSoundMatch.presetsXML.variables.flybellAnimationSound;	
				 
				sound = new CoreSound(new URLRequest(flyBell_str),mybuffer);
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			} 
			
			if (isWhichSound_str == "deal_letter_sound") {
				trace("At Reset&="+_dragLipMc_arr[_getCurrentItemValue].letterToDeal)
				_letterXML = LetterToSoundMatch.presetsXML.deal.(@letter == _dragLipMc_arr[_getCurrentItemValue].letterToDeal);						
				sound = new CoreSound(new URLRequest(_letterXML.monsterHandBoardSound),mybuffer);
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
				isDragPlaying = true;	
				//trace("isBuffering "+sound.isBuffering )
			}
			
			function onLoadProgress(event:ProgressEvent):void 
			{ 
				var loadedPct:uint =         Math.round(100 * (event.bytesLoaded / event.bytesTotal)); 
				//trace("The sound is " + loadedPct + "% loaded."); 
			} 
			//
			function onIOError(event:IOErrorEvent) 
			{ 
				//trace("The sound could not be loaded: " + event.text); 
			}
		
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);				
				ch1 = new SoundChannel();						
				ch1 = sound.play();				
				ch1.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);					
				
				function onSoundComplete(e:Event):void {					 			
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);				
					trace(" Sound Finished = " +succSndCon_str +" isWhichSound_str= "+isWhichSound_str)	
					if(isWhichSound_str == "monsterIntro_sound"){
						dispatchEvent(new Event("START_ROUNDS"))
					}
					
					if(isWhichSound_str == "monster_sound"){
						//dispatchEvent(new Event("START_ROUNDS"))
						dispatchEvent(new Event("END_DEAL_LETTER_SOUND"))
					}
					if(isWhichSound_str == "monster_Phonic_sound"){
						//dispatchEvent(new Event("START_ROUNDS"))
						dispatchEvent(new Event("END_DEAL_LETTER_SOUND"))
					}
					
					if(isWhichSound_str == "startBell"){
						dispatchEvent(new Event("BELL_SOUND_FINISHED"))
					}
					if(isWhichSound_str == "deal_letter_sound"){
						dispatchEvent(new Event("END_DEAL_LETTER_SOUND"))
					}
					
					if (succSndCon_str == "succ_snd_Con") {
						succSndCon_str = "";
						playSuccessOrTryAgainSound("correct")
					}
				}
			}
		}
		
		private function playCorrectSound_Handler(e:Event):void 
		{
			//trace("PLAY_CORRECT_SOUND")
			playSuccessOrTryAgainSound("correct")
			 	
		}
		private function playWrongSound_Handler(e:Event):void {
			//trace("PLAY_WRONG_SOUND")
			playSuccessOrTryAgainSound("wrong")
			 
		}
		
		private function endCorrOrWrongSound_Handler(e:Event):void 
		{
				//trace("END_CORR_WNG_SOUND_FINISHED")
				if (_isCorrectOrWrong == "wrong") {
					enableDragListeners()	
				}
				
				if (_isCorrectOrWrong == "correct") {
					goToNextLevel()
				}
				
				////trace("_isCorrectOrWrong&=" + _isCorrectOrWrong)
			 				
				//LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)				
				//LetterToSoundMatch.LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterLip_Handler)
				
				_isCorrectOrWrong = "monsterSnd";
		}
		
		private function goToNextLevel():void 
		{
			//trace(" goToNextLevel ")
			 
			
			if (_isCorrectOrWrong == "correct")
			{
				_shootCount++;	
				_dragAttempt = 0;	
				attemptCount =0;
				isMonsterLipClicked = false;
				isHitTest = false;
				_isCorrectOrWrong = "";	
				isInitMonsterEffect_bool = true;
				 
				LetterToSoundMatch.monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
				 
				getRandomCombination()
				
				 
				LetterToSoundMatch.blackLipMonsterArr[0].visible = true;
				LetterToSoundMatch.monsterLoaderArr[0].visible = false;
					
				for(var k=0;k<drapLipLen;k++){				
					_dragLipMc_arr[k].x = _dragLipMc_arr[k].posx;
					_dragLipMc_arr[k].y = _dragLipMc_arr[k].posy;
					_dragLipMc_arr[k].mouseChildren = true;
					_dragLipMc_arr[k].mouseEnabled = true;	
					_dragLipMc_arr[k].visible = false;
				}
				
								
				if(_shootCount>totalLen){
					//trace("Activity Ends/")
					disableDragListeners();					 
					showResultPopUpAtEnd();
					resetLen = totalLen;
				}else{
					 				 
					resetAllEvents()
				}
				 
			}else{				
				enableDragListeners()
			}						
			
			_isCorrectOrWrong = "";		
			isDragPlaying = false;
			for(var i=0;i<drapLipLen;i++){				
				_lip_AnimMc_Arr[i].x = _lip_AnimMc_Arr[i].posx;
				_lip_AnimMc_Arr[i].y = _lip_AnimMc_Arr[i].posy;
				_lip_AnimMc_Arr[i].visible = true;
			}	
			
			CrossMarkDisable()
			_getCurrentItemValue = 0;
		}
		
		private function resetAllEvents():void {
			startAnimation()
		}
		//----------------------------------------------------------------------------------//
		
		
		private function showResultPopUpAtEnd():void {	
			var resultCnt = 0;
			var resultFailTxt:String;
			var resultSuccessTxt:String;
			var myPattern:RegExp;
			
			glow_str = "";			
			if (ths.getChildByName("levelMc") != null) {
					ths.removeChild(ths.getChildByName("levelMc"))
			}
			
			levelEndMc = new mc_levelEnd();
			
			ths.addChild(levelEndMc);
			levelEndMc.name = "levelMc";
			
			resultSuccessTxt = LetterToSoundMatch.presetsXML.variables.round_complete.resultTextSuccess;			
			levelEndMc.successContainer.SuccessTxt.text  = resultSuccessTxt;
			
			_sync1 = Ui.EnableTextSync(levelEndMc.successContainer.SuccessTxt, LetterToSoundMatch.presetsXML); 		
			//
			resultFailTxt = LetterToSoundMatch.presetsXML.variables.round_complete.resultTextFail;	 
			levelEndMc.failContainer.FailTxt.text  = resultFailTxt;				
			_sync2 = Ui.EnableTextSync(levelEndMc.failContainer.FailTxt, LetterToSoundMatch.presetsXML);
				
				
			levelEndMc.levelStatus.gotoAndStop(_isCorrectArr.length + 1);			 
			
			for ( var j = 0; j < _isCorrectArr.length; j++) {				
				if (_isCorrectArr[j] == 1) {					
					levelEndMc.levelStatus["star"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;
					resultCnt++
				}else if(_isCorrectArr[j] == 2){
					levelEndMc.levelStatus["secondtry"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;
					 
				}else if(_isCorrectArr[j] >= 3){
					levelEndMc.levelStatus["thirdtry"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;
					 
				}
			}
			

			
			if(resultCnt == 3 || resultCnt == 2){									
				if(resultCnt == 3){
					levelEndMc.levelStatus["emptyIcon"+3].visible = false;
				}else if(resultCnt == 2){
					levelEndMc.levelStatus["emptyIcon"+2].visible = false;
				}
				levelEndMc.titleTf.text = LetterToSoundMatch.presetsXML.variables.round_complete.levelSucceedTitle;
				
				levelEndMc.restart.visible = false;
				
				glow_str = "nextbtn";
				levelEndMc.failContainer.visible = false
				levelEndMc.successContainer.visible = true;				
				ths.textSyncDelay_mc.gotoAndPlay(2)
				ths.textSyncDelay_mc.addFrameScript(ths.textSyncDelay_mc.totalFrames-1,successTextSync)
				 
			}
			
			
			if(resultCnt == 1 || resultCnt == 0 ){				
				levelEndMc.restart.visible = true;
				glow_str = "restartbtn";			
				levelEndMc.failContainer.visible = true;
				levelEndMc.successContainer.visible = false
				levelEndMc.titleTf.text = LetterToSoundMatch.presetsXML.variables.round_complete.levelFailedTitle;			
				  ths.textSyncDelay_mc.gotoAndPlay(2)
				 ths.textSyncDelay_mc.addFrameScript(ths.textSyncDelay_mc.totalFrames-1,failTextSync)
		
			}

			_isCorrectArr = null;
			_isCorrectArr = new Array()
			
			
			for(var i=0;i<drapLipLen;i++){				 
				_dragLipMc_arr[i].visible = true;
				_dragLipMc_arr[i].x = _dragLipMc_arr[i].posx
				_dragLipMc_arr[i].y = _dragLipMc_arr[i].posy
			}			
						 
		}
		
		private function successTextSync() {
			trace(" successTextSync ")
			//ths.textSyncDelay_mc.addFrameScript(ths.textSyncDelay_mc.totalFrames-1,null)
			_sync1.speakAndDo(celebrateCompletion);		
			addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
			levelEndMc.nxtBtn.buttonMode = true;
			levelEndMc.nxtBtn.addEventListener(MouseEvent.CLICK,onNextBtn_Handler)
		}
		
		
		private function failTextSync() {
			//ths.textSyncDelay_mc.addFrameScript(ths.textSyncDelay_mc.totalFrames-1,null)
			trace(" failTextSync ")
			_sync2.speakAndDo(failSyncSoundCompletion)
			addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				
			levelEndMc.restart.addEventListener(MouseEvent.CLICK, onReset_Handler)
			levelEndMc.restart.buttonMode = true;
			
		}
		
		
		private function celebrateCompletion(e):void {				
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  LetterToSoundMatch.presetsXML.variables.masteredSound  ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 //trace(" celebrateCompletion Sound Completed ");	
				
			}
		}
		private function failSyncSoundCompletion(e):void {		 
			/*addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				
			levelEndMc.restart.addEventListener(MouseEvent.CLICK, onReset_Handler)
			levelEndMc.restart.buttonMode = true;*/
		}
		
		
		private function playLetterAudio(e : Event):void {
			//trace(" Finished audio ")
		}
		
		private function onNextBtn_Handler(e:MouseEvent):void 
		{
			SoundMixer.stopAll() ;
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
			glow.alpha = 0;
			levelEndMc.nxtBtn.filters = [glow];
			//trace(" Next Btn Handler")
			
		}
		
		private function createGlowEffectForButn_Handlers(e):void 
		{
			 
			btn_blinkCnt++
			if(btn_blinkCnt %5==0){		
				glow.alpha = 1;			
				glowCnt++
				if (glowCnt == 3) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}
				if (glow_str == "restartbtn") {
					levelEndMc.restart.filters = [glow];
				}else if(glow_str == "nextbtn"){
					levelEndMc.nxtBtn.filters = [glow];
				}				
			} 
			
			if(btn_blinkCnt == 50){
				btn_blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
				glow.alpha = 0;		
				if (glow_str == "restartbtn") {
					levelEndMc.restart.filters = [glow];
				}else if(glow_str == "nextbtn"){
					levelEndMc.nxtBtn.filters = [glow];
				}	
				 
			}
		}
		
		 
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		private function onReset_Handler (event:MouseEvent):void {
			event.target.parent.visible = false;			
			_isCorrectArr = null;		
			_isCorrectArr = new Array();
			roundCount++;				
			SoundMixer.stopAll() ;	
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
			glow.alpha = 0;
			levelEndMc.nxtBtn.filters = [glow];
			for ( var j = 0; j < resetLen;j++){						
				_levelStatus["emptyIcon"+(j+1)].visible= true;				
				_levelStatus["star"+(j+1)].visible= false;				
				_levelStatus["wrong"+(j+1)].visible = false;				
				_levelStatus["star"+(j+1)].visible= false;					
				_levelStatus["secondtry"+(j+1)].visible= false;				
				_levelStatus["thirdtry"+(j+1)].visible = false;					
			}
			
			for(var i=0;i<drapLipLen;i++){				
				_lip_AnimMc_Arr[i].x = _lip_AnimMc_Arr[i].posx;
				_lip_AnimMc_Arr[i].y = _lip_AnimMc_Arr[i].posy;
				_lip_AnimMc_Arr[i].visible = true;
			}
			 
			for(var i1=0;i1<drapLipLen;i1++){				
				_dragLipMc_arr[i1].x = _dragLipMc_arr[i1].posx;
				_dragLipMc_arr[i1].y = _dragLipMc_arr[i1].posy;
				_dragLipMc_arr[i1].visible = false; 
			} 			
			 
 
			LetterToSoundMatch.blackLipMonsterArr[0].visible = true;
			LetterToSoundMatch.monsterLoaderArr[0].visible = false;
			
			_getCurrentItemValue = 0
			_shootCount = 1;	
			_dragAttempt = 0;	
			attemptCount =0;
			consAttempt = -1;
			resetLen =0;
			isMonsterLipClicked = false;
			//ths.round_mc._txt.text = String(Number(_shootCount));
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount))
			_isCorrectOrWrong = "";
			enableDragListeners()
			CrossMarkDisable()
			isStart_Game = true	
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			
			startAnimation()
		}
		
		
		private function playSuccessOrTryAgainSound(_isCorrectOrWrong:String)
		{
			disableDragListeners()
			 if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			var _successSndArr = [];
			var _failureSndArr = [];		
			var getRandomSound:Number = 0;
			var i = 0;
			var sound:CoreSound
			var mybuffer:SoundLoaderContext = new SoundLoaderContext(1000,true);
						
			if (_isCorrectOrWrong == "correct") {				 
				for( i=0;i<LetterToSoundMatch.presetsXML.variables.successSounds.sound.length();i++){
					_successSndArr[i] = LetterToSoundMatch.presetsXML.variables.successSounds.sound[i]					
				}
				getRandomSound = randomRange(0, _successSndArr.length - 1)	
				sound = new CoreSound(new URLRequest(_successSndArr[getRandomSound]),mybuffer);
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
				
				 
			}else if (_isCorrectOrWrong == "wrong") {	
			 
				for( i=0;i<LetterToSoundMatch.presetsXML.variables.failSounds.sound.length();i++){
					_failureSndArr[i] = LetterToSoundMatch.presetsXML.variables.failSounds.sound[i]						 
				}
				getRandomSound = randomRange(0, _failureSndArr.length - 1)		
				sound = new CoreSound(new URLRequest(_failureSndArr[getRandomSound]),mybuffer);
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
				 
			}
			
			function onComplete(e:Event):void {
				 
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);				
				ch = new SoundChannel();				
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
				function onSoundComplete(e:Event):void {		
					
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);	
					dispatchEvent(new Event("END_CORR_WNG_SOUND_FINISHED"))
					if (isMonsterLipClicked == false) {							 
							 LetterToSoundMatch.monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
							 //LetterToSoundMatch.LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
						}
						isMonsterLipClicked = false;
				}				
			}	
		}
		
		
		
		
		
		public function getRandomNoRepeat():Array
		{
			var letterArray:Array = RandomLetters.stringShuffule(letterarr, orderarr, LetterToSoundMatch.LETTER_TO_DEAL, 3)
			 var arr = []
			 arr.push(letterArray[0],letterArray[1],letterArray[2])
			
			
			var shuffledLetters:Array = new Array(letterArray.length);		
			
			var randomPos:Number = 0;
			for (var i:int = 0; i < shuffledLetters.length; i++) //use shuffledLetters.length because splice() will change letters.length
			{
				randomPos = int(Math.random() * letterArray.length);
				shuffledLetters[i] = letterArray.splice(randomPos, 1)[0];    //note this the other way around to the naive approach	
				
			}
			for (var k = 0; k < 3;k++ ) {
				trace(" Random Letter *&* "+arr[k].letter )
			}
			 
			
			return arr//shuffledLetters 
		}
		
		private function randomRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (1 + maxNum - minNum)) + minNum);
		}
		private function myNewIsNaN(val:Number): Boolean
		{
			return val != val;
		}
		
		function randomNumber(low:Number=NaN, high:Number=NaN):Number
		{
			var low:Number = low;
			var high:Number = high;

			if (isNaN(low))
			{
				throw new Error("low must be defined");
			}
			if (isNaN(high))
			{
				throw new Error("high must be defined");
			}

			return Math.round(Math.random() * (high - low)) + low;
		}
		 
			
	}
}