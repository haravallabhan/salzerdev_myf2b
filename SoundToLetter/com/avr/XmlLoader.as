﻿package com.avr{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import com.myf2b.core.*;


	public class XmlLoader {
		private var _func:Function;
		private var xmlLoader:CoreURLLoader;
		private var urlRequest:URLRequest;
		public var data:XML;
		public function XmlLoader(url,func) {
			_func = func;
			xmlLoader = new CoreURLLoader();

			load(url,func);
			xmlLoader.addEventListener(Event.COMPLETE,onXMLLoaded);
		}
		public function load(url,func) {
			urlRequest = new URLRequest(url);
			_func = func;
			xmlLoader.load(urlRequest);

		}
		private function onXMLLoaded(e:Event):void {
			data = XML(e.currentTarget.data);
			_func();
		}

	}
}