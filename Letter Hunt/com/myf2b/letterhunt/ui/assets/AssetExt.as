package com.myf2b.slimegame.ui.assets
{
	import flash.display.MovieClip;

	public class AssetExt extends MovieClip
	{
		protected var _mc:MovieClip;
		
		public override function AssetExt(mc:MovieClip) {
			_mc = mc;
		}
		
		public override function set x(value:Number):void{
			_mc.x = value;
		}
		
		public override function get x():Number{
			return _mc.x;
		}
		
		public override function set y(value:Number):void{
			_mc.y = value;
		}
		
		public override function get y():Number{
			return _mc.y;
		}
		
		public override function set alpha(value:Number):void{
			_mc.alpha = value;
		}
		
		public override function get alpha():Number{
			return _mc.alpha;
		}
		
		public override function set scaleX(value:Number):void{
			_mc.scaleX = value;
		}
		
		public override function get scaleX():Number{
			return _mc.scaleX;
		}
		
		public override function set scaleY(value:Number):void{
			_mc.scaleY = value;
		}
		
		public override function get scaleY():Number{
			return _mc.scaleY;
		}
		
		public function get mc():MovieClip{
			return _mc;
		}
		
	}
}