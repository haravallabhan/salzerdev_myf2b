﻿package com.myf2b.lettertosoundmatch.utils 
{
	import com.myf2b.core.CoreURLLoader;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class XMLProvider 
	{
		private static var _callback:Function;
		private static var _urlLoader:CoreURLLoader;
		
		public static function load(url:String, callback:Function):void {
			_callback = callback;
			
			if (!_urlLoader) _urlLoader = new CoreURLLoader();
			
			loaderListeners = true;
			
			_urlLoader.load(new URLRequest( url ));
			
			
		}
		
		private static function set loaderListeners(value:Boolean):void {
			if (value) {
				_urlLoader.addEventListener(Event.COMPLETE, onLoadComplete);
				//_urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				
				_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			}
			else {
				_urlLoader.removeEventListener(Event.COMPLETE, onLoadComplete);
				//_urlLoader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
				_urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, onIoError);
			}
		}
		
		/*private static function onHttpStatus(e:HTTPStatusEvent):void {
			trace( "XMLProvider.onHttpStatus > e : " + e );
		}*/
		
		private static function onIoError(e:IOErrorEvent):void {
			loaderListeners = false;
			_urlLoader = null;
		}
		
		private static function onLoadComplete(e:Event):void {
			loaderListeners = false;
			
			_urlLoader = null;
			_callback(e);
		}
		
	}
}