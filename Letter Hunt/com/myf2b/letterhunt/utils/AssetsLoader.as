﻿package com.myf2b.lettertosoundmatch.utils
{
	import com.myf2b.lettertosoundmatch.manager.CoverManager;
	import com.myf2b.core.CoreLoader;
	
	import flash.display.DisplayObject;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	
	public class AssetsLoader
	{
		private static var _loader:CoreLoader;
		private static var _onCompleteCallback:Function;
		private static var _asset:MovieClip;
		private static var _isInitted:Boolean = false;
		
		public static function init() {
			_loader = new CoreLoader();
			_isInitted = true;
		}
		
		public static function load(url:String, onCompleteCallback:Function) :void {
			_onCompleteCallback = onCompleteCallback;
			if ( !_isInitted ) {
				init();
			}
			if ( url ) {
				_loader.load(new URLRequest(url) );
				_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
				_loader.contentLoaderInfo.addEventListener( Event.COMPLETE, onLoadComplete );
			}
		}
		
		private static function onProgress( e:ProgressEvent ) : void {
			CoverManager.instance.updateProgress(e.bytesLoaded, e.bytesTotal);
		}
		
		
		private static function onLoadComplete(e:Event):void {
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadComplete);
			_asset = (_loader.content as MovieClip) as MovieClip;
			_onCompleteCallback();
		}
		
		public static function get data():MovieClip {
			return _asset;
		}
		
		
	}
}