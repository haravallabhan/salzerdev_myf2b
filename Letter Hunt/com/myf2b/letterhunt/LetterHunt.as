﻿package com.myf2b.letterhunt 
{
	
	import com.avr.XmlLoader;
	import com.greensock.TweenLite;
	import com.greensock.text.SplitTextField;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.letterhunt.manager.SoundManager;
	import com.myf2b.ui.TextFieldSoundSync;
	import com.myf2b.ui.Ui;
	import flash.filters.GlowFilter;
	import flash.text.StyleSheet;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.filters.BitmapFilterQuality;	
		
	/**
	 * 
	 * @author haravallabhanjn
	 * 
	 */
	public class LetterHunt extends MovieClip {
		
		public static const BACK_TO_MAP:String = "back_to_map";
		public static var LETTER_TO_DEAL:String = "m";
		public static var _contentKey:String = "1";
		private var _gameXMLURL:String;	
		public static var moduleSessionID:String;
		private var str:String;
		private var letterOrNumber_str:String;	
			
		public static var monsterMC:MovieClip;
		private var tweenTxt_mc:MovieClip;		
		public static var containerMc:MovieClip;	
		public static var containerMc1:MovieClip;
		
		public var loader:Loader = new Loader();		
		public static var liploader:Loader = new Loader();
		public static var numberBoardLoader:Loader = new Loader();
		public static var monst_mc:MovieClip
		public static var numberBoard_mc:MovieClip
		
		public static var letterItemloader:Loader = new Loader();
		
		
		private var xmlLoader:XmlLoader;
		public var xml:XML;
		public static var presetsXML:XML;	
		private var _letterXML:XMLList;
		public static var xmlList:XMLList;
		
		public static var monsterLoaderArr = [];		
		public static var numberBoardLoaderArr = [];		
		public static var LipLoaderArr = [];
		public static var LetterItemLoaderArr = [];
		private var push_arr = [];		
		private var total_arr = []
		private var highSongPlaying_arr = [];
		private var disableMc_arr = [];
		private var blink_arr = [];
		private var disableMc1_arr = [];		
		private var tween_arr = [];
		private var clicked_arr = [];
		
		
		private var tweenCnt:Number = 0;
		
		private var boardHitCnt:Number = 0;
		private var blinkCnt:Number = 0;
		private var posx:Number = 200;
		private var posy:Number = 350;
		private var maxWidth:Number = 850;
		private var maxHeight:Number = 50;
		private var getTargetNo:Number = 0;
		private var highLight_bool:Boolean = false;
		private var nextBtn_bool:Boolean = false;
		private var counterTextField:TextField;
		private var currentHighLightMc:MovieClip
		
		var coordX:Number
		var coordY:Number
		var timer:Timer
		
		private var ch:SoundChannel;
		private var format:TextFormat;	
		var mc2:MovieClip
		//		
		private var ths:*;
		private var stg:*;		
		
		private var shouldKeepLooking:Boolean;
		private var isRestartLevel:Boolean = false;
		private var _sync : TextFieldSoundSync;
		////////////////////////////////////////
		 
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB;
		private var glow:GlowFilter = new GlowFilter();
		var glow_str:String;
		//
		private var btn_blinkCnt = 0;
		private var playbtn_blinkCnt = 0;
		private var monster_blinkCnt = 0;
		///////////////////////////////////////
		
		
		private var prevIndex = 0;
		private var isRhymesPlaying:Boolean = false;
		var txtField:TextField
		var textSyncTxtField:TextField
		
		private var FONT_DEF_COLOR = 0x0066FF;
		private var FONT_BLINK_COLOR = 0xFFCC00;
		private var FONT_CLICKED_COLOR = 0xFF0000;
		private var monsterWidth:Number = 150;
		private var monsterHeight:Number = 150;
		var isGameComplete:Boolean = false;
		
		var FONT_SIZE = 55;
		var songText;
	 
			
		/**
		 * 
		 * @param contentKey
		 * 
		 */
		public static var gameScreenProxy:GameScreen;
		public static var gameIntroProxy:GameIntroScreen
		
		 
		public function LetterHunt(aThs,contentKey:String="1") {
		
			ths =  LetterHuntShell.isThis();
			stg =  LetterHuntShell.isStage();
			ths.dummy_mc.visible = false;
			
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
			
			addEventListener(Event.ADDED,init);
			addEventListener(Event.REMOVED_FROM_STAGE,destroy);
			_contentKey = contentKey;
			 
			 
			//moduleSessionID = Reporter.getNewModuleSessionID();//To DB		
			shouldKeepLooking = true
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
			_contentKey = null;
			SoundMixer.stopAll();
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function init(e:Event):void {
			removeEventListener(Event.ADDED,init);
			stop();
			visible = false;
			
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}					// Otherwise build the URL for the CDN
			else {
				_gameXMLURL = Core.filesURL+"/letter_hunt/"+_contentKey+"/game.xml";
			}
			//xmlLoader = new XmlLoader("http://salzer-ror.cloudapp.net/xmls/16/xml_feed",presetsXMLLoaded)
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/6/xml_feed",presetsXMLLoaded);//game.xml
			//xmlLoader = new XmlLoader("game.xml",presetsXMLLoaded);//		
			
		}
		
		/**
		 * 
		 * 
		 */
		private function presetsXMLLoaded():void {				
			presetsXML = XML(xmlLoader.data);			
			gameIntroProxy = new GameIntroScreen();
			gameIntroProxy.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", endOfIntroScreen_Handler);						
		}
		
		private function endOfIntroScreen_Handler(e:Event):void 
		{
			LETTER_TO_DEAL = gameIntroProxy.selectedObject;

						
			gameScreenProxy = new GameScreen();	
			xmlList = presetsXML.deal.(@letter == LETTER_TO_DEAL);
			songText = xmlList.rhymesText;
			FONT_DEF_COLOR = presetsXML.variables.rhymeTextColor;
			FONT_CLICKED_COLOR = presetsXML.variables.rhymeIdentifiedLetterColor;
			FONT_BLINK_COLOR = presetsXML.variables.rhymeMissedLetterColor;		
			
			ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
			ths.next_mc.buttonMode = true;
			ths.playBtn.addEventListener(MouseEvent.CLICK, onPlayBtnClick);
			ths.playBtn.buttonMode = true;
			
			addEventListener("SOUNDS_ACCESSIABLE_HANDLER", onSoundsAccessiable_Handler)
			//loadLetterItemGraphic()
			loadMonsterGraphic();
			
		}
		
		
		private function loadMonsterGraphic():void{
			loader.load(new URLRequest(xmlList.monsterGraphic)) 
			loader.cacheAsBitmap = true;			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete);
			//addEventListener("INSTRUCT", gameScreenProxy.autoPlayInstruction);
		}
		
		private function loadComplete(e):void{
			monst_mc = new MovieClip;
			ths.addChild(monst_mc);
			if(monsterLoaderArr == null){
				monsterLoaderArr = new Array();
			}
			
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			monsterLoaderArr[0] = new MovieClip();
			monsterLoaderArr[0] =  (e.target as LoaderInfo).content;
			monsterLoaderArr[0].x = 440;//xmlList.monsterLocationX;
			monsterLoaderArr[0].y = 180;//xmlList.monsterLocationY;
			
			var ratio:Number;//ratio
			ratio = monsterLoaderArr[0].height/monsterLoaderArr[0].width;//calculation ratio
			//trace(ratio)
			if (monsterLoaderArr[0].width>monsterWidth) {
				monsterLoaderArr[0].width = monsterWidth;
				monsterLoaderArr[0].height = Math.round(monsterLoaderArr[0].width*ratio);
			}

			if (monsterLoaderArr[0].height>monsterHeight) {
				monsterLoaderArr[0].height = monsterHeight;
				monsterLoaderArr[0].width = Math.round(monsterLoaderArr[0].height/ratio);
			}
			 
			
			//	monsterLoaderArr[0].width = 150;
			//monsterLoaderArr[0].height = 150;		
			
			monst_mc.addChild(monsterLoaderArr[0]);	
			monst_mc.buttonMode = true;	
			monst_mc.addEventListener(MouseEvent.CLICK, onHitBoard_Handler);			
			numberBoardLoader.load(new URLRequest(presetsXML.variables.numberBoard));
			numberBoardLoader.cacheAsBitmap = true;			
			numberBoardLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, boardLoadComplete);		
		}

		private function boardLoadComplete(e):void{
			numberBoard_mc = new MovieClip;
			ths.addChild(numberBoard_mc);
			if(numberBoardLoaderArr  == null){
				numberBoardLoaderArr  = new Array();
			}
			
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			numberBoardLoaderArr[0] = new MovieClip();
			numberBoardLoaderArr[0] =  (e.target as LoaderInfo).content;
			numberBoardLoaderArr[0].x = presetsXML.variables.numberBoardLocationX; //xmlList.monsterLocationX;
			numberBoardLoaderArr[0].y = presetsXML.variables.numberBoardLocationY; //xmlList.monsterLocationY;
			 			
			numberBoard_mc.buttonMode = true;	
			numberBoardLoaderArr[0].buttonMode = true;
			counterTextField = numberBoardLoaderArr[0].hitCount_Tf;
			counterTextField.mouseEnabled = false;
			counterTextField.text = "0";
			numberBoard_mc.addChild(numberBoardLoaderArr[0]);			
			//loadLipGraphic();
			
			
			//Code For Lip is edited:			
			createLettersField();
			setFormatValues(songText);			
			createRhymesPlaying();
			createTweeningText();
			
			
			
			numberBoard_mc.addEventListener(MouseEvent.CLICK, onHitBoard_Handler);			
			dispatchEvent(new Event("INSTRUCT") );
		//	addEventListener(Event.ENTER_FRAME, createGlowEffectForPlayButn);
			//addEventListener(Event.ENTER_FRAME, createGlowEffectForMonster);
			
		}
		
		
		private function onMonster_Handler(e):void {
			
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, onMonster_Handler);	
			if (nextBtn_bool == true) {
				
				  SoundMixer.stopAll()
				 var len:int = blink_arr.length;					  
				 for ( var i:int = 0; i < len; i++ ) {												 
					 containerMc.removeChild( blink_arr[i]);							
				 }	
				 
				 // clear the array
				 blink_arr = []
				 blink_arr.length = 0;							 
				for ( var j:int = 0; j < push_arr.length; j++ ) {							 
					if (push_arr[j].alpha == 0) {
						 push_arr[j].alpha = 1; 
					}							 
				 }
				 
				 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				 if (currentHighLightMc) {
					 currentHighLightMc.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
				 } 
			 }
			ths.dummy_mc.visible = true;
			ths.setChildIndex(ths.dummy_mc,ths.numChildren-1)
			playSndInstructions("monsterClickSound")
		}
		
		//		
		private function loadLipGraphic():void{
			liploader.load(new URLRequest(xmlList.lipGraphic))
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE,lipLoader_loadComplete);
			liploader.cacheAsBitmap = true;					
		}
		
		private function lipLoader_loadComplete(e:Event):void{
			
			if(LipLoaderArr == null){
				LipLoaderArr = new Array()
			}
			
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);			
			LipLoaderArr[0] = new MovieClip();			
			LipLoaderArr[0] =  (e.target as LoaderInfo).content
			LipLoaderArr[0].x = xmlList.lipLocationX;
			LipLoaderArr[0].y = xmlList.lipLocationY;		
			
			ths.addChild(LipLoaderArr[0]);		
			
			LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onHitBoard_Handler);
			LipLoaderArr[0].buttonMode = true;	
			
			createLettersField();
			createRhymesPlaying();
			createTweeningText();			
		}
		
		public function onMonsterLip_Handler(e:MouseEvent):void {
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, onMonsterLip_Handler);	
			if (nextBtn_bool == true) {
				  SoundMixer.stopAll()
				 var len:int = blink_arr.length;					  
				 for ( var i:int = 0; i < len; i++ ) {												 
					 containerMc.removeChild( blink_arr[i]);							
				 }	
				 
				 // clear the array
				 blink_arr = []
				 blink_arr.length = 0;							 
				for ( var j:int = 0; j < push_arr.length; j++ ) {							 
					if (push_arr[j].alpha == 0) {
						 push_arr[j].alpha =1 
					}							 
				 }
				 
				 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				 if (currentHighLightMc) {
					 currentHighLightMc.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
				 } 
			 }
			ths.dummy_mc.visible = true;
			ths.setChildIndex(ths.dummy_mc,ths.numChildren-1)
			playSndInstructions("monsterClickSound")			
		}
		
		public function onPlayBtnClick(e:MouseEvent):void {			
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, onPlayBtnClick);
			glow.alpha = 0; 
			playbtn_blinkCnt = 490;
			ths.playBtn.filters = [glow];
			ths.dummy_mc.visible = true;
			ths.setChildIndex(ths.dummy_mc, ths.numChildren - 1)		 
			 if (nextBtn_bool == true) {
				  SoundMixer.stopAll()
				 var len:int = blink_arr.length;	
				 
				 for ( var i:int = 0; i < len; i++ ) {												 
					 containerMc.removeChild( blink_arr[i]);							
				 }	
				 
				 // clear the array
				 blink_arr = []
				 blink_arr.length = 0;							 
				for ( var j:int = 0; j < push_arr.length; j++ ) {							 
					if (push_arr[j].alpha == 0) {
						 push_arr[j].alpha =1 
					}							 
				 }
				 
				 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				 if (currentHighLightMc) {
					 currentHighLightMc.addEventListener(MouseEvent.CLICK, onHighLightClick_Handler)					 
				 } 
			 }
			 			 
			playSndInstructions("playSong")			
		}
		//
		private function loadLetterItemGraphic():void{
			letterItemloader.load(new URLRequest(xmlList.letterItem))
			letterItemloader.contentLoaderInfo.addEventListener(Event.COMPLETE,letterItemLoader_loadComplete);
			letterItemloader.cacheAsBitmap = true;	

		}
		
		private function letterItemLoader_loadComplete(e:Event):void{			
			if(LetterItemLoaderArr == null){
				LetterItemLoaderArr = new Array()
			}
			
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);			
			LetterItemLoaderArr[0] = new MovieClip();			
			LetterItemLoaderArr[0] =  (e.target as LoaderInfo).content
			LetterItemLoaderArr[0].x = xmlList.letterItemX;
			LetterItemLoaderArr[0].y = xmlList.letterItemY;			
			//ths.parent.addChild(LetterItemLoaderArr[0])
			ths.addChild(LetterItemLoaderArr[0])
		//	ths.setChildIndex(LetterItemLoaderArr[0], ths.numChildren-1)
			LetterItemLoaderArr[0].addEventListener(MouseEvent.CLICK, onLetterItem_Handler);
			LetterItemLoaderArr[0].buttonMode = true;	
			
		}
		//
		private function onLetterItem_Handler(e:MouseEvent):void {
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, onLetterItem_Handler);
			if (nextBtn_bool == true) {
				  SoundMixer.stopAll()
				 var len:int = blink_arr.length;					  
				 for ( var i:int = 0; i < len; i++ ) {												 
					 containerMc.removeChild( blink_arr[i]);							
				 }	
				 
				 // clear the array
				 blink_arr = []
				 blink_arr.length = 0;							 
				for ( var j:int = 0; j < push_arr.length; j++ ) {							 
					if (push_arr[j].alpha == 0) {
						 push_arr[j].alpha =1 
					}							 
				 }
				 
				 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				 if (currentHighLightMc) {
					 currentHighLightMc.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
				 } 
			 }
			ths.dummy_mc.visible = true;
			ths.setChildIndex(ths.dummy_mc,ths.numChildren-1)
			playSndInstructions("mangoSnd")
		}
		
		private function createLettersField() {	
			 
/*			FONT_SIZE = xmlList.rhymesTextSize;	
			var FONT_WEIGHT_BOLD = xmlList.rhymesTextBold;
			var TEXT_ALIGNMENT = xmlList.rhymesTextAlign;	
			format = new TextFormat();
			format.font = "Verdana";
			format.color = FONT_DEF_COLOR;
			format.size =  FONT_SIZE;  
			format.align = TEXT_ALIGNMENT;

			if(FONT_WEIGHT_BOLD == "yes"){
				newFont = new ArialBold();
				format.bold = true;
			}else{
				newFont = new ArialRegular();	
				format.bold = false;
			}
			format.font = "Verdana";*/
			var TEXT_LOCATION_X = xmlList.rhymesTextLocationX;	
			var TEXT_LOCATION_Y = xmlList.rhymesTextLocationY;				
			posx = TEXT_LOCATION_X ;	
			posy = TEXT_LOCATION_Y ;	
			
			containerMc = new MovieClip();
			ths.addChild(containerMc)			
			
			containerMc1 = new MovieClip();
			ths.addChild(containerMc1)
			 
		}
		
		private function createRhymesPlaying():void 
		{
			
			txtField = createCustomTextField(posx, posy, maxWidth, maxHeight);			
			//containerMc.addChild(txtField);
			txtField.antiAliasType = AntiAliasType.ADVANCED;
			txtField.embedFonts = true;
			txtField.autoSize = TextFieldAutoSize.LEFT;
			txtField.mouseEnabled = false;			
			txtField.height = 100;				
		  
			textSyncTxtField = createCustomTextField(posx, posy, maxWidth, maxHeight);			
			textSyncTxtField.antiAliasType = AntiAliasType.ADVANCED;			
			textSyncTxtField.embedFonts = true;			
			textSyncTxtField.autoSize = TextFieldAutoSize.LEFT;
			textSyncTxtField.mouseEnabled = false;
			textSyncTxtField.height = 100;	
			textSyncTxtField.visible = false;
			
			songText = songText.split("<br />").join("");
			songText = songText.split("\n\r").join("");			
			
			textSyncTxtField.htmlText = txtField.htmlText = songText;
			textSyncTxtField.multiline = txtField.multiline = true;
			textSyncTxtField.wordWrap = txtField.wordWrap = true;	
 
			containerMc1.addChild(textSyncTxtField);	
			containerMc.addChild(txtField);
			txtField.setTextFormat(format)							
			textSyncTxtField.setTextFormat(format);	
			
			_sync = Ui.EnableTextSync(textSyncTxtField, presetsXML);			
			containerMc.mouseChildren = true;
			containerMc1.mouseChildren = true;
			
			counterOn();			 
		}
		
		function setFormatValues(songString: String):void {
			
			format = new TextFormat();
			var newFont = new ArialRegular();			
			var styleArry:Array = songString.split("style=");
			if (songString.indexOf("<em>") != -1) {
				newFont = new ArialItalic();
				format.italic = true;
			}else if (songString.indexOf("<strong>") != -1) {
				if (songString.indexOf("<em>") != -1) {
					newFont = new ArialBoldItalic();
					format.italic = true;
					format.bold = true;
				}else{				
					newFont = new ArialBold();
					format.bold = true;
					format.italic = false;
				}
			}else{
				newFont = new ArialRegular();	
				format.bold = false;
			}
			
		
			for (var i = 0; i<styleArry.length; i++) {
				if (styleArry[i].indexOf("\"") != -1) {
					var index = styleArry[i].indexOf("\"");
					var lastIndex = styleArry[i].lastIndexOf("\"");
					if (lastIndex != -1) {
						var styleFormat = styleArry[i].substring(index+1, lastIndex);
						var styleText:Array = styleFormat.split(":");
						if (styleText[0] == "text-align") {
							trace(styleText[1].replace(";", ""));
							if(styleText[1] != null)
								format.align = styleText[1].replace(";", "").replace(" ", "");
						}else if (styleText[0] == "font-size") {
							if(styleText[1] != null)
								format.size =  styleText[1].replace("px", ""); 							
						}else if (styleText[0] == "color") {
							if (styleText[1] != null) {
								var color:uint = uint("0x" + styleText[1].replace(";", ""));
								format.color = "0x"+styleText[1].replace(";", "").replace("#", "");//color;
							}
						}
					}
				}
			}
			format.font =  newFont.fontName;
		}
		
		
		function counterOn():void {
			 
			containerMc1.visible = false;
			textSyncTxtField.visible = false;
			
			txtField.visible = true;	
		 
			for(var i = 0;i < total_arr.length;i++){
				total_arr[i].mov.visible = true;
			}		
			
			for(var j = 0;j < disableMc_arr.length;j++){
				disableMc_arr[j].visible = true;
			//	push_arr[j].textColor = 0x0066FF//FONT_CLICKED_COLOR;				 
				var txtMc:TextField = disableMc_arr[j].getChildAt(0) as TextField
				txtMc.textColor = FONT_CLICKED_COLOR;// 0x0066FF//FONT_DEF_COLOR;
				trace(" FONT_DEF_COLOR= "+FONT_CLICKED_COLOR)
			}
			
			for(var k = 0;k < push_arr.length;k++){
				push_arr[k].visible = true;
				
			}
			
			
		}
		
		function songOn():void {
			//containerMc1.y = containerMc1.y - 30;
			containerMc1.visible = true;
			textSyncTxtField.visible = true;
			txtField.visible = false; 		
			highSongPlaying_arr = [];
			var mc:MovieClip 
			trace("SongOn")			
			for(var i=0;i<total_arr.length;i++){
				total_arr[i].mov.visible = false;				  
			}			
			
			for(var j=0;j<disableMc_arr.length;j++){
				disableMc_arr[j].visible = true;			
			}
			 
			for(var k = 0;k < push_arr.length;k++){
				push_arr[k].visible = false;				 
				if (clicked_arr[k] != undefined ) {
					push_arr[k].visible = true;							 
					mc = new MovieClip()
					ths.addChild(mc);
					mc.addChild(push_arr[k].parent)					
					push_arr[k].textColor = FONT_CLICKED_COLOR//0xFF0000//FONT_CLICKED_COLOR;
					//trace("containerMc= "+containerMc.getChildByName("myMc").name)
					
				}
			}
			
			
		}
		
		function createTweeningText() 
		{									 
			//push_arr.length = 0
			var rex:RegExp = /[^\w!]/gim; 
			 var mc:MovieClip
			push_arr = SplitTextField.split( txtField, SplitTextField.TYPE_CHARACTERS, containerMc)			 			
			for (var s = 0; s < push_arr.length; s++)
			{					 
				mc = new MovieClip();										
				containerMc.addChild(mc);
				push_arr[s].text = push_arr[s].text.replace(rex, "");
				if (push_arr[s].text == "") {
					continue;
				}
				mc.addChild(push_arr[s]);	
				mc.addEventListener(MouseEvent.CLICK, onHighLightClick_Handler)	 	
				mc.buttonMode = true;					
				mc.no = s;
				mc.name = "myMc";
				mc.x = posx;
				mc.y = posy;
				mc.px = posx
			    mc.py = posy
				 
				var letter:String = String(push_arr[s].text).toLowerCase();
				if(String(push_arr[s].text) !=""){
					if (letter == LETTER_TO_DEAL) {					
						total_arr.push( { mov:mc, movx:mc.x, movy:mc.y } );
						tween_arr[s] = new duplicate_mc()
						ths.addChild(tween_arr[s])
						tween_arr[s].visible = false;						
					}
				}			
			}
			
		}
		
	 
		
		private function onHighLightClick_Handler(e:MouseEvent):void {		  
			trace("onHighLightClick_Handler ")
			var letter:String = String(push_arr[e.target.no].text).toLowerCase();	
			if (letter == LETTER_TO_DEAL )
			{		
				disableMc_arr.push(e.target)				
				e.target.removeEventListener(MouseEvent.CLICK, onHighLightClick_Handler)
				e.target.buttonMode = false;
			
				var txtMc:TextField = e.target.getChildAt(0) as TextField
				txtMc.textColor = FONT_CLICKED_COLOR;					
				var txtStr:String = txtMc.text;	
				//trace(" Get Txt Coloor "+ FONT_CLICKED_COLOR)
				clicked_arr[e.target.no] = txtMc//e.target
				
				getTargetNo  = Number(String(e.target.no));
				
				tween_arr[e.target.no]._txt.text = txtStr//LETTER_TO_DEAL
				tween_arr[e.target.no]._txt.mouseEnabled = false;			
				tween_arr[e.target.no]._txt.embedFonts = true;	
				tween_arr[e.target.no]._txt.setTextFormat(format);
				tween_arr[e.target.no]._txt.textColor = FONT_CLICKED_COLOR;		
				
				tween_arr[e.target.no].bg.alpha = 0;
				
				tween_arr[e.target.no].x = posx + push_arr[e.target.no].x;
				tween_arr[e.target.no].y = posy + push_arr[e.target.no].y;			 
				tween_arr[getTargetNo].visible = true;
				
				TweenLite.to(tween_arr[getTargetNo], 1, {x:numberBoardLoaderArr[0].x+30, y:numberBoardLoaderArr[0].y+20,rotation:Math.random()*270,onComplete:onFinishTween,onCompleteParams:[5, tween_arr[getTargetNo]]});						
				boardHitCnt++
				counterTextField.text = String(Number(boardHitCnt))	
				playSndInstructions("ticker");	
				 
			}else {				
				if (nextBtn_bool == false) {
					 
					ths.dummy_mc.visible = true;
					ths.setChildIndex(ths.dummy_mc,ths.numChildren-1)
					e.target.removeEventListener(MouseEvent.CLICK, onHighLightClick_Handler)
					currentHighLightMc = e.target as MovieClip;						
					onWordClick_Handler()	
					ths.next_mc.removeEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				}
			} 
		}
		
		private function onHitBoard_Handler(e:MouseEvent):void 
		{
			if (nextBtn_bool == true) {
				  SoundMixer.stopAll()
				 var len:int = blink_arr.length;					  
				 for ( var i:int = 0; i < len; i++ ) {												 
					 containerMc.removeChild( blink_arr[i]);							
				 }	
				 
				 // clear the array
				 blink_arr = []
				 blink_arr.length = 0;							 
				for ( var j:int = 0; j < push_arr.length; j++ ) {							 
					if (push_arr[j].alpha == 0) {
						 push_arr[j].alpha = 1;
						 
					}							 
				 }
				 
				 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				 if (currentHighLightMc) {
					 currentHighLightMc.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
				 } 
			 }
			 
			 ths.dummy_mc.visible = true;
			 ths.setChildIndex(ths.dummy_mc, ths.numChildren - 1)
			 
			playSndInstructions("boardNumberSound")
		}
		
		private function onWordClick_Handler() {	
			
			format.color =  FONT_BLINK_COLOR;				
			blink_arr = []		 		 
			for(var i=0;i<push_arr.length;i++){
				var letter:String = String(push_arr[i].text).toLowerCase();	
				 	trace(push_arr[i].textColor+" != "+clicked_arr[i])					 
					
				//if (push_arr[i].textColor == clicked_arr[i].textColor)
				//{
					//parseInt(FONT_DEF_COLOR, 16) ) {					 
				
				if (clicked_arr[i] == undefined ) 
				{	 
					if (letter == LETTER_TO_DEAL ) {						
						var blinkMc:MovieClip = new MovieClip()
						blinkMc = new blinking_mc();
						blinkMc.x = posx + push_arr[i].x - 2;								
						blinkMc.y = posy + push_arr[i].y							
						containerMc.addChild(blinkMc);
						blinkMc.name = "blinkMc";
						blinkMc.bg.alpha = 0;
						blinkMc._txt.text = push_arr[i].text;						
					//	blinkMc._txt.textColor = FONT_BLINK_COLOR;						
						blinkMc._txt.autoSize = TextFieldAutoSize.LEFT;
						blinkMc._txt.mouseEnabled = false;
						blinkMc._txt.embedFonts = true;						
						blinkMc._txt.setTextFormat(format);			 
						blinkMc.gotoAndStop(1)	
						push_arr[i].alpha = 0;						 
						blink_arr.push(blinkMc)							 
					}
				}else{
					ths.dummy_mc.visible = true;
					ths.setChildIndex(ths.dummy_mc, ths.numChildren - 1);				
				}
				 
			}
			playSndInstructions("hintSound")
		}
		
		
		public function playSndInstructions(numberOrLetterSnd:String, aSnd_str:String = ""):void {

			letterOrNumber_str = numberOrLetterSnd;
			var sound:CoreSound;	
			_letterXML = LetterHunt.presetsXML.deal.(@letter == LETTER_TO_DEAL)			
			 if(letterOrNumber_str == "letterSound" || letterOrNumber_str == "monsterClickSound"){
				 if (_letterXML.letterSound != "") {
					 sound = new CoreSound(new URLRequest(_letterXML.letterSound));
					 sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
				 }
			 }else if(letterOrNumber_str == "ticker"){
				sound = new CoreSound(new URLRequest(LetterHunt.presetsXML.variables.successSounds.sound[0]));
				sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			 }else if (letterOrNumber_str == "complete") {
					sound = new CoreSound(new URLRequest(LetterHunt.presetsXML.variables.successSounds.sound[1]));
					sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			 }else if (letterOrNumber_str == "hintSound") {
				 var wrongSnd_str:String = LetterHunt.presetsXML.variables.failSounds.sound[0]
				 sound = new CoreSound(new URLRequest(wrongSnd_str));
				 sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);				 
			 }else if (letterOrNumber_str == "playSong") {
				 songOn();
				 _sync.speakAndDo(playLetterAudio);
				 nextBtn_bool = true;				 
			 } else if(letterOrNumber_str == "mangoSnd"){				
				// SoundMixer.stopAll(); 
				 sound=new CoreSound(new URLRequest(_letterXML.letterItemSound));
				 sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			 }else if(letterOrNumber_str =="boardNumberSound"){
				  var num:* = counterTextField.text;
				  if (boardHitCnt>0) {
					var numberStr:String = LetterHunt.presetsXML.variables.numberSounds.numbers[boardHitCnt-1]	
					sound = new CoreSound(new URLRequest(numberStr));				 
					sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);  
				  }else {
					ths.dummy_mc.visible = false;
				  }
			 }else {
				  //SoundMixer.stopAll(); 
				 sound=new CoreSound(new URLRequest(aSnd_str));
				 sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			 }
			 
			function onComplete(e:Event):void {				
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);				
				ch = new SoundChannel();				 
				ch = sound.play();				
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {
					if (letterOrNumber_str == "hintSound") {
						var len:int = blink_arr.length;					  
						 for ( var i:int = 0; i < len; i++ ) {												 
							 containerMc.removeChild( blink_arr[i]);							
						 }	
						 // clear the array
						 blink_arr = []
						 blink_arr.length = 0;							 
						for ( var j:int = 0; j < push_arr.length; j++ ) {							 
							if (push_arr[j].alpha == 0) {
								 push_arr[j].alpha =1 
							}							 
						 }
						 
						 ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
						 if (currentHighLightMc) {
							 currentHighLightMc.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
						 }
					}
					
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);	
					ths.dummy_mc.visible = false;			 
					nextBtn_bool = false;				 
					addListener();
					if(!isGameComplete){
						hasGameEnded(); // check if the game is complete
						
					}
				}
			}
		}
		
		private function playLetterAudio(e : Event):void {
			ths.dummy_mc.visible = false;
			nextBtn_bool = false
			trace(" Letter Song Sound Finished.../")
			counterOn();			
		};	
		
		 
		
		private function onSoundsAccessiable_Handler(e:Event):void 
		{
				
		}
		
		private function onHints_SoundsAccessiable_Handler(e:Event):void 
		{
			var len:int = blink_arr.length;					  
			for ( var i:int = 0; i < len; i++ ) {												 
			 containerMc.removeChild( blink_arr[i]);							
			}	

			// clear the array
			blink_arr = []
			blink_arr.length = 0;							 
			for ( var j:int = 0; j < push_arr.length; j++ ) {							 
				if (push_arr[j].alpha == 0) {
					 push_arr[j].alpha =1 
				}			 
			}

			ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler);

		}
		
		
	 
				
		private function onFinishTween(param1:Number, param2:MovieClip):void {
			param2.visible = false;			 
			TweenLite.killTweensOf(param2)	
		}
		
		
		private function createCustomTextField(x:Number, y:Number, width:Number, height:Number):TextField
		{
			var result:TextField = new TextField();
			result.x = x;
			result.y = y;
			result.width = width;
			result.height = height;
			result.multiline = true
			result.wordWrap = true;		
			return result;
		}
		
		private function removeListener(){
			for(var i=0;i<total_arr.length;i++){
				total_arr[i].mov.buttonMode = false;
				total_arr[i].mov.removeEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
			}
		}
		private function addListener(){
			for(var i=0;i<total_arr.length;i++){
				total_arr[i].mov.buttonMode = true;
				total_arr[i].mov.addEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
			}
			
			for(var j=0;j<disableMc_arr.length;j++){
				disableMc_arr[j].removeEventListener(MouseEvent.CLICK,onHighLightClick_Handler)
				disableMc_arr[j].buttonMode = false;
			}
		}
		
		private function hasGameEnded() {		
		 
			if (total_arr.length == boardHitCnt) {				
				isGameComplete = true;
				playSndInstructions("complete");	
				createPulseNext_Btn();
				shouldKeepLooking = false;
				trace(" GGGGGGGGGG FIIIII //")
				ths.playBtn.visible = false;
				ths.restart_mc.visible = true;
				ths.restart_mc.addEventListener(MouseEvent.CLICK, onRestStart_Handler)
				ths.restart_mc.buttonMode = true;
				ths.next_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler)
				
			}else {
				isGameComplete = false;
				shouldKeepLooking = true;			
			}
			
		}
		
		
		
		
		private function createPulseNext_Btn():void 
		{
			containerMc.mouseChildren = false;
			containerMc.mouseEnabled = false;
			addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers);
			
		}
		
		private function createGlowEffectForButn_Handlers(e:Event):void 
		{			 
			btn_blinkCnt++
			if(btn_blinkCnt %2==0){		
				glow.alpha = 1;			
				glowCnt++;
				if (glowCnt == 5) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}			 
				ths.next_mc.filters = [glow];				
			} 
			if(btn_blinkCnt == 1000){
				btn_blinkCnt = 0;
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
				glow.alpha = 0;						 
				ths.next_mc.filters = [glow];				 
			}
		}
		
		private function createGlowEffectForPlayButn(e:Event):void 
		{
			 
			playbtn_blinkCnt++
			if(playbtn_blinkCnt %2==0){		
				glow.alpha = 1;			
				glowCnt++;
				if (glowCnt == 5) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}			 
				ths.playBtn.filters = [glow];	
			} 
			if(playbtn_blinkCnt == 500){
				playbtn_blinkCnt = 0;
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForPlayButn)				 
				glow.alpha = 0;						 
				ths.playBtn.filters = [glow];
			}
		}
		
		private function createGlowEffectForMonster(e:Event):void 
		{
			 
			monster_blinkCnt++
			if(monster_blinkCnt %2==0){		
				glow.alpha = 1;			
				glowCnt++;
				if (glowCnt == 5) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}			 
				monst_mc.filters = [glow];	 
			} 
			if(monster_blinkCnt == 200){
				monster_blinkCnt = 0;
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForMonster)				 
				glow.alpha = 0;						 
				monst_mc.filters = [glow];	 
				
			}
		}
		
			
		
		function getMinusOrPlus():int{
			var rand : Number = Math.random()*5;
			if (rand<1) return -1
			else return 1;
		}
		
	
		private function onNextArrow_Handler(e:MouseEvent):void {
			
			if (shouldKeepLooking && !nextBtn_bool) {
					if(ths.dummy_mc.visible){
					 ths.dummy_mc.visible = false;
					}		
					nextBtn_bool = true;
					onWordClick_Handler();		
					ths.next_mc.removeEventListener(MouseEvent.CLICK, onNextArrow_Handler)					 
			}else {	
				if(!isGameComplete){
					hasGameEnded();
				}
			}
			
			if (isRestartLevel == true) {
				//resetGame()
				ths.next_mc.removeEventListener(MouseEvent.CLICK, onNextArrow_Handler)
			//	GameNextLevel()
			}
			
			if (!shouldKeepLooking && !nextBtn_bool) {
				trace(" Call Next Activity")
				 SoundMixer.stopAll()
				 btn_blinkCnt = 0;
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)					 
				glow.alpha = 0;						 
				ths.next_mc.filters = [glow];
			}
			
		}
		
		
		
		private function GameLevelEnd():void {			
			containerMc.mouseChildren = false;
			containerMc.mouseEnabled = false;
			ths.dummy_mc.visible = true;
			ths.setChildIndex(ths.dummy_mc, ths.numChildren - 1);
		 
		}
		
		private function GameNextLevel():void 
		{
			trace("GameNextLevel ")
		}
		/////////
		
		private function onRestStart_Handler(e:MouseEvent):void {
			trace("onRestStart_Handler")
			ths.restart_mc.visible = false;
			ths.playBtn.visible = true;
			 
			 
			btn_blinkCnt = 0;
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
			glow.alpha = 0;						 
			ths.next_mc.filters = [glow];	
			
			counterTextField = numberBoardLoaderArr[0].hitCount_Tf;
			counterTextField.mouseEnabled = false;
			counterTextField.text = "0";
			
			while (containerMc.numChildren > 0) {
				containerMc.removeChildAt(0)
			}
			while (containerMc1.numChildren > 0) {
				containerMc1.removeChildAt(0)
			}
			 SoundMixer.stopAll()
			 
			isGameComplete = true;
			shouldKeepLooking = true;	
			nextBtn_bool = false;
			isGameComplete = false;
			push_arr = [];
			total_arr = [];
			disableMc_arr = [];
			clicked_arr = [];
			tween_arr = [];
			boardHitCnt = 0;
		
			
			createLettersField();
			setFormatValues(songText);			
			createRhymesPlaying();
			createTweeningText();
		}
	
	}
}



