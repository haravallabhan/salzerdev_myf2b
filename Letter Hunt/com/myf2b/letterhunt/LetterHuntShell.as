﻿package com.myf2b.letterhunt {
	

	/**
     * @company Footsteps 2 Brilliance
     * @author Hara J N
     */
 
    import com.myf2b.core.*;
    import com.myf2b.core.ScreenManager;
    import com.myf2b.core.Session;
    
    import flash.display.MovieClip;
   
	
 	public class LetterHuntShell extends MovieClip {
		public static var mainScreenProxy:LetterHunt;
		
		//public static var gameScreenProxy:GameSpace;
		public static var ths:*
		public static var stg:*
			
		public function LetterHuntShell(id:String = "1")  {
            Core.useStaging = true;			 
			ths = this;
			stg = stage;
			AppSettings.setSetting( "com.myf2b.core.Preferences", "prod_CDNs", ["http://cdn.myf2b.com", "http://cdn2.myf2b.com"] );
			AppSettings.setSetting( "com.myf2b.core.Preferences", "stage_CDNs", ["http://stage.cdn.myf2b.com", "http://stage.cdn2.myf2b.com"] );
            Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			ScreenManager.getInstance().stage = stage;
			ScreenManager.getInstance().pushModule("com.myf2b.letterhunt.LetterHunt", ["135"]);				
			mainScreenProxy = new LetterHunt(this,id)			 
			//gameScreenProxy = new GameSpace(this)			
        }
			
		public static function isThis():*{
			return ths
		}
		public static function isStage():*{
			return stg
		}
		
	}
	
}
