package com.myf2b.letterhunt.manager
{
	import com.myf2b.core.CoreSound;
	
	import flash.events.Event;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	public class SoundManager
	{
		private static var _soundsArr:Array;
		private static var _channel:SoundChannel;
		private static var _timeoutId:uint;
		private static var _timeoutsArr:Array;
		private static var _onComplete:Function;
		private static var _soundCounter:uint = 0;
		private static var _currentKey:String;
		
		public static function add( arr:Array, onComplete:Function = null ):void{
			_onComplete = onComplete;
			if (!_timeoutsArr){
				_timeoutsArr = new Array();
			}
			if (!_soundsArr){
				_soundsArr = new Array();
			}
			for ( var i:uint = 0; i < arr.length; i++ ) {
				_soundsArr[ arr[i].key ] = new CoreSound( new URLRequest( arr[i].url ) );
				if ( i == arr.length - 1 ){
					_soundsArr[ arr[i].key ].addEventListener( Event.COMPLETE, onComplete);
				}
			}
		}
		
		public static function play(key:String, delay:Number = 0):void{
			if (delay == 0){
				//works4me
				//trace(_channel);
				if(_channel != null){
					_channel.stop()
				}
				_channel = _soundsArr[key].play();
			}
			else{
				_timeoutsArr.push( setTimeout( startPlay, delay, key) );
			}
		}
		
		private static function startPlay(key:String):void{
			_channel = _soundsArr[key].play();
		}
		
		public static function reset():void{
			for (var i:uint = 0; i < _timeoutsArr.length; i++) {
				clearTimeout( _timeoutsArr[i] );
			}
			_timeoutsArr = new Array();
			_channel.stop();
			SoundMixer.stopAll();
		}
		
		public static function playSequence(key:String):void {
			_soundCounter = 0;
			_currentKey = key;
			if ( _soundsArr[ _currentKey + _soundCounter ] ) {
				_channel = _soundsArr[ _currentKey + _soundCounter ].play();
				_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete );
			}
		}
		
		public static function onSoundComplete(e:Event):void {
			_soundCounter++;
			if ( _soundsArr[ _currentKey + _soundCounter ] ) {
				_channel = _soundsArr[ _currentKey + _soundCounter ].play();
			}
			else{
				_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete );
			}	
		}
	}
}