﻿package com.myf2b.letterhunt
{
	
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Session;
	import com.myf2b.letterhunt.manager.ReporterManager;
	import com.myf2b.letterhunt.manager.SoundManager;
	import com.myf2b.letterhunt.manager.tween.Ease;
	import com.myf2b.letterhunt.manager.tween.TweenManager;
	import com.myf2b.ui.*;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.filters.GlowFilter;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import fl.controls.UIScrollBar;
	
	
	public class GameScreen extends MovieClip {
		private var speed:Number=1;
		private var groupNode;
		private var assetNode;
		private var _letterToDeal:String;
		private var level:Number=0;
		private var presetsXML:XML = LetterHunt.presetsXML;
		public var monsterClip:MovieClip;
		private var USERPOPUP_TITLE = ""
		private var USERPOPUP_INFO = ""
		private var USERPOPUP_PARENT_INFO = ""
		private var IS_A_TEACHER:Boolean = false;
		private var USERPOPUP_SUBTITLE = ""
		private var _sync1 : TextFieldSoundSync;
		private var _sync2 : TextFieldSoundSync;
		private var _sync3 : TextFieldSoundSync;
		var sb:UIScrollBar = new UIScrollBar();
		var soundsArr:Array;
		private var ths:*;
		private var stg:*;
	
		private var userPopUp:userPopup;
		private static var xmlList:XMLList;
		
		
		public function GameScreen() {			
			ths = LetterHuntShell.isThis();
			stg = LetterHuntShell.isStage();
			_letterToDeal = LetterHunt.LETTER_TO_DEAL;
			init();
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			xmlList = presetsXML.deal.(@letter == _letterToDeal)	
			soundsArr = [ { key:"letterSound", url: xmlList.letterSound },
									{key:"instructionAudio0", url: xmlList.letterSound },
									{ key:"instructionAudio1", url: presetsXML.variables.additionalInstructionSound}];	
			SoundManager.add(soundsArr, destroy);

			
		}
		
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
		}
		
		private function init():void {
			//trace("init ")
			
			ths.topBar.cacheAsBitmap = true;			
			ths.topBar.instructionTxt.text = presetsXML.variables.instructionTxt;
			_sync1 = Ui.EnableTextSync(ths.topBar.instructionTxt, presetsXML);
			ths.topBar.dealLetter_mc._txt.text = _letterToDeal.toUpperCase() + _letterToDeal;
			//_sync2 = Ui.EnableTextSync(ths.topBar.dealLetter_mc._txt, presetsXML);
			ths.topBar.additionalInstruction.text=  presetsXML.variables.additionalInstruction;
			_sync3 = Ui.EnableTextSync(ths.topBar.additionalInstruction, presetsXML);			
			
			ths.topBar.soundBtn.buttonMode= ths.topBar.backToMap.buttonMode=true;
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);	
			ths.topBar.dealLetter_mc._txt.mouseEnabled = false;			
			
			ths.topBar.dealLetter_mc.buttonMode = true;
			ths.topBar.dealLetter_mc.addEventListener(MouseEvent.CLICK, playLetterSound);
			
			//User PopUp Tearcher or Parent//
			if(Session.getInstance().role == "Teacher"){
				USERPOPUP_TITLE = presetsXML.variables.popupForTeacherTitle 
				USERPOPUP_INFO =  presetsXML.variables.popupInfoForTeacher;
				IS_A_TEACHER = true;
				
			}else if(Session.getInstance().role == "Parent"){
				IS_A_TEACHER = false;
			}
			
			//USERPOPUP_SUBTITLE = presetsXML.variables.popupSubTitle;
			//trace("USERPOPUP_SUBTITLE="+USERPOPUP_SUBTITLE)
			
			//User PopUp Tearcher or Parent//
		 
			ths.topBar.userIcon.buttonMode=true;
			ths.topBar.soundBtn.buttonMode=true;
			ths.topBar.langBtn.buttonMode=true;
			
			
			if(IS_A_TEACHER){
				ths.topBar.userIcon.teacherIcon.visible = true;
				ths.topBar.userIcon.parentIcon.visible = false;
			}else{
				ths.topBar.userIcon.teacherIcon.visible = false;
				ths.topBar.userIcon.parentIcon.visible = true;
			}
			
			if(Session.getInstance().language == "en"){
				////trace("tpbar= "+ths.topBar.langBtn.en)
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;
			}else if(Session.getInstance().language == "esp"){
				ths.topBar.langBtn.en.visible=false;
				ths.topBar.langBtn.esp.visible=true;				
			}
			
			ths.topBar.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			ths.topBar.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);
			// //Popup setting Language
		}
		private function changeLang(e : MouseEvent ):void {
			if(ths.topBar.langBtn.en.visible){
				ths.topBar.langBtn.en.visible=false;			
				ths.topBar.langBtn.esp.visible=true;
			}else{
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;				
			}
			
		}
		
		private function showInfoForUser(e : MouseEvent ):void {
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
				userPopUp = new userPopup();
				ths.addChild(userPopUp)
				
				userPopUp.name = "userPopUp"
				userPopUp.userPopupClose.addEventListener(MouseEvent.CLICK, closePopup);
				userPopUp.userPopupTitle.text = USERPOPUP_TITLE;//Config.USERPOPUP_PARENT
				userPopUp.userPopupInfo.text = USERPOPUP_INFO;		
				assignScrollBar(userPopUp.userPopupInfo)
			if(IS_A_TEACHER){
				userPopUp.popupDivider.visible=true;
				userPopUp.infoForParentTitle.visible = true;
				userPopUp.infoForParent.visible = true;
				userPopUp.infoForParentTitle.text = USERPOPUP_SUBTITLE;
				userPopUp.infoForParent.text = USERPOPUP_PARENT_INFO;
				
			}else{
				userPopUp.popupDivider.visible=false;
				userPopUp.infoForParentTitle.visible = false;
				userPopUp.infoForParent.visible = false;				
			}
			
			show();
		};
		
		private function onSpeaker(e:MouseEvent):void {
			ReporterManager.interaction( { action: "click", actionObject: "gameInfoSpeaker" });
			//SoundManager.play("gameInfo");
			
		}
		
		
		public function nextLevel():void {
			level++;
		}
		
		
		private function onObjectTimerComplete(e:TimerEvent):void {
		}
		

		
		private function onObjectTimer(e:TimerEvent):void {
		}
		
		private function formatTime(val):String {
			return null;
		}
		
		public function autoPlayInstruction(e:Event):void {
			e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			_sync1.speakAndDo(playLetterAudio);
		}

		private function soundClicked(e:MouseEvent):void {
			e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			ReporterManager.interaction( { action: "soundclick", actionObject: "letterHuntSpeaker" });
			_sync1.speakAndDo(playLetterAudio);
		}
		
		private function playLetterSound(e : Event ):void {
			ReporterManager.interaction( { action: "click", actionObject: "playLetterSound" });
			SoundManager.play("letterSound"); 
		};
		
		private function playLetterAudio(e : Event):void {
			
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  xmlList.letterSound ) );				
			ch=_sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,playAdditionalInstructionSound);
				//_sync2.speakAndDo(playAdditionalInstructionSound);
		};		
		
		private function playAdditionalInstructionSound(e : Event ):void {
			ReporterManager.interaction( { action: "click", actionObject: "playAdditionalInstructionSound" });
			//SoundManager.playSequence("instructionAudio"); 
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);
			_sync3.speakAndDo(triggerFunction);
			
		};
		
		private function triggerFunction(e : Event):void {
			var glow:GlowFilter = new GlowFilter();
			glow.alpha = 0;
			LetterHunt.monst_mc.filters = [glow];
		}
		
		private function playSoundInstructions(url:String):void {
		
			ths.topBar.soundBtn.removeEventListener(MouseEvent.CLICK,soundClicked);
			var sound:CoreSound=new CoreSound(new URLRequest(url));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {

				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {
					//playLetterSound(e);
					playLetterAudio(e);
					//playSoundInstructions(presetsXML.variables.instructionSound);
					/*LetterHunt.containerMc.mouseChildren = true;
					LetterHunt.containerMc.mouseEnabled = true;
					LetterHunt.LipLoaderArr[0].mouseChildren = true;
					LetterHunt.LipLoaderArr[0].mouseEnabled = true;
					LetterHunt.monst_mc.mouseChildren = true;
					LetterHunt.monst_mc.mouseEnabled = true;*/
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);
				}
				
			}
		}
		
		public function show():void {
			LetterHunt.LipLoaderArr[0].visible = false;
			TweenManager.tween(userPopUp, 0.5, { alpha:1, scaleX:1, scaleY:1 },{ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		public function hide():void{
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
			TweenManager.tween(userPopUp, 0.5, {alpha:0, scaleX:0, scaleY:0}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
			
		}
		
		private function closePopup(e:MouseEvent):void {	
			LetterHunt.LipLoaderArr[0].visible = true;
			hide();				
		}
		function assignScrollBar(tf:TextField):void {
			sb = new UIScrollBar();
			sb.move(tf.x + tf.width, tf.y);
			sb.setSize(sb.width, tf.height);			
			
			sb.scrollTarget = tf;	
			if (tf.text.length>=420) {
				sb.visible = true;
			}else {
				sb.visible = false;
			}
			
			userPopUp.addChild(sb);            
		}
		
		
	}
	
}

