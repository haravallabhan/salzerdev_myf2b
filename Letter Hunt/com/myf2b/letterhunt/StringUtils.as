﻿package  com.myf2b.letterhunt {

    import flash.text.*;

    public class StringUtils {

        public function StringUtils() {}

        public static function highlight(block:TextField, search:String, appliedFormat:TextFormat) {
		
            var positions:Array = getPositions(block.text, search);
			 
            var len:uint = positions.length;
			
            for(var i:int = 0; i<len; i++){
				 
                block.setTextFormat(appliedFormat, positions[i].posStart, positions[i].posEnd);
            }
        }

        public static function getPositions(original:String, search:String):Array {

            var positions:Array = [];
            var startPosition:Number;
            var endPosition:Number;

            while (startPosition != -1) {
                startPosition = original.indexOf(search, endPosition);
                endPosition = startPosition + search.length;
                if(startPosition > -1) positions.push({posStart:startPosition, posEnd:endPosition});
            }

            return positions;
        }
    }
}