﻿package com.avr
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.SampleDataEvent;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
	import flash.media.Microphone;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	//import flashfx.sfx.recorder.SoundRecorderEvent;
	
	/**
	* Dispatched when recording starts.
	*/
	[Event(name = "enterRecordingState", type = "flashfx.sfx.recorder.SoundRecorderEvent")]
	
	/**
	* Dispatched when user denied mic access.
	*/
	[Event(name = "deny", type = "flashfx.sfx.recorder.SoundRecorderEvent")]
	
	/**
	* Dispatched when recording stops.
	*/
	[Event(name = "stop", type = "flashfx.sfx.recorder.SoundRecorderEvent")]
	
	/**
	 * Sound recording tool.
	 * @author Luke Zielinski
	 */
	public class SoundRecorder extends EventDispatcher
	{
		/**
		 * Maximum record duration in seconds.
		 * When record time exceed that value, recording stops immediately.
		 */
		private var recordLimit:int = 5;
		
		static private var _instance:SoundRecorder;
		private var _data:ByteArray;
		private var _mic:Microphone;
		public var _recording:Boolean = false;
		private var _power:Number = 0;
		private var _status:String;
		
		private var _recordLimitTimer:Timer;
		
		public function SoundRecorder(limit) 
		{
			recordLimit = limit;
			if (Microphone.names.length == 0) {
				return;
			}
			
			_mic = Microphone.getMicrophone();
			_mic.setSilenceLevel(0);
			_mic.gain = 65;
			_mic.rate = 44;
			_mic.addEventListener(StatusEvent.STATUS, statusHandler);
			
			_recordLimitTimer = new Timer(recordLimit * 1000, 1);
			_recordLimitTimer.addEventListener(TimerEvent.TIMER_COMPLETE, stopRecording);
		}
		
		private function statusHandler(e:StatusEvent):void 
		{
			if (e.code == "Microphone.Muted") {
				dispatchEvent(new Event(SoundRecorderEvent.DENY));
			}
			
			if (e.code == "Microphone.Unmuted") {
				dispatchEvent(new Event(SoundRecorderEvent.ENTER_RECORDING_STATE));
				_recordLimitTimer.reset();
				_recordLimitTimer.start();
			}
		}
		
		private function micSampleDataHandler(e:SampleDataEvent):void 
		{
			var maxAmp:Number = 0;
			
			while(e.data.bytesAvailable)
			{
				var sample:Number = e.data.readFloat();
				_data.writeFloat(sample);
				if (sample > maxAmp) maxAmp = sample;
			}
			
			_power = maxAmp;
		}
		
		/**
		 * Get recorder instance.
		 */
		//static public  function get instance():SoundRecorder {
			//if (!_instance) _instance = new SoundRecorder(new SoundRecorderSingletonEnforcer());
			//return _instance;
		//}
		
		/**
		 * Recorded data (mono 44kHZ).
		 */
		public function get data():ByteArray { return _data; }
		
		/**
		 * Microphone of recorder.
		 */
		public function get mic():Microphone { return _mic; }
		
		/**
		 * Current sound power.
		 */
		public function get power():Number { return _power; }
		
		/**
		 * Start recording.
		 * If user deny mic access, <code>stopRecording</code> is being executed.
		 */
		public function startRecording():void {
			if (_status == "Microphone.Muted") return;
			if (!_mic.muted) {
				dispatchEvent(new Event(SoundRecorderEvent.ENTER_RECORDING_STATE));
				_recordLimitTimer.reset();
				_recordLimitTimer.start();
			}
			
			
			if (_recording) {
				stopRecording();
			} else {
				_recording = true;
				if (_data) _data.clear();
				_data = new ByteArray();
				
				_mic.addEventListener(SampleDataEvent.SAMPLE_DATA, micSampleDataHandler);
			}
		}
		
		/**
		 * Stop recordnig.
		 */
		public function stopRecording(e:Event = null):void {
			_recordLimitTimer.reset();
			
			_mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, micSampleDataHandler);
			_recording = false;
			_data.position = 0;
			
			dispatchEvent(new Event(SoundRecorderEvent.STOP));
		}
	}
	
}

class SoundRecorderSingletonEnforcer { };

