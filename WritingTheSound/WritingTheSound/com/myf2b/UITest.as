﻿package com.myf2b.uitest
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import com.myf2b.core.*;
	import com.myf2b.ui.*;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.Font;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Adam Jurkiewicz
	 */
	public class UiTest extends MovieClip
	{
		private var _sync : TextFieldSoundSync;
		
		public function UiTest() 
		{
			Font.registerFont( ArialNormal );
			_loadTestXML();
		}
		
		private function _loadTestXML():void 
		{
			var loader : CoreURLLoader = new CoreURLLoader();
			loader.addEventListener( Event.COMPLETE, _xmlLoaded );
			loader.load ( new URLRequest( "classy.xml"));
		}
		
		private function _xmlLoaded(e:Event):void 
		{
			var xml : XML = new XML(e.target.data);
			
			play_mc.addEventListener( MouseEvent.CLICK, _playSync );
			textSync_txt.text = xml.config.splash.infoText;
			
			_sync = Ui.EnableTextSync(textSync_txt, xml);
			
		}
		
		private function _playSync(e:MouseEvent):void 
		{
			_sync.speak();
		}
		
	}

}