﻿package com.myf2b.writingthesound {

	import com.avr.XmlLoader;
	import com.greensock.TweenLite;
	import com.greensock.text.SplitTextField;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.writingthesound.GameScreen
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.printing.PrintJob;
	import flash.printing.PrintJobOptions;

	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.system.Security;

	import flash.printing.*;

	import flash.geom.ColorTransform;

	/**
	 *
	 * @author Prabhakar D
	 *
	 */
	public class WritingTheSound extends MovieClip {

		//public static var mainScreenProxy:LetterToSoundMatch;
		//
		public static const BACK_TO_MAP: String = "back_to_map";
		public static var LETTER_TO_DEAL: String = "f";
		public static var _contentKey: String = "1";
		private var _gameXMLURL: String;
		public static var moduleSessionID: String;
		private var str: String //= //"Mangos taste good.\n\tm m m\n\tm m m\n\tm m m\nMangos taste good.\n\tm m m\nLet's eat some now! M."//"Let's eat some now! M";
		private var letterOrNumber_str: String;

		public static var monsterMC: MovieClip;
		private var tweenTxt_mc: MovieClip;
		public static var containerMc: MovieClip;

		public var loader: Loader = new Loader();
		public static var liploader: Loader = new Loader();

		public static var monst_mc: MovieClip

		private var xmlLoader: XmlLoader;
		public var xml: XML;
		public static var presetsXML: XML;
		private var _letterXML: XMLList;
		public var _Letter_Xml: XMLList
		private var xmlList: XMLList;
		var mcExt: MovieClip;
		var ldr: Loader = new Loader();
		var urlStr: String = "";
		private var _shootCount: Number = 1;
		private var attemptCount: Number = 0;
		private var consAttempt: Number = -1;
		private var totalLen: Number = 3
		private var _isCorrectArr = []
		private var resetLen: Number = 0
		private var isAlphabetSnd: String = "";
		public static var isWriteOn_Off = ""

		public static var monsterLoaderArr = [];
		public static var LipLoaderArr = [];

		//		
		private var ths: * ;
		private var stg: * ;

		private var levels: level

		//For FlyBell
		private var isRoundInfor_str: String;

		public var myColor: ColorTransform = new ColorTransform();
		
		public static var LETTER_DEAL: String;
		/**
		 *
		 * @param contentKey
		 *
		 */
		public static var gameScreenProxy: GameScreen;


		private var push_arr = []
		var theParent: Object;
		public function WritingTheSound(aThs, contentKey: String = "1") {


			//Security.allowDomain("*");
			//trace(" SoundToLetterMatch ")
			Security.allowDomain("http://stage2.myf2b.com");
			ths = WritingTheSoundShell.isThis();
			stg = WritingTheSoundShell.isStage();
			//	ths = aThs	

			//Fly Function-06-08-2014

			//createRoundInformation("RoundStarted");



			addEventListener(Event.ADDED, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			_contentKey = contentKey;
			ths.next_mc.visible = false;
			ths.hide_mc.visible = false;
			//moduleSessionID = Reporter.getNewModuleSessionID();//To DB

		}

		/**
		 *
		 * @param e
		 *
		 */
		private function destroy(e: Event): void {
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			//trace("Listen and Record destroy");
			gameScreenProxy = null;
			//mainScreenProxy = null;
			//gameSpaceProxy = null;
			_contentKey = null;
			//SoundMixer.stopAll();
		}

		/**
		 *
		 * @param e
		 *
		 */
		private function init(e: Event): void {
			removeEventListener(Event.ADDED, init);
			stop();
			visible = false;
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else {
				//_gameXMLURL = Core.filesURL + "/listen_and_record/" + _contentKey + "/game.xml";
			}

			//Gobal game.xml
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/8/xml_feed", presetsXMLLoaded);

			//Local XML
			//xmlLoader = new XmlLoader("game.xml", presetsXMLLoaded);
			
			ths.addEventListener("letterID", isValidationLetter_Handler)
			
			ths.addEventListener("IS_VALIDATION", isValidation_Handler)
			ths.addEventListener("IS_RESET_HANDLER", is_Reset_Handler);
			ths.addEventListener("IS_NEXTLEVEL", is_Next_Level);
			ths.monster_mc.addEventListener(MouseEvent.CLICK, onClick_GreenMonsterHandler)
			ths.monster_mc.buttonMode = true;
			ths.restart_btn.addEventListener(MouseEvent.CLICK, restartBtn_Handler)
			ths.restart_btn.buttonMode = true;
		}

		public function presetsXMLLoaded(): void {
			trace("presetsXMLLoaded")
			//presetsXML = XML(e.target.data);
			presetsXML = XML(xmlLoader.data);
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL)

			isWriteOn_Off = _letterXML.writeHandOnOff
			trace("isWriteOn_Off=" + presetsXML.variables.animInstructionTxt);

			ths.startit_2(_letterXML.letterAnimation, presetsXML.variables.animInstructionTxt);

			loadMonsterGraphic();

			//The startit() function comes from WritingTheSoundShell class:
			//_letterXML.letterimage call's the tag name from XML:

			ths.addEventListener("IS_MovieOver", IS_MovieEnds);
		}
		
		
		
		//To Find The Letter:22-8-2014
		private function isValidationLetter_Handler(e: Event): void 
		{
			
			if (ths.isFindLetter == "letterID") {
				letterFn()
			}
			
		}
		
		public function letterFn()
		{
			//return LETTER_DEAL;
			
			LETTER_DEAL = LETTER_TO_DEAL
			
		 
			 
		}
		
		//End Code For Find The Letter:22-8-2014
		
		
		
		
		
		private function loadMonsterGraphic(): void {

			trace("_letterXML.monsterGraphic=" + _letterXML.monsterGraphic);
			//loader.load(new URLRequest("http://stage2.myf2b.com/uploads/file_upload/attachment/130/SndToLetterMatchMonster.swf"));
			loader.load(new URLRequest(_letterXML.monsterGraphic));
			loader.cacheAsBitmap = true;
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete(0))
		}

		private function loadComplete(i): Function {

			return function (e: Event): void {
				if (monsterLoaderArr == null) {
					monsterLoaderArr = new Array()
				}
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				monsterLoaderArr[0] = new MovieClip();
				monsterLoaderArr[0] = (e.target as LoaderInfo).content;

				/*monsterLoaderArr[0].x = _letterXML.monsterLocationX;
				monsterLoaderArr[0].y = _letterXML.monsterLocationY;
				ths.addChild(monsterLoaderArr[0])*/

				ths.addChild(monsterLoaderArr[0])
				monsterLoaderArr[0].x = _letterXML.monsterLocationX;
				monsterLoaderArr[0].y = _letterXML.monsterLocationY;

				monsterLoaderArr[0].width = 180; //_letterXML.monsterWidth;
				monsterLoaderArr[0].height = 191 //_letterXML.monsterHight;

				monsterLoaderArr[0].buttonMode = true;
				//monsterLoaderArr[0].gotoAndStop(2)				
				//monsterLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonster_Handler)

				//monsterMC = SoundToLetterMatch.monsterLoaderArr[0].getChildAt(0);				 

				//ths.monMc.addEventListener(MouseEvent.CLICK, onLetterBoard_Handler)
				ths.buttonMode = true;

				loadLipGraphic();
			}
		}

		private function loadLipGraphic(): void {

			liploader.load(new URLRequest(_letterXML.lipGraphic))
			//liploader.load(new URLRequest("http://salzer-ror.cloudapp.net/uploads/file_upload/attachment/9/Lip.swf"));
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, lipLoader_loadComplete);
			liploader.cacheAsBitmap = true;

			//liploader.addEventListener(MouseEvent.CLICK, onLipClick_Handler)
		}

		private function lipLoader_loadComplete(e: Event): void {

			if (LipLoaderArr == null) {
				LipLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);

			LipLoaderArr[0] = new MovieClip();
			LipLoaderArr[0] = (e.target as LoaderInfo).content
			LipLoaderArr[0].x = _letterXML.lipLocationX;
			LipLoaderArr[0].y = _letterXML.lipLocationY;

			ths.addChild(LipLoaderArr[0])
			//LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterLip_Handler);
			LipLoaderArr[0].buttonMode = true;
			LipLoaderArr[0].visible = true;


			LipLoaderArr[0].scaleX = .7;
			LipLoaderArr[0].scaleY = .7;
		}

		public function IS_MovieEnds(e: Event): void {

			//ths.startit(_letterXML.letterimage);
			ths.startit("E:/a-z-footsteps2brilliance/hand_taken_off/a-z/DrawSmall_Final_m/drawing_small_m/f.swf");
			gameScreenProxy = new GameScreen();
			createRoundInformation("StartBell");

			startFunction();

		}
		private function startFunction(): void {
			levels = new level()
			ths.levelMc.addChild(levels);
			levels.gotoAndStop(1)
		}

		private function onClick_GreenMonsterHandler(e: MouseEvent): void {
			ths.isCorrect_Wrong = "monster";
			playSoundInstructions()
		}

		private function isValidation_Handler(e: Event): void {
			trace("isValidation_Handler")
			if (ths.isCorrect_Wrong == "correct") {
				correctFn()
			} else if (ths.isCorrect_Wrong == "wrong") {
				wrongFn()
			}
			
			playSoundInstructions()
		}

		private function is_Reset_Handler(e: Event): void {
			ths.roundFly_mc.visible = true;
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1);
			ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, roundInformation);
		}
		public function correctFn(): void {
			//ths._levelStatus.gotoAndStop(_shootCount+1);	

			levels.gotoAndStop(_shootCount + 1)
			trace("inn=" + levels)
			attemptCount++;
			if (attemptCount == 1) {
				_isCorrectArr.push(1)

			} else if (attemptCount == 2) {
				_isCorrectArr.push(2)

			} else if (attemptCount >= 3) {
				_isCorrectArr.push(3)
			}
			trace("_isCorrectArr.length=" + _isCorrectArr.length)

			if (_isCorrectArr.length == "3") {
				ths.hide_mc.visible = true;
			}

			for (var j = 0; j < _isCorrectArr.length; j++) {
				if (_isCorrectArr[j] == 1) {

					levels["star" + (j + 1)].visible = true;
					levels["emptyIcon" + (j + 1)].visible = false
					levels["wrong" + (j + 1)].visible = false;
					consAttempt++
					trace("****1******")
					ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartBtn_Handler);
					
					reclear_1();
				}
				else if (_isCorrectArr[j] == 2) {
					levels["secondtry" + (j + 1)].visible = true
					levels["emptyIcon" + (j + 1)].visible = false
					levels["wrong" + (j + 1)].visible = false;
					trace("******2*****")
					consAttempt = -1
					
					reclear_2();
					ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartBtn_Handler);
					
				}
				else if (_isCorrectArr[j] == 3) {
					//if (levels["thirdtry" + (j + 1)] != null) {
					levels["thirdtry" + (j + 1)].visible = true;
					//}
					//if (levels["emptyIcon"+(j+1)] != null) {
					levels["emptyIcon" + (j + 1)].visible = false
					//}
					//if (levels["wrong"+(j+1)] != null) {
					levels["wrong" + (j + 1)].visible = false;
					//}
					consAttempt = -1
					trace("******3******")
					
					//reclear_3();
					ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartBtn_Handler);
					
				}
			}
		}
			
		public function reclear_1():void 
		{
			trace("***** Level-1*****");
			//ths.restart_btn11.addEventListener(MouseEvent.CLICK, restartClick_1);
			ths.restart_btn.addEventListener(MouseEvent.CLICK, restartClick_1);
			
		}
		
		public function restartClick_1(e:MouseEvent):void 
		{
			trace("$$$ Level-1$$$");
			trace("1");
			ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_1);
			while (ths.contentMc.numChildren > 0) {
				ths.contentMc.removeChildAt(0);
			}
		
			ths.startit(_letterXML.letterimage);
			
			
			levels.gotoAndStop(_shootCount + 1)
			//_isCorrectArr = null;
			//_isCorrectArr = new Array();
			
			//_isCorrectArr = new Array();
			ths.startit(_letterXML.letterimage);
			//startFunction()
			attemptCount = 0;
			ths.next_mc.visible = false;
			ths.fadeScreen.visible = false;
			ths.m_mc.visible = false;
			ths.printScreen_mc.visible = false;
			
			
			//attemptCount++;
		}
		public function reclear_2():void 
		{
			trace("***** Level-2*****");
			//ths.restart_btn11.addEventListener(MouseEvent.CLICK, restartClick_1);
			ths.restart_btn.addEventListener(MouseEvent.CLICK, restartClick_2);
		}
		public function restartClick_2(e:MouseEvent):void
		{
			trace("$$$ Level-2$$$");
			trace("2");
			ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_2);
			while (ths.contentMc.numChildren > 0) {
				ths.contentMc.removeChildAt(0);
			}
		
			ths.startit(_letterXML.letterimage);
			
			
			levels.gotoAndStop(_shootCount + 1);
			//levels.gotoAndStop(1);
			//levels["wrong" + _shootCount].visible = false;
			//levels["emptyIcon" + _shootCount].visible = true;
			//_isCorrectArr = null;
			//_isCorrectArr = new Array();
			
			//_isCorrectArr = new Array();
			ths.startit(_letterXML.letterimage);
			//startFunction()
			attemptCount = 0;
			ths.next_mc.visible = false;
			ths.fadeScreen.visible = false;
			ths.m_mc.visible = false;
			ths.printScreen_mc.visible = false;
				
			//attemptCount++;
		}
		public function reclear_3():void 
		{
			trace("***** Level-3*****");
			//ths.restart_btn11.addEventListener(MouseEvent.CLICK, restartClick_1);
			ths.restart_btn.addEventListener(MouseEvent.CLICK, restartClick_3);
		}
		
		public function restartClick_3(e:MouseEvent):void
		{
			trace("$$$ Level-3$$$");
			trace("3");
			ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_3);
			
			while (ths.contentMc.numChildren > 0) {
				ths.contentMc.removeChildAt(0);
			}
		
			ths.startit(_letterXML.letterimage);
			
			
			//levels.gotoAndStop(_shootCount + 1);
			//levels.gotoAndStop(1);
			//levels["wrong" + _shootCount].visible = false;
			//levels["emptyIcon" + _shootCount].visible = true;
			//_isCorrectArr = null;
			//_isCorrectArr = new Array();
			
			//_isCorrectArr = new Array();
			ths.startit(_letterXML.letterimage);
			//startFunction()
			attemptCount = 0;
			ths.next_mc.visible = false;
			ths.fadeScreen.visible = false;
			ths.m_mc.visible = false;
			ths.printScreen_mc.visible = false;
			
			
			//attemptCount++;
		}
		
		public function wrongFn(): void {
			/*	ths._levelStatus.gotoAndStop(_shootCount + 1);			
			ths._levelStatus["wrong"+_shootCount].visible = true;
			ths._levelStatus["star"+_shootCount].visible= false;
			ths._levelStatus["emptyIcon"+_shootCount].visible= false;
			ths._levelStatus["secondtry"+_shootCount].visible= false;
			ths._levelStatus["thirdtry"+_shootCount].visible= false;*/
			trace("wrongFn_shootCount =" + _shootCount);

			levels.gotoAndStop(_shootCount + 1);
			levels["wrong" + _shootCount].visible = true;
			levels["star" + _shootCount].visible = false;
			levels["emptyIcon" + _shootCount].visible = false;
			levels["secondtry" + _shootCount].visible = false;
			levels["thirdtry" + _shootCount].visible = false;

			attemptCount++;
			
		}
		
		public function playSoundInstructions(): void {

			// loader.removeEventListener(MouseEvent.CLICK,playSoundInstructions);
			var sound: CoreSound
			if (ths.isCorrect_Wrong == "correct") {
				sound = new CoreSound(new URLRequest(presetsXML.variables.successSounds.sound)); //	
			} else if (ths.isCorrect_Wrong == "wrong") {
				sound = new CoreSound(new URLRequest(presetsXML.variables.failSounds.sound));
			} else if (ths.isCorrect_Wrong == "monster") {
				sound = new CoreSound(new URLRequest(_letterXML.letterSound));
			}

			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);

			function onComplete(e: Event): void {
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch: SoundChannel = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);

				function onSoundComplete(e: Event): void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					if (ths.isCorrect_Wrong == "correct") {
						nextLevel()
						ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));

					}
				}
			}
		}

		private function nextLevel() {
			_shootCount++;

			attemptCount = 0;
			trace("_shootCount" + _shootCount);
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));
			trace("totalLen" + totalLen);
			if (_shootCount > totalLen) {
				resetLen = totalLen;
				trace("Game End")
				ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_1);
				ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_2);
				ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartClick_3);
				ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartBtn_Handler);
				ths.next_mc.visible = true;
				ths.next_mc.buttonMode = true;
				ths.next_mc.addEventListener(MouseEvent.CLICK, arrowNextBtn_Handler)
				ths.fadeScreen.alpha = 0;
				ths.fadeScreen.visible = true;
				ths.topBar.instructionTxt.text = presetsXML.variables.printInstructionTxt;
				
				trace("Fade Screen Added");
			} else {

				//createRoundInformation("StartBell");
				ths.addEventListener("IS_NEXTLEVEL", is_Next_Level);

				trace("Else_Function");
			}

			/*if(consAttempt == 2){
				consAttempt = -1;	
				resetLen = 2
				 trace("cons attempt")
				
				ths.next_mc.visible = true;
				ths.next_mc.addEventListener(MouseEvent.CLICK, arrowNextBtn_Handler)
				ths.next_mc.buttonMode = true;
				
				
				//Fade Screen Function done here:
				ths.fadeScreen.alpha = 0;
				ths.fadeScreen.visible = true;
				trace("Fade Screen Added");
			}*/

			ths.isCorrect_Wrong = "";
		}


		private function restartBtn_Handler(e: MouseEvent): void {
			trace("wrong= " + levels["wrong" + _shootCount])
			trace(" start =" + levels["star" + _shootCount])
			trace(" emptyIcon =" + levels["emptyIcon" + _shootCount])
			trace(" secondtry =" + levels["secondtry" + _shootCount])
			trace(" thirdtry=" + levels["thirdtry" + _shootCount])

			if (levels["wrong" + _shootCount] != null) {
				levels["wrong" + _shootCount].visible = false;
			}

			if (levels["star" + _shootCount] != null) {
				levels["star" + _shootCount].visible = false;
			}
			if (levels["emptyIcon" + _shootCount] != null) {
				levels["emptyIcon" + _shootCount].visible = false;
			}
			if (levels["secondtry" + _shootCount] != null) {
				levels["secondtry" + _shootCount].visible = false;
			}
			if (levels["thirdtry" + _shootCount] != null) {
				levels["thirdtry" + _shootCount].visible = false;
			}

			while (ths.contentMc.numChildren > 0) {
				ths.contentMc.removeChildAt(0);
			}
			while (ths.contentMc.numChildren > 0) {
				ths.levelMc.removeChildAt(0);
			}

			_shootCount = 1;
			_isCorrectArr = null;
			_isCorrectArr = new Array();
			ths.startit(_letterXML.letterimage);
			startFunction()
			attemptCount = 0;
			ths.next_mc.visible = false;
			ths.fadeScreen.visible = false;
			ths.m_mc.visible = false;
			ths.printScreen_mc.visible = false;

			ths.roundFly_mc.roundAnim_mc._txt.text = "0"

			ths.topBar.instructionTxt.text = presetsXML.variables.instructionTxt
			WritingTheSoundShell.isWritingInst = false

			//createRoundInformation("StartBell");

		}

		private function arrowNextBtn_Handler(e: MouseEvent): void 
		{
			trace("print job")
					
			ths.removeChild(monsterLoaderArr[0]);
			ths.removeChild(LipLoaderArr[0]);
			ths.printScreen_mc.visible = true;
			ths.printScreen_mc.print_btn.addEventListener(MouseEvent.CLICK, gotoPrintFun);
			ths.m_mc.visible = true;
		}

		private function gotoPrintFun(e: MouseEvent): void {

			var printJob: PrintJob = new PrintJob();
			var options: PrintJobOptions = new PrintJobOptions();
			options.printAsBitmap = true;
			trace("print called!");
			ths.printScreen_mc.visible = false;
			ths.m_mc.x = 511.95
			ths.m_mc.y = 382.45
			ths.m_mc.width = 1024
			ths.m_mc.height = 768

			if (printJob.start()) {
				var printSprite = new Sprite();

				var bitmapData: BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
				bitmapData.draw(stage);
				var screenShot: Bitmap = new Bitmap(bitmapData);

				printSprite.addChild(screenShot);

				//========== printjob bug fix – prevent blank pages: ==========
				printSprite.x = 2000; //keep it hidden to the side of the stage
				stage.addChild(printSprite); //add to stage – prevents blank pages
				//=============================================================
				//trace(“before printSprite width: ” + printSprite.width + ” printJob.pageWidth: ” + printJob.pageWidth);
				//scale it to fill the page (portrait orientation):
				var myScale: Number;
				myScale = Math.min(printJob.pageWidth / printSprite.width, printJob.pageHeight / printSprite.height);
				printSprite.scaleX = printSprite.scaleY = myScale;
				var printArea: Rectangle = new Rectangle(0, 0, printJob.pageWidth / myScale, printJob.pageHeight / myScale);

				//var printArea:Rectangle = new Rectangle(0, 0, 1024, 768);//trace(“after printSprite width: ” + printSprite.width + ” printJob.pageWidth: ” + printJob.pageWidth);
				printJob.addPage(printSprite, printArea, options);
				printJob.send();
				stage.removeChild(printSprite);
				printSprite = null;
			}
			ths.m_mc.visible = false;
		}

		private function createRoundInformation(aParam: String) {
			trace("createRoundInformation")
			trace("createRoundInformation_shootCount = " + _shootCount);
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));
			ths.roundFly_mc.gotoAndPlay(2)
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
			isRoundInfor_str = aParam
			ths.roundFly_mc.visible = true;
			ths.roundFly_mc.addFrameScript(10 - 1, roundFlyDepth);
			ths.roundFly_mc.addFrameScript(20 - 1, roundFlyAnimation);
		}

		private function roundFlyDepth() {
			trace("movieclip depth front")
			ths.roundFly_mc.addFrameScript(10 - 1, null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
		}

		private function roundFlyAnimation() {
			trace(" roundFlyAnimation ")
			ths.roundFly_mc.addFrameScript(20 - 1, null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
			ths.roundFly_mc.stop()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.gotoAndStop(1)
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2)
			ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, roundInformation);
		}

		public function roundInformation() {
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);
			ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, null);
			//ths.roundFly_mc.visible = false;
			ths.roundFly_mc.play()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.play()
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames - 1, roundFlyFreezeAnimation);
		}

		public function roundFlyFreezeAnimation() {
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames - 1, null);
			ths.roundFly_mc.gotoAndStop(1);
		}

		public function is_Next_Level(e: Event): void {

			createRoundInformation("StartBell");
		}


	}
}