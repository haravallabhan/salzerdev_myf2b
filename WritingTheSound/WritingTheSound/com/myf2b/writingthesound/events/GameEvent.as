package com.myf2b.slimegame.events
{
	public class GameEvent
	{
		public static const BACK_TO_SPLASH:String = "backToSplash";
		public static const BACK_TO_GAMES:String = "backToGames";
	}
}