package com.myf2b.slimegame.events
{
	import flash.events.Event;
	
	public class BookmarkerEvent extends Event
	{
		static public const SHOW_PROGRESS:String = "showProgress";
		
		public var data : * ;
		
		public function BookmarkerEvent(type:String, _data : * = null)
		{
			super(type);
			data = _data;
		}
		
		override public function clone():Event
		{
			return new BookmarkerEvent(type);
		}
	
	}
}