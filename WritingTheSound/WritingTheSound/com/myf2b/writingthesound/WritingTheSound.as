﻿package com.myf2b.writingthesound {

	import com.avr.XmlLoader;
	import com.greensock.TweenLite;
	import com.greensock.text.SplitTextField;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.writingthesound.GameScreen
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.printing.PrintJob;
	import flash.printing.PrintJobOptions;

	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.system.Security;
	import flash.filters.GlowFilter;

	import flash.printing.*;

	import flash.geom.ColorTransform;
	import com.myf2b.ui.*;
	import flash.filters.BitmapFilterQuality;

	/**
	 *
	 * @author Prabhakar D
	 *
	 */
	public class WritingTheSound extends MovieClip {

		//public static var mainScreenProxy:LetterToSoundMatch;
		//
		public static const BACK_TO_MAP: String = "back_to_map";
 
		public static var LETTER_TO_DEAL: String = "m"; 
		
		public static var UPPER_OR_LOWER: Boolean = true; 
		
		public static var _contentKey: String = "1";
		private var _gameXMLURL: String;
		public static var moduleSessionID: String;
		private var str: String 
		private var letterOrNumber_str: String;

		public static var monsterMC: MovieClip;
		private var tweenTxt_mc: MovieClip;
		public static var containerMc: MovieClip;

		public var loader: Loader = new Loader();
		public static var liploader: Loader = new Loader();
		
		public var Printoutloader:Loader = new Loader();

		public static var monst_mc: MovieClip

		private var xmlLoader: XmlLoader;
		public var xml: XML;
		public static var presetsXML: XML;
		private var _letterXML: XMLList;
		public var _Letter_Xml: XMLList
		private var xmlList: XMLList;
		var mcExt: MovieClip;
		var ldr: Loader = new Loader();
		var urlStr: String = "";
		public static var _shootCount: Number = 1;
		private var attemptCount: Number = 0;
		private var consAttempt: Number = -1;
		private var totalLen: Number = 3
		private var _isCorrectArr = []
		private var resetLen: Number = 0
		private var isAlphabetSnd: String = "";
		public static var isWriteOn_Off = ""

		public static var monsterLoaderArr = [];
		public static var LipLoaderArr = [];
		public var PrintoutloaderArr = [];
		public static var _sync2: TextFieldSoundSync;
		public static var _sync3: TextFieldSoundSync;
		public static var _sync4: TextFieldSoundSync;
		private var printMc:MovieClip = new MovieClip()

		var _roundCount: uint;
		private var isRestart_bool: Boolean = false;
		private var isLevelInfo: String = ""

		//		
		private var ths: * ;
		private var stg: * ;

		private var levels: level

		//For FlyBell
		private var isRoundInfor_str: String;

		public var myColor: ColorTransform = new ColorTransform();

		public static var LETTER_DEAL: String;
		/**
		 *
		 * @param contentKey
		 *
		 */
		public static var gameScreenProxy: GameScreen;

		private var push_arr = []

		var theParent: Object;
		//
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB;
		private var glow: GlowFilter = new GlowFilter();
		private var glow_str: String;
		private var btn_blinkCnt: Number = 0;
		public var introScreen:GameIntroScreen;
		
		public var temp:Object;
		public static var selectedObject:String = "";

		private var maxWidth:Number = 200;
		private var maxHeight:Number = 200;
		
		//

		public function WritingTheSound(aThs, contentKey: String = "1") {

			//Security.allowDomain("*");
			//////trace(" SoundToLetterMatch ")
			Security.allowDomain("http://stage2.myf2b.com");
			ths = WritingTheSoundShell.isThis();
			stg = WritingTheSoundShell.isStage();
			//	ths = aThs	

			//Fly Function-06-08-2014

			
		 
			addEventListener(Event.ADDED, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			_contentKey = contentKey;
			ths.next_mc.visible = false;
			ths.hide_mc.visible = false;
			
			addEventListener("BELL_SOUND_FINISHED", onBellSoundFinish_Handler)
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor //0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
			//moduleSessionID = Reporter.getNewModuleSessionID();//To DB
		}
		



		/**
		 *
		 * @param e
		 *
		 */
		private function destroy(e: Event): void {
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			//////trace("Listen and Record destroy");
			gameScreenProxy = null;
			//mainScreenProxy = null;
			//gameSpaceProxy = null;
			_contentKey = null;
			//SoundMixer.stopAll();
		}
		/**
		 *
		 * @param e
		 *
		 */
		public function init(e: Event): void {
			removeEventListener(Event.ADDED, init);
			stop();
			visible = false;
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else {
				//_gameXMLURL = Core.filesURL + "/listen_and_record/" + _contentKey + "/game.xml";
			}

			//Gobal game.xml
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/8/xml_feed", presetsXMLLoaded);
	
			//Local XML
			
			//xmlLoader = new XmlLoader("game.xml", presetsXMLLoaded);			
			//ths.addEventListener("letterID", isValidationLetter_Handler);

			ths.addEventListener("VALIDATION_HANDLER", validation_Handler)
			ths.addEventListener("RESET_HANDLER", reset_Handler);
			ths.addEventListener("NEXT_LEVEL_HANDLER", next_Level_Handler);
			ths.monster_mc.addEventListener(MouseEvent.CLICK, onClick_GreenMonsterHandler)
			ths.monster_mc.buttonMode = true;
			//ths.restart_btn.addEventListener(MouseEvent.CLICK, restartBtn_Handler);
			ths.restart_btn.buttonMode = true;
		}
		
		public function presetsXMLLoaded(): void {
			////trace("presetsXMLLoaded")
			//presetsXML = XML(e.target.data);
			presetsXML = XML(xmlLoader.data);
			introScreen = new GameIntroScreen();			
			introScreen.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", onIntroSrn_GoBtn_Handler)
			//////trace("isWriteOn_Off=" + presetsXML.variables.animInstructionTxt);
		}
		private function onIntroSrn_GoBtn_Handler(e:Event):void {
			 
			LETTER_TO_DEAL = introScreen.selectedObject
			trace("onIntroSrn_GoBtn_Handler= " +introScreen.selectedObject)
			
			_letterXML = presetsXML.deal.(@letter ==String(introScreen.selectedObject));			
			//_letterXML = presetsXML.deal.(@letter == "E")//String(introScreen.selectedObject));			
			isWriteOn_Off = _letterXML.writeHandOnOff;		 	
			 
			//trace("letter== " + isWriteOn_Off);			
			ths.startit_2(_letterXML.letterAnimation, presetsXML.variables.animInstructionTxt,introScreen.selectedObject);			
			gameScreenProxy = new GameScreen();			
			loadMonsterGraphic();

			//The startit() function comes from WritingTheSoundShell class:
			//_letterXML.letterimage call's the tag name from XML:

			//ths.addEventListener("IS_MovieOver", IS_MovieEnds);
			ths.addEventListener("MOVIE_END_HANDLER", movieEnd_Handler);
		}

		private function loadMonsterGraphic(): void {
			//loader.load(new URLRequest("http://stage2.myf2b.com/uploads/file_upload/attachment/130/SndToLetterMatchMonster.swf"));
			loader.load(new URLRequest(_letterXML.monsterGraphic));
			loader.cacheAsBitmap = true;
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete(0))
		}

		private function loadComplete(i): Function {

			return function (e: Event): void {
				if (monsterLoaderArr == null) {
					monsterLoaderArr = new Array()
				}
				var mc: MovieClip = new MovieClip()
				ths.addChild(mc)
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				monsterLoaderArr[0] = new MovieClip();
				monsterLoaderArr[0] = (e.target as LoaderInfo).content;

				/*monsterLoaderArr[0].x = _letterXML.monsterLocationX;
				  monsterLoaderArr[0].y = _letterXML.monsterLocationY;
				 ths.addChild(monsterLoaderArr[0])*/

				mc.addChild(monsterLoaderArr[0]);
				monsterLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterLip_Handler);
				monsterLoaderArr[0].x = 17;//_letterXML.monsterLocationX;
				monsterLoaderArr[0].y = 240;//_letterXML.monsterLocationY;

				var ratio:Number;//ratio
				ratio = monsterLoaderArr[0].height/monsterLoaderArr[0].width;//calculation ratio
				//trace(ratio)
				if (monsterLoaderArr[0].width>maxWidth) {

					monsterLoaderArr[0].width = maxWidth;
					monsterLoaderArr[0].height = Math.round(monsterLoaderArr[0].width*ratio);
				}

				if (monsterLoaderArr[0].height>maxHeight) {
					monsterLoaderArr[0].height = maxHeight;
					monsterLoaderArr[0].width = Math.round(monsterLoaderArr[0].height/ratio);
				}
				
				
				trace("wid= "+monsterLoaderArr[0].width  )				
				trace("hig= "+monsterLoaderArr[0].height  )
			
				//monsterLoaderArr[0].width = 180; //_letterXML.monsterWidth;
				//monsterLoaderArr[0].height = 191 //_letterXML.monsterHight;				

				monsterLoaderArr[0].buttonMode = true;
				mc.buttonMode = true;
				mc..addEventListener(MouseEvent.CLICK, onMonsterLip_Handler);
				//monsterLoaderArr[0].visible = true;
				//loadLipGraphic();
			}
		}

		private function loadLipGraphic(): void {
			liploader.load(new URLRequest(_letterXML.lipGraphic))
			//liploader.load(new URLRequest("http://stage2.myf2b.com/uploads/file_upload/attachment/841/WriteTheSound_Lip.swf"));
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, lipLoader_loadComplete);
			liploader.cacheAsBitmap = true;
		}

		private function onMonsterLip_Handler(e: MouseEvent): void {

			trace("Hit the target");

			var sound: CoreSound
			sound = new CoreSound(new URLRequest(_letterXML.letterSound));
			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			var ch: SoundChannel = new SoundChannel();
			ch = sound.play();

			function onComplete(e: Event): void {

				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch: SoundChannel = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);

				function onSoundComplete(e: Event): void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				}
			}
		}

		private function lipLoader_loadComplete(e: Event): void {
		
			if (LipLoaderArr == null) {
				LipLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);

			LipLoaderArr[0] = new MovieClip();
			LipLoaderArr[0] = (e.target as LoaderInfo).content
			LipLoaderArr[0].x = _letterXML.lipLocationX;
			LipLoaderArr[0].y = _letterXML.lipLocationY;

			ths.addChild(LipLoaderArr[0])
			LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterLip_Handler);
			LipLoaderArr[0].buttonMode = true;
			LipLoaderArr[0].visible = true;

			//LipLoaderArr[0].scaleX = .8;
			//LipLoaderArr[0].scaleY = .8;

			
		}

		public function movieEnd_Handler(e: Event): void {
			
			//ths.topBar.numChildren(hLightDeal_mc).visible = false;

		if(ths.topBar.getChildByName("Deal_mc")!=null){
			ths.topBar.removeChild(ths.topBar.getChildByName("Deal_mc"));
		   }
			
			ths.startit(_letterXML.letterimage);
			//ths.topBar.instruct_mc.visible = false;
			ths.topBar.animTextMc.visible = false;
			//ths.topBar.writeInstruct_mc.visible = false;
			trace("***_shootCount at next btn = " + _shootCount)
			
			//ths.startit("D:/footsteps2brilliance/WritingTheSound/WritingTheSound/WriteTheSoundCharacters/z.swf");			
			createRoundInformation("StartBell");
			levelInformation();
		}

		private function levelInformation(): void {
			levels = new level()
			ths.levelMc.addChild(levels);
			levels.gotoAndStop(1)
		}

		private function onClick_GreenMonsterHandler(e: MouseEvent): void {
			ths.isCorrect_Wrong = "monster";
			playSoundInstructions()
		}

		private function validation_Handler(e: Event): void {
			//trace("isValidation_Handler "+ths.isCorrect_Wrong)
			if (ths.isCorrect_Wrong == "corr") {
				correctFn()
			}
			if (ths.isCorrect_Wrong == "wrg") {
				wrongFn()
			}
			playSoundInstructions()
		}

		private function reset_Handler(e: Event): void {
			ths.roundFly_mc.visible = true;
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1);
			ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, roundInformation);
		}

		public function correctFn(): void {

			//ths._levelStatus.gotoAndStop(_shootCount+1);	
			////trace(" correctFn= "+_shootCount)

			levels.gotoAndStop(_shootCount + 1)

			attemptCount++;
			if (attemptCount == 1) {
				_isCorrectArr.push(1)

			} else if (attemptCount == 2) {
				_isCorrectArr.push(2)
			} else if (attemptCount >= 3) {
				_isCorrectArr.push(3)
			}
			//trace("_isCorrectArr=  " + _isCorrectArr)
			//trace("attemptCount=  "+attemptCount)			



			if (_isCorrectArr.length == "3") {
				ths.hide_mc.visible = true;
				ths.arrowHide_mc.visible = true;
			}

			for (var j = 0; j < _isCorrectArr.length; j++) {
				if (_isCorrectArr[j] == 1) {
					levels["emptyIcon" + (j + 1)].visible = false
					levels["wrong" + (j + 1)].visible = false;
					levels["emptyIcon" + (j + 1)].visible = false
					levels["star" + (j + 1)].visible = true;
					consAttempt++;
					ths.arrowHide_mc.visible = true;
					isLevelInfo = "star"
				} else if (_isCorrectArr[j] == 2) {
					levels["secondtry" + (j + 1)].visible = true
					levels["emptyIcon" + (j + 1)].visible = false
					levels["wrong" + (j + 1)].visible = false;
					isLevelInfo = "secondtry";
					consAttempt = -1
				} else if (_isCorrectArr[j] == 3) {
					if (levels["thirdtry" + (j + 1)] != null) {
						levels["thirdtry" + (j + 1)].visible = true;
					}
					if (levels["emptyIcon" + (j + 1)] != null) {
						levels["emptyIcon" + (j + 1)].visible = false
					}
					if (levels["wrong" + (j + 1)] != null) {
						levels["wrong" + (j + 1)].visible = false;
					}
					isLevelInfo = "thirdtry";

				}
			}

		}

		public function wrongFn(): void {

			levels.gotoAndStop(_shootCount + 1);
			levels["wrong" + _shootCount].visible = true;
			levels["star" + _shootCount].visible = false;
			levels["emptyIcon" + _shootCount].visible = false;
			levels["secondtry" + _shootCount].visible = false;
			levels["thirdtry" + _shootCount].visible = false;
			isLevelInfo = "wrong";
			attemptCount++;
			SoundMixer.stopAll()
		}

		public function playSoundInstructions(): void {

			// loader.removeEventListener(MouseEvent.CLICK,playSoundInstructions);
			var sound: CoreSound
			if (ths.isCorrect_Wrong == "corr") {
				sound = new CoreSound(new URLRequest(presetsXML.variables.successSounds.sound)); //	
			} else if (ths.isCorrect_Wrong == "wrg") {
				sound = new CoreSound(new URLRequest(presetsXML.variables.failSounds.sound));
			} else if (ths.isCorrect_Wrong == "monster") {
				sound = new CoreSound(new URLRequest(_letterXML.letterSound));
			} else if (ths.isCorrect_Wrong == "bellSound") {
				sound = new CoreSound(new URLRequest(presetsXML.variables.flybellAnimationSound));
			}

			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);

			function onComplete(e: Event): void {

				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch: SoundChannel = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);

				function onSoundComplete(e: Event): void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					if (ths.isCorrect_Wrong == "corr") {
						glow_str = "childNextBtn"
						WritingTheSoundShell.childReset_btn.visible = true;
						nextLevel()
						//ths.arrowHide_mc.visible = true;	

						//addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);

						ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));
					}
					if (ths.isCorrect_Wrong == "wrg") {
						trace(" WRONG_ATTEMPT_HANDLER")
						//	WritingTheSoundShell.childReset_btn.visible = true;
						WritingTheSoundShell.childReset_btn.visible = false;
					}

					if (ths.isCorrect_Wrong == "bellSound") {
						dispatchEvent(new Event("BELL_SOUND_FINISHED"))
					}
				}
			}
		}

		public function nextLevel() {

			ths.arrowHide_mc.visible = false;
			_roundCount = _shootCount;
			_shootCount++;
			attemptCount = 0;

			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));

			if (_shootCount > totalLen) {

				ths.arrowHide_mc.visible = true;
				resetLen = totalLen;
				WritingTheSoundShell.childReset_btn.visible = false;
				trace("Game End/")
				glow_str = "";
				glow_str = "nextbtn"
				//addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);
				ths.restart_btn.removeEventListener(MouseEvent.CLICK, restartBtn_Handler);

				ths.next_mc.visible = true;
				ths.next_mc.buttonMode = true;
				ths.next_mc.addEventListener(MouseEvent.CLICK, arrowNextBtn_Handler)
				ths.fadeScreen.alpha = 0;
				ths.fadeScreen.visible = true;
				//ths.topBar.instructionTxt.text = presetsXML.variables.printInstructionTxt;				
				//////trace("Fade Screen Added");

				//ths.topBar.instruct_mc.visible = false;
				ths.topBar.animTextMc.visible = false;
				//ths.topBar.writeInstruct_mc.visible = false;

			//	ths.topBar.printInstruct_mc.printInstructionTxt.text = presetsXML.variables.printInstructionTxt
			//	_sync3 _sync4 = Ui.EnableTextSync(ths.topBar.printInstruct_mc.printInstructionTxt, presetsXML);

			} else {

				//createRoundInformation("StartBell");
				ths.addEventListener("NEXT_LEVEL_HANDLER", next_Level_Handler);

				//////trace("Else_Function" + _shootCount);				
				if (_shootCount == 2) {
					//ths.topBar.instruct_mc.visible = false;
					ths.topBar.animTextMc.visible = false;
				//	ths.topBar.writeInstruct_mc.writingInstructionTxt.text = presetsXML.variables.wiritingInstructionTxt
					//_sync2 = Ui.EnableTextSync(ths.topBar.writeInstruct_mc.writingInstructionTxt, presetsXML);
					 
				}
			}		 
		}


		private function restartBtn_Handler(e: MouseEvent): void {		

			for (var p = 1; p <= 3; p++) {
				levels["emptyIcon" + p].visible = false
				levels["wrong" + p].visible = false;
				levels["emptyIcon" + p].visible = true;
				levels["star" + p].visible = false;
			}
			while (ths.contentMc.numChildren > 0) {
				ths.contentMc.removeChildAt(0);
			}
			while (ths.contentMc.numChildren > 0) {
				ths.levelMc.removeChildAt(0);
			}


			_shootCount = 1;
			_isCorrectArr = null;
			_isCorrectArr = new Array();
			//attemptCount = 0;
			WritingTheSoundShell.isWritingInst = false
			ths.next_mc.visible = false;
			ths.fadeScreen.visible = false;
			//ths.m_mc.visible = false;
			ths.printScreen_mc.visible = false;

			//////////////////start next level//////////////////
			trace("start next level");
			ths.startit(_letterXML.letterimage, 2);			

		}

		private function getResetLevelInfo(): void {
			//trace(" isLevelInfo at & "+isLevelInfo +" * "+_shootCount)	
		}

		private function arrowNextBtn_Handler(e: MouseEvent): void {
			//trace("print job")

			//ths.removeChild(monsterLoaderArr[0]);
			//ths.removeChild(LipLoaderArr[0]);
			//LipLoaderArr[0].visible = false;
			monsterLoaderArr[0].visible = false;

			ths.printScreen_mc.visible = true;
			ths.printScreen_mc.buttonMode = true;
			glow_str = "printbtn"
			ths.printScreen_mc.print_btn.addEventListener(MouseEvent.CLICK, gotoPrintFun);
			ths.printScreen_mc.next_btn.addEventListener(MouseEvent.CLICK, onPrintScreenNextBtn_Handler)
			//ths.m_mc.visible = true;
			
			Printoutloader.load(new URLRequest(_letterXML.letterPrintout));
			Printoutloader.contentLoaderInfo.addEventListener(Event.COMPLETE, letterPrintout_loadComplete);
			Printoutloader.cacheAsBitmap = true;
			
			//addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);			
			ths.printScreen_mc.printInstructionTxt.text = presetsXML.variables.printInstructionTxt

			_sync4 = Ui.EnableTextSync(ths.printScreen_mc.printInstructionTxt, presetsXML);
			
			_sync4.speak();
			ths.dummy_mc_2.gotoAndPlay(2)
			ths.dummy_mc_2.addFrameScript(ths.dummy_mc_2.totalFrames-1, callTextSync)

			_sync4 = Ui.EnableTextSync(ths.printScreen_mc.printInstructionTxt, presetsXML);						
	}
	
		private function letterPrintout_loadComplete(e:Event):void 
		{
			
			
				if (PrintoutloaderArr == null) {
					PrintoutloaderArr = new Array()
				}
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);

				PrintoutloaderArr[0] = new MovieClip();
				PrintoutloaderArr[0] = (e.target as LoaderInfo).content
				PrintoutloaderArr[0].x = _letterXML.PrintoutLocationX;
				PrintoutloaderArr[0].y = _letterXML.PrintoutLocationY;
				printMc = MovieClip(PrintoutloaderArr[0])
				printMc.width =  627;
				printMc.height =  451;
				ths.addChild(PrintoutloaderArr[0])
		
		}
		
		private function callTextSync():void 
		{
			trace("callTextSync _sync4")
			ths.dummy_mc_2.addFrameScript(ths.dummy_mc_2.totalFrames-1, null)
			//_sync.speak()
			_sync4.speakAndDo(endAudio)
		}
		private function endAudio(e) {
			trace(" Audio Ends _sync4 ")
			
			
		}

		private function onPrintScreenNextBtn_Handler(e: MouseEvent): void {
			trace("onPrintScreenNextBtn_Handler ")
		}

		private function gotoPrintFun(e: MouseEvent): void {
			var printJob: PrintJob = new PrintJob();
			var options: PrintJobOptions = new PrintJobOptions();
			options.printAsBitmap = true;
			////trace("print called!");
 
			printMc.x =  0;
			printMc.y =  0;
			printMc.width =  1024;
			printMc.height =  768
			
			/*ths.m_mc.x = 511.95
			ths.m_mc.y = 382.45
			ths.m_mc.width = 1024
			ths.m_mc.height = 768*/

			if (printJob.start()) {
				var printSprite = new Sprite();

				var bitmapData: BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight);
				bitmapData.draw(stage);
				var screenShot: Bitmap = new Bitmap(bitmapData);

				printSprite.addChild(screenShot);

				//========== printjob bug fix – prevent blank pages: ==========
				printSprite.x = 2000; //keep it hidden to the side of the stage
				stage.addChild(printSprite); //add to stage – prevents blank pages
				//=============================================================
				//////trace(“before printSprite width: ” + printSprite.width + ” printJob.pageWidth: ” + printJob.pageWidth);
				//scale it to fill the page (portrait orientation):



				var realW: Number = printMc.width//ths.m_mc.width;
				var realH: Number = printMc.height//ths.m_mc.height;
				var orgX: Number = printMc.x //ths.m_mc.x;
				var orgY: Number = printMc.y//ths.m_mc.y;
				
				options.printAsBitmap = true;
				var pageCount: Number = 0;
				var cscaleX: Number, cscaleY: Number;

				if (printJob.orientation.toLowerCase() != "landscape") {

					trace("landscape not =\t");				
					//ths.m_mc.x = ths.m_mc.width;					
					printMc.x = printMc.width					
					trace("xwid= "+printMc.width)
					cscaleX = printJob.pageWidth / realH;
					cscaleY = printJob.pageHeight / realW;
					trace("cscaleX= " + cscaleX)
					trace("cscaleY= " + cscaleY)
					
				} else {

					trace("landscape=\t");
					cscaleX = printJob.pageWidth / realW;
					cscaleY = printJob.pageHeight / realH;
				}



				var myScale: Number;
				myScale = Math.min(printJob.pageWidth / printSprite.width, printJob.pageHeight / printSprite.height);
				printSprite.scaleX = printSprite.scaleY = myScale;
				var printArea: Rectangle = new Rectangle(0, 0, printJob.pageWidth / myScale, printJob.pageHeight / myScale);

				//var printArea:Rectangle = new Rectangle(0, 0, 1024, 768);//////trace(“after printSprite width: ” + printSprite.width + ” printJob.pageWidth: ” + printJob.pageWidth);
				printJob.addPage(printSprite, printArea, options);
				printJob.send();


				//stage.removeChild(printSprite);
				//	printSprite = null;


				//monsterLoaderArr[0].visible = true;
				//LipLoaderArr[0].visible = true

				/*ths.m_mc.x = 522.55
				ths.m_mc.y = 487.65
				ths.m_mc.width = 627
				ths.m_mc.height = 451.75*/
				printMc.x =  200;
				printMc.y =  250
				printMc.width =  627;
				printMc.height =  451;
				trace(" print ../")
				
				//ths.printScreen_mc.visible = true;
			//	ths.m_mc.visible = true;
			printMc.visible = true;
				
			} else {
			trace(" print cancel.../")

				/*ths.m_mc.x = 522.55
				ths.m_mc.y = 487.65
				ths.m_mc.width = 627
				ths.m_mc.height = 451.75
				ths.m_mc.visible = true;*/
				
				printMc.x =  200//522.55;
				printMc.y =  250//487.65
				printMc.width =  627;
				printMc.height =  451;				
				printMc.visible = true;
			}

			//ths.m_mc.visible = true;
		}

		private function createRoundInformation(aParam: String) {
			////trace("createRoundInformation")
			////trace("createRoundInformation_shootCount = " + _shootCount);
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount));
			ths.roundFly_mc.gotoAndPlay(2)
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
			isRoundInfor_str = aParam
			ths.roundFly_mc.visible = true;
			ths.roundFly_mc.addFrameScript(5 - 1, roundFlyDepth);
			ths.roundFly_mc.addFrameScript(11 - 1, roundFlyAnimation);
		}

		private function roundFlyDepth() {
			////trace("movieclip depth front")
			ths.roundFly_mc.addFrameScript(5 - 1, null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
		}

		private function roundFlyAnimation() {
			////trace(" roundFlyAnimation ")
			ths.roundFly_mc.addFrameScript(11 - 1, null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren - 1)
			ths.roundFly_mc.stop()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.gotoAndStop(1)

			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2)
			ths.isCorrect_Wrong = "bellSound"
			playSoundInstructions()
			//	ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, roundInformation);
		}

		private function onBellSoundFinish_Handler(e: Event): void {
			//trace("onBellSoundFinish_Handler")
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);
			//ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, null);
			//ths.roundFly_mc.visible = false;
			ths.roundFly_mc.play()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.play()
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames - 1, roundFlyFreezeAnimation);

		}



		public function roundInformation() {
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);
			//ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames - 1, null);
			//ths.roundFly_mc.visible = false;
			ths.roundFly_mc.play()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.play()
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames - 1, roundFlyFreezeAnimation);
		}

		public function roundFlyFreezeAnimation() {
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames - 1, null);
			ths.roundFly_mc.gotoAndStop(1);
		}

		public function next_Level_Handler(e: Event): void {
			createRoundInformation("StartBell");
		}
		//
		private function createGlowEffectForBtn_Handlers(e): void {
			btn_blinkCnt++
			if (btn_blinkCnt % 5 == 0) {
				glow.alpha = 1;
				glowCnt++
				if (glowCnt == 3) {
					glowCnt = 0
					glow.alpha = 0;
				}
				//trace("glow_str *(* "+glow_str)
				if (glow_str == "printbtn") {
					ths.printScreen_mc.print_btn.filters = [glow];
					ths.printScreen_mc.next_btn.filters = [glow];

				}
				if (glow_str == "nextbtn") {
					ths.next_mc.filters = [glow];
					////trace(" Glooow")
				}
				if (glow_str == "childNextBtn") {
					WritingTheSoundShell.childReset_btn.filters = [glow];
					////trace(" Glooow")
				}
			}

			if (btn_blinkCnt == 0) {
				btn_blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);
				glow.alpha = 0;

				if (glow_str == "printbtn") {
					ths.printScreen_mc.print_btn.filters = [glow];
					ths.printScreen_mc.next_btn.filters = [glow];
				}
				if (glow_str == "nextbtn") {
					ths.next_mc.filters = [glow];
				}

				if (glow_str == "childNextBtn") {
					WritingTheSoundShell.childReset_btn.filters = [glow];
					////trace(" Glooow")
				}

			}
		}

	}
}