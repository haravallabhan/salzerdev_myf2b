﻿package com.myf2b.writingthesound {

	import com.myf2b.core.AssetManager;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Session;
	import com.myf2b.writingthesound.manager.tween.Ease;
	import com.myf2b.writingthesound.manager.tween.TweenManager;
	import flash.text.TextField;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import fl.controls.UIScrollBar;
	import com.myf2b.ui.*;
	 import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;

	public class GameScreen extends MovieClip {
		private var speed: Number = 1;
		private var groupNode;
		private var assetNode;
		private var _letterToDeal: String;
		private var level: Number = 0;
		private var presetsXML: XML = WritingTheSound.presetsXML;
		private var instructionText: String = presetsXML.variables.animInstructionTxt;
		public var monsterClip: MovieClip;
		private var USERPOPUP_TITLE = ""
		private var USERPOPUP_INFO = ""
		private var USERPOPUP_PARENT_INFO = ""
		private var IS_A_TEACHER: Boolean = false;
		private var USERPOPUP_SUBTITLE = ""

		private var ths: * ;
		private var stg: * ;

		private var userPopUp: userPopup;
		var sb: UIScrollBar = new UIScrollBar();

		private var _sync: TextFieldSoundSync;
		//	private var _sync1 : TextFieldSoundSync;
		
		private var _letterXML: XMLList;
		public var _Letter_Xml: XMLList
		private var xmlList: XMLList;


		public function GameScreen() {
			
			ths = WritingTheSoundShell.isThis();
			stg = WritingTheSoundShell.isStage();

			_letterToDeal = WritingTheSound.LETTER_TO_DEAL;
			trace(" _letterToDeal= "+_letterToDeal)
			

			init();
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			
			trace("GameScreen")
		}


		public function destroy(e: Event): void {
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			//trace("GameScreen destroy");
			//stopGame();

		}

		public function init(): void {
			//trace("init ")
			ths.topBar.cacheAsBitmap = true;
			var myPattern: RegExp = /#/g;
			instructionText = instructionText.replace(myPattern, _letterToDeal);
			//ths.topBar.instructionTxt.text = instructionText;			ths.topBar.animTextMc.visible = true;

			trace(" gAMESCREEN ****** " + ths.topBar.animTextMc.anim_instructionTxt.text);

			_sync = Ui.EnableTextSync(ths.topBar.animTextMc.anim_instructionTxt, presetsXML);

			//_sync1 = Ui.EnableTextSync(ths.topBar.instructionTxt, presetsXML);

			ths.topBar.soundBtn.buttonMode = ths.topBar.backToMap.buttonMode = true;
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
			
			//ths.dummy_mc.gotoAndPlay(2)
			//ths.dummy_mc.addFrameScript(ths.dummy_mc.totalFrames-1, callTextSync)
			
			
			//ths.topBar.animTextMc.dealLetter_mc._txt.text = _letterToDeal;
			
			ths.topBar.animTextMc.anim_instructionTxt.autoSize = TextFieldAutoSize.LEFT;
			ths.topBar.animTextMc.anim_instructionTxt.multiline = true;
			ths.topBar.animTextMc.anim_instructionTxt.wordWrap = true;
			//
			var mcX:int=(ths.topBar.animTextMc.anim_instructionTxt.x+ths.topBar.animTextMc.anim_instructionTxt.getLineMetrics(ths.topBar.animTextMc.anim_instructionTxt.numLines-1).width)//+(ths.topBar.animTextMc.width+20);
			var mcY:int = (ths.topBar.animTextMc.anim_instructionTxt.y + ths.topBar.animTextMc.anim_instructionTxt.textHeight)
			 //
			
			 trace("mcX = " + mcX);
			 trace("mcY = " + mcY);
			
			  var hLightDeal_mc = new dealLetter_mc();
			  
			   hLightDeal_mc._txt.text = _letterToDeal//txtStr;
			   hLightDeal_mc._txt.mouseEnabled = false;   
			   
			   hLightDeal_mc.name = "Deal_mc";
			  // hLightDeal_mc.x = mcX 
			  // hLightDeal_mc.y = mcY
			  
			   hLightDeal_mc.y = mcY 
			   hLightDeal_mc.x = mcX  + 225;
			   if (ths.topBar.animTextMc.anim_instructionTxt.numLines == 1) {
				
				   hLightDeal_mc.y = mcY -10
			       hLightDeal_mc.x = mcX  + 228; 
				   
				   
			   }else{
				 hLightDeal_mc.y = mcY -10
			     hLightDeal_mc.x = mcX  + 228;// instTxtY + push_arr[lastNum].y; 
			   }
			   hLightDeal_mc.buttonMode = true;   
			   hLightDeal_mc.addEventListener(MouseEvent.CLICK, PlayLetterSound)   
			   ths.topBar.addChild(hLightDeal_mc);
			 
			 
			 
			
		   
		   //ths.topBar.animTextMc.dealLetter_mc.addEventListener(MouseEvent.CLICK, PlayLetterSound);
		   
	
			//User PopUp Tearcher or Parent//
			if (Session.getInstance().role == "Teacher") {
				USERPOPUP_TITLE = presetsXML.variables.popupForTeacherTitle
				USERPOPUP_INFO = presetsXML.variables.popupInfoForTeacher;
				USERPOPUP_PARENT_INFO = presetsXML.variables.popupInfoForParent;
				IS_A_TEACHER = true;

			} else if (Session.getInstance().role == "Parent") {
				USERPOPUP_TITLE = presetsXML.variables.popupForParentTitle
				USERPOPUP_INFO = presetsXML.variables.popupInfoForParent;
				IS_A_TEACHER = false;
			}

			USERPOPUP_SUBTITLE = presetsXML.variables.popupSubTitle;
			//trace("USERPOPUP_SUBTITLE="+USERPOPUP_SUBTITLE)

			//User PopUp Tearcher or Parent//

			ths.topBar.userIcon.buttonMode = true;
			ths.topBar.soundBtn.buttonMode = true;
			ths.topBar.langBtn.buttonMode = true;

			if (IS_A_TEACHER) {
				ths.topBar.userIcon.teacherIcon.visible = true;
				ths.topBar.userIcon.parentIcon.visible = false;
			} else {
				ths.topBar.userIcon.teacherIcon.visible = false;
				ths.topBar.userIcon.parentIcon.visible = true;
			}

			if (Session.getInstance().language == "en") {
				////trace("tpbar= "+ths.topBar.langBtn.en)
				ths.topBar.langBtn.en.visible = true;
				ths.topBar.langBtn.esp.visible = false;
			} else if (Session.getInstance().language == "esp") {
				ths.topBar.langBtn.en.visible = false;
				ths.topBar.langBtn.esp.visible = true;
			}

			ths.topBar.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			ths.topBar.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);
			// //Popup setting Language

			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, onSpeaker);
		}
		
		private function callTextSync():void 
		{
			trace("callTextSync1")
			ths.dummy_mc.addFrameScript(ths.dummy_mc.totalFrames-1, null)
			//_sync.speak()
			_sync.speakAndDo(endAudio)
		}
		private function endAudio(e) {
			trace(" Audio Ends ");
			
			//ths.topBar.dealLetter_mc.buttonMode = true;
			
			PlayLetterSound(null);
			
		}
		
		private function PlayLetterSound(e:MouseEvent):void 
		{
			trace("Letter Indix M Box");
			
			 var ch:SoundChannel = new SoundChannel();
			 _letterXML = presetsXML.deal.(@letter == _letterToDeal)
			   var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );    
			   ch = _sound.play();
			   ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);    
			   function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 
				 
			   }
			
		}
		
		public function changeLang(e: MouseEvent): void {
			if (ths.topBar.langBtn.en.visible) {
				ths.topBar.langBtn.en.visible = false;
				ths.topBar.langBtn.esp.visible = true;
			} else {
				ths.topBar.langBtn.en.visible = true;
				ths.topBar.langBtn.esp.visible = false;
			}

		}

		public function showInfoForUser(e: MouseEvent): void {
			if (ths.getChildByName("userPopUp") != null) {
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
			userPopUp = new userPopup();
			ths.addChild(userPopUp)
			userPopUp.name = "userPopUp"
			userPopUp.userPopupClose.addEventListener(MouseEvent.CLICK, closePopup);
			userPopUp.userPopupTitle.text = USERPOPUP_TITLE; //Config.USERPOPUP_PARENT
			userPopUp.userPopupInfo.text = USERPOPUP_INFO;
			assignScrollBar(userPopUp.userPopupInfo)
			if (IS_A_TEACHER) {
				userPopUp.popupDivider.visible = true;
				userPopUp.infoForParentTitle.visible = true;
				userPopUp.infoForParent.visible = true;
				userPopUp.infoForParentTitle.text = USERPOPUP_SUBTITLE;
				userPopUp.infoForParent.text = USERPOPUP_PARENT_INFO;

			} else {
				userPopUp.popupDivider.visible = false;
				userPopUp.infoForParentTitle.visible = false;
				userPopUp.infoForParent.visible = false;
			}

			show();
		};

		public function onSpeaker(e: MouseEvent): void {
			//ReporterManager.interaction( { action: "click", actionObject: "gameInfoSpeaker" });
			//	SoundManager.play("gameInfo");
		}

		public function stopGame(): void {
			removeAllInstances();
		}

		private function loadComplete(): void {
			//trace("Load Complete");
		}

		public function startGame(node1, node2): void {
			//level=0;
			//groupNode=node1;
			//assetNode=node2;
			//visible=true;
			//startLevel();
			//levelOverScreen.setDetails(0,0,13,15,assetNode);
			//objectTimer.stop();
		}

		public function startLevel(): void {
			//trace("start")
		}

		public function nextLevel(): void {
			level++;
		}
		public function playAgain(): void {

		}

		public function onObjectTimerComplete(e: TimerEvent): void {
			removeAllInstances();
		}

		public function removeAllInstances(): void {}

		public function onObjectTimer(e: TimerEvent): void {}

		public function formatTime(val): String {
			return null;
		}

		public function soundClicked(e: MouseEvent): void {
			trace(" ********* ")
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			/*if (ths.topBar.animTextMc.visible == false) {				 
				WritingTheSoundShell._sync1.speak();
				 trace(" WritingTheSoundShell")
			 }
			 
			 if (ths.topBar.instruct_mc.visible == false) {				 
				 WritingTheSound._sync2.speak();
				 trace(" Let me clicked ")
			 } 
			 
			 if (ths.topBar.writeInstruct_mc.visible == false) {
				  WritingTheSound._sync3.speak();
			 }*/

			

			//trace(" inst clicked" + presetsXML.variables.instructionSound)
			if (ths.topBar.animTextMc.visible == false) {				 
				WritingTheSoundShell._sync1.speak();	
				
				trace("1");
			 }
			 else {
				
				 _sync.speakAndDo(playLetterAudio);
				 
				 trace("2");
			 }
			
			
			//Reporter.interaction("listen_and_record", ListenAndRecord._contentKey, ListenAndRecord.moduleSessionID);
			//playSndInstructions();

		}
		
		private function playLetterAudio(e):void 
		{
			PlayLetterSound(null);
		}

		public function autoPlayInstruction(e: Event): void {
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			
			trace("Auto Play");
			_sync.speakAndDo(playLetterAudio1);
		}

		private function playLetterAudio1(e: Event): void {
		
			
			trace("Auto Play  22");
			
			dispatchEvent(new Event("AUTO_PLAY_INSTRUCTION_ENDS"))

			/* var ch:SoundChannel = new SoundChannel();
			   var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );    
			   ch = _sound.play();
			   ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);    
			   function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 dispatchEvent(new Event("AUTO_PLAY_INSTRUCTION_SND_ENDS"))     
			   }*/
		}

		public function playSndInstructions(): void {
			trace("RECORDING INSTRUCTIONS");

			ths.topBar.soundBtn.removeEventListener(MouseEvent.CLICK, soundClicked);
			var sound: CoreSound = new CoreSound(new URLRequest(presetsXML.variables.instructionSound));
			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);

			function onComplete(e: Event): void {
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch: SoundChannel = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);

				function onSoundComplete(e: Event): void {
					trace("introsnd comp")
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
				}

			}
		}

		public function show(): void {
			TweenManager.tween(userPopUp, 0.5, {
				alpha: 1,
				scaleX: 1,
				scaleY: 1
			}, {
				ease: Ease.EXPONENTIAL,
				easeType: Ease.EASE_OUT
			});
		}

		public function hide(): void {
			if (ths.getChildByName("userPopUp") != null) {
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
			TweenManager.tween(userPopUp, 0.5, {
				alpha: 0,
				scaleX: 0,
				scaleY: 0
			}, {
				ease: Ease.EXPONENTIAL,
				easeType: Ease.EASE_OUT
			});

		}

		private function closePopup(e: MouseEvent): void {
			hide();
		}
		function assignScrollBar(tf: TextField): void {
			trace("textLen=" + tf.text.length)
			sb = new UIScrollBar();
			sb.move(tf.x + tf.width, tf.y);
			sb.setSize(sb.width, tf.height);

			sb.scrollTarget = tf;
			if (tf.text.length >= 420) {
				sb.visible = true;
			} else {
				sb.visible = false;
			}

			userPopUp.addChild(sb);
		}


	}

}