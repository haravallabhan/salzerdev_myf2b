﻿package com.myf2b.writingthesound {


	/**
	 * @company Footsteps 2 Brilliance
	 * @author Hara J N
	 */

	import com.myf2b.core.Core;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.Session;
	import flash.display.TriangleCulling;
	import flash.events.Event;

	import flash.display.MovieClip;
	import flash.geom.ColorTransform;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.myf2b.ui.*;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;


	public class WritingTheSoundShell extends MovieClip {
		private var instructionText: String;
		public static var _sync1: TextFieldSoundSync;

		public static var mainScreenProxy: WritingTheSound;
		public var isCorrect_Wrong: String = "";
		public var isCorrect_correct: String = "";
		public var mcExt: MovieClip;
		public var mcExt_2: MovieClip;
		public var ldr: Loader = new Loader();
		public var ldr_2: Loader = new Loader();
		public static var isWritingInst: Boolean = false;
		public static var childReset_btn: MovieClip


		public static var ths: *
		public static var stg: *

		public var myColor: ColorTransform = new ColorTransform();

		public var myLink: String = "";

		//
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB //0xCC3333;
		private var glow: GlowFilter = new GlowFilter();
		private var glow_str: String;
		private var btn_blinkCnt: Number = 0;

		public var letterCnt: Number = 0;
		private var dealLetter:String = "";
		
		//


		public function WritingTheSoundShell(id: String = "1") {

			Core.useStaging = true;
			ths = this;
			stg = stage;

			Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			ScreenManager.getInstance().stage = stage;
			ScreenManager.getInstance().pushModule("com.myf2b.writingthesound.WritingTheSound", ["135"]);
			mainScreenProxy = new WritingTheSound(this, id)

			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor //0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
		}

		public static function isThis(): * {
			return ths
		}
		public static function isStage(): * {
			return stg
		}

		//This Function again calls WritingTheSound

		public function startit_2(aUrl_2: String, animInstTxt: String,aDealLetter:String): void {
			trace("Anim=" + aDealLetter)			
			dealLetter = aDealLetter;			
			ldr_2.contentLoaderInfo.addEventListener(Event.COMPLETE, swfLoaded_2);
			//ldr.load(new URLRequest(aUrl));
			ldr_2.load(new URLRequest(aUrl_2));
			ths.topBar.animTextMc.anim_instructionTxt.text = animInstTxt
			
		}

		private function swfLoaded_2(e: Event): void {

			mcExt_2 = MovieClip(ldr_2.contentLoaderInfo.content);
			ldr_2.contentLoaderInfo.removeEventListener(Event.COMPLETE, swfLoaded_2);
			//ths.addChild(mcExt_2);
			ths.animcontentMc.addChild(mcExt_2)
			mcExt_2.handGlow_mc.buttonMode = true;
			mcExt_2.handGlow_mc.play();
			mcExt_2.myMovie.stop();
			//mcExt_2.addEventListener("RESET_HANDLER", onRest_Handler);	
			mcExt_2.addEventListener(MouseEvent.CLICK, onStageClick);
		}

		public function onStageClick(e: MouseEvent): void {
			mcExt_2.handGlow_mc.stop();
			mcExt_2.handGlow_mc.visible = false;
			mcExt_2.myMovie.play();
			mcExt_2.myMovie.addFrameScript(mcExt_2.myMovie.totalFrames - 1, letterAnimationFly);
		}

		public function letterAnimationFly(): void {
			playit.visible = true;			
			mcExt_2.myMovie.stop();
			//mcExt_2.addEventListener(MouseEvent.CLICK, onStageClick);
			playit.addEventListener(MouseEvent.CLICK, playnextMovie);
			//addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);
		}

		public function playnextMovie(e: MouseEvent): void {
			mcExt_2.myMovie.stop();
			playit.visible = false;
			
			mcExt_2.removeEventListener(MouseEvent.CLICK, onStageClick);
			ths.animcontentMc.removeChild(mcExt_2)
			playit.removeEventListener(MouseEvent.CLICK, playnextMovie);
			mcExt_2.removeEventListener(MouseEvent.CLICK, onStageClick);
			mcExt_2.myMovie.addFrameScript(mcExt_2.myMovie.totalFrames - 1, false);
			dispatchEvent(new Event("MOVIE_END_HANDLER"))

			removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);
		}

		// This function called from WritingTheSound.as //

		public function startit(aUrl: String): void {
			trace("aUrl="+aUrl)
			trace("char= " + dealLetter)
		
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, swfLoaded);
			ldr.load(new URLRequest(aUrl));

			//ldr.load(new URLRequest("E:/CoreProject/WritingTheSound/WritingTheSound/WriteTheSoundCharacters/HigherA.swf"));
		}


		public function swfLoaded(e: Event): void {

			mcExt = MovieClip(ldr.contentLoaderInfo.content);
			ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, swfLoaded);
			ths.addChild(mcExt);
			ths.contentMc.addChild(mcExt)


			letterCnt = 1;
			mcExt.dealWithPins(letterCnt);

			mcExt.addEventListener("RESET_HANDLER", onRest_Handler);
			mcExt.addEventListener("VALIDATION_HANDLER", onValidation_Handler);
			 
			mcExt.dealLetter(dealLetter, WritingTheSound.isWriteOn_Off);
			trace("WritingTheSound.selectedObject=== " + dealLetter)
			
			ths.topBar.animTextMc.visible = false;
			ths.topBar.instruct_mc.instructionTxt.text = WritingTheSound.presetsXML.variables.instructionTxt
			
			_sync1 = Ui.EnableTextSync(ths.topBar.instruct_mc.instructionTxt, WritingTheSound.presetsXML);
			//ths.dummy_mc.gotoAndPlay(2)
			//ths.dummy_mc.addFrameScript(ths.dummy_mc.totalFrames-1, callTextSync)

		}
		private function callTextSync():void 
		{
			trace("callTextSync")
			ths.dummy_mc.addFrameScript(ths.dummy_mc.totalFrames-1, null)
			_sync1.speak()
			
		}
		
		

		private function onValidation_Handler(e: Event): void {
			trace("VALIDATION_HANDLER ")
			trace("chh *(* " + mcExt.resetBtn)

			childReset_btn = mcExt.resetBtn
			childReset_btn.visible = false;
			isCorrect_Wrong = mcExt.correctOrWrng

			dispatchEvent(new Event("VALIDATION_HANDLER"))

			//mcExt.dealWithPins(3)

		}

		public function onRest_Handler(eParam): void {
			//trace(" MainRESET_HANDLER")
			dispatchEvent(new Event("NEXT_LEVEL_HANDLER"))

			if (letterCnt == 1) {
				mcExt.dealWithPins(2)
				//mcExt.dealWithPins(3)

				letterCnt = 2;
			} else if (letterCnt == 2) {
				mcExt.dealWithPins(2)

				letterCnt = 3;
			} else if (letterCnt == 3) {
				mcExt.dealWithPins(3)
			}

			if (!isWritingInst) {
				isWritingInst = true;
				//ths.topBar.instructionTxt.text = WritingTheSound.presetsXML.variables.wiritingInstructionTxt;
			}
		}

		// This function called from WritingCharacters.as//
		public function isValidation(aparam) {
			isCorrect_Wrong = aparam;
			isCorrect_correct = aparam;
			//	dispatchEvent(new Event("VALIDATION_HANDLER"))			 
		}

		private function createGlowEffectForBtn_Handlers(e): void {
			btn_blinkCnt++
			if (btn_blinkCnt % 5 == 0) {
				glow.alpha = 1;
				glowCnt++
				if (glowCnt == 3) {
					glowCnt = 0
					glow.alpha = 0;
				}
				playit.filters = [glow];
			}
			trace("Enter Frame");

			if (btn_blinkCnt == 0) {
				btn_blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers);
				glow.alpha = 0;
				playit.filters = [glow];
			}
		}

	}
}