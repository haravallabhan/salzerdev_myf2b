﻿package com.myf2b.slimegame.manager 
{
	//import flash.net.SharedObject;
	import com.myf2b.core.Bookmarker;
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.BookmarkerEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class SaveManager extends EventDispatcher
	{
		//private static var _sharedObject:SharedObject;
		private static var _moduleName:String = "rhyme_slime";
		
		private static var _bookmarkXML : XML;
		private static var _bookmarker : Bookmarker;
		
		private static var _instance : SaveManager = new SaveManager();
		
		/*public static function set key(value:String):void{
			_key = value;
		}*/
		
		public static function getInstance():SaveManager
		{
			if ( _instance == null ) _instance = new SaveManager();
			return _instance;
		}
		
		public static function load():void {
			
			/*_sharedObject = SharedObject.getLocal(_key);
			
			if ( ! _sharedObject.data.levels ) 
			{
				_sharedObject.data.levels = makeDefaultLevels();
				_sharedObject.data.mastered = false;
				_sharedObject.flush();
			}
			return _sharedObject.data;*/
			
			_bookmarker = new Bookmarker ( _moduleName , Config.GAME_XML_ID );
			_bookmarker.addEventListener( Event.COMPLETE, _onBookmarkLoaded);
			_bookmarker.addEventListener( IOErrorEvent.IO_ERROR, _onBookmarkLoaded);
			_bookmarker.load();
		}
		
		static private function _onBookmarkLoaded(e:Event):void
		{
			e.target.removeEventListener(Event.COMPLETE, _onBookmarkLoaded);
			e.target.removeEventListener(IOErrorEvent.IO_ERROR, _onBookmarkLoaded);
			
			// if bookmarked xml != null
			if (e.target.data != null) {
				_bookmarkXML = e.target.data;
			} 
			// Otherwise create a new empty bookmarkXML
			else {
				_createCleanXML();
			}
			
			_instance.dispatchEvent( new BookmarkerEvent( BookmarkerEvent.SHOW_PROGRESS, _bookmarkXML ));
		}
		
		/*public static function makeDefaultLevels():Array {
			var arr:Array = new Array();
			for (var i:uint = 0; i < 12; i++){
				arr.push( { mastered: false } );
			}
			return arr;
		}*/
		
		public static function save(setId:Number):void{
			/*_sharedObject = SharedObject.getLocal(_key);
			_sharedObject.data.levels[setId].mastered = true;
			_sharedObject.flush();*/
			
			_bookmarkXML.levels.level[setId] = 1;
			_bookmarker.save(_bookmarkXML);
		}
		
		public static function saveMastered():void{
			/*_sharedObject = SharedObject.getLocal(_key);
			_sharedObject.data.mastered = true;
			_sharedObject.flush();*/
		}
		
		public static function resetLevels():void{
			/*_sharedObject = SharedObject.getLocal(_key);
			_sharedObject.data.levels = makeDefaultLevels();
			_sharedObject.data.mastered = false;
			_sharedObject.flush();*/
		}
		
		private static function _createCleanXML():void
		{
			_bookmarkXML =	<data>
									<levels/>
							</data>;
								
			for ( var i:int = 0; i < 12 ; i++) 
			{
				_bookmarkXML.levels.appendChild( <level>0</level> );
			}
		}
		
		public static function getBookmarkXML():XML
		{
			return _bookmarkXML;
		}
		
	}
}