﻿package {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.media.SoundChannel;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.ui.Mouse;
	import flash.display.Loader;
	import flash.display.LoaderInfo;

	public class WritingCharacters extends MovieClip {

		public var penSprite: Sprite = new Sprite();
		public var pixelValue: uint;
		public var ltrA: letter_a = new letter_a();
		public var pns: pins = new pins();
		public var firstPoint: Boolean = false;
		public var firstPoint2: Boolean = false;
		public var secondPoint: Boolean = false;
		public var traceLetter: Boolean = true;
		public var totFrames: Number = 0;
		public var rslt: resultmc = new resultmc();
		public var trAgn: tryagainbtn = new tryagainbtn();
		public var subBtn: submitbtn = new submitbtn();
		public var resetBtn: reset = new reset();
		public var curPin1;
		public var curPin2;
		public var canvasBitmap: Bitmap;
		public var lineThicknes: Number = 30;

		public var myCursor: MyCursorClass;
		public var isTesting: String = "hello";
		public var correctOrWrng: String = "";
		//public var isWritingOnOrOff:String = "writeHandOff";
		//public var handMc:hand_mc;
		public var letterToDeal: String = "";
		public var isWriteOn_Off: String = "";
		public var isUpperCase:Boolean = false;

		public var tickX: Number = 1;

		public function dealLetter(aDealLetter: String, aWriteOn_Off: String) {
			trace("working aDealLetter " +aDealLetter);
			trace("working aWriteOn_Off " +aWriteOn_Off);
			letterToDeal = aDealLetter;
			trace("working letterToDeal " +letterToDeal);
			isWriteOn_Off = aWriteOn_Off;
			trace("letterToDeal ==" + letterToDeal);
			if (letterToDeal == letterToDeal.toLowerCase()) {
				isUpperCase = false;	 //lower case	
				
				trace("Lower Case");
				
			}else {
				isUpperCase = true;      // upper case
				
				trace("upper Case");
			}
			
		}

		public function dealWithPins(aParam: Number) {				
			
			trace("aParam = " + aParam);
			tickX = aParam
		
			if (tickX == 1) {
				pns.gotoAndStop(1);
				//trace("Control come here " + tickX);
			}

			if (tickX == 2) {
				pns.gotoAndStop(2);
				//trace("Control come here  " + tickX);
			}

			if (tickX == 3) {
				pns.gotoAndStop(3);
				//trace("Control come here  " + tickX);
			}
		}
		
		public function WritingCharacters() {

			if (tickX == 1) {
				//Mouse.hide();				
				if (isUpperCase == false) { // lower case
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else {
						totFrames = 3;				
					}
				}else {
					
					trace(" Upper CASE")
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else { // write off
						
						//
						if (letterToDeal == "E"){
							totFrames = 5;
							//trace("E= totFrames = "+totFrames)
						}
						if (letterToDeal == "A" || letterToDeal == "C"){
							totFrames = 4;
						}
						
					}
				}
	    		// this creates an instance of the library MovieClip with the
				// name, "MyCursorClass".  this contains your mouse cursor art
	
				myCursor = new MyCursorClass();
				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				myCursor.name = "cursor"
				// you'll want to make sure the child is added above everything
				// else, possibly in its own container
				addChild(myCursor);
				myCursor.buttonMode = true;
				//this.setChildIndex(myCursor,numChildren-1)

				// respond to mouse move events
				addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
				rslt.visible = false;
				// fla function ends

				this.canvasBitmap = new Bitmap(new BitmapData(1024, 768, false, 0x8ffffff), "auto", true);
				canvasBitmap.bitmapData.draw(ltrA);
				//trace("ltrA.x=" + ltrA.x);
				//trace("ltrA.y=" + ltrA.y);
				this.addChild(canvasBitmap);

				this.addChild(penSprite);
				this.addChild(pns);
				pns.gotoAndStop(1); 

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);

				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
				this.addEventListener(MouseEvent.MOUSE_OVER, onRollOver_Handler)
				curPin1.addEventListener(MouseEvent.MOUSE_DOWN, onPin1Click_Handler)
			}

			if (tickX == 2) {
				//Mouse.hide();
				if (isUpperCase == false) { // lower case
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else {
						totFrames = 3;				
					}
				}else {
					
					trace(" Upper CASE")
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else { // write off
						
						if (letterToDeal == "E"){
							totFrames = 5;
						}
						if (letterToDeal == "A" || letterToDeal == "C"){
							totFrames = 4;
						}
						
					}
				}

				// this creates an instance of the library MovieClip with the
				// name, "MyCursorClass".  this contains your mouse cursor art
				//
				myCursor = new MyCursorClass();
				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				myCursor.name = "cursor"
				// you'll want to make sure the child is added above everything
				// else, possibly in its own container
				//
				addChild(myCursor);

				//this.setChildIndex(myCursor,numChildren-1)

				// respond to mouse move events
				addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
				rslt.visible = false;
				//fla function ends

				this.canvasBitmap = new Bitmap(new BitmapData(1024, 768, false, 0x8ffffff), "auto", true);
				canvasBitmap.bitmapData.draw(ltrA);
				//trace("ltrA.x=" + ltrA.x);
				//trace("ltrA.y=" + ltrA.y);
				this.addChild(canvasBitmap);

				this.addChild(penSprite);
				this.addChild(pns)
				pns.gotoAndStop(2);

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);

				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
				this.addEventListener(MouseEvent.MOUSE_OVER, onRollOver_Handler)
				curPin1.addEventListener(MouseEvent.MOUSE_DOWN, onPin1Click_Handler)
			}

			if (tickX == 3) {
				//Mouse.hide();
				if (isUpperCase == false) { // lower case
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else {
						totFrames = 3;				
					}
				}else {
					
					trace(" Upper CASE")
					if (isWriteOn_Off == "writeHandOn") {
						totFrames = 4;
						//trace("****letterToDeal\t\t" + letterToDeal);					
					} else { // write off
						
						if (letterToDeal == "E"){
							totFrames = 5;							
						}
						if (letterToDeal == "A" || letterToDeal == "C"){
							totFrames = 4;
						}
					}
				}

				// this creates an instance of the library MovieClip with the
				// name, "MyCursorClass".  this contains your mouse cursor art
				myCursor = new MyCursorClass();
				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				myCursor.name = "cursor"
				// you'll want to make sure the child is added above everything
				// else, possibly in its own container
				//
				addChild(myCursor);
				//this.setChildIndex(myCursor,numChildren-1)
				// respond to mouse move events
				addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
				rslt.visible = false;
				//fla function ends
				this.canvasBitmap = new Bitmap(new BitmapData(1024, 768, false, 0x8ffffff), "auto", true);
				canvasBitmap.bitmapData.draw(ltrA);
				//trace("ltrA.x=" + ltrA.x);
				//trace("ltrA.y=" + ltrA.y);
				this.addChild(canvasBitmap);
				this.addChild(penSprite);
				this.addChild(pns)
				pns.gotoAndStop(3);
				trace("*******pns******* = " + pns["inside" + tickX]);
				//pns["inside"+1].name = "points";
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
				this.addEventListener(MouseEvent.MOUSE_OVER, onRollOver_Handler)
				curPin1.addEventListener(MouseEvent.MOUSE_DOWN, onPin1Click_Handler)
			}
		}
		private function mouseMoveHandler(evt: MouseEvent): void {

		}
		private function onPin1Click_Handler(evt): void {

			if (tickX == 1) {
				myCursor.visible = true;
				myCursor.buttonMode = true;
				setChildIndex(MovieClip(pns), numChildren - 1)
				setChildIndex(MovieClip(myCursor), numChildren - 1)
				Mouse.hide();
			}
			if (tickX == 2) {
				trace("onPin1Click_Handler");
				myCursor.visible = true;
				myCursor.buttonMode = true;
				setChildIndex(MovieClip(pns), numChildren - 1)
				setChildIndex(MovieClip(myCursor), numChildren - 1)
				Mouse.hide();
			}
			if (tickX == 3) {
				myCursor.visible = true;
				myCursor.buttonMode = true;
				setChildIndex(MovieClip(pns), numChildren - 1)
				setChildIndex(MovieClip(myCursor), numChildren - 1)
				Mouse.hide();
			}
		}
		public function mouseDown(e: MouseEvent): void {
			//myCursor.x = e.stageX;
			//myCursor.y = e.stageY;
			////trace(e.target.name +" = "+e.target.parent.name)
			myCursor.visible = false;

			this.penSprite.graphics.moveTo(e.stageX, e.stageY);
			var _point1: Point = localToGlobal(new Point(mouseX, mouseY));
			if (curPin1.hitTestPoint(_point1.x, _point1.y, true)) {
				firstPoint = true;
			}
			this.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
			this.addEventListener(MouseEvent.MOUSE_UP, this.mouseUp);
		}
		private function mouseMove(e: MouseEvent): void {
			if (tickX == 1) {
				myCursor.x = e.stageX;
				myCursor.y = e.stageY;
				penSprite.graphics.lineTo(e.stageX, e.stageY);
				pixelValue = canvasBitmap.bitmapData.getPixel(e.stageX, e.stageY);
				if (pixelValue == 249825) {
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
					resetFn()
				}
				myCursor.visible = true;
				myCursor.buttonMode = true;
				Mouse.hide();
				if (traceLetter) {
					if (pixelValue.toString(16) == "784bba" && pixelValue.toString(16) == "d6a4d6") {
						traceLetter = false;
					} else if (pixelValue.toString(16) == "3cfe1") {
						traceLetter = false;
					}
				}
				if (isWriteOn_Off == "writeHandOn") {
					var _point2: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point2.x, _point2.y, true)) {
						pns["inside" + tickX].nextFrame();
						curPin2 = pns["inside" + tickX].pn5;
					} else {
						if (pixelValue.toString(16) == "575fa9") {
							this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
							resetFn();
							autocall();
							trace(" Touched the line");
						}
					}
				} else {

					if (pixelValue.toString(16) == "575fa9") {
						this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
						resetFn();
						autocall();
						trace(" Touched the line");
					}
				}
			}
			if (tickX == 2) {

				myCursor.x = e.stageX;
				myCursor.y = e.stageY;

				penSprite.graphics.lineTo(e.stageX, e.stageY);
				pixelValue = canvasBitmap.bitmapData.getPixel(e.stageX, e.stageY);

				if (pixelValue == 249825) {
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
					resetFn()
				}
				myCursor.visible = true;
				myCursor.buttonMode = true;
				Mouse.hide();

				if (traceLetter) {
					if (pixelValue.toString(16) == "784bba" && pixelValue.toString(16) == "d6a4d6") {
						traceLetter = false;

					} else if (pixelValue.toString(16) == "3cfe1") {
						traceLetter = false;
					}
				}
				if (isWriteOn_Off == "writeHandOn") {

					var _point23: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point23.x, _point23.y, true)) {
						pns["inside" + tickX].nextFrame();
						curPin2 = pns["inside" + tickX].pn5;
					} else {
						if (pixelValue.toString(16) == "575fa9") {
							this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
							resetFn();
							autocall();
						}
					}
				} else {

					if (pixelValue.toString(16) == "575fa9") {
						this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
						resetFn();
						autocall();
					}
				}
			}
			if (tickX == 3) {

				myCursor.x = e.stageX;
				myCursor.y = e.stageY;
				penSprite.graphics.lineTo(e.stageX, e.stageY);
				pixelValue = canvasBitmap.bitmapData.getPixel(e.stageX, e.stageY);

				if (pixelValue == 249825) {
					this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
					resetFn()
				}
				myCursor.visible = true;
				myCursor.buttonMode = true;
				Mouse.hide();
				if (traceLetter) {

					if (pixelValue.toString(16) == "784bba" && pixelValue.toString(16) == "d6a4d6") {
						traceLetter = false;

					} else if (pixelValue.toString(16) == "3cfe1") {
						traceLetter = false;
					}
				}

				if (isWriteOn_Off == "writeHandOn") {

					var _point24: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point24.x, _point24.y, true)) {
						pns["inside" + tickX].nextFrame();
						curPin2 = pns["inside" + tickX].pn5;
					} else {

						if (pixelValue.toString(16) == "575fa9") {
							this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
							resetFn();
							autocall();
						}
					}
				} else {
					if (pixelValue.toString(16) == "575fa9") {
						this.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
						resetFn();
						autocall();
					}
				}
			}
		}

		private function onRollOver_Handler(e: MouseEvent): void {

		}

		public function mouseUp(e: MouseEvent): void {
			trace("Error function");
			if (tickX == 1) {

				if (isWriteOn_Off == "writeHandOn") {
					var _point22: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point22.x, _point22.y, true)) {
						secondPoint = true;
					}
					if (firstPoint && secondPoint && traceLetter) {

						if (pns["inside" + tickX].currentFrame < totFrames) {

							pns["inside" + tickX].nextFrame();
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
						} else {

							Mouse.show();
							myCursor.visible = true;
							myCursor.buttonMode = true;
							rslt.x = 860;
							rslt.y = 530;

							//Add the cursor at the Mark point-08-08-2014
							myCursor.x = pns["inside" + tickX].marks.x
							myCursor.y = pns["inside" + tickX].marks.y

							rslt.gotoAndStop(2);
							this.addChild(rslt);
							resetBtn.x = 860;
							resetBtn.y = 600;
							resetBtn.scaleX = 0.7;
							resetBtn.scaleY = 0.7;
							this.addChild(resetBtn);
							resetBtn.visible = true;
							resetBtn.buttonMode = true;
							resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
							this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
							this.addChild(pns)
							correctOrWrng = "corr"
							dispatchEvent(new Event("VALIDATION_HANDLER"))
							//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//
							myCursor.visible = false;
						}

					} else {
						//trace("end 2 trye")
						myCursor.visible = true;
						myCursor.buttonMode = true;
						Mouse.show();
						//Mouse.show();
						//myCursor.x = 355
						//myCursor.y = 420
						//Add the cursor at the Mark point-08-08-2014
						myCursor.x = pns["inside" + tickX].marks.x
						myCursor.y = pns["inside" + tickX].marks.y
						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(3);
						this.addChild(rslt);
						trAgn.x = 860;
						trAgn.y = 630;
						trAgn.scaleX = 0.7;
						trAgn.scaleY = 0.7;
						//this.addChild(trAgn);
						firstPoint = false;
						secondPoint = false;
						traceLetter = true;
						penSprite.mouseEnabled = false;
						//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
						//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
						//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
						correctOrWrng = "wrg"
						//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
						dispatchEvent(new Event("VALIDATION_HANDLER"))
						autocall();
					}

				} else {
					trace("createWriting_HandOut tick 1: ")
					if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 5;
						createWriting_HandOut2()
					}
					
					if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 4;
						createWriting_HandOut3();
					}
					else {
						
						totFrames = 3;
						createWriting_HandOut()
					}
					
				}
			}
			if (tickX == 2) {

				if (isWriteOn_Off == "writeHandOn") {
					var _point222: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point222.x, _point222.y, true)) {
						secondPoint = true;
					}
					if (firstPoint && secondPoint && traceLetter) {

						if (pns["inside" + tickX].currentFrame < totFrames) {
							//trace("currentFrame")
							pns["inside" + tickX].nextFrame();
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
						} else {
							Mouse.show();
							myCursor.visible = true;
							myCursor.buttonMode = true;
							rslt.x = 860;
							rslt.y = 530;
							//myCursor.x = 355
							//myCursor.y = 420
							//Add the cursor at the Mark point-08-08-2014
							myCursor.x = pns["inside" + tickX].marks.x
							myCursor.y = pns["inside" + tickX].marks.y
							rslt.gotoAndStop(2);
							this.addChild(rslt);
							resetBtn.x = 860;
							resetBtn.y = 600;
							resetBtn.scaleX = 0.7;
							resetBtn.scaleY = 0.7;
							this.addChild(resetBtn);
							resetBtn.visible = true;
							resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
							this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
							this.addChild(pns)
							correctOrWrng = "corr"
							dispatchEvent(new Event("VALIDATION_HANDLER"))
							//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//
							myCursor.visible = false;
						}

					} else {

						myCursor.visible = true;
						myCursor.buttonMode = true;
						Mouse.show();
						//Mouse.show();
						//myCursor.x = 355
						//myCursor.y = 420
						//Add the cursor at the Mark point-08-08-2014
						myCursor.x = pns["inside" + tickX].marks.x
						myCursor.y = pns["inside" + tickX].marks.y
						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(3);
						this.addChild(rslt);
						trAgn.x = 860;
						trAgn.y = 630;
						trAgn.scaleX = 0.7;
						trAgn.scaleY = 0.7;
						//this.addChild(trAgn);
						firstPoint = false;
						secondPoint = false;
						traceLetter = true;
						penSprite.mouseEnabled = false;
						//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
						//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
						//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
						correctOrWrng = "wrg"
						//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
						dispatchEvent(new Event("VALIDATION_HANDLER"))
						autocall();
					}
				} else {
					if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 5;
						createWriting_HandOut2()
					}
					else if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 4;
						createWriting_HandOut3();
					}
					else {
						
						totFrames = 3;
						createWriting_HandOut()
					}
				}
			}
			if (tickX == 3) {

				if (isWriteOn_Off == "writeHandOn") {
					var _point2222: Point = localToGlobal(new Point(mouseX, mouseY));
					if (curPin2.hitTestPoint(_point2222.x, _point2222.y, true)) {
						secondPoint = true;
					}
					if (firstPoint && secondPoint && traceLetter) {

						if (pns["inside" + tickX].currentFrame < totFrames) {
							pns["inside" + tickX].nextFrame();
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
						} else {
							Mouse.show();
							myCursor.visible = true;
							myCursor.buttonMode = true;
							rslt.x = 860;
							rslt.y = 530;
							//myCursor.x = 355
							//myCursor.y = 420
							//Add the cursor at the Mark point-08-08-2014
							myCursor.x = pns["inside" + tickX].marks.x
							myCursor.y = pns["inside" + tickX].marks.y
							rslt.gotoAndStop(2);
							this.addChild(rslt);
							resetBtn.x = 860;
							resetBtn.y = 600;
							resetBtn.scaleX = 0.7;
							resetBtn.scaleY = 0.7;
							this.addChild(resetBtn);
							resetBtn.visible = true;
							resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
							this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
							this.addChild(pns)
							correctOrWrng = "corr"
							dispatchEvent(new Event("VALIDATION_HANDLER"))
							//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//
							myCursor.visible = false;
						}

					} else {
						//trace("end 2 trye")
						myCursor.visible = true;
						myCursor.buttonMode = true;
						Mouse.show();
						//Mouse.show();
						//myCursor.x = 355
						//myCursor.y = 420
						//Add the cursor at the Mark point-08-08-2014
						myCursor.x = pns["inside" + tickX].marks.x
						myCursor.y = pns["inside" + tickX].marks.y
						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(3);
						this.addChild(rslt);
						trAgn.x = 860;
						trAgn.y = 630;
						trAgn.scaleX = 0.7;
						trAgn.scaleY = 0.7;
						//this.addChild(trAgn);
						firstPoint = false;
						secondPoint = false;
						traceLetter = true;
						penSprite.mouseEnabled = false;
						//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
						//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
						//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
						correctOrWrng = "wrg"
						//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
						dispatchEvent(new Event("VALIDATION_HANDLER"))
						autocall();
					}

				} else {

					if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 5;
						createWriting_HandOut2();
					}
					else if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						
						totFrames = 4;
						createWriting_HandOut3();
					}
					else {
						
						totFrames = 3;
						createWriting_HandOut();
					}
					
				}
			}
			this.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			this.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		//Code For A starts here:
		private function createWriting_HandOut3():void 
		{
			
			//canvasBitmap.bitmapData.draw( penSprite, null, null, null, null, true );
			 
			trace("U r in  AAAA Capital code"); 

			var _point2:Point = localToGlobal(new Point(mouseX,mouseY));
			trace("mouseUp= " + curPin2.name +" _point2= "+_point2)			
			if(curPin2.hitTestPoint(_point2.x,_point2.y,true))
			{
				
				secondPoint=true;
				//-------------starting position of my cursor--------------//
				myCursor.x = 510
				myCursor.y = 330
				
				
			}

			//trace(firstPoint,secondPoint,traceLetter);
			if(firstPoint && secondPoint && traceLetter)
			{			
				trace("after reset ")
				if(pns["inside" + tickX].currentFrame<totFrames)
				{
					
					pns["inside" + tickX].nextFrame();
					firstPoint=false;
					secondPoint=false;
					traceLetter=true;
					
					if(curPin1==pns["inside" + tickX].pn1)
					{
						
					
						curPin1 = pns["inside" + tickX].pn2;
						curPin2 = pns["inside" + tickX].pn4	;		
							trace(curPin1.name+" Second Frame " + pns["inside" + tickX].pn2.name);
							trace(curPin2.name+" Second Frame " + pns["inside" + tickX].pn4.name);
							
						Mouse.show();
						
						
							//--------------second position of my cursor------------//
							myCursor.x = 510;
							myCursor.y = 330;
						
					}
					else if(curPin2==pns["inside" + tickX].pn4)
					{
						
						trace("Third Frame");
						curPin1 = pns["inside" + tickX].pn5;						
						curPin2 = pns["inside" + tickX].pn6;
						trace(curPin1.name+" Third Frame " + pns["inside" + tickX].pn5.name);
						trace(curPin2.name+" Third Frame " + pns["inside" + tickX].pn6.name);
						
						
						Mouse.show();
						//--------------third position of my cursor------------//
						myCursor.x = 435;
						myCursor.y = 520;

						 
						
					}
					
					
					
					else if(curPin2==pns["inside" + tickX].pn6)
					{
						
						trace("fouth Frame");
						curPin1=pns["inside" + tickX].pn5;
						curPin2=pns["inside" + tickX].pn6;
						trace(curPin1.name+" fouth Frame " + pns["inside" + tickX].pn5.name);
						trace(curPin2.name+" fouth Frame " + pns["inside" + tickX].pn6.name);
						
							Mouse.show();
								rslt.x = 860;
								rslt.y = 530;
								rslt.gotoAndStop(2);
								this.addChild(rslt);
								resetBtn.x = 860;
								resetBtn.y = 600;
								resetBtn.scaleX = 0.7;
								resetBtn.scaleY = 0.7;
								this.addChild(resetBtn);
								resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);

								this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

								firstPoint = false;
								secondPoint = false;
								traceLetter = true;

								myCursor.visible = false
								Mouse.show();
								correctOrWrng = "corr"
								
								dispatchEvent(new Event("VALIDATION_HANDLER"))
								//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//					
							}

						} else {
							//trace("3");
							Mouse.show();
							myCursor.visible = true;
							myCursor.buttonMode = true;
							rslt.x = 860;
							rslt.y = 530;
							rslt.gotoAndStop(2);
							this.addChild(rslt);
							resetBtn.x = 860;
							resetBtn.y = 600;
							resetBtn.scaleX = 0.7;
							resetBtn.scaleY = 0.7;
							this.addChild(resetBtn);
							resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
							this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
							firstPoint = false;
							secondPoint = false;
							traceLetter = true;
							this.addChild(pns)
						}

					} else {
						//trace("end 2 trye")
						myCursor.visible = true;
						myCursor.buttonMode = true;
						Mouse.show();
						//Mouse.show();
						//myCursor.x = 355
						//myCursor.y = 420

						//Add the cursor at the Mark point-08-08-2014
						myCursor.x = pns["inside" + tickX].marks.x
						myCursor.y = pns["inside" + tickX].marks.y

						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(3);
						this.addChild(rslt);
						trAgn.x = 860;
						trAgn.y = 630;
						trAgn.scaleX = 0.7;
						trAgn.scaleY = 0.7;
						//this.addChild(trAgn);
						firstPoint = false;
						secondPoint = false;
						traceLetter = true;
						penSprite.mouseEnabled = false;
						//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
						//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
						//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
						correctOrWrng = "wrg"
						
						trace(" Error in createWriting_HandOut2");
						//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
						dispatchEvent(new Event("VALIDATION_HANDLER"))
						autocall();
					}

					this.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
					this.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			}
			
		//Code for A Ends here:
		
		
		private function createWriting_HandOut2():void 
		{
			trace(" U r in  EEEE Capital code ");
			var _point2:Point = localToGlobal(new Point(mouseX,mouseY));
			trace("mouseUp = " + curPin2.name +" _point2= "+_point2)			
			if(curPin2.hitTestPoint(_point2.x,_point2.y,true))
			{
				secondPoint=true;
				//-------------starting position of my cursor--------------//
				myCursor.x = 440
				myCursor.y = 325
			}
				//trace(firstPoint,secondPoint,traceLetter);
				if(firstPoint && secondPoint && traceLetter)
				{			
					trace("after reset ")
					if (pns["inside" + tickX].currentFrame < totFrames) {
					pns["inside" + tickX].nextFrame();
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					if (curPin1 == pns["inside" + tickX].pn1)
						{
							
							trace("Second Frame2");
							curPin1 = pns["inside" + tickX].pn2;
							curPin2 = pns["inside" + tickX].pn4;	
							Mouse.show();
							//--------------second position of my cursor------------//
							
							if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 430;
							myCursor.y = 330;
							}
							
							
						}
						else if(curPin2==pns["inside" + tickX].pn4)
						{
							
							trace("Third Frame 3");
							curPin1 = pns["inside" + tickX].pn5;						
							curPin2 = pns["inside" + tickX].pn6;
							
							Mouse.show();
							myCursor.x = 434;
							myCursor.y = 450;
						}
						
						else if(curPin2==pns["inside" + tickX].pn6)
						{
							
							trace("fourth Frame");
							curPin1=pns["inside" + tickX].pn7;
							curPin2=pns["inside" + tickX].pn8;
							
							Mouse.show();
							myCursor.x = 433;
							myCursor.y = 590;
							
						}
			
						else if (curPin2 == pns["inside" + tickX].pn8) {
							
							//trace("Third Frame3");
							curPin1 = pns["inside" + tickX].pn7;
							curPin2 = pns["inside" + tickX].pn8;

							Mouse.show();
							rslt.x = 860;
							rslt.y = 530;
							rslt.gotoAndStop(2);
							this.addChild(rslt);
							resetBtn.x = 860;
							resetBtn.y = 600;
							resetBtn.scaleX = 0.7;
							resetBtn.scaleY = 0.7;
							this.addChild(resetBtn);
							resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);

							this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

							firstPoint = false;
							secondPoint = false;
							traceLetter = true;

							myCursor.visible = false
							Mouse.show();
							correctOrWrng = "corr"
							
							dispatchEvent(new Event("VALIDATION_HANDLER"))
							//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//					
						}

					} else {
						//trace("3");
						Mouse.show();
						myCursor.visible = true;
						myCursor.buttonMode = true;
						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(2);
						this.addChild(rslt);
						resetBtn.x = 860;
						resetBtn.y = 600;
						resetBtn.scaleX = 0.7;
						resetBtn.scaleY = 0.7;
						this.addChild(resetBtn);
						resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
						firstPoint = false;
						secondPoint = false;
						traceLetter = true;
						this.addChild(pns)
					}

				} else {
					//trace("end 2 trye")
					myCursor.visible = true;
					myCursor.buttonMode = true;
					Mouse.show();
					//Mouse.show();
					//myCursor.x = 355
					//myCursor.y = 420

					//Add the cursor at the Mark point-08-08-2014
					myCursor.x = pns["inside" + tickX].marks.x
					myCursor.y = pns["inside" + tickX].marks.y

					rslt.x = 860;
					rslt.y = 530;
					rslt.gotoAndStop(3);
					this.addChild(rslt);
					trAgn.x = 860;
					trAgn.y = 630;
					trAgn.scaleX = 0.7;
					trAgn.scaleY = 0.7;
					//this.addChild(trAgn);
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					penSprite.mouseEnabled = false;
					//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
					//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
					//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
					correctOrWrng = "wrg"
					
					trace(" Error in createWriting_HandOut2");
					//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
					dispatchEvent(new Event("VALIDATION_HANDLER"))
					autocall();
				}

				this.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
				this.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}
		
		private function createWriting_HandOut(): void {
			
			trace("createWriting_HandOut Else condition");
			
			var _point2: Point = localToGlobal(new Point(mouseX, mouseY));
			if (curPin2.hitTestPoint(_point2.x, _point2.y, true)) {
				//trace("Prabhakar");
				secondPoint = true;
				//-------------starting position of my cursor--------------//
				//myCursor.x  = 550
				//myCursor.y  = 325
			}

			//trace(firstPoint,secondPoint,traceLetter);
			if (firstPoint && secondPoint && traceLetter) {
				//trace("1 "+pns["inside"+tickX].currentFrame);
				if (pns["inside" + tickX].currentFrame < totFrames) {

					pns["inside" + tickX].nextFrame();
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;

					if (curPin1 == pns["inside" + tickX].pn1) {

						//trace("Second Frame2");
						curPin1 = pns["inside" + tickX].pn2;
						curPin2 = pns["inside" + tickX].pn4;


						Mouse.show();
						//--------------second position of my cursor------------//
						
						if (letterToDeal == "f" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 470
							myCursor.y = 430
						}

						if (letterToDeal == "i" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 515
							myCursor.y = 340
						}


						if (letterToDeal == "j" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							trace("jack jj world");
							myCursor.x = 525
							myCursor.y = 325
						}

						if (letterToDeal == "k" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 555
							myCursor.y = 415
						}

						if (letterToDeal == "t" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 440
							myCursor.y = 415
						}

						if (letterToDeal == "x" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 580
							myCursor.y = 410
						}

						if (letterToDeal == "y" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 594
							myCursor.y = 325
						}

						//This code for Caps A Letters:

						if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 430
							myCursor.y = 530
						}

						//This code for Caps A Letters:
						if (letterToDeal == "B" && isWriteOn_Off == "writeHandOff") {
							//--------------second position of my cursor------------//
							myCursor.x = 440
							myCursor.y = 325
						}

					} else if (curPin2 == pns["inside" + tickX].pn4) {

						//trace("Third Frame3");
						curPin1 = pns["inside" + tickX].pn2;
						curPin2 = pns["inside" + tickX].pn4;

						Mouse.show();
						rslt.x = 860;
						rslt.y = 530;
						rslt.gotoAndStop(2);
						this.addChild(rslt);
						resetBtn.x = 860;
						resetBtn.y = 600;
						resetBtn.scaleX = 0.7;
						resetBtn.scaleY = 0.7;
						this.addChild(resetBtn);
						resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);

						this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

						firstPoint = false;
						secondPoint = false;
						traceLetter = true;

						myCursor.visible = false
						Mouse.show();
						correctOrWrng = "corr"
						
						dispatchEvent(new Event("VALIDATION_HANDLER"))
						//MovieClip(this.parent.parent).isValidation("correct");// this function referes Wriring TheSound Shell//					
					}

				} else {
					//trace("3");
					Mouse.show();
					myCursor.visible = true;
					myCursor.buttonMode = true;
					rslt.x = 860;
					rslt.y = 530;
					rslt.gotoAndStop(2);
					this.addChild(rslt);
					resetBtn.x = 860;
					resetBtn.y = 600;
					resetBtn.scaleX = 0.7;
					resetBtn.scaleY = 0.7;
					this.addChild(resetBtn);
					resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					this.addChild(pns)
				}

			} else {
				//trace("end 2 trye")
				myCursor.visible = true;
				myCursor.buttonMode = true;
				Mouse.show();
				//Mouse.show();
				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				rslt.x = 860;
				rslt.y = 530;
				rslt.gotoAndStop(3);
				this.addChild(rslt);
				trAgn.x = 860;
				trAgn.y = 630;
				trAgn.scaleX = 0.7;
				trAgn.scaleY = 0.7;
				//this.addChild(trAgn);
				firstPoint = false;
				secondPoint = false;
				traceLetter = true;
				penSprite.mouseEnabled = false;
				//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
				//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
				//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
				this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
				correctOrWrng = "wrg"
				//MovieClip(this.parent.parent).isValidation("wrong") // this function referes Wriring TheSound Shell//
				dispatchEvent(new Event("VALIDATION_HANDLER"))
				autocall();
			}

			this.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
			this.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		}

		private function createWriting_HandOn(): void {

			if (tickX == 1) {

				if (curPin1 == pns["inside" + tickX].pn1) {
					//trace("Second Frame");

					curPin1 = pns["inside" + tickX].pn4;
					curPin2 = pns["inside" + tickX].pn5;
					Mouse.show();
					rslt.x = 425;
					rslt.y = 610;
					rslt.gotoAndStop(2);
					this.addChild(rslt);
					resetBtn.x = 600;
					resetBtn.y = 600;
					resetBtn.scaleX = 0.7;
					resetBtn.scaleY = 0.7;
					this.addChild(resetBtn);
					resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OVER,resetRollOver_Handler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OUT,resetRollOut_Handler);

					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

					firstPoint = false;
					secondPoint = false;
					traceLetter = true;

					myCursor.visible = false
					Mouse.show();

				} else if (curPin2 == pns["inside" + tickX].pn5) {
					//trace("Third Frame");
					//curPin1 = pns["inside"+tickX].pn4;
					//curPin2 = pns["inside"+tickX].pn5;
				}

			}

			if (tickX == 2) {

				if (curPin1 == pns["inside" + tickX].pn1) {
					//trace("Second Frame");

					curPin1 = pns["inside" + tickX].pn4;
					curPin2 = pns["inside" + tickX].pn5;
					Mouse.show();
					rslt.x = 425;
					rslt.y = 610;
					rslt.gotoAndStop(2);
					this.addChild(rslt);
					resetBtn.x = 600;
					resetBtn.y = 600;
					resetBtn.scaleX = 0.7;
					resetBtn.scaleY = 0.7;
					this.addChild(resetBtn);
					resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OVER,resetRollOver_Handler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OUT,resetRollOut_Handler);

					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

					firstPoint = false;
					secondPoint = false;
					traceLetter = true;

					myCursor.visible = false
					Mouse.show();

				} else if (curPin2 == pns["inside" + tickX].pn5) {
					//trace("Third Frame");
					//curPin1 = pns["inside"+tickX].pn4;
					//curPin2 = pns["inside"+tickX].pn5;
				}
			}

			if (tickX == 3) {

				if (curPin1 == pns["inside" + tickX].pn1) {
					//trace("Second Frame");

					curPin1 = pns["inside" + tickX].pn4;
					curPin2 = pns["inside" + tickX].pn5;
					Mouse.show();
					rslt.x = 425;
					rslt.y = 610;
					rslt.gotoAndStop(2);
					this.addChild(rslt);
					resetBtn.x = 600;
					resetBtn.y = 600;
					resetBtn.scaleX = 0.7;
					resetBtn.scaleY = 0.7;
					this.addChild(resetBtn);
					resetBtn.addEventListener(MouseEvent.CLICK, resetHandler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OVER,resetRollOver_Handler);
					//resetBtn.addEventListener(MouseEvent.ROLL_OUT,resetRollOut_Handler);

					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

					firstPoint = false;
					secondPoint = false;
					traceLetter = true;

					myCursor.visible = false
					Mouse.show();

				} else if (curPin2 == pns["inside" + tickX].pn5) {
					//trace("Third Frame");
					//curPin1 = pns["inside"+tickX].pn4;
					//curPin2 = pns["inside"+tickX].pn5;
				}
			}
		}

		public function resetFn() {
			//trace("resetFn")
			Mouse.show();
			if (tickX == 1) {

				if (rslt.parent) {
					this.removeChild(rslt);
				}
				if (trAgn.parent) {
					this.removeChild(trAgn);
				}
				this.removeChild(penSprite);

				if (firstPoint && secondPoint && traceLetter) {

				} else {
					//trace("resetF")
					myCursor.visible = true;
					myCursor.buttonMode = true;
					Mouse.show();
					//Mouse.show();
					//myCursor.x = 355
					//myCursor.y = 420

					//Add the cursor at the Mark point-08-08-2014
					myCursor.x = pns["inside" + tickX].marks.x
					myCursor.y = pns["inside" + tickX].marks.y

					rslt.x = 860;
					rslt.y = 530;
					rslt.gotoAndStop(3);
					this.addChild(rslt);
					trAgn.x = 860;
					trAgn.y = 630;
					trAgn.scaleX = 0.7;
					trAgn.scaleY = 0.7;
					//this.addChild(trAgn);
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					penSprite.mouseEnabled = false;
					//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
					//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
					//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
					//trace(" Wrong 3 ")
					correctOrWrng = "wrg"
					dispatchEvent(new Event("VALIDATION_HANDLER"))
					//MovieClip(this.parent.parent).isValidation("wrong");
				}

				pns.gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;

				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 2) {

				if (rslt.parent) {
					this.removeChild(rslt);
				}
				if (trAgn.parent) {
					this.removeChild(trAgn);
				}
				this.removeChild(penSprite);

				if (firstPoint && secondPoint && traceLetter) {

				} else {
					//trace("resetF")
					myCursor.visible = true;
					myCursor.buttonMode = true;
					Mouse.show();
					//Mouse.show();
					//myCursor.x = 355
					//myCursor.y = 420

					//Add the cursor at the Mark point-08-08-2014
					myCursor.x = pns["inside" + tickX].marks.x
					myCursor.y = pns["inside" + tickX].marks.y

					rslt.x = 860;
					rslt.y = 530;
					rslt.gotoAndStop(3);
					this.addChild(rslt);
					trAgn.x = 860;
					trAgn.y = 630;
					trAgn.scaleX = 0.7;
					trAgn.scaleY = 0.7;
					//this.addChild(trAgn);
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					penSprite.mouseEnabled = false;
					//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
					//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
					//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
					//trace(" Wrong 3 ")
					correctOrWrng = "wrg"
					dispatchEvent(new Event("VALIDATION_HANDLER"))
					//MovieClip(this.parent.parent).isValidation("wrong");
				}

				pns.gotoAndStop(2);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;

				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 3) {
				if (rslt.parent) {
					this.removeChild(rslt);
				}
				if (trAgn.parent) {
					this.removeChild(trAgn);
				}
				this.removeChild(penSprite);

				if (firstPoint && secondPoint && traceLetter) {

				} else {
					//trace("resetF")
					myCursor.visible = true;
					myCursor.buttonMode = true;
					Mouse.show();
					//Mouse.show();
					//myCursor.x = 355
					//myCursor.y = 420

					//Add the cursor at the Mark point-08-08-2014
					myCursor.x = pns["inside" + tickX].marks.x
					myCursor.y = pns["inside" + tickX].marks.y

					rslt.x = 860;
					rslt.y = 530;
					rslt.gotoAndStop(3);
					this.addChild(rslt);
					trAgn.x = 860;
					trAgn.y = 630;
					trAgn.scaleX = 0.7;
					trAgn.scaleY = 0.7;
					//this.addChild(trAgn);
					firstPoint = false;
					secondPoint = false;
					traceLetter = true;
					penSprite.mouseEnabled = false;
					//trAgn.addEventListener(MouseEvent.CLICK,tragainHandler);
					//trAgn.addEventListener(MouseEvent.ROLL_OVER,tryAgainRollOver_Handler);
					//trAgn.addEventListener(MouseEvent.ROLL_OUT,tryAgainRollOut_Handler);
					this.removeEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
					//trace(" Wrong 3 ")
					correctOrWrng = "wrg"
					dispatchEvent(new Event("VALIDATION_HANDLER"))
					//MovieClip(this.parent.parent).isValidation("wrong");
				}

				pns.gotoAndStop(3);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;

				//myCursor.x = 355
				//myCursor.y = 420

				//Add the cursor at the Mark point-08-08-2014
				myCursor.x = pns["inside" + tickX].marks.x
				myCursor.y = pns["inside" + tickX].marks.y

				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}
		}
		
		//Alter work 31-7-2014
		private function autocall():void {

			if (tickX == 1) {
				trace("\t\t autocall .. 1 ");
				Mouse.show();
				myCursor.visible = true;
				myCursor.buttonMode = true;
								
				//this.removeChild(rslt);
				//this.removeChild(trAgn);
				
				this.removeChild(penSprite);
				pns.gotoAndStop(1);
				
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				
				if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						 
						Mouse.show();
						if(rslt.parent){
							this.removeChild(rslt);
						}
						 
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						 
						pns.gotoAndStop(1);
						 
						// isReset_bool = true;
						if(penSprite.parent){
							this.removeChild(penSprite);
						}
							 
						curPin1 = pns["inside" + tickX].pn1;
						 
						if (MovieClip(pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip(pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip(pns["inside" + tickX].pn2);
						}
						 
					}
					
					if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						if(rslt.parent){
							this.removeChild(rslt);
						}
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						pns.gotoAndStop(1);
						 if(penSprite.parent){
						this.removeChild(penSprite);
						 }
							
						curPin1 =  pns["inside" + tickX].pn1;
						if (MovieClip( pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip( pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip( pns["inside" + tickX].pn2);
						}
					}
			 
				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				//this.penSprite.graphics.lineStyle(lineThicknes, 0xffffff);
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}
			if (tickX == 2) {
				trace("\t\t autocall .. 1 ");
				Mouse.show();
				myCursor.visible = true;
				myCursor.buttonMode = true;
								
				//this.removeChild(rslt);
				//this.removeChild(trAgn);
				
				this.removeChild(penSprite);
				pns.gotoAndStop(2);
				
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				
				if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						 
						Mouse.show();
						if(rslt.parent){
							this.removeChild(rslt);
						}
						 
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						 
						pns.gotoAndStop(2);
						 
						// isReset_bool = true;
						if(penSprite.parent){
							this.removeChild(penSprite);
						}
							 
						curPin1 = pns["inside" + tickX].pn1;
						 
						if (MovieClip(pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip(pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip(pns["inside" + tickX].pn2);
						}
						 
					}
					
					if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						if(rslt.parent){
							this.removeChild(rslt);
						}
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						pns.gotoAndStop(1);
						if(penSprite.parent){
						this.removeChild(penSprite);
						 }
							
						curPin1 =  pns["inside" + tickX].pn1;
						if (MovieClip( pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip( pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip( pns["inside" + tickX].pn2);
						}
					}
			 
				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				//this.penSprite.graphics.lineStyle(lineThicknes, 0xffffff);
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 3) {
				trace("\t\t autocall .. 1 ");
				Mouse.show();
				myCursor.visible = true;
				myCursor.buttonMode = true;
								
				//this.removeChild(rslt);
				//this.removeChild(trAgn);
				
				this.removeChild(penSprite);
				pns.gotoAndStop(3);
				
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				
				if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff")
					{
						 
						Mouse.show();
						if(rslt.parent){
							this.removeChild(rslt);
						}
						 
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						 
						pns.gotoAndStop(3);
						 
						if(penSprite.parent){
							this.removeChild(penSprite);
						}
							 
						curPin1 = pns["inside" + tickX].pn1;
						 
						if (MovieClip(pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip(pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip(pns["inside" + tickX].pn2);
						}
						 
					}
					
					if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff")
					{
						if(rslt.parent){
							this.removeChild(rslt);
						}
						if(trAgn.parent){
							this.removeChild(trAgn);
						}
						pns.gotoAndStop(1);
						
						if(penSprite.parent){
						this.removeChild(penSprite);
						 }
							
						curPin1 =  pns["inside" + tickX].pn1;
						if (MovieClip( pns["inside" + tickX].pn2) == null ) {
							curPin2 = MovieClip( pns["inside" + tickX].pn25);	
						}else {
							curPin2 = MovieClip( pns["inside" + tickX].pn2);
						}
					}
			 
				penSprite = new Sprite();
				this.addChild(penSprite);
				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)
				//this.penSprite.graphics.lineStyle(lineThicknes, 0xffffff);
				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}
		}

		private function tragainHandler(e: MouseEvent): void {

			if (tickX == 1) {

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);
				pns.gotoAndStop(1);
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace(" RESET_HANDLER1234")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

			}

			if (tickX == 2) {

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);
				pns.gotoAndStop(2);
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace(" RESET_HANDLER_1234")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 3) {

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);
				pns.gotoAndStop(3);
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace(" RESET_HANDLER1234")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);

			}

		}

		public function resetHandler(e: MouseEvent): void {

			Mouse.show();
			if (tickX == 1) {

				trace("Frist Condition");

				if (letterToDeal == "i" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "f" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "j" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "k" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "t" && isWriteOn_Off == "writeHandOff") {
					//-------------starting position of my cursor--------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "x" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "y" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 4;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
					
				} else if (letterToDeal == "B" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 3;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				}  else if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 5;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				}else if (letterToDeal == "C" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 4;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				}
				else {

					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				}

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);
				pns.gotoAndStop(1);
				pns["inside" + tickX].gotoAndStop(1);
				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);

				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace("WriteCharacter RESET_HANDLER123 ")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 2) {

				trace("Second Condition");

				if (letterToDeal == "i" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "f" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "j" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "k" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "t" && isWriteOn_Off == "writeHandOff") {
					//-------------starting position of my cursor--------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "x" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "y" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 4;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				} else if (letterToDeal == "B" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 3;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				} else if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 5;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				} else if (letterToDeal == "C" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					totFrames = 4;
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					
				}
				else {

					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				}

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);

				pns.gotoAndStop(2);
				pns["inside" + tickX].gotoAndStop(1);

				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);


				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace("WriteCharacter RESET_HANDLER123 ")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}

			if (tickX == 3) {

				trace("******************3rd**** Condition");


				if (letterToDeal == "i" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "f" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "j" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "k" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "t" && isWriteOn_Off == "writeHandOff") {
					//-------------starting position of my cursor--------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "x" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "y" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				} else if (letterToDeal == "A" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					totFrames = 4;
				} else if (letterToDeal == "B" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					totFrames = 3;
				} else if (letterToDeal == "E" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					totFrames = 5;
				}else if (letterToDeal == "C" && isWriteOn_Off == "writeHandOff") {
					//--------------second position of my cursor------------//
					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
					totFrames = 4;
				}
				else {

					myCursor.x = pns["inside" + tickX].marks.x;
					myCursor.y = pns["inside" + tickX].marks.y;
				}

				//this.removeChild(rslt);
				this.rslt.visible = false;
				//this.removeChild(trAgn);
				myCursor.visible = true;
				myCursor.buttonMode = true;
				this.resetBtn.visible = false;
				//this.removeChild(resetBtn);
				this.removeChild(penSprite);

				pns.gotoAndStop(3);
				pns["inside" + tickX].gotoAndStop(1);

				curPin1 = pns["inside" + tickX].pn1;
				curPin2 = pns["inside" + tickX].pn2;
				penSprite = new Sprite();
				this.addChild(penSprite);


				this.setChildIndex(pns, numChildren - 1)
				this.setChildIndex(myCursor, numChildren - 1)

				trace("WriteCharacter RESET_HANDLER123 ")
				dispatchEvent(new Event("RESET_HANDLER")); // this function triggers Wriring TheSound Shell.as//

				this.penSprite.graphics.lineStyle(lineThicknes, 0xFFC233);
				this.addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
			}
		}
		//
		private function resetRollOver_Handler(e: MouseEvent): void {
			myCursor.visible = false
			Mouse.show();
		}
		private function resetRollOut_Handler(e: MouseEvent): void {
			myCursor.visible = true
			Mouse.hide();
		}
		private function tryAgainRollOver_Handler(e: MouseEvent): void {
			myCursor.visible = false
			Mouse.show();
		}
		private function tryAgainRollOut_Handler(e: MouseEvent): void {
			myCursor.visible = true
			//Mouse.hide();
		}

	}
}