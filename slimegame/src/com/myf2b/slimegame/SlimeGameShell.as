﻿package com.myf2b.slimegame 
{
	
	
	import flash.display.*;
	import flash.events.*;
	import flash.system.System;
	import com.myf2b.core.*;
	import com.greensock.TweenLite;
	import flash.utils.*;
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author Hara J N
	 */
	import com.myf2b.slimegame.SlimeGame;
	com.myf2b.slimegame.SlimeGame;
	
	public class SlimeGameShell extends MovieClip
	{
		
		public function SlimeGameShell(id:String = "1")
		{
			Core.useStaging = false;
			AppSettings.setSetting( "com.myf2b.core.Preferences", "prod_CDNs", ["http://cdn.myf2b.com", "http://cdn2.myf2b.com"] );
			AppSettings.setSetting( "com.myf2b.core.Preferences", "stage_CDNs", ["http://stage.cdn.myf2b.com", "http://stage.cdn2.myf2b.com"] );
           			
			Session.getInstance().userID = 13147; // lilius
			Session.getInstance().language = "en";
			Session.getInstance().role = "Parent";
			ScreenManager.getInstance().stage = stage;
			ScreenManager.getInstance().pushModule("com.myf2b.slimegame.SlimeGame", ["165"]);
		}

	}

}