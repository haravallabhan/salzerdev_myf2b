package com.myf2b.slimegame.manager.tween
{
	/*
	import com.gskinner.motion.easing.Back;
	import com.gskinner.motion.easing.Bounce;
	import com.gskinner.motion.easing.Circular;
	import com.gskinner.motion.easing.Cubic;
	import com.gskinner.motion.easing.Elastic;
	import com.gskinner.motion.easing.Exponential;
	import com.gskinner.motion.easing.Linear;
	import com.gskinner.motion.easing.Quintic;
	import com.gskinner.motion.easing.Sine;
	*/
	
	import com.gskinner.motion.easing.Back;
	import com.gskinner.motion.easing.Bounce;
	import com.gskinner.motion.easing.Circular;
	import com.gskinner.motion.easing.Cubic;
	import com.gskinner.motion.easing.Elastic;
	import com.gskinner.motion.easing.Exponential;
	import com.gskinner.motion.easing.Linear;
	import com.gskinner.motion.easing.Quintic;
	import com.gskinner.motion.easing.Sine;
	
	public class Ease
	{
		public static const EASE_IN :String = "easeIn";
		public static const EASE_OUT :String = "easeOut";
		public static const EASE_IN_OUT :String = "easeInOut"
		
		public static const NONE:String = "easeNone";
		
		public static const BACK :Class =  Back;
		public static const BOUNCE :Class = Bounce;
		public static const CIRCULAR :Class = Circular;
		public static const CUBIC :Class = Cubic;
		public static const ELASTIC :Class = Elastic;
		
		public static const EXPONENTIAL :Class = Exponential;
		public static const LINEAR :Class = Linear;
		public static const QUINTIC :Class = Quintic;
		public static const SINE :Class = Sine;
	}
}