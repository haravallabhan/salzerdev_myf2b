package com.myf2b.slimegame.manager 
{
	import com.myf2b.slimegame.ui.cover.Cover;
	import com.myf2b.slimegame.ui.cover.CoverStates;
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class CoverManager 
	{
		private static var _instance:CoverManager;
		private var _cover:Cover;
		
		public static function get instance():CoverManager {
			if (!_instance) {
				_instance = new CoverManager();
			}
			return _instance;
		}
		
		public function init( value:Cover ):void {
			_cover = value;
		}
		
		public function show( state:String ):void {
			_cover.show( state );
		}
		
		public function updateProgress( bytesLoaded:uint, bytesTotal:uint ):void{
			
			_cover.updateProgress(bytesLoaded, bytesTotal);
		}
		
		public function hide(state:String):void {
			_cover.hide(state);
		}
		
	}
}