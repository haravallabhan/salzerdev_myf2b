package com.myf2b.slimegame.manager
{
	public class LevelManager
	{
		public static var ITEMS_COUNT:uint = 0;
		
		public static var SET_NUMBER:uint = 1;
		public static var LEVEL:String = "A";
		public static var ROUND:uint = 1;
		
		public static var CORRECT_ANSWERS_COUNT:uint = 0;
		public static var WRONG_ANSWERS_COUNT:uint = 0;
		public static var HINT_USED:Boolean = false;
		public static var FIRST_ATTEMPT_ANSWERS_COUNT:uint = 0;
		
		public static var LEVEL_ANSWERS_COUNT:uint;
		
		public static var LEVEL_MASTERED:Boolean;
		
		public static var SUB_LEVEL:uint = 0;
		
		public static function reset():void{
			SET_NUMBER = 1;
			LEVEL = "A";
			ROUND = 1;
			
			CORRECT_ANSWERS_COUNT = 0;
			WRONG_ANSWERS_COUNT = 0;
			FIRST_ATTEMPT_ANSWERS_COUNT = 0;
			
			HINT_USED = false;
			
			SUB_LEVEL = 1;
		}
		
		public static function retry():void{
			CORRECT_ANSWERS_COUNT = 0;
			WRONG_ANSWERS_COUNT = 0;
			FIRST_ATTEMPT_ANSWERS_COUNT = 0;
			
			HINT_USED = false;
			
			ROUND++;
		}
		
		public static function nextSubLevel():void{
			SUB_LEVEL++;
		}
		
		public static function nextLevel():void{
			switch (LEVEL){
				case "A":
					LEVEL = "B";
					break;
				
				case "B":
					if (HINT_USED){
						LEVEL = "C";
					}
					break;
				
				default:
					break;
			}
			ROUND = 1;
			
			SUB_LEVEL = 1;
			FIRST_ATTEMPT_ANSWERS_COUNT = 0;
			CORRECT_ANSWERS_COUNT = 0;
			WRONG_ANSWERS_COUNT = 0;
			
			HINT_USED = false;
		}
	}
}