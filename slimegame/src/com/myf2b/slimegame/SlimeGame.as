﻿package com.myf2b.slimegame
{
	import com.myf2b.core.Core;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.Session;
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.BookmarkerEvent;
	import com.myf2b.slimegame.events.GameEvent;
	import com.myf2b.slimegame.events.SplashEvent;
	import com.myf2b.slimegame.manager.CoverManager;
	import com.myf2b.slimegame.manager.ReporterManager;
	import com.myf2b.slimegame.manager.SaveManager;
	import com.myf2b.slimegame.ui.VersionNumber;
	import com.myf2b.slimegame.ui.buttons.BackToGameButton;
	import com.myf2b.slimegame.ui.cover.Cover;
	import com.myf2b.slimegame.ui.cover.CoverStates;
	import com.myf2b.slimegame.ui.gameScreen.GameScreen;
	import com.myf2b.slimegame.ui.splashScreen.SplashScreen;
	import com.myf2b.slimegame.utils.XMLProvider;
	
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.SoundMixer;
	import flash.system.Security
	
	/**
	 * ...
	 * @author Hara JN
	 */
	public class SlimeGame extends MovieClip
	{
		private var _xml:XML;
		private var _versionNumber:VersionNumber;
		private var _splashScreen:SplashScreen;
		private var _gameScreen:GameScreen;
		private var _backToGamesBtn:BackToGameButton;
		private var _cover:Cover;
		private var _selectedLevelId:int;
		private var _selectedLetter:String = "m";
		private var _screensContainer:Sprite;
		private var _overlayContainer:Sprite;
		private var _uiContainer:Sprite;
		private var _pointsArr:Array;
		private var _savedData:Array;
		private var _masteredCount:uint = 0;
		private var _dataXML:XML;
		private var _letterXML:XMLList;
		private var _lettersArr:Array;
		private var _contentKey : String = "";
		public static var gameIntroProxy:GameIntroScreen
		private var _standAlone : Boolean = false;
		
		public function SlimeGame( contentKey : String = "-1" ) {
			XML.ignoreWhitespace = true;
			_contentKey = contentKey;
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );				
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function onAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
/*			if ( LoaderInfo(this.root.loaderInfo).parameters.xml_path != undefined ) 
				Config.DATA_URL = LoaderInfo(this.root.loaderInfo).parameters.xml_path as String;
			else 
				Config.DATA_URL = Core.filesURL + "/slime_game/" + _contentKey + "/game.xml";
			*/
			Core.log("LOADER INFO PARAMS " + this.root.loaderInfo.parameters.xml_path );
			Core.log("MY CONFING URL IS " + Config.DATA_URL );
			
			// FOR STAND ALONE BUILD ONLY
			Config.GAME_KEY = _contentKey;
			Config.GAME_XML_ID = _contentKey;
			
			ReporterManager.init();
			
			_screensContainer = new Sprite();
			addChild( _screensContainer );  
			
			_uiContainer = new Sprite();
			addChild( _uiContainer );
			
			_overlayContainer = new Sprite();
			addChild( _overlayContainer );
			
			_versionNumber = new VersionNumber();
			_uiContainer.addChild( _versionNumber );
			
			gameIntroProxy = new GameIntroScreen();
			gameIntroProxy.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", endOfIntroScreen_Handler)
			_screensContainer.addChild(gameIntroProxy);				
			
			//SaveManager.key = Config.SAVE_KEY;

		
		}
		
		/**
		 * This function is for setting up the game elements for each attempts.
		 * The system needs atleast 5 sets including the set with correct picks.
		 * And each set should have atleast 5 items <item> under <items> in the xml. 
		 * This function reads the XML for setting up the elements in the game.
		 */
		private function loadData():void {
			_cover = new Cover();
			_overlayContainer.addChild( _cover );
			CoverManager.instance.init( _cover );
			
			CoverManager.instance.show(CoverStates.LOADING_DATA);
			//Config.DATA_URL = "http://files3.myf2b.com/rhyme_slime/195/game.xml";
			//Config.DATA_URL = "game.xml"
			Config.DATA_URL = "http://stage2.myf2b.com/xmls/2/xml_feed?temp="+(Math.random() * 10000);//"http://localhost:8080/f2b/xmls/xml_feed7.xml"
			if (Config.DATA_URL) {
				XMLProvider.load( Config.DATA_URL, onXMLLoaded);
			}
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function onXMLLoaded(e:Event):void {
			
			_xml = XML(e.currentTarget.data);
			Config.MASTERY_PERCENTS = _xml.config.mastery.percents.(@numOfItems == _xml.@itemsCount );
			_selectedLevelId = _xml.sets.set.(@key == _selectedLetter.toLowerCase()).@order;
			trace("Order :"+_selectedLevelId);
			Config.CONFIG_XML = _xml.config;
			Config.SPLASH_GFX = _xml.@splashImage;
			Config.GAME_GFX = _xml.@gameImage;
			Config.SETS_XML = _xml.sets;
			Config.ITEMS_COUNT = _xml.@itemsCount;
			Config.RESULT_TEXT_LEVELA = _xml.config.game.round_complete.resultTextLevelA;
			Config.RESULT_TEXT_LEVELA_AUDIO = _xml.config.game.round_complete.resultTextLevelAAudio;

			Config.RESULT_TEXT_LEVELB = _xml.config.game.round_complete.resultTextLevelB;
			Config.RESULT_TEXT_LEVELB_AUDIO = _xml.config.game.round_complete.resultTextLevelBAudio;
			
			Config.FAILURE_INFO = _xml.config.game.round_complete.failureInfo;
			Config.FAILURE_INFO_AUDIO = _xml.config.game.round_complete.failureInfoAudio;
			Config.LEVEL_FAILED_TITLE = _xml.config.game.round_complete.levelFailedTitle; 
			Config.LEVELA_SUCCESS_TITLE = _xml.config.game.round_complete.levelASucceedTitle;
			Config.LEVELB_SUCCESS_TITLE = _xml.config.game.round_complete.levelBSucceedTitle;
			//Config.NO_OF_ROUNDS_IN_LEVEL = 5;
			Config.USERPOPUP_TITLE=  _xml.config.game.popupForTeacherTitle;
			Config.USERPOPUP_INFO =  _xml.config.game.popupInfoForTeacher;

			if(Session.getInstance().role == "Teacher"){
				Config.IS_A_TEACHER = false;
			}else if(Session.getInstance().role == "Parent"){
				Config.IS_A_TEACHER = true;
			}
			SaveManager.getInstance().addEventListener( BookmarkerEvent.SHOW_PROGRESS, onStartLevel );
			SaveManager.load();			 
		}
		
		private function endOfIntroScreen_Handler(e:Event):void {
			
			
			_selectedLetter = gameIntroProxy.selectedObject;
			trace("_selectedLetter=" + _selectedLetter);
				loadData();
		}

		
		private function showSplash( e : BookmarkerEvent ) : void {
			
			if ( e != null ) _splashScreen = new SplashScreen( e.data  as XML );
			else _splashScreen = new SplashScreen(SaveManager.getBookmarkXML());
			
			_splashScreen.addEventListener(SplashEvent.START_LEVEL, onStartLevel);
			_splashScreen.addEventListener(SplashEvent.BACK_TO_MAP, onBackToGames);			
			_screensContainer.addChild( _splashScreen );		
		}
		
		private function onStartLevel( e : BookmarkerEvent):void {
			
			clearScreensContainer();
			
			//_selectedLevelId = (e.currentTarget as SplashScreen).selectedLevelId;
			//_selectedLevelId =0;
			_gameScreen = new GameScreen( _xml,  _selectedLevelId,  _selectedLetter);
			_gameScreen.addEventListener( GameEvent.BACK_TO_SPLASH, onBackToSplash);
			_gameScreen.addEventListener( GameEvent.BACK_TO_GAMES, onBackToGames);
			_screensContainer.addChild( _gameScreen );
		}
		
		private function onBackToSplash(e:Event):void {
			_gameScreen.reset();
			clearScreensContainer();
			showSplash(null);
		}
		
		private function onBackToGames(e:Event):void {
			if (_splashScreen) {
				_splashScreen.reset();
			}
			if ( _gameScreen ){
				_gameScreen.reset();
			}
			clearScreensContainer();
			SoundMixer.stopAll();
			ScreenManager.getInstance().pop();
		}
		
		private function clearScreensContainer():void {
			while ( _screensContainer.numChildren ) {
				_screensContainer.removeChildAt(0);
			}
		}
		
	}
}


