package com.myf2b.slimegame.ui.cover
{
	import flash.display.Sprite;
	import flash.text.TextField;
	
	//import fl.controls.ProgressBar;
	
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class Cover extends Sprite
	{
		public var tf:TextField;
		//public var pb:ProgressBar;
		
		private var _textsArr:Array;
		private var _currentState:String;
		
		public function Cover() {
			//pb.mode = "manual";
			_textsArr = new Array();
			_textsArr[CoverStates.LOADING_DATA] = "Loading data.";
			_textsArr[CoverStates.DATA_ERROR] = "Data error.";
		}
		
		public function show( value:String ):void {
			//pb.reset();
			_currentState = value;
			tf.text = _textsArr[value];
			visible = true;
		}
		
		public function updateProgress(bytesLoaded:uint, bytesTotal:uint):void{
			//pb.setProgress(bytesLoaded, bytesTotal);
		}
		
		public function hide( value:String ):void {
			if (value == _currentState) {
				visible = false;
			}
		}
		
	}
}