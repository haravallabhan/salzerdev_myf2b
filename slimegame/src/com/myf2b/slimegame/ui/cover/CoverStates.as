package com.myf2b.slimegame.ui.cover
{
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class CoverStates 
	{
		public static const LOADING_DATA:String = "loadingData";
		public static const DATA_ERROR:String = "dataError";
	}

}