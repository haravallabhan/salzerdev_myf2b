﻿package com.myf2b.slimegame.ui.assets
{
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.SplashEvent;
	import com.myf2b.slimegame.manager.CoverManager;
	import com.myf2b.slimegame.manager.LevelManager;
	import com.myf2b.slimegame.manager.ReporterManager;
	import com.myf2b.slimegame.manager.SaveManager;
	import com.myf2b.slimegame.manager.SoundManager;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.buttons.BaseButton;
	import com.myf2b.slimegame.ui.buttons.LevelButton;
	import com.myf2b.slimegame.ui.cover.CoverStates;
	//import com.myf2b.slimegame.ui.effects.Fireworks;
	import com.myf2b.slimegame.ui.splashScreen.TrophyScreen;
	import com.myf2b.slimegame.utils.Random;
	import com.gskinner.motion.GTween;
	import com.myf2b.core.Reporter;
	import com.myf2b.ui.*;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	public class SplashAssets extends EventDispatcher
	{
		private var _itemsArr:Array;
		private var _intervalId:Number;
		private var _counter:Number;
		private var _tmpLvlBtn:LevelButton;
		private var _selectedLevel:LevelButton;
		private var _disableSounds:Boolean = false;
		private var _buttonsArr:Array;
		private var _randIndexObj:Object;
		private var _masteredCount:uint;
		private var _previousId:int;
		private var _mastered:MovieClip;
		private var _assetMc:MovieClip;
		private var _isMastered:Boolean;
		private var _trophyScreen:TrophyScreen;
		//private var _fireworks:Fireworks;
		
		public function SplashAssets(assetMc:MovieClip)
		{
			/*
			0 - not btn
			1 - normal btn
			2 - level btn
			*/
			
			_disableSounds = false;
			
			_counter = 0;
			
			_randIndexObj = new Object();
			_randIndexObj.soundsCount = Config.CONFIG_XML.splash.bgSounds.sound.length();
			
			_assetMc = assetMc;
			
			
			var selected = assetMc.infoText.text;
		}
		
		private function onSoundsComplete(e:Event):void {
			if ( LevelManager.LEVEL_MASTERED ) {
				playBgSound();
				
				//_fireworks = new Fireworks();
				//_assetMc.addChild( _fireworks );
				
				LevelManager.LEVEL_MASTERED = false;
			}
				
			CoverManager.instance.hide( CoverStates.LOADING_DATA );
			//starts showing items
			_intervalId = setInterval(show, 100);
		}
		
		private function playBgSound():void{
			SoundManager.play( getRandomValue("sound") );
		}
		
		private function getRandomValue(value:String):String{
			while( _randIndexObj.tmpId == _randIndexObj.lastId ) {
				_randIndexObj.tmpId = Math.round(  Math.random() * ( _randIndexObj.soundsCount - 1));
			}
			_randIndexObj.lastId = _randIndexObj.tmpId;
			return value + _randIndexObj.lastId;
		}
		
		public function disableMouse( tf:TextField):void{
			tf.mouseEnabled = false;
			tf.mouseWheelEnabled = false;
		}
		
		private function onSpeaker(e:MouseEvent):void{
			ReporterManager.interaction( { action: "click", actionObject:"splashSpeaker" } );
			SoundManager.play("splashInfo");
		}
		
		private function onBackToGames(e:MouseEvent):void{
			ReporterManager.interaction( { action: "click", actionObject:"splashBackToGamesButton" } );
			dispatchEvent(new Event( SplashEvent.BACK_TO_MAP ))
		}
		
		private function show():void {
			if ( !_disableSounds && ( _itemsArr[_counter].type == 2 ) ) {
				SoundManager.play("splashItem", 500);
			}
			
			TweenManager.tween( _itemsArr[_counter].mc, 1, { scaleX:1, scaleY:1, alpha:1 } );
			_counter++;
			if (_counter == _itemsArr.length) {
				clearInterval(_intervalId);
			}
		}
		
		private function onLevelClick(e:MouseEvent):void {
			if ( _selectedLevel ) {
				_selectedLevel.unselect();
			}
			_selectedLevel = (e.currentTarget as LevelButton);
			_selectedLevel.select();
			
			SoundManager.playSequence( _selectedLevel.name + "_" );
			
			ReporterManager.interaction( { action: "click" , actionObject: "level_" + _selectedLevel.name} );
		}
		
		private function onGoClick(e:MouseEvent):void{
			if ( !_selectedLevel ) {
				for ( var i:uint = 0; i < _buttonsArr.length; i++ ) {
					if ( _buttonsArr[i].mc.currentFrame == 1 ) {
						_selectedLevel = _buttonsArr[i];
						_selectedLevel.select();
						break;
					}
				}
			}
			ReporterManager.interaction( { action: "click" , actionObject: "goButton_level_" + _selectedLevel.name} );
			dispatchEvent(new Event(SplashEvent.START_LEVEL));
		}
		
		public function set isMastered(value:Boolean):void{
			if ( value ) {
				showMasteredStar();
			}
		}
		
		public function set levels(arr:Array):void{
			_masteredCount = 0;
			
			for (var i:uint = 0; i < arr.length; i++) {
				
				if ( arr[i] == 1 ) {
					_masteredCount++;
					_buttonsArr[i].mc.gotoAndStop(2);
				}
			}
			
			if ( _masteredCount == arr.length  )
			{
				//SaveManager.saveMastered();
				showMasteredStar();
			}
			
			if ( _masteredCount == 1 ) {
				var wordsArr:Array = Config.CONFIG_XML.splash.masteredText.split(" ");
				for (var j:uint = 0; j  < wordsArr.length; j++ ) {
					if ( wordsArr[j].charAt( wordsArr[j].length - 1 ) == "s" ){
						wordsArr[j] = wordsArr[j].slice(0, wordsArr[j].length - 1 );
					}
				}
				_assetMc.mastered.labelTf.text = wordsArr.join(" ");
			}
			else{
				_assetMc.mastered.labelTf.text = Config.CONFIG_XML.splash.masteredText;
			}
			
			_mastered.tf.text = _masteredCount.toString();
		}
		
		private function showMasteredStar():void{
			new BaseButton(_assetMc.finishBtn);
			
			_assetMc.finishBtn.addEventListener( MouseEvent.CLICK, onShowTrophy );
			_assetMc.finishBtn.filters = [ new GlowFilter(0xffff00, 20, 20) ];
		}
		
		private function onShowTrophy(e:MouseEvent):void{
			dispatchEvent(new Event(SplashEvent.SHOW_TROPHY));
		}
		
		public function playAgain():void {
			for (var i:uint = 0; i < _buttonsArr.length; i++ ) {
				_buttonsArr[i].mc.gotoAndStop(1)
			}
		}
		
		public function get selectedLevel():int {
			return _buttonsArr.indexOf(_selectedLevel)
		}
		
		public function reset():void {
			_disableSounds = true;
			SoundManager.reset();
			clearInterval(_intervalId);
		}
		
	}
}