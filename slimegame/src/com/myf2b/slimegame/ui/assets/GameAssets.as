﻿package com.myf2b.slimegame.ui.assets
{
	import com.gskinner.motion.GTween;
	import com.greensock.text.SplitTextField;
	import com.myf2b.core.Session;
	import com.myf2b.core.CoreSound;
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.GameEvent;
	import com.myf2b.slimegame.manager.CoverManager;
	import com.myf2b.slimegame.manager.LevelManager;
	import com.myf2b.slimegame.manager.ReporterManager;
	import com.myf2b.slimegame.manager.SaveManager;
	import com.myf2b.slimegame.manager.SoundManager;
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.buttons.BaseButton;
	import com.myf2b.slimegame.ui.cover.CoverStates;
	import com.myf2b.slimegame.ui.gameScreen.GameItem;
	import com.myf2b.slimegame.ui.gameScreen.GunFireButton;
	import com.myf2b.slimegame.ui.gameScreen.JellyBullet;
	import com.myf2b.slimegame.ui.gameScreen.JellyGun;
	import com.myf2b.slimegame.ui.gameScreen.LevelInfo;
	import com.myf2b.slimegame.ui.gameScreen.LevelStatus;
	import com.myf2b.slimegame.ui.gameScreen.LevelTarget;
	import com.myf2b.slimegame.ui.gameScreen.Points;
	import com.myf2b.slimegame.ui.overlay.LevelEnd;
	import com.myf2b.slimegame.ui.overlay.UserPopup;
	import com.myf2b.ui.*;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.SoundMixer;
	import flash.text.TextField;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.filters.BitmapFilterQuality;
	
	
	public class GameAssets extends EventDispatcher
	{
		private var _currentX:Number = 0;
		private var _currentY:Number = 0;
		private var _item:GameItem;
		private var _dataXml:XML;
		private var _colsCount:uint = 0;
		private var _counter:uint = 0;
		private var _boundsObj:Object;
		private var _verticalSpacing:uint = 0;
		private var _horizontalSpacing:uint = 0;
		private var _intervalId:uint;
		private var _itemsArr:Array;
		private var _basketsObj:Object;
		private var _lettersArr:Array;
		private var _deltaX:Number;
		private var _deltaY:Number;
		private var _currentItem:GameItem;
		private var _topChild:MovieClip;
		private var _levelEnd:LevelEnd;
		private var _disableSounds:Boolean = false;
		private var _mouseXY:Object;
		private var _gameItemsObj:Object;
		private var _gameBg:MovieClip;
		private var _loadedDataCounter:uint = 0;
		private var _randIndexObj:Object;
		private var _playingItem:GameItem;
		private var _isInBasketTween:Boolean = false;
		private var _isInDrag:Boolean = false;
		private var _basketsContainer:Sprite;
		private var _mouseInputGrabber:Sprite;
		private var _basketSoundCounter:uint = 0;
		
		private var _assetMc:MovieClip;
		private var _levelId:uint;
		private var _levelInfo:LevelInfo;
		private var _itemsContainer:Sprite;
		private var _points:Points;
		private var _levelTarget:LevelTarget;
		private var _dataObj:Object;
		private var _levelKey:String;
		private var _jellyGun:JellyGun;
		private var _distance : Number
		private var _angle : Number;
		private var _jellyBullet :JellyBullet;
		private var _bulletObj:Object;
		private var _isLevelComplete : Boolean;
		private var _fireButton:GunFireButton;
		private var _targetPointsArr:Array;
		private var _currentIndex:Number = 0;
		private var _currentItemsCounter:uint = 0;
		private var _distractItemsCounter:uint = 0;
		
		private var _functObj:Object;
		private var _gunPnt:Point;
		private var _midPnt:Point;
		private var _tmpTarget:GameItem;
		private var _currentTarget:GameItem;
		private var _randomIndex:uint;
		private var _randIndex:uint;
		private var _toPnt:Point;
		private var _itemMiddlePnt:Point;
		private var _time:Number;
		private var _settingUpNextLevel:Boolean;
		private var _loadedImagesCount:uint;
		private var shotCount:uint;
		private var attemptCount:uint;
		private var _levelStatus:LevelStatus;
		private var _starAttempt:Boolean = false; 
		
		private var _userPopup:UserPopup;
		private var _sync : TextFieldSoundSync;
		
		private var instTxtX:Number = 518;
		private var instTxtY:Number = 30;
		private var push_arr:Array ;
		private var _roundCount:uint;
		private var soundsArr:Array = [];
		
		public var isRoundInfor_str:String = "";
		public var isRndInform_bool:Boolean = false;
		
		
				
		public function GameAssets( assetMc:MovieClip, dataXml:XML, levelId:uint ) {
			_assetMc = assetMc;
			_dataXml = dataXml;
			_levelId = levelId;
			_dataObj = prepareDataToLoad();
			_roundCount = 0;
			
			
			
			new BaseButton(_assetMc.speakerBtn);
			new BaseButton(_assetMc.backToGamesBtn);


			_assetMc.topBarBg.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			_mouseXY = new Object();
			_disableSounds = false;
			_loadedDataCounter = 0;
			
			_levelKey = _dataXml.sets.set[ _levelId ].@key;
			trace("levelKey--------------"+_levelKey);
		 
			LevelManager.reset();
			LevelManager.LEVEL_ANSWERS_COUNT = 15;
			LevelManager.SET_NUMBER = parseInt(_dataXml.sets.set[_levelId].@order) + 1;
			
			_itemsArr = new Array();
			
			_item = new GameItem(null);
			
			_boundsObj = new Object();
			_boundsObj.width = 787;
			_boundsObj.height = 400;
			_boundsObj.itemWidth = _item.width;
			_boundsObj.itemHeight = _item.tf.y + 2 * _item.tf.height;
			_levelInfo = new LevelInfo( _assetMc.levelInfo );
			_levelInfo.update();
			_levelInfo.y += 50;
			_levelInfo.alpha = 0;
			TweenManager.tween(_levelInfo.mc, 1, {alpha : 1, y : _levelInfo.y - 50} );
			
			_levelStatus = new LevelStatus(_assetMc.levelStatus, false);
		
			/**/
			if(Config.IS_A_TEACHER){
				_assetMc.userIcon.gotoAndStop(2);// teacherIcon.visible = true;
				//_assetMc.userIcon.parentIcon.visible = false;
			}else {
				_assetMc.userIcon.gotoAndStop(1);
				//_assetMc.userIcon.teacherIcon.visible = false;
				//_assetMc.userIcon.parentIcon.visible = true;
			}
			_assetMc.userIcon.buttonMode = true;
			
			setDefaultLang();
			
			_assetMc.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			_assetMc.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);
			
			_assetMc.speakerBtn.addEventListener(MouseEvent.CLICK, onSpeaker);
			var instructionText:String = Config.CONFIG_XML.game.infoText;

			_assetMc.tf.text = "";
			_assetMc.tf.text = instructionText;
			_sync = Ui.EnableTextSync(_assetMc.tf, _dataXml);
			var mcX:int = (_assetMc.tf.x + _assetMc.tf.getLineMetrics(_assetMc.tf.numLines - 1).width);
			var mcY:int = (_assetMc.tf.y + _assetMc.tf.textHeight);
			_assetMc.dealLetter_mc.buttonMode = true;
			_assetMc.dealLetter_mc._txt.text = _levelKey;
			_assetMc.dealLetter_mc._txt.mouseEnabled = false;			
			_assetMc.dealLetter_mc.addEventListener(MouseEvent.CLICK, playLetterSound);
			_assetMc.dealLetter_mc.x = mcX;
			_assetMc.dealLetter_mc.y = mcY ;
			_assetMc.dealLetter_mc.x = mcX + 15; 
			if (_assetMc.tf.numLines == 1) {
				_assetMc.dealLetter_mc.y = mcY - 43;
			}else{
				_assetMc.dealLetter_mc.y = mcY - 33; 
			}			
			

			_assetMc.speakerBtn.mouseChildren = false;			
			_assetMc.speakerBtn.buttonMode=true;
			_assetMc.langBtn.buttonMode=true;
			
			
			_assetMc.backToGamesBtn.addEventListener(MouseEvent.CLICK, onBackToGames);
			/**/
			
			disableMouse( _assetMc.tf );
			
			setupContainer();
			
			soundsArr = [ { key:"gameItem", url: Config.CONFIG_XML.game.itemSound },
				{key:"gameInfo0", url: Config.CONFIG_XML.game.infoSound }, 
				{key:"gameInfo1", url: _dataXml.sets.set[_levelId].sounds.sound}, 
				{key:"letterSound", url: _dataXml.sets.set[_levelId].sounds.sound}, 
				{ key: "mastered", url: Config.CONFIG_XML.game.masteredSound },
				{ key:"contactSound", url: Config.CONFIG_XML.game.contactSounds }];
			//trace(" *(*(*(  "+soundsArr)
			var j:uint = 0;
			for ( j = 0; j < Config.CONFIG_XML.game.successSounds.sound.length(); j++ ) {
				soundsArr.push( { key:"success" + j, url: Config.CONFIG_XML.game.successSounds.sound[j] } );
			}
			for ( j = 0; j < Config.CONFIG_XML.game.failSounds.sound.length(); j++ ) {
				soundsArr.push( { key:"fail" + j, url: Config.CONFIG_XML.game.failSounds.sound[j] } );
			}
			SoundManager.add( soundsArr , onGameSoundsComplete );
			/**/
		
			_jellyGun = new JellyGun( _assetMc.jellyGun );
			
			_assetMc.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_assetMc.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			
			_itemsContainer.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_itemsContainer.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			
			_levelEnd = new LevelEnd( _assetMc.levelEnd,_dataXml );
			_levelEnd.hide();
			
			_userPopup = new UserPopup(_assetMc.userPopup,_assetMc);
			_userPopup.hide();
			
			_randIndexObj = new Object();
			_randIndexObj["success"] = { count: Config.CONFIG_XML.game.successSounds.sound.length() };
			_randIndexObj["fail"] = { count: Config.CONFIG_XML.game.failSounds.sound.length() };
			
			_bulletObj = new Object();
			
			_fireButton = new GunFireButton( _assetMc.guns.gunFireButton );
			_fireButton.addEventListener( GunFireButton.FIRE, onGunFire);
			shotCount = 0;
			
		}
		private function setDefaultLang():void {	
			if(Session.getInstance().language == "en"){
				_assetMc.langBtn.gotoAndStop(1);
			}else if(Session.getInstance().language == "es"){
				_assetMc.langBtn.gotoAndStop(2);		
			}
		}
		
		private function changeLang(e : MouseEvent ):void {
			if(Session.getInstance().language == "en"){
				Session.getInstance().language = "es";
			}else{
				Session.getInstance().language = "en";
			}
			setDefaultLang();

		}
		
		private function showInfoForUser(e : MouseEvent ):void {			
			_userPopup.show();
			// _assetMc.tf 
			 
		};
		
		private function playLetterSound(e : MouseEvent ):void {
			playSound("letterSound"); 
		};
		
		
		private function onMouseOver( e : MouseEvent ):void {
			onMouseUp(null);
		}
		
		private function onMouseDown( e : MouseEvent ):void {

			if ( !( e.target is GameItem ) && e.target != _fireButton.mc && 
				e.target != _assetMc.speakerBtn && e.target != _assetMc.tf && e.target != _assetMc.langBtn &&
				e.target != _assetMc.dealLetter_mc && e.target != _assetMc.backToGamesBtn && 
				e.target != _assetMc.userIcon && e.target != _assetMc.userPopup.userPopupClose && 
				e.target != _assetMc.topBarBg && !(e.target is Sprite)) {

				_assetMc.addEventListener(Event.ENTER_FRAME, onFrame);
			}
		}
		
		private function onMouseUp( e : MouseEvent ) : void {
			if ( _assetMc.hasEventListener( Event.ENTER_FRAME ) ) {
				_assetMc.removeEventListener( Event.ENTER_FRAME, onFrame );
			}
		}
		
		private function onGunFire(e:Event) : void {
			if ( _settingUpNextLevel ) return;
			
			_gunPnt = new Point( _assetMc.jellyGun.gunEnd.x, _assetMc.jellyGun.gunEnd.y );
			_gunPnt = _assetMc.jellyGun.localToGlobal( _gunPnt );
			
			_midPnt = new Point( _assetMc.jellyGun.x, _assetMc.jellyGun.y );
			
			_targetPointsArr = new Array();
			_functObj = new Object();
			if ( Math.abs( _gunPnt.x - _midPnt.x ) < 1 ) {
				_currentTarget = _itemsArr[1];
				for ( var k:uint = ( _currentTarget.y + _itemsContainer.y - _currentTarget.bg.height / 2 ) ;
					k < ( _currentTarget.y + _itemsContainer.y + _currentTarget.bg.height / 2 ); k++ ) {
					_targetPointsArr.push( new Point( _currentTarget.x + _itemsContainer.x, k ) );
				}
			}
			
			_functObj.a = (_gunPnt.y - _midPnt.y) / (_gunPnt.x - _midPnt.x);
			_functObj.b = _midPnt.y - ( _functObj.a * _midPnt.x );
			
			for ( var j:uint = 0; j < _itemsArr.length; j++ ) {
				_tmpTarget = _itemsArr[j];
				_itemMiddlePnt = _itemsContainer.localToGlobal( new Point( _itemsArr[j].x, _itemsArr[j].y ));
				
				for ( var i:int = _itemMiddlePnt.x - _tmpTarget.bg.width / 2; i < _itemMiddlePnt.x + _tmpTarget.bg.width / 2; i++ ) {
					if ( _tmpTarget.bg.hitTestPoint( i, _functObj.a * i + _functObj.b, true ) ) {
						_currentTarget = _tmpTarget;
						_targetPointsArr.push(new Point( i, _functObj.a * i + _functObj.b ) );
					}
				}
			}
			
			if ( _targetPointsArr.length > 0 ) {
				_randomIndex = Math.round( Math.random() * ( _targetPointsArr.length - 1 ) );
				
				_toPnt = _targetPointsArr[ _randomIndex ];
				
				_bulletObj.gunEndPoint = new Point( _assetMc.jellyGun.gunEnd.x, _assetMc.jellyGun.gunEnd.y );
				_bulletObj.gunEndPoint = _assetMc.jellyGun.localToGlobal(_bulletObj.gunEndPoint);
				_bulletObj.distance = Point.distance( _bulletObj.gunEndPoint, _targetPointsArr[ _randomIndex ] );
				
				_jellyBullet = new JellyBullet();
				_jellyBullet.x = _bulletObj.gunEndPoint.x;
				_jellyBullet.y = _bulletObj.gunEndPoint.y;
				_jellyBullet.rotation = Math.random() * 360;
				
				_assetMc.addChildAt( _jellyBullet, _assetMc.getChildIndex( _itemsContainer ) + 1 );
				
				_currentItem = _currentTarget;
				
				check( _jellyBullet );
				
				TweenManager.tween( _jellyBullet, _bulletObj.distance / 1500, { x: _toPnt.x, y: _toPnt.y }, 
					{ease:Ease.LINEAR, easeType: Ease.NONE }, { onComplete: onBulletComplete } );
			}else {
				_toPnt = new Point(( _itemsContainer.y - _functObj.b ) / _functObj.a, _itemsContainer.y);
				
				_bulletObj.gunEndPoint = new Point( _assetMc.jellyGun.gunEnd.x, _assetMc.jellyGun.gunEnd.y );
				_bulletObj.gunEndPoint = _assetMc.jellyGun.localToGlobal(_bulletObj.gunEndPoint);
				_bulletObj.distance = Point.distance( _bulletObj.gunEndPoint, new Point(_toPnt.x, _toPnt.y) );
				
				_jellyBullet = new JellyBullet();
				_jellyBullet.x = _bulletObj.gunEndPoint.x;
				_jellyBullet.y = _bulletObj.gunEndPoint.y;
				_jellyBullet.rotation = Math.random() * 360;
				
				_assetMc.addChildAt( _jellyBullet, _assetMc.getChildIndex( _itemsContainer ) + 1 );
				
				TweenManager.tween( _jellyBullet, _bulletObj.distance / 1500, { x: _toPnt.x, y: _toPnt.y }, 
					{ease:Ease.LINEAR, easeType: Ease.NONE }, { onComplete: onBulletComplete } );
			}
			
		}
		
		private function onLevelTargetMouseDown(e:Event):void{
			//_levelTarget.playSound();
		}
		
		/**
		 *This function loads the assets for the particular level. The distract object is for loading all the wrong items for the level.  
		 * @return 
		 * 
		 */
		private function prepareDataToLoad():Object {
			var randNum:uint;
			var levelXml:XML = _dataXml.sets.set[_levelId];
			var shuffleIdsArr:Array = new Array();
			for ( var k:uint = 0; k < levelXml.items.item.length(); k++ ) {
				shuffleIdsArr.push(k);
			}
			randomize(shuffleIdsArr);
			var randObj:Object = new Object();
			//current level items ids array
			randObj.currentIdsArr = new Array();
			randObj.currentIdsArr = shuffleIdsArr;
			randObj.distractIdsArr = new Array();
			
			var distaractObj:Object;
			var idsArr:Array = new Array();
			
			for ( var i:uint = 0; i < _dataXml.sets.set.length()-1; i++ ) {
				
				distaractObj = new Object();
				while ( idsArr.indexOf( randNum ) != -1 || randNum == _levelId ) {
					randNum = Math.round( Math.random() * ( _dataXml.sets.set.length() - 1 ) );
				}
				idsArr.push( randNum );
				distaractObj.levelId = randNum;
				distaractObj.itemId = Math.round( Math.random() * ( _dataXml.sets.set[ distaractObj.levelId ].items.item.length() - 1 ) );
				randObj.distractIdsArr.push( distaractObj );
				
			}
			
			return randObj;
		}
		
		/**
		 *This function creats a container for the items to be shot 
		 * 
		 */
		private function setupContainer():void{
			_itemsContainer = new Sprite();
			_itemsContainer.graphics.beginFill(0x000000, 0);
			_itemsContainer.graphics.drawRect(0, 0, Config.GAME_AREA_WIDTH, Config.GAME_AREA_HEIGHT);
			_itemsContainer.graphics.endFill();
			_itemsContainer.x = 140;
			_itemsContainer.y = 135;
			
			var index : uint = _assetMc.getChildIndex( _assetMc.jellyGun );
			_assetMc.addChildAt( _itemsContainer, index - 1 )
		}
		
		private function onGameSoundsComplete(e:Event):void{
			_loadedDataCounter++;
			
			if ( _loadedDataCounter == 1 ) {
				setupItems();
			}
		}
		
		public function disableMouse( tf:TextField):void{
			//tf.mouseEnabled = false;
			tf.mouseWheelEnabled = false;
		}
		
		private function randomize(arr:Array):void{
			var random:Number;
			var tmpObj:Object;
			
			for ( var i:uint = 0; i < arr.length; i++ ) {
				random = Math.round( Math.random() * (arr.length - 1) );
				tmpObj = arr[random];
				arr[random] = arr[i];
				arr[i] = tmpObj;
			}
		}
		
		private function resetValues():void{
			_itemsArr = new Array();
			_distractItemsCounter = 0;
		}
		
		private function setupItems():void {
			
			_roundCount ++;
			var roundFlyDone = createRoundInformation("roundStart");
			while ( _itemsContainer.numChildren ) {
				_itemsContainer.removeChildAt(0);
			}
			//trace("====================================SETTING UP ITEMS FOR THE SLIME GAME===============================");
			if(roundFlyDone){
			
				_counter = 0;
				_itemsArr = new Array();
				_loadedImagesCount = 0;
				_assetMc.roundFly_mc.roundAnim_mc._txt.text = String(Number(_roundCount))	
				_colsCount = Math.ceil( Config.ITEMS_COUNT / 2);
				_horizontalSpacing = ( _boundsObj.width - _colsCount * _boundsObj.itemWidth ) / (_colsCount + 1);
				_verticalSpacing = ( _boundsObj.height - 2 * _boundsObj.itemHeight ) / ( ( LevelManager.ITEMS_COUNT / _colsCount) + 1);
				_currentX = _horizontalSpacing;
				_currentY = _verticalSpacing - 20;
				var idsArr:Array = [ { levelId: _levelId, itemId: _dataObj.currentIdsArr[_currentItemsCounter] } ];
				_currentItemsCounter++;
				for ( var j:uint = _distractItemsCounter; j < _distractItemsCounter+2; j++ ) {
						idsArr.push( { levelId: _dataObj.distractIdsArr[j].levelId, itemId: _dataObj.distractIdsArr[j].itemId } );
				}
				/* 
				 * For each iteration the system needs 2 distractors. Initially the number of sets was 10 and hence for the 5 attempts 
				 * the system need 10 distractors and hence the variable _distractItemsCounter was incrimented by 2 without any condition.
				 * Now the number of sets could vary and so the distractors count should go beyond the total number of sets. So we have added 
				 * the condition that when the difference between the incrimented distractors item and the toal sets should be greated than 1 to
				 * make the actual distractor incrimination.
				 * */
				if( (_dataXml.sets.set.length()-1) - (_distractItemsCounter+2) > 1){
						_distractItemsCounter += 2;
				}else if((_dataXml.sets.set.length()-1) - (_distractItemsCounter+2) == 1) {
						_distractItemsCounter += 1;
				}
				randomize( idsArr );
				LevelManager.ITEMS_COUNT = idsArr.length;
				LevelManager.CORRECT_ANSWERS_COUNT = idsArr.length;
				
				for ( var i:uint = 0; i < idsArr.length; i++ )
				{
					if (i > 0) {
						if ( ( i % _colsCount ) == 0  ) {
							_currentX = _horizontalSpacing;
							_currentY += _boundsObj.itemHeight + _verticalSpacing + 20;
						}
						else{
							_currentX += _boundsObj.itemWidth + _horizontalSpacing;
						}
					}
					_item = new GameItem( _dataXml.sets.set[ idsArr[i].levelId ].items.item[ idsArr[i].itemId ], i , _dataXml.sets.set[ idsArr[i].levelId ].@key );
					_item.scaleX = _item.scaleY = 0;
					_item.alpha = 0;
					_item.moveTo( 10 + i * 337, 129 );
					_item.addEventListener( GameItem.IMAGE_LOADED, onImageLoaded );
					_item.addEventListener( MouseEvent.MOUSE_DOWN, onItemMouseDown );
					_itemsContainer.addChild( _item );
					_itemsArr.push( _item );
				}
				
				if ( LevelManager.ITEMS_COUNT == Config.ITEMS_COUNT ) {
					if ( _itemsArr.length ) {
						_intervalId = setInterval(show, 100);
					}
				}
				else{
					CoverManager.instance.show( CoverStates.DATA_ERROR );
				}
			
				attemptCount=0;
				_starAttempt = false;
			}

		}
		
		private function show():void{
			if ( !_disableSounds ) {
				SoundManager.play("gameItem", 500);
			}
			TweenManager.tween( _itemsArr[_counter], 1, { scaleX:1, scaleY:1, alpha:1 }, null, { onComplete: onShowTweenComplete } );
			_counter++;
			if (_counter == _itemsArr.length) {
				clearInterval(_intervalId);
			}
		}
		
		private function onShowTweenComplete( tween:GTween ) :void{
			if ( LevelManager.LEVEL == "A" ) 
			{
				TweenManager.tween( tween.target.tf, 1, { alpha:1 }, {ease:Ease.ELASTIC, easeType:Ease.EASE_OUT} );
			}
			if (_counter == _itemsArr.length) {
				_settingUpNextLevel = false;
			}
		}
		
		private function onFrame(e:Event) :void {

			_distance = Point.distance(new Point( _assetMc.jellyGun.x, _assetMc.jellyGun.y ), new Point( _assetMc.mouseX, _assetMc.mouseY ));
			_angle = Math.asin( (_assetMc.jellyGun.y - _assetMc.mouseY) / _distance );
			if ( _assetMc.jellyGun.x < _assetMc.mouseX ) {
				_angle = Math.PI - _angle;
			}
			_assetMc.jellyGun.rotation = (_angle * 180 / Math.PI);
		}
		
		private function playSound(soundToPlay:String) :void{
			SoundMixer.stopAll();
			ReporterManager.wordExposure(_levelKey);
			SoundManager.play(soundToPlay);			
		}
		
		private function playItemSound( item : GameItem ) :void{
			SoundMixer.stopAll();
			
			if (_playingItem) _playingItem.stopSound();
			
			_playingItem = item;
			_playingItem.playSound();
			
			ReporterManager.interaction( { action: "GameItem Sound", actionObject: "gameItem_" + _playingItem.text } );
			ReporterManager.wordExposure( _playingItem.text );
		}
		
		private function check( bulletMc : JellyBullet ):void {
			if ( _currentItem.key == _levelKey ) {
				bulletMc.mistaken = false;
				_currentItem.mistaken = false;
				ReporterManager.interaction( { action: "shoot", actionObject: "gameItem_" + _currentItem.text, success: true });
				ReporterManager.wordExposure(_currentItem.text);
			}
			else{
				if ( !_currentItem.checked ) {
					LevelManager.WRONG_ANSWERS_COUNT++;
				}
				bulletMc.mistaken = true;
				_currentItem.mistaken = true;
				
				ReporterManager.interaction( { action: "shoot", actionObject: "gameItem_" + _currentItem.text, success: false });
				ReporterManager.wordExposure(_currentItem.text);
			}
		}
		
		private function onBulletHideComplete( tween : GTween ):void{
			tween.target.parent.removeChild( tween.target );
		}
		
		/**
		 * 
		 * @param tween
		 * This function is called after the shot is taken. Decides correct or wrong.
		 * 
		 */
		private function onBulletComplete( tween:GTween ) : void {
					
			
			if ( JellyBullet( tween.target ).mistaken ) {
				TweenManager.tween(tween.target, .5, {alpha:0}, null, { onComplete : onBulletHideComplete } );
			}else{
				onBulletHideComplete( tween );
			}
			if ( _currentItem ) {
				attemptCount++;
				_currentItem.updateMistaken();
				if ( _currentItem.mistaken ) {
					shotCount++;	
					_starAttempt = false;
					SoundManager.play( getRandomValue("fail") );
					_levelStatus.markAttempt(shotCount, attemptCount, 0);
					ReporterManager.interaction( { action: "click", actionObject: "BulletHitFailed"+ attemptCount});
					shotCount--;	
				}
				else {
					if(attemptCount == 1){
						_starAttempt = true;
						LevelManager.FIRST_ATTEMPT_ANSWERS_COUNT++;
						ReporterManager.interaction( { action: "click", actionObject: "HitForStar"+ attemptCount});
					}
					shotCount++;	
					ReporterManager.interaction( { action: "click", actionObject: "BullsEyeonAttempt"+ attemptCount});
					_levelStatus.markAttempt(shotCount,attemptCount, 1);
					_levelStatus.mark(shotCount,0xffcc22);
					_starAttempt = false;
					SoundManager.play("contactSound");
					if(shotCount < 5){
						SoundManager.play( getRandomValue("success"), 3 );
					}
				}
			}
			_playingItem = null;
			_currentItem = null;
			
			if ( (isLevelComplete && !_settingUpNextLevel) || shotCount == 5) {
				showNextLevel();
				//////			 
			}
		}
		
		
		private function get isLevelComplete():Boolean{
			_isLevelComplete = false;
			for (var i:uint = 0; i < _itemsArr.length; i++ ) {
				if ( !_itemsArr[i].mistaken && _itemsArr[i].checked ) {
					_isLevelComplete = true;
					break;
				}
			}
			return _isLevelComplete;
		}
		
		private function showNextLevel():void{

			if ( _currentItemsCounter >= Config.NO_OF_ROUNDS_IN_LEVEL) { // _dataObj.currentIdsArr.length 
				_dataObj = prepareDataToLoad();
				
				_currentItemsCounter = 0;
				_distractItemsCounter = 0;
				
				//_assetMc.levelEnd.nxtBtn.addEventListener(LevelEnd.COMPLETE, onNextLevel);
				//_assetMc.levelEnd.restart.addEventListener(LevelEnd.RETRY, onRetryLevel);
				_assetMc.levelEnd.nxtBtn.buttonMode = true;
				_assetMc.levelEnd.restart.buttonMode = true;
				
				_assetMc.levelEnd.nxtBtn.addEventListener(MouseEvent.CLICK, onNextLevel);				
				_assetMc.levelEnd.restart.addEventListener(MouseEvent.CLICK, onRetryLevel);
				 
				_levelEnd.scaleX = _levelEnd.scaleY = 0;
				_levelEnd.alpha = 0;
				_levelEnd.show();
				_assetMc.setChildIndex(_assetMc.levelEnd, _assetMc.numChildren-1);
			}
			else {
				_settingUpNextLevel = true;
				for ( var i:uint = 0; i < _itemsArr.length; i++ ) {
					TweenManager.tween( _itemsArr[i], .7, { alpha : 0, scaleX : 0, scaleY : 0 });
				}
				setTimeout(setupItems, 1000);
			}
		}
		
		private function onNextLevel(e:Event) : void {
			shotCount = 0;
			isRndInform_bool = false;
			
			LevelManager.nextLevel();
			ReporterManager.interaction( { action: "click", actionObject: "NextLevel"+LevelManager.LEVEL});
			_levelInfo.update();
			_assetMc.levelEnd.nxtBtn.removeEventListener(MouseEvent.CLICK, onNextLevel);
			_assetMc.levelEnd.restart.removeEventListener(MouseEvent.CLICK, onRetryLevel);
			_levelEnd.hide();
			_levelStatus = new LevelStatus(_assetMc.levelStatus, false);
			_roundCount = 0;
			setupItems();
			
		}
		
		private function onBonusPoints( e : Event ):void{
//			_points.add(100);
		}
		
		private function onRetryLevel(e:Event) : void {
			ReporterManager.interaction( { action: "click", actionObject: "RetryLevbel"});
			shotCount = 0;
			isRndInform_bool = false;
			LevelManager.retry();
			_levelInfo.update();
			_assetMc.levelEnd.nxtBtn.removeEventListener(MouseEvent.CLICK, onNextLevel);
			_assetMc.levelEnd.restart.removeEventListener(MouseEvent.CLICK, onRetryLevel);
			_levelEnd.hide();
			_levelStatus = new LevelStatus(_assetMc.levelStatus, false);
			_roundCount = 0;
			setupItems();
		}
		
		private function onMasteredLevel(e:Event):void{
			SaveManager.save(LevelManager.SET_NUMBER - 1);
			LevelManager.LEVEL_MASTERED = true;
			onBackToSplash();
		}
		
		private function getRandomValue(value:String):String{
			if (_randIndexObj[value].count > 1) {
				while( _randIndexObj[value].tmpId == _randIndexObj[value].lastId ){
					_randIndexObj[value].tmpId = Math.round(  Math.random() * ( _randIndexObj[value].count - 1 ) );
				}
				_randIndexObj[value].lastId = _randIndexObj[value].tmpId;
			}
			else{
				_randIndexObj[value].lastId = 0;
			}
			return value + _randIndexObj[value].lastId;
		}
		
		private function onItemMouseOut(e:MouseEvent) :void {
			if (_currentItem && _isInDrag) {
				if ( (_currentItem.y + _currentItem.height / 2) >= _itemsContainer.height ) {
					if (_currentItem){
						_currentItem.dispatchEvent(new MouseEvent(MouseEvent.MOUSE_UP));
					}
				}
			}
		}
		
		private function onItemMouseDown(e:MouseEvent) :void {
			if ( e.target is GameItem )
			{
				_distance = Point.distance(new Point( _assetMc.jellyGun.x, _assetMc.jellyGun.y ), new Point( _assetMc.mouseX, _assetMc.mouseY ));
				_angle = Math.asin( (_assetMc.jellyGun.y - _assetMc.mouseY) / _distance );
				if ( _assetMc.jellyGun.x < _assetMc.mouseX ) {
					_angle = Math.PI - _angle;
				}
				
				_time = Math.abs( _assetMc.jellyGun.rotation - (_angle * 180 / Math.PI) ) / 130;
				TweenManager.tween( _assetMc.jellyGun, _time, { rotation : (_angle * 180 / Math.PI) }, {ease:Ease.LINEAR, easeType: Ease.NONE } );
				
				playItemSound( e.target as GameItem );
			}
		}
		
		private function onImageLoaded( e:Event ) : void {
			_loadedImagesCount++;
			if ( _loadedImagesCount == LevelManager.ITEMS_COUNT ) {
				CoverManager.instance.hide( CoverStates.LOADING_DATA );
				
			}

		}
		
		private function onBackToSplash(e:MouseEvent = null) : void {
			dispatchEvent( new Event( GameEvent.BACK_TO_SPLASH ) );
		}
		
		private function onBackToGames(e:MouseEvent):void {
			dispatchEvent(new Event( GameEvent.BACK_TO_GAMES ));
		}
		
		private function createRoundInformation(aParam:String):Boolean{
			_assetMc.roundFly_mc.gotoAndPlay(2);				
			_assetMc.setChildIndex(_assetMc.roundFly_mc, _assetMc.numChildren-1)				
			isRoundInfor_str = aParam					
			if (_assetMc.mouseChildren) {
				_assetMc.mouseChildren = false;
			}
			_assetMc.roundFly_mc.roundAnim_mc._txt.text = String(Number(_roundCount))
			_assetMc.roundFly_mc.visible = true;		
			ReporterManager.interaction( { action: "click", actionObject: "Round" + _roundCount });
			_assetMc.roundFly_mc.addFrameScript(7-1,roundFlyDepth);
			_assetMc.roundFly_mc.addFrameScript(11 - 1, roundFlyAnimation);	
			_assetMc.roundFly_mc.addFrameScript(_assetMc.roundFly_mc.totalFrames -1, function ():void{
				_assetMc.roundFly_mc.gotoAndStop(1);
			});	
			//if(_assetMc.roundFly_mc.currentFrame == _assetMc.roundFly_mc.totalFrames)
				trace(_assetMc.roundFly_mc.currentFrame+"----"+_assetMc.roundFly_mc.totalFrames);			
			return true;
		}
		
		private function roundFlyDepth(){
			_assetMc.roundFly_mc.addFrameScript(7-1,null);
			_assetMc.setChildIndex(_assetMc.roundFly_mc, _assetMc.numChildren-1)
		}
				
		private function roundFlyAnimation(){
			_assetMc.roundFly_mc.addFrameScript(11-1,null);
			_assetMc.setChildIndex(_assetMc.roundFly_mc, _assetMc.numChildren-1)			
			_assetMc.roundFly_mc.stop()
			startBellSound();
		}
		private function startBellSound():void 
		{
			//flybellAnimationSound
			_assetMc.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2)
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  _dataXml.config.game.flybellAnimationSound ) );				
			ch=_sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete );
		}
		
		private function onSoundComplete(e:Event):void 
		{
			_assetMc.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);
			_assetMc.roundFly_mc.play();
			e.target.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete );
			_assetMc.mouseChildren = true;
		}
		
		private function stopFlyAnim():void 		
		{	
			_assetMc.roundFly_mc.addFrameScript(10 - 1, null)
			_assetMc.roundFly_mc.stop()
		}

		
		public function roundInformation() {		
			
			_assetMc.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);			
			_assetMc.roundFly_mc.play()
			
		}
		
		

		
/*		private function onHint(e:MouseEvent):void {
			var i:uint;
			if ( LevelManager.LEVEL == "A" ) {
				for ( i = 0 ; i < _itemsArr.length; i++ ){
					if ( _itemsArr[i].key == _levelKey ) {
						_itemsArr[i].mark( 0xffff00 );
						break;
					}	
				}
			}
			else if ( LevelManager.LEVEL == "B" ) {
				for ( i = 0 ; i < _itemsArr.length; i++ ){
					if ( _itemsArr[i].key == _levelKey ) {
						//_itemsArr[i].playSound();
						TweenManager.tween( _itemsArr[i], .7, {rotationY: 360}, 
						{ease:Ease.LINEAR, easeType: Ease.NONE }, { onComplete : onRotationComplete } );
						break;
					}
				}
			}
			else if ( LevelManager.LEVEL == "C" ) {
				_levelTarget.playSound();
			}
			
			ReporterManager.interaction( { action: "click", actionObject: "hintLevel" + LevelManager.LEVEL });
			LevelManager.HINT_USED = true;
		}
*/		
		private function onRotationComplete( tween : GTween ) :void{
			( tween.target as MovieClip ).rotationY = 0;
			( tween.target as MovieClip ).transform.matrix = null;
		}
		
		private function onSpeaker(e:MouseEvent):void {
			_sync.speakAndDo(playLetterAudio);
			ReporterManager.interaction( { action: "click", actionObject: "gameInfoSpeaker" });
			//SoundManager.playSequence("gameInfo");
		}
		
		private function instruct(e:Event):void {
			_sync.speakAndDo(playLetterAudio);
			ReporterManager.interaction( { action: "autoplay", actionObject: "gameInfoSpeaker" });
			//SoundManager.playSequence("gameInfo");
		}
		
		private function playLetterAudio(e : Event):void {
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  soundsArr[3].url ) );				
			ch=_sound.play();
			//_sync2.speakAndDo(playAdditionalInstructionSound);
		};		
		
		public function reset():void{
			_disableSounds = true;
			clearInterval(_intervalId);
			SoundManager.reset();
			
		}
		
	}
}