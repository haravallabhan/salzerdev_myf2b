package com.myf2b.slimegame.ui.overlay
{
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.manager.LevelManager;
	import com.myf2b.slimegame.manager.SoundManager;
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.assets.AssetExt;
	import com.myf2b.slimegame.ui.buttons.BaseButton;
	import com.myf2b.slimegame.ui.effects.Fireworks;
	import com.myf2b.slimegame.ui.gameScreen.LevelStatus;
	import com.myf2b.slimegame.utils.Wedge;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import com.myf2b.ui.*;
	
	
	public class LevelEnd extends AssetExt
	{
		public static const COMPLETE:String = "complete";
		public static const RETRY:String = "retry";
		public static const MASTERED:String = "mastered";
		public static const BONUS_POINTS:String = "bonusPoints";
		
		public static const BONUS_MODE:String = "bonusMode";
		public static const NORMAL_MODE:String = "normalMode";
		
		private var _grade:Number;
		private var _levelComplete:Boolean;
		
		private var _degreesCounter:Number = 0;
		private var _wedge:Wedge;
		private var _gradeDegrees:Number;
		private var _intervalId:Number;
		
		private var _percentsCounter :Number;
		private var _percentsStep :Number = 2.2222;
		private var _degreesStep :Number = 8;
		private var _mode:String;
		
		////////////////////////////////////////////
		private var blinkCnt = 0;
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB//0xCC3333;					
		private var btn_blinkCnt = 0;
		private var glow_str:String;
		
		private var glow:GlowFilter = new GlowFilter();	
		private var _sync1 : TextFieldSoundSync;
		private var _sync2 : TextFieldSoundSync;
		private var _sync3 : TextFieldSoundSync;
		
		private var failureContainer_mc : MovieClip;
		private var successContainer_mc_A : MovieClip;
		private var successContainer_mc_B : MovieClip;
		////////////////////////////////////////////
		
		
		private var _levelEndStatus: LevelStatus;
		private var _dataXml:XML;
		
		
		
		
		public function LevelEnd(mc:MovieClip,_dataXml) {
			super(mc);
			_dataXml = _dataXml
		//	_mc.visible = true;
			_percentsStep = ( _degreesStep * 100 ) / 360;
			
			_mc.scaleX = _mc.scaleY = 0;
			_mc.alpha = 0;
			var LevelA_Complete:String = Config.RESULT_TEXT_LEVELA;
			var LevelB_Complete:String = Config.RESULT_TEXT_LEVELB;
			var gameFailed:String = Config.FAILURE_INFO;
			
			successContainer_mc_A = new MovieClip;
			mc.addChild(successContainer_mc_A);
			successContainer_mc_A.addChild(_mc.successDescTf_levelA);
			successContainer_mc_A.visible = false;
			_mc.successDescTf_levelA.text = LevelA_Complete;
			
			successContainer_mc_B = new MovieClip;
			mc.addChild(successContainer_mc_B);
			successContainer_mc_B.addChild(_mc.successDescTf_levelB);
			successContainer_mc_B.visible = false;			
			_mc.successDescTf_levelB.text = LevelB_Complete;		
			
			failureContainer_mc = new MovieClip;
			mc.addChild(failureContainer_mc);
			failureContainer_mc.addChild(_mc.failDescTf);
			failureContainer_mc.visible = false;			
			_mc.failDescTf.text = gameFailed ;			
			

			_sync1 = Ui.EnableTextSync(_mc.successDescTf_levelA, _dataXml); 						
			_sync2 = Ui.EnableTextSync(_mc.failDescTf, _dataXml); 	
			_sync3 = Ui.EnableTextSync(_mc.successDescTf_levelB, _dataXml); 						

			//new BaseButton(_mc.restart);
			//new BaseButton(_mc.nxtBtn);
			_wedge = new Wedge( 0x47B84B, -198.05, -4.15, 107, 0, 0);
			_mc.addChild(_wedge);
			//_mc.endInfoTxt.width = 520;
			

			
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//

		}
		
		public function set mode(value:String):void{
			_mode = value;
		/*	switch ( _mode ) {
				case NORMAL_MODE:
					//_mc.bonusMc.visible = false;
					break;
				case BONUS_MODE:
					//_mc.bonusMc.visible = true;
					break;
				default:
					break;
			}*/
		}
		
		private function onWedge(e:Event):void {
			_degreesCounter += _degreesStep;
			_percentsCounter += _percentsStep;
		}
		
		public function show():void {
			mode = NORMAL_MODE;
			TweenManager.tween(_mc, 0.5, {alpha:1, scaleX:1, scaleY:1}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
			startCount();
		}
		
		private function startCount():void {
			_levelComplete = false;
	
			//_grade = ( LevelManager.LEVEL_ANSWERS_COUNT - LevelManager.WRONG_ANSWERS_COUNT ) / LevelManager.LEVEL_ANSWERS_COUNT;
			_levelEndStatus = new LevelStatus(_mc.levelStatus, true);
			
			if (LevelManager.FIRST_ATTEMPT_ANSWERS_COUNT >= Config.NO_OF_ROUNDS_IN_LEVEL * (Config.MASTERY_PERCENTS / 100))
			{
				_mc.nxtBtn.visible = true;
				
				_levelComplete = true;
				if ( LevelManager.LEVEL == "A" ) {
					successContainer_mc_A.visible = true;
					successContainer_mc_B.visible = false;
					_mc.successDescTf_levelA.visible = true;
					_mc.successDescTf_levelB.visible = false;
					trace(" rest 1")
					//_mc.nxtBtn.x = 428;
				}else{
					successContainer_mc_B.visible = true;
					successContainer_mc_A.visible = false;
					_mc.successDescTf_levelA.visible = false;
					_mc.successDescTf_levelB.visible = true;
					trace(" rest 2")
					
				}
				_mc.restart.visible = false;
				_mc.nxtBtn.x = 428;				
				failureContainer_mc.visible = false;	
				_mc.failDescTf.visible = false;				
				glow_str = "nextbtn";				
			}else {
				_mc.nxtBtn.visible = true;
				_mc.restart.visible = true;	
				_levelComplete = false;
				successContainer_mc_A.visible = false;	
				successContainer_mc_B.visible = false;	
				_mc.successDescTf_levelA.visible = false;
				_mc.successDescTf_levelB.visible = false;
				failureContainer_mc.visible = true;				
				_mc.failDescTf.visible = true;				
				glow_str = "restartbtn";	
				trace(" rest 3")
				_mc.nxtBtn.x = 568.65;
			}
			addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
		
			if ( !_levelComplete ) {
				_mc.titleTf.text = Config.LEVEL_FAILED_TITLE
				_sync2.speak();	
			}
			else {
				if ( ( LevelManager.LEVEL == "B" ) || LevelManager.LEVEL == "C" ) {
					_mc.titleTf.text = Config.LEVELB_SUCCESS_TITLE;
					_sync3.speakAndDo(celebrateCompletion);	
				}			
				else{
					if ( LevelManager.LEVEL == "A" ) {
						_mc.titleTf.text = Config.LEVELA_SUCCESS_TITLE;
						_sync1.speak();		
					}
					else if ( LevelManager.LEVEL == "B" ) {
						_mc.titleTf.text = Config.LEVELB_SUCCESS_TITLE;
						_sync3.speakAndDo(celebrateCompletion);	
					}
				}
								
			}

			_wedge.graphics.clear();
			
			_degreesCounter = 0;
			_percentsCounter = 0;
			
			_mc.addEventListener(Event.ENTER_FRAME, onWedge);
		}
		
		private function celebrateCompletion(e):void {
			SoundManager.play("mastered");
			//var fireworks:Fireworks = new Fireworks(20000);
			
		}
		
		private function createGlowEffectForButn_Handlers(e):void 
		{
			
			btn_blinkCnt++
			if(btn_blinkCnt %10==0){		
				glow.alpha = 1;			
				glowCnt++
				if (glowCnt == 3) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}	
				if (glow_str == "nextbtn") {
					_mc.nxtBtn.filters = [glow];
				}else{
					_mc.restart.filters = [glow];				
				}
			} 
			
			if(btn_blinkCnt == 100){
				btn_blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
				glow.alpha = 0;					 
				if (glow_str == "restartbtn") {
					_mc.restart.filters = [glow];
				}else{
					_mc.nxtBtn.filters = [glow];				
				}				 
			}
		}
		
		public function hide():void{
			TweenManager.tween(_mc, 0.5, {alpha:0, scaleX:0, scaleY:0}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		/*private function dispatch(e:MouseEvent):void {
			if ( _levelEndButton.state == LevelEndButton.PLAY_AGAIN ) {
				dispatchEvent(new Event(RETRY));
			}
			else if ( _levelEndButton.state == LevelEndButton.NEXT_SET ) {
				dispatchEvent(new Event(MASTERED));
			}
			else{
				dispatchEvent(new Event(COMPLETE));
			}
		}*/
		
	}
}