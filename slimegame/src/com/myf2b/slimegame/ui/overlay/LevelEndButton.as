package com.myf2b.slimegame.ui.overlay
{
	import flash.display.MovieClip;
	
	public class LevelEndButton extends MovieClip {
		
		public static const PLAY_AGAIN:String = "playAgain";
		public static const LEVEL_B:String = "levelB";
		public static const LEVEL_C:String = "levelC";
		public static const NEXT_SET:String = "nextSet";
		
		private var _state:String;
		private var _mc:MovieClip;
		
		public function LevelEndButton(mc:MovieClip) {
			_mc = mc;
			_mc.stop();
		}
		
		public function set state(value:String):void{
			_state = value;
			
			switch ( value ) {
				case PLAY_AGAIN :
					_mc.gotoAndStop(1);
					break;
				
				case LEVEL_B:
					_mc.gotoAndStop(2);
					break;
				
				case LEVEL_C:
					_mc.gotoAndStop(3);
					break;
				
				case NEXT_SET:
					_mc.gotoAndStop(4);
					break;
				
				default:
					break;
			}
			
		}
		
		public function get state():String{
			return _state;
		}
		
	}
}