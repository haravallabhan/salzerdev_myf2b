﻿package com.myf2b.slimegame.ui.effects
{
	//import com.myf2b.ui.GameEffectsManager;
	import com.myf2b.ui.effects.SingleFirework;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	public class Fireworks extends Sprite
	{
		private var _fireworks:MovieClip;
		private var _colorTransform:ColorTransform;
		private var _intervalId:uint;
		
		private var _fireworksArr:Array;
		
		public function Fireworks( time:uint )
		{
			//trace( " TIME " + time );
			
			_fireworksArr = new Array();
			
			_intervalId = setInterval( showFirework, 200 );
			setTimeout(stopFirework, time);
			
			//addEventListener( Event.ENTER_FRAME, onEnterFrame );
		}
		
		private function showFirework():void {
			_fireworks =  new SingleFirework() ;//GameEffectsManager.createFireworks() 
			_fireworks.x = Math.random() * 1024;
			_fireworks.y = Math.random() * 768;
			
			_colorTransform = new ColorTransform();
			_colorTransform.color = Math.random() * Math.pow( 256, 3 );
			_fireworks.transform.colorTransform = _colorTransform;
			addChild( _fireworks);
			_fireworksArr.push( _fireworks );
		}
		
		private function stopFirework() : void {
			clearInterval( _intervalId );
		}
		
		/*
		private function onEnterFrame( e : Event ) : void {
			for ( var i:uint = 0; i < _fireworksArr.length; i++ ) {
				if ( _fireworksArr[i].currentFrame == _fireworksArr[i].totalFrames ) {
					this.removeChild( _fireworksArr.shift() );
					break;
				}
			}
			if ( _fireworks && !_fireworksArr.length ) {
				if ( this.parent ) {
					removeEventListener( Event.ENTER_FRAME, onEnterFrame );
					this.parent.removeChild( this );
				}
			}
		}
		*/
		
	}
}