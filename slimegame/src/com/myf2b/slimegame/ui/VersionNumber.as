package com.myf2b.slimegame.ui
{
	import flash.display.MovieClip;
	import flash.net.SharedObject;
	import flash.text.TextField;
	
	
	public class VersionNumber extends MovieClip
	{
		public var tf:TextField;
		private var _versionObj:Object;
		
		public function VersionNumber() 
		{
			x = 719;
			y = 750;
			
			_versionObj = { major:1, minor:0, build:0, revision:0};
			
			tf.text = _versionObj.major + "." + _versionObj.minor + "." + _versionObj.build + "." + _versionObj.revision;
			
			mouseChildren = false;
			mouseEnabled = false;
		}
		
		private function get defaultVersion():Object{
			return { major:1, minor:0, build:0, revision:0 };
		}
		
	}
}
