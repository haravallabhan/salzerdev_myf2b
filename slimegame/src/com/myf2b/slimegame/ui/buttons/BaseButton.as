package com.myf2b.slimegame.ui.buttons 
{
	import com.gskinner.motion.GTween;
	import com.gskinner.motion.easing.Bounce;
	import com.gskinner.motion.easing.Sine;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class BaseButton extends MovieClip 
	{
		private var _gt:GTween;
		
		public function BaseButton(mc:MovieClip = null) {
			
			if (!mc){
				buttonMode = true;
				addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
				addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			}
			else{
				mc.stop();
				mc.buttonMode = true;
				mc.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
				mc.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			}
		}
		
		protected function onMouseOver(e:MouseEvent):void {
			if (_gt && !_gt.paused ) {
				_gt.paused = true;
			}
			_gt = new GTween(e.currentTarget, 0.1, { scaleX: 1.1, scaleY:1.1 }, { ease: Bounce.easeOut } );
			_gt.paused = false;
		}
		
		protected function onMouseOut(e:MouseEvent):void {
			if (_gt && !_gt.paused ) {
				_gt.paused = true;
			}
			_gt = new GTween(e.currentTarget, 0.1, { scaleX: 1, scaleY:1 }, { ease: Bounce.easeIn } );
			_gt.paused = false;
		}
		
	}
}