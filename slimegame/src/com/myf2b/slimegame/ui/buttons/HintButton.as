package com.myf2b.slimegame.ui.buttons
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class HintButton extends BaseButton
	{
		private var _mc:MovieClip;
		
		public function HintButton(mc:MovieClip=null)
		{
			super(mc);
			
			_mc = mc;
			_mc.stop();
		}
		
		override protected function onMouseOver(e:MouseEvent):void{
			_mc.gotoAndStop(2);
			super.onMouseOver(e);
		}
		
		override protected function onMouseOut(e:MouseEvent):void{
			_mc.gotoAndStop(1);
			super.onMouseOut(e);
		}
		
	}
}