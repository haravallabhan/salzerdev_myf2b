﻿package com.myf2b.slimegame.ui.buttons
{
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.gskinner.motion.GTween;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	
	public class LevelButton extends BaseButton
	{
		private var _selected:Boolean = false;
		private var _mc:MovieClip;
		private var _glowFilter:GlowFilter = new GlowFilter(0xffff00, 1, 15, 15);
		private var _tween:GTween;
		
		public function LevelButton(mc:MovieClip=null)
		{
			super(mc);
			_mc = mc;
			_mc.addEventListener( MouseEvent.CLICK, dispatchEvent);
		}
		
		override protected function onMouseOut(e:MouseEvent):void {
			if (!selected) super.onMouseOut(e);
		}
		
		public function get mc():MovieClip{
			return _mc;
		}
		
		public function get selected():Boolean{
			return _selected;
		}
		
		public function select():void{
			_selected = true;
			
			_glowFilter = new GlowFilter(0xffff00, 1, 0, 0);
			
			TweenManager.tween(_glowFilter, 0.5, {blurX : 30, blurY : 30, alpha: 1},  {ease: Ease.ELASTIC, easeType: Ease.EASE_OUT }, {onChange: onTweenChange});
		}
		
		private function onTweenChange(tween:GTween):void{
			_tween = tween;
			
			_mc.filters = [_glowFilter];
		}
		
		public function unselect():void{
			_selected = false;
			
			if (_tween) {
				_tween.paused = true;
			}
			_mc.filters = [];
			
			_mc.dispatchEvent(new MouseEvent( MouseEvent.MOUSE_OUT ));
		}
		
		override public function get name():String{
			return _mc.name;
		}
		
	}
}