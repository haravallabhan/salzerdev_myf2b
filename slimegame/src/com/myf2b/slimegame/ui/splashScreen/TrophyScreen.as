package com.myf2b.slimegame.ui.splashScreen
{
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.buttons.BaseButton;
	import com.gskinner.motion.GTween;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	public class TrophyScreen extends EventDispatcher
	{
		public static var PLAY_AGAIN:String = "playAgain";
		public static var MY_BADGES:String = "myBadges";
		
		public var playAgainBtn:MovieClip;
		public var badgesBtn:MovieClip;
		
		private var _mc:MovieClip;
		
		public function TrophyScreen( mc:MovieClip )
		{
			super();
			
			_mc = mc;
			
			new BaseButton(_mc.playAgainBtn);
			new BaseButton(_mc.badgesBtn);
			
			_mc.alpha = 0;
			_mc.mouseEnabled = false;
			_mc.mouseChildren = false;
			_mc.badgesBtn.visible = false;
			_mc.playAgainBtn.addEventListener(MouseEvent.CLICK, onPlayAgain);
			_mc.badgesBtn.addEventListener(MouseEvent.CLICK, onBadges);
		}
		
		public function onPlayAgain(e:MouseEvent):void{
			dispatchEvent( new Event( PLAY_AGAIN ) );
		}
		
		private function onBadges(e:MouseEvent):void{
			dispatchEvent( new Event( MY_BADGES ) );
		}
		
		public function show():void{
			_mc.mouseEnabled = true;
			_mc.mouseChildren = true;
			TweenManager.tween(_mc, .8, {alpha:1}, {ease: Ease.SINE, easeType:Ease.EASE_OUT } );
		}
		
		public function hide():void{
			_mc.mouseEnabled = false;
			_mc.mouseChildren = false;
			TweenManager.tween(_mc, .8, {alpha:0}, {ease: Ease.SINE, easeType:Ease.EASE_OUT }, {onComplete: onHideComplete} );
		}
		
		public function onHideComplete(tween:GTween):void{
			//this.parent.removeChild(_mc);
		}
		
	}
}