package com.myf2b.slimegame.ui.splashScreen 
{
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.SplashEvent;
	import com.myf2b.slimegame.manager.SaveManager;
	import com.myf2b.slimegame.ui.assets.SplashAssets;
	import com.myf2b.slimegame.utils.AssetsLoader;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class SplashScreen extends Sprite
	{
		private static const ASSETS_COMPLETE:String = "assetsComplete";
		
		private var _trophy:TrophyScreen;
		private var _splashAssets : SplashAssets;
		private var _xml:XML;
		private var _levelsArr:Array;
		private var _isMastered:Boolean;
		
		public function SplashScreen( xml : XML ) {
			
			_levelsArr = new Array();
			
		/*	for (var i:int = 0; i < xml.levels.level.length() ; i++) 
			{
				_levelsArr.push( Number( xml.levels.level[i]) );
			}*/
			
			AssetsLoader.load(Config.SPLASH_GFX, onComplete);
		}
		
		private function onComplete():void{
			_splashAssets = new SplashAssets( AssetsLoader.data as MovieClip );
			_splashAssets.addEventListener(SplashEvent.START_LEVEL, dispatchEvent );
			//_splashAssets.addEventListener(SplashEvent.SHOW_TROPHY, onShowTrophy);
			//_splashAssets.addEventListener(SplashEvent.BACK_TO_MAP, dispatchEvent);
			
			
			addChildAt( AssetsLoader.data, 0 );
			
			//_trophy = new TrophyScreen( AssetsLoader.data.trophyScreen );
			
			//_s//plashAssets.levels = _levelsArr;
			//_splashAssets.isMastered = _isMastered;
		}
		
		private function onShowTrophy(e:Event):void {
			/*_trophy.addEventListener(TrophyScreen.PLAY_AGAIN, onTrophyPlayAgain);
			_trophy.addEventListener(TrophyScreen.MY_BADGES, onTrophyMyBadges);
			
			_trophy.show();*/
		}
		
		public function onTrophyPlayAgain(e:Event):void{
			/*_trophy.removeEventListener(TrophyScreen.PLAY_AGAIN, onTrophyPlayAgain);
			_trophy.removeEventListener(TrophyScreen.MY_BADGES, onTrophyMyBadges);
			_trophy.hide();*/
			
			//SaveManager.resetLevels();
			//_splashAssets.playAgain();
		}
		
		public function onTrophyMyBadges(e:Event):void{
			//todo - watisdiz
		}
		
		public function get selectedLevelId():int{
			return _splashAssets.selectedLevel;
		}
		
		public function reset():void{
			_splashAssets.reset();
		}
		
	}
}