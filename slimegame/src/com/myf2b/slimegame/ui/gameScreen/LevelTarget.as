package com.myf2b.slimegame.ui.gameScreen
{
	import com.gskinner.motion.GTween;
	import com.myf2b.core.CoreSound;
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.manager.LevelManager;
	import com.myf2b.slimegame.manager.SoundManager;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.utils.AssetsLoader;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.text.TextField;
	
	public class LevelTarget
	{
		private var _mc:MovieClip;
		private var _targetMc:MovieClip;
		private var _targetContainer:Sprite;
		private var _boundsObj:Object;
		private var _dataXml:XML;
		private var _soundsCounter:uint;
		private var _channel:SoundChannel;
		private var _itemSoundsArr:Array;
		private var _key:String;
		
		private var _sound:Sound;
		private var _soundLoaded:Boolean = false;
		private var _firstSoundPlayed:Boolean = false;
		private var _showTweenComplete:Boolean = false;
		
		private var _tween:GTween;
		
		private var _imageMask:Sprite;
		
		public function LevelTarget(mc:MovieClip) {
			_mc = mc;
			_mc.tf.alpha = 0;
			_mc.buttonMode = true;
			_mc.mouseChildren = false;
			
			_imageMask = new Sprite();
			_imageMask.graphics.beginFill(0);
			_imageMask.graphics.drawRect(0,0, _mc.width, _mc.height);
			_imageMask.graphics.endFill();
		}
		
		public function set xmlData( value:XML ) : void {
			_dataXml = value;
			
			_soundLoaded = false;
			_showTweenComplete = false;
			
			if ( _tween ) {
				_tween.paused = true;
			}
			
			_mc.tf.alpha = 0;
			if ( LevelManager.LEVEL == "A" ) {
				_mc.tf.text = _dataXml.word;
			}
			
			_itemSoundsArr = new Array();
			for (var i:uint = 0; i < _dataXml.sounds.sound.length(); i++) {
				_sound = new CoreSound( new URLRequest( _dataXml.sounds.sound[i] ) );
				if ( i == _dataXml.sounds.sound.length() - 1 ) {
					_sound.addEventListener( Event.COMPLETE, onSoundLoadComplete );
				}
				_itemSoundsArr.push( _sound );
			}
			AssetsLoader.load( _dataXml.image, onImageLoaded );
		}
		
		private function onSoundLoadComplete( e : Event ):void{
			_soundLoaded = true;
			if (_showTweenComplete) {
				playSound();
			}
		}
		
		public function playSound() : void{
			_soundsCounter = 0;
			if (_channel){
				_channel.stop();
				_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
			_channel = _itemSoundsArr[ _soundsCounter ].play();
			_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
		}
		
		private function onSoundComplete( e:Event ) :void{
			_soundsCounter++;
			if (_itemSoundsArr[ _soundsCounter ]) {
				_channel = _itemSoundsArr[ _soundsCounter ].play();
				_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
		private function onImageLoaded() : void {
			_targetMc = AssetsLoader.data;
			_boundsObj = _targetMc.getBounds( _targetMc );
			
			_targetMc.x = - _boundsObj.x - _targetMc.width / 2;
			_targetMc.y = - _boundsObj.y - _targetMc.height / 2;
			
			
			if ( _targetContainer ) {
				TweenManager.tween( _targetContainer, .7, {alpha: 0}, null, { onComplete: onHideTweenComplete } );
				_mc.tf.alpha = 0;
			}
			else{
				onHideTweenComplete(null);
			}
			//trace("After Image L:oad...________________3__________________________");
		}
		
		private function onHideTweenComplete(tween:GTween) : void {
			if ( _targetContainer ) {
				_targetContainer.parent.removeChild(_targetContainer);
			}
			_targetContainer = new Sprite();
			_targetContainer.addChild(_targetMc);
			
			_targetContainer.x = _mc.width / 2;
			_targetContainer.y = _mc.height / 2;
			
			if ( LevelManager.LEVEL == "A" ) {
				_targetContainer.y -= 20;
			}
			
			_mc.addChild( _targetContainer );
			_mc.addChild( _imageMask );
			
			_targetContainer.alpha = 0;
			_targetContainer.scaleX = _targetContainer.scaleY = 0;
			_targetContainer.mask = _imageMask;
			
			TweenManager.tween( _targetContainer, 1, {alpha: 1, scaleX:1, scaleY:1}, null, { onComplete: onTweenComplete } );
			//trace("After Image L:oad...________________5_________________________");
		}
		
		private function onTweenComplete(tween:GTween) : void {
			if ( _soundLoaded )  {
				playSound();
			}
			_showTweenComplete = true;
			if (LevelManager.LEVEL == "A") {
				_tween = TweenManager.tween( _mc.tf, 1, {alpha: 1} );
			}
		}
	}
}