package com.myf2b.slimegame.ui.gameScreen
{
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.assets.AssetExt;
	import com.gskinner.motion.GTween;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	public class Points extends Sprite
	{
		private var _currentScore:Number = 0;
		private var _intervalId:uint;
		private var _toScore:uint;
		private var _mc:MovieClip;
		
		public function Points(mc:MovieClip) {
			super();
			_mc = mc;
			reset();
		}
		
		public function add(value:Number):void {
			_toScore += value;
			if ( _intervalId ) clearInterval( _intervalId );
			_intervalId = setInterval(onChange, 35);
		}
		
		private function onChange():void{
			_currentScore++;
			_mc.tf.text = _currentScore.toString();
			if ( _currentScore == _toScore ){
				clearInterval( _intervalId );
			}
		}
		
		public function reset():void{
			_currentScore = 0;
			_toScore = 0;
			_mc.tf.text = _currentScore.toString();
		}
		
	}
}