package com.myf2b.slimegame.ui.gameScreen
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	public class GunFireButton extends EventDispatcher
	{
		public static const FIRE:String = "fire";
		
		private var _mc:MovieClip;
		
		public function GunFireButton( mc : MovieClip )
		{
			_mc = mc;
			_mc.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_mc.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_mc.addEventListener( MouseEvent.MOUSE_OUT, onMouseUp );
				
			_mc.buttonMode = true;
		}
		
		private function onMouseDown(e:MouseEvent) : void {
			_mc.scaleX = _mc.scaleY = .95;
			this.dispatchEvent( new Event( FIRE ) );
		}
		
		private function onMouseUp(e:MouseEvent) : void {
			_mc.scaleX = _mc.scaleY = 1;
		}
		
		public function get mc():MovieClip {
			return _mc;
		}
		
	}
}