package com.myf2b.slimegame.ui.gameScreen
{
	import com.gskinner.motion.GTween;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.utils.AssetsLoader;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class GameItem extends MovieClip
	{
		public static const IMAGE_LOADED:String = "imageLoaded";
		public var tf:TextField;
		public var isInBasket:Boolean = false;
		
		public var brokenGlass:MovieClip;
		public var splat:MovieClip;
		public var masks:MovieClip;
		public var checked:Boolean = false;
		
		private var _xmlItem:XML;
		private var _loader:CoreLoader;
		private var _key:String;
		private var _mc:MovieClip;
		private var _startX:Number;
		private var _startY:Number;
		private var _glowFilter:GlowFilter;
		private var _tween:GTween;
		private var _itemSound:Sound;
		private var _itemSoundsArr:Array;
		private var _soundsCounter:uint;
		private var _channel:SoundChannel;
		private var _bounds:Rectangle;
		private var _mistaken:Boolean = false;
		private var _txtFormat:TextFormat;
		private var _container:Sprite;
		private var _imageMask:Sprite;
		
		public function GameItem( xmlItem:XML, id : uint = 0, key : String = "" )
		{
			super();
			
			mouseChildren = false;
			buttonMode = true;
			
			_xmlItem = xmlItem;
						
			bg.gotoAndStop( (id + 1) );
			masks.gotoAndStop( (id + 1) );
			
			_key = key;
			
			if ( _xmlItem ) {
				tf.alpha = 0;
				tf.text = _xmlItem.word;
				
				tf.autoSize = TextFieldAutoSize.CENTER;
				tf.mouseEnabled = false;
				tf.textColor = 0xff00ff;
				_loader = new CoreLoader();
				
				_loader.load(new URLRequest( _xmlItem.image ));
				_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
				
				_itemSoundsArr = new Array();
				for (var i:uint = 0; i < _xmlItem.sounds.sound.length(); i++) {
					_itemSoundsArr.push( new CoreSound( new URLRequest( _xmlItem.sounds.sound[i] ) ) )
				}
				
				rotate( brokenGlass );
				rotate( splat );
				
				brokenGlass.visible = false;
				splat.visible = false;
			}
		}
		
		private function rotate( mc ) : void {
			mc.rotation = Math.random() * 360;
		}
		
		private function onLoadComplete(e:Event) : void {
			
			LoaderInfo( e.currentTarget ).removeEventListener( Event.COMPLETE, onLoadComplete );
			_mc = LoaderInfo( e.currentTarget ).loader.content as MovieClip;
			_bounds = _mc.getBounds(_mc);
			_mc.x = -_bounds.x - _mc.width / 2;
			_mc.y = -_bounds.y - _mc.height / 2;
			_mc.mouseChildren = false;
			//_mc.mouseEnabled = true;
			addChildAt(_mc, 2);
			
			addChild(masks);
			
			mc.mask = masks;
			
			dispatchEvent(new Event(IMAGE_LOADED));
		}
		
		public function set mistaken( value : Boolean ):void{
			_mistaken = value;
			checked = true;
		}
		
		public function get mistaken():Boolean {
			return _mistaken;
		}
		
		public function updateMistaken():void {
			if ( mistaken ) {
				brokenGlass.visible = true;
				
				_txtFormat = tf.getTextFormat();
				_txtFormat.color = 0xff0000;
				tf.setTextFormat( _txtFormat );
			}
			else {
				splat.visible = true;
			}
		}
		
		public function get key():String{
			return _key;
		}
		
		public function get mc():MovieClip{
			return _mc;
		}
		
		public function get startXY():Object{
			return {x: _startX, y: _startY};
		}
		
		public function moveTo(_x:Number, _y:Number):void{
			_startX = _x;
			_startY = _y;
			x = _x;
			y = _y;
			
			/**/
			//testing
			dispatchEvent(new Event(IMAGE_LOADED));
			/**/
		}
		
		public function get text():String{
			return tf.text;
		}
		
		public function mark(color:uint):void{
			_glowFilter = new GlowFilter(color, 1, 0, 0);
			TweenManager.tween(_glowFilter, 1, {blurX : 20, blurY : 20, strength: 4},  {ease: Ease.ELASTIC, easeType: Ease.EASE_OUT }, {onChange: onTweenChange});
		}
		
		private function onTweenChange(tween:GTween):void{
			_tween = tween;
			bg.filters = [_glowFilter];
		}
		
		public function playSound( delay:Number = 0 ):void{
			
			_soundsCounter = 0;
			
			if ( _channel ) {
				_channel.stop();
				_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
			
			if ( _itemSoundsArr[ _soundsCounter ] && _itemSoundsArr[ _soundsCounter ].url ) {
				_channel = _itemSoundsArr[ _soundsCounter ].play();
				_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
		private function onSoundComplete( e:Event ) :void{
			_soundsCounter++;
			if (_itemSoundsArr[ _soundsCounter ]){
				_channel = _itemSoundsArr[ _soundsCounter ].play();
				_channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
		public function stopSound():void{
			if (_channel){
				_channel.stop();
				_channel.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			}
		}
		
	}
}