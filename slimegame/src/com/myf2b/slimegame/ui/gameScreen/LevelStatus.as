package com.myf2b.slimegame.ui.gameScreen
{
	import com.gskinner.motion.GTween;
	import com.myf2b.slimegame.manager.tween.Ease;
	import com.myf2b.slimegame.manager.tween.TweenManager;
	import com.myf2b.slimegame.ui.assets.AssetExt;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.filters.GlowFilter;
	
	public class LevelStatus extends AssetExt
	{
		private var _round:uint = 1;
		private var _level:String = "A";
		private var mcArr:Array ;
		private var _glowFilter:GlowFilter;
		private var _tween:GTween;
		private var _activeMC:MovieClip;
		private var _resultArr:Array;
		private static var _isCorrectArr:Array;
		
		public function LevelStatus( assetMc : MovieClip, isLevelEnd:Boolean ) : void {
			super(assetMc);
			_mc.gotoAndStop(1);
			/*_mc.emptyIcon1.visible=true;
			_mc.emptyIcon2.visible=true;
			_mc.emptyIcon3.visible=true;
			_mc.emptyIcon4.visible=true;
			_mc.emptyIcon5.visible=true;*/
			for(var i=1;i<=5;i++){
				_mc["emptyIcon"+i].visible = true;
			}
			
			
			if(isLevelEnd){
				showResultAtEnd();
			}else{
				_resultArr = new Array();
			}
		}
		
				
		public function markAttempt(shotCount: uint, attemptCount:uint, mark: uint):void{
			_mc.gotoAndStop(shotCount+1);
			
			if(_isCorrectArr == null){				
				_isCorrectArr = new Array();
			 
			}
			trace("Shot Count : "+shotCount);
			if(mark ==1){//correct
				_activeMC = new MovieClip();
				_mc["wrong"+shotCount].visible= false;
				if(attemptCount == 1){
					_mc["emptyIcon"+shotCount].visible=false;
					_mc["secondtry"+shotCount].visible= false;
					_mc["thirdtry"+shotCount].visible= false;
					_mc["star"+shotCount].visible= true;
					_activeMC = _mc["star"+shotCount]; 
					_isCorrectArr.push(0);
				}else if(attemptCount == 2){
					_mc["secondtry"+shotCount].visible= true;
					_mc["star"+shotCount].visible= false;
					_mc["emptyIcon"+shotCount].visible=false;
					_mc["thirdtry"+shotCount].visible= false;
					_activeMC = _mc["secondtry"+shotCount];
					_isCorrectArr.push(1);
				}else{
					_mc["secondtry"+shotCount].visible= false;
					_mc["star"+shotCount].visible= false;
					_mc["emptyIcon"+shotCount].visible=false;
					_mc["thirdtry"+shotCount].visible= true;
					_activeMC = _mc["thirdtry"+shotCount];						
					_isCorrectArr.push(2);
				}
			}else{
				_mc["emptyIcon"+shotCount].visible=false;
				_mc["secondtry"+shotCount].visible= false;
				_mc["thirdtry"+shotCount].visible= false;
				_mc["star"+shotCount].visible= false;
				_mc["wrong"+shotCount].visible= true;
				_activeMC = _mc["wrong"+shotCount];
			}
			
		}
		
	
			
		private function showResultAtEnd():void{
			_mc.gotoAndStop(_isCorrectArr.length+1);
			for ( var j = 0; j < _isCorrectArr.length;j++){
				if(_isCorrectArr[j] == 0){
					_mc["star"+(j+1)].visible= true;
					_mc["emptyIcon"+(j+1)].visible= false;
					
				}else if(_isCorrectArr[j] == 1){
					_mc["secondtry"+(j+1)].visible= true;
					_mc["emptyIcon"+(j+1)].visible= false;
				}else{
					_mc["thirdtry"+(j+1)].visible= true;
					_mc["emptyIcon"+(j+1)].visible= false;					
				}
			}
			_isCorrectArr = null;
			 
			
		}
			
		
		public function mark(shotCount:uint, color:uint):void{
			_glowFilter = new GlowFilter(color, 1, 0, 0);
			TweenManager.tween(_glowFilter, 1, {blurX : 20, blurY : 20, strength: 4},  {ease: Ease.ELASTIC, easeType: Ease.EASE_OUT }, {onChange: onTweenChange});
		}
		
		private function onTweenChange(tween:GTween):void{
			_tween = tween;
			_activeMC.filters = [_glowFilter]
		}
		
		/*private function get resultArr():Array{
			return _resultArr;
		}
		
		private function set resultArr(resultCode:uint):Array{
			_resultArr.push(resultCode);
			return _resultArr;
		}*/
		
		public function resetMarks(): void{

			/*_mc.star1.visible= false;
			_mc.star2.visible= false;
			_mc.star3.visible= false;
			_mc.star4.visible= false;
			_mc.star5.visible= false;
			_mc.secondtry1.visible= false;
			_mc.secondtry2.visible= false;
			_mc.secondtry3.visible= false;
			_mc.secondtry4.visible= false;
			_mc.secondtry5.visible= false;*/
			 
			for(var i=1;i<=5;i++){
				_mc["star"+i].visible = false;
				_mc["secondtry"+i].visible = false;
			}
			_isCorrectArr = new Array();		
		}
		
	}
}