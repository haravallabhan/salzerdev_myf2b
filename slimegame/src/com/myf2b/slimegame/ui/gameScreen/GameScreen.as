﻿package com.myf2b.slimegame.ui.gameScreen 
{
	import com.myf2b.slimegame.data.Config;
	import com.myf2b.slimegame.events.GameEvent;
	import com.myf2b.slimegame.manager.CoverManager;
	import com.myf2b.slimegame.ui.assets.GameAssets;
	import com.myf2b.slimegame.ui.cover.CoverStates;
	import com.myf2b.slimegame.utils.AssetsLoader;
	
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class GameScreen extends Sprite 
	{
		private var _xmlList:XML;
		private var _levelId:uint;
		private var _selectedLetter:String;
		private var _gameAssets:GameAssets;
		
		public function GameScreen( xmlList : XML, levelId:uint, selectedLetter:String ) {
			_xmlList = xmlList;
			_levelId = levelId;
			_selectedLetter = selectedLetter;
			CoverManager.instance.show( CoverStates.LOADING_DATA );
			AssetsLoader.load(Config.GAME_GFX, onComplete);
		}
		
		private function onComplete():void {
			_gameAssets = new GameAssets(AssetsLoader.data, _xmlList, _levelId);
			trace("What after this OnComplete"+typeof(AssetsLoader.data));
			 
//			_gameAssets.addEventListener(GameEvent.BACK_TO_GAMES, dispatchEvent);
		//	_gameAssets.addEventListener(GameEvent.BACK_TO_SPLASH, dispatchEvent);
			addChildAt(AssetsLoader.data, 0);
			//addChild(AssetsLoader.data);
			
			
		}
		
		public function reset():void{
			_gameAssets.reset();
		}
		
	}
}