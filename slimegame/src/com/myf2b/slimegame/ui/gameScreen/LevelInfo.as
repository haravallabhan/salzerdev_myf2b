﻿package com.myf2b.slimegame.ui.gameScreen
{
	import com.myf2b.slimegame.manager.LevelManager;
	import com.myf2b.slimegame.ui.assets.AssetExt;
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	public class LevelInfo extends AssetExt
	{
		
		private var _round:uint = 1;
		private var _level:String = "A";
		
		public function LevelInfo( mc : MovieClip ) : void {
			super(mc);
		}
		
		public function update():void{
			
			if(LevelManager.LEVEL == "B"){
				_mc.gotoAndStop(2);
			}else{
				_mc.gotoAndStop(1);				
			}
			_mc.levelTf.text = "Level " + LevelManager.LEVEL;
		}
		
	}
}