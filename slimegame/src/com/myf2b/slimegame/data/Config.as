package com.myf2b.slimegame.data 
{
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class Config 
	{
		public static const SAVE_KEY:String = "slime_game";
		
		public static const SMALL_GRADE:Number = 0.75;
		public static const BIG_GRADE:Number = 0.8;
		
		public static var MASTERY_PERCENTS:Number;
		public static var DATA_URL:String;
		public static var DEFAULT_POINTS:uint;
		public static var HINT_POINTS:uint;
		public static var SEPARATOR:String;
		public static var SPLASH_GFX:String;
		public static var GAME_GFX:String;
		public static var CONFIG_XML:XMLList;
		
		public static var SETS_XML :XMLList;
		public static var BONUS_POINTS_TEXT:String;
		public static var BONUS_NO_HINT_TEXT:String;
		
		public static var LEVEL_FAILED_TITLE:String = "WHOOPS!";
		public static var LEVELA_SUCCESS_TITLE:String = "CONGRATULATIONS!";
		public static var LEVELB_SUCCESS_TITLE:String = "CONGRATULATIONS!";
		
		public static var RESULT_TEXT_LEVELA:String;
		public static var RESULT_TEXT_LEVELA_AUDIO:String;
		public static var RESULT_TEXT_LEVELB:String;
		public static var RESULT_TEXT_LEVELB_AUDIO:String;
		
		public static var FAILURE_INFO:String;
		public static var FAILURE_INFO_AUDIO:String;
		
		public static var GAME_XML_ID:String = "135";
		public static var GAME_KEY:String = "135";
		
		public static const GAME_MODULE_ID:String = "slime_game"
		public static var GAME_SESSION_ID:String = "";
		
		public static const GAME_AREA_WIDTH:Number = 768;
		public static const GAME_AREA_HEIGHT:Number = 544;
		
		public static var ITEMS_COUNT:uint = 0;
		
		public static var USERPOPUP_TITLE:String = "For Educators";
		
		//public static var USERPOPUP_TITLE_PARENT = "For Parents";
		
		public static var USERPOPUP_INFO:String = "This information is for teachers.";
		
		public static var IS_A_TEACHER:Boolean = false;
		
		public static var NO_OF_ROUNDS_IN_LEVEL:uint = 5;
		
		
		
		
	}
}