package com.myf2b.listenandrecord.manager.tween 
{
	import com.gskinner.motion.GTween;
	import com.gskinner.motion.easing.Elastic;
	
	
	
	
	public class TweenManager 
	{
		private static var _distance:Number;
		
		private static var _tween:GTween;
		private static var _easeObj:Object;
		
		//public static function tween( target:Object, duration:Number, params:Object, easeParams:Object = null, callbacksParams:Object = null ):void {
		public static function tween( target:Object, duration:Number, params:Object, easeParams:Object = null, callbacksParams:Object = null ) : GTween {
			
			if ( easeParams ) {
				_easeObj = {ease: easeParams["ease"][easeParams["easeType"]] };
			}
			else{
				_easeObj = {ease: Elastic.easeInOut };
			}
			
			_tween = new GTween( target, duration, params, _easeObj  );
			
			if ( callbacksParams ) {
				if ( callbacksParams.hasOwnProperty("onComplete") ) {
					_tween.onComplete = callbacksParams.onComplete;
				}
				if ( callbacksParams.hasOwnProperty("onChange") ) {
					_tween.onChange = callbacksParams.onChange;
				}
				if ( callbacksParams.hasOwnProperty("onInit") ) {
					_tween.onInit = callbacksParams.onInit;
				}
			}
			
			return _tween;
		}
		
		public static function stop():void{
			_tween.paused = true;
			
		}
		
	}
}