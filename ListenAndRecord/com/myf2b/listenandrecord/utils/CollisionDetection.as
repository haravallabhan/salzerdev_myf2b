﻿package com.myf2b.lettertosoundmatch.utils
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;

	public class CollisionDetection
	{
		private static var _boundsArr:Array;
		private static var _matrix:Matrix;
		private static var _bitmapDataArr:Array;
		private static var _bitmapArr:Array;
		
		public static function check(mc1:MovieClip, mc2:MovieClip):Boolean{
			
			_boundsArr = new Array();
			_boundsArr[0] = mc1.getRect(mc1);
			_boundsArr[1] = mc2.getRect(mc2);
			
			_bitmapDataArr = new Array();
			_bitmapDataArr[0] = new BitmapData(Math.abs(_boundsArr[0].x) + _boundsArr[0].width, Math.abs(_boundsArr[0].y) + _boundsArr[0].height, true, 0);
			_bitmapDataArr[1] = new BitmapData(Math.abs(_boundsArr[1].x) + _boundsArr[1].width, Math.abs(_boundsArr[1].y) + _boundsArr[1].height, true, 0);
			
			_matrix = new Matrix();
			_matrix.tx = -_boundsArr[0].x;
			_matrix.ty = -_boundsArr[0].y;
			_bitmapDataArr[0].draw(mc1, _matrix);
			
			_matrix = new Matrix();
			_matrix.tx = -_boundsArr[1].x;
			_matrix.ty = -_boundsArr[1].y;
			
			_bitmapDataArr[1].draw(mc2, _matrix);
			
			/*debug*/
			/*
			var content:Sprite = new Sprite();
			cont.addChild(content);
			
			var bmp1:Bitmap = new Bitmap(_bitmapDataArr[0]);
			var bmp2:Bitmap = new Bitmap(_bitmapDataArr[1]);
			bmp1.smoothing = true;
			bmp2.smoothing = true;
			
			bmp1.x = mc1.x + _boundsArr[0].x + mc1.parent.x;
			bmp1.y = mc1.y + _boundsArr[0].y + mc1.parent.y;
			
			bmp2.x  = mc2.x + _boundsArr[1].x;
			bmp2.y = mc2.y + _boundsArr[1].y;
			
			content.addChild(bmp1);
			content.addChild(bmp2);
			
			content.y = -200;
			*/
			/*/debug*/
			
			var result:Boolean = _bitmapDataArr[0].hitTest(new Point(mc1.x + _boundsArr[0].x, mc1.y + _boundsArr[0].y), 1, _bitmapDataArr[1], new Point(mc2.x + _boundsArr[1].x, mc2.y + _boundsArr[1].y), 1);
			
			return result;
		}
		
	}
}