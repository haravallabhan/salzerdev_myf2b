﻿package com.myf2b.listenandrecord 
{
	
	import com.avr.XmlLoader;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.listenandrecord.events.BookmarkerEvent;
	import com.myf2b.listenandrecord.manager.SaveManager;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	import flash.system.Security
	
	/**
	 * 
	 * @author haravallabhanjn
	 * 
	 */
	public class ListenAndRecord extends MovieClip {
		private var xmlLoader:XmlLoader;
		public var xml:XML;
		public static var presetsXML:XML;
		
		public static const BACK_TO_MAP:String = "back_to_map";
		private var _letterXML:XMLList;

		public static var mainScreenProxy:ListenAndRecord;
		public static var gameScreenProxy:GameScreen;

		public static var loader:CoreLoader = new CoreLoader();
		public static var liploader:CoreLoader = new CoreLoader();
		private var monsterMC:MovieClip;
		private var lipMC = []
		public static var _contentKey:String = "1";
		private var _gameXMLURL:String;
		private var _lip:Lip;
		private var _itemContainer = new Sprite();
		
		private var mouthMc:MovieClip
		private var ldr_mc:MovieClip
		private var lipLoadedArray = []
		private var monsterLoadedArray = []
		
		public static var moduleSessionID:String;
		private var monster_blinkCnt = 0;
		
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB//0xCC3333;
		private var glow:GlowFilter = new GlowFilter();
		private var maxWidth:Number = 350;
		private var maxHeight:Number = 350;
		var glow_str:String;
		
		public static var gameIntroProxy:GameIntroScreen

		
		/**
		 * 
		 * @param contentKey
		 * 
		 */
		public function ListenAndRecord(contentKey:String="1") {
			addEventListener(Event.ADDED,init);
			addEventListener(Event.REMOVED_FROM_STAGE,destroy);
			_contentKey = contentKey;
			moduleSessionID = Reporter.getNewModuleSessionID();//To DB
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
		//	gameScreenProxy = null;
			mainScreenProxy = null;
			_contentKey = null;
			SoundMixer.stopAll();
		}
		
		/**
		 * Initializes the dame and load the XML content for assets
		 * @param e
		 * 
		 */
		private function init(e:Event):void {
			removeEventListener(Event.ADDED,init);
			stop();
			visible = false;
			//Security.allowDomain("*")
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else {
				_gameXMLURL = Core.filesURL+"/listen_and_record/"+_contentKey+"/game.xml";
			}
			//	xmlLoader=new XmlLoader(_gameXMLURL,presetsXMLLoaded);
			//xmlLoader = new XmlLoader("game.xml", presetsXMLLoaded);
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/3/xml_feed", presetsXMLLoaded);//game.xml
		 
		}
		
		/**
		 * 
		 * 
		 */
		private function presetsXMLLoaded():void {
		
			presetsXML = XML(xmlLoader.data);
			setTimeout(showStuff,100);
			gotoAndStop(2);
			
			
			
			gameIntroProxy = new GameIntroScreen();
			addChild(gameIntroProxy)
			
			gameIntroProxy.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", endOfIntroScreen_Handler);	
			
		//	SaveManager.getInstance().addEventListener( BookmarkerEvent.SHOW_PROGRESS, loadMonsterGraphic );
		//	SaveManager.load();					
		}
		
		
		private function endOfIntroScreen_Handler(e:Event):void 
		{
			ConfigConstants.LETTER_TO_DEAL = gameIntroProxy.selectedObject;
			
			trace(ConfigConstants.LETTER_TO_DEAL + " = " + gameIntroProxy.selectedObject)
			
			gameScreenProxy = gameScreen;
			
			loadXMLListForLetter();
			loadMonsterGraphic();
			
			mainScreenProxy = this;
			
			for (var i = 0; i < this.numChildren;i++ ) {
				if (this.getChildAt(i).name == "gameScreen") {
					var mc = this.getChildByName("gameScreen")
					mc.getChildAt("topBar").dealLetter_mc._txt.text = ConfigConstants.LETTER_TO_DEAL
				}
			}
		 
		}
		
		private function loadMonsterGraphic():void {
			trace("url= " + _letterXML.monsterGraphic)
			
			loader.load(new URLRequest(_letterXML.monsterGraphic));
			loader.cacheAsBitmap = true;
			loader.addEventListener(MouseEvent.CLICK, playLetterSound);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete);
			//addEventListener(Event.ENTER_FRAME, createGlowEffectForMonster);
		//	addEventListener("INSTRUCT", gameScreenProxy.instruct);
			//gameScreenProxy.addEventListener("AUTO_PLAY_END", autoPlayEnd_Handler)
		}
		
		private function autoPlayEnd_Handler(e:Event):void {
			trace("AUTO_PLAY_ENDS ")
			addEventListener(Event.ENTER_FRAME, createGlowEffectForMonster);
		}
		
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function loadComplete(e:Event):void {	
			
		   var monster_mc :MovieClip = new MovieClip;	
		   monsterLoadedArray.push(e.target.content);	   
		   monsterLoadedArray[0].x = 160;//_letterXML.monsterLocationX;//160;
		   monsterLoadedArray[0].y = 240;//_letterXML.monsterLocationY;//230		
		  //  monsterLoadedArray[0].x =  monsterLoadedArray[0].x+125
		 
		//   monsterLoadedArray[0].width = 330;
		 // monsterLoadedArray[0].height = 350;	
		   var ratio:Number;//ratio
			ratio = monsterLoadedArray[0].height/monsterLoadedArray[0].width;//calculation ratio
			//trace(ratio)
			if (monsterLoadedArray[0].width>maxWidth) {

				monsterLoadedArray[0].width = maxWidth;
				monsterLoadedArray[0].height = Math.round(monsterLoadedArray[0].width*ratio);
			}

			if (monsterLoadedArray[0].height>maxHeight) {
				monsterLoadedArray[0].height = maxHeight;
				monsterLoadedArray[0].width = Math.round(monsterLoadedArray[0].height/ratio);
			}
				
				
			 
		   trace("width " + monsterLoadedArray[0].width)			 
		   trace("height " + monsterLoadedArray[0].height)
		   
		   monster_mc.addChild(monsterLoadedArray[0]);
		   gameBG.addChildAt(monster_mc, 1);
		   monster_mc.buttonMode = true;		
		   monster_mc.addEventListener(MouseEvent.CLICK, playLetterSound)
		   dispatchEvent(new Event("INSTRUCT") );
		    
		   //loadLipGraphic();
		}
		
	
	
	private function createGlowEffectForMonster(e:Event):void {		 
		monster_blinkCnt++
		if(monster_blinkCnt % 2==0){		
			glow.alpha = 1;			
			glowCnt++
			if (glowCnt == 5) {				 
				glowCnt = 0
				glow.alpha = 0;	
			}
			 
			if(monsterLoadedArray[0])
			monsterLoadedArray[0].filters = [glow];
		} 
		if(monster_blinkCnt == 100){
			monster_blinkCnt = 0
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForMonster)				 
			glow.alpha = 0;	
			if(monsterLoadedArray[0])
			monsterLoadedArray[0].filters = [glow];
		}
	}	
		
		
		
		/**
		 * 
		 * 
		 */
		private function loadLipGraphic():void{
			liploader.load(new URLRequest(_letterXML.lipGraphic));
			liploader.cacheAsBitmap = true;
			liploader.x =  _letterXML.lipLocationX;
			liploader.y =  _letterXML.lipLocationY;
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, liploaded);
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function liploaded(e) {
		   lipLoadedArray.push(e.target.content);	   
		   lipLoadedArray[0].x = _letterXML.lipLocationX//0 + i * 100;
		   lipLoadedArray[0].y = _letterXML.lipLocationY;				   
		   addChildAt(lipLoadedArray[0], 1);
		   lipLoadedArray[0].addEventListener(MouseEvent.CLICK, playLetterSound)
		   lipLoadedArray[0].buttonMode = true;
		  
		}
		
		
		 
		private function playMovie(e:MouseEvent):void {				

			 lipLoadedArray[0].mouth_mc.play()
			 lipLoadedArray[0].mouth_mc.addFrameScript(lipLoadedArray[0].mouth_mc.totalFrames-1, endMouthAnim)
	 
		}
		
		private function endMouthAnim() {	

			lipLoadedArray[0].mouth_mc.gotoAndStop(1)
			lipLoadedArray[0].mouth_mc.addFrameScript(lipLoadedArray[0].mouth_mc.totalFrames-1, null)
		
		}
				
		/**
		 * 
		 * @param e
		 * 
		 */
		private function playLetterSound(e:MouseEvent):void{
			e.currentTarget.removeEventListener(MouseEvent.CLICK, playLetterSound);
			playSound();
		}
		
		/**
		 * 
		 * 
		 */
		private function playSound():void {
			loader.removeEventListener(MouseEvent.CLICK,playSound);
			var sound:CoreSound=new CoreSound(new URLRequest(_letterXML.letterSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					loader.addEventListener(MouseEvent.CLICK,playLetterSound);
				}
			}
		}
		
		/**
		 * 
		 * 
		 */
		private function loadXMLListForLetter():void {
		 
			_letterXML = presetsXML.deal.(@letter == ConfigConstants.LETTER_TO_DEAL);
			
			 
		}
		
		private function showStuff():void {
			visible=true;
		}
		
		public function backToGames():void {
			trace("BackToGames called");
			ScreenManager.getInstance().pop();
		}
		
		
	}
}



