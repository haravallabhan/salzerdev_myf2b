﻿package com.myf2b.listenandrecord {
	

	/**
     * @company Footsteps 2 Brilliance
     * @author Hara J N
     */
 
    import flash.display.*;
    import flash.events.*;
    import com.myf2b.core.*;
    import flash.display.MovieClip;
   
 	public class ListenAndRecordShell extends MovieClip {
		
		public function ListenAndRecordShell(id:String = "1")  {
            Core.useStaging = true;
			// Set the possible CDNs
			AppSettings.setSetting( "com.myf2b.core.Preferences", "prod_CDNs", ["http://cdn.myf2b.com", "http://cdn2.myf2b.com"] );
			AppSettings.setSetting( "com.myf2b.core.Preferences", "stage_CDNs", ["http://stage.cdn.myf2b.com", "http://stage.cdn2.myf2b.com"] );
            Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			ScreenManager.getInstance().stage = stage;
			ScreenManager.getInstance().pushModule("com.myf2b.listenandrecord.ListenAndRecord", ["3"]);			
        }
	}
	
}
