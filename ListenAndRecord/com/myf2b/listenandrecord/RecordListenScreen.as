﻿package com.myf2b.listenandrecord
{
	
import com.myf2b.ui.RecordPanel;
import flash.events.Event;
import com.myf2b.listenandrecord.manager.ReporterManager;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.media.Microphone;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.utils.ByteArray;
import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;

	public class RecordListenScreen extends Sprite	{
	private var recordPanel:RecordPanel;
	
	private var glowCnt = 0;
	private var blurx = 25;
	private var blury = 25;
	private var glowColor = 0x009EDB//0xCC3333;
	private var glow:GlowFilter = new GlowFilter();
	var glow_str:String;
	//
	private var btn_blinkCnt = 0 ;
	
	public function RecordListenScreen(){
		recordMain.buttonMode=true
		recordMain.addEventListener(MouseEvent.CLICK, showRecordListenBtns);
		//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
	}

	
	private function showRecordListenBtns(e:MouseEvent):void {
		e.currentTarget.removeEventListener(MouseEvent.CLICK, showRecordListenBtns);
		ReporterManager.interaction( { action: "RecordingInitiated", actionObject: "showRecordListenBtns" } );
		recordMain.visible = false;
		recordPanel = new RecordPanel();
		configRecordPanel();
		parent.addChildAt(recordPanel,1);
		recordPanel.listen_mc.addEventListener(MouseEvent.CLICK,showNextBtn);
	}
	
	

	
	private function showNextBtn(e:MouseEvent):void {
		ReporterManager.interaction( { action: "click", actionObject: "showNextBtn" } );
		e.currentTarget.removeEventListener(MouseEvent.CLICK, showNextBtn);
		nextStep.visible=true;
		nextStep.buttonMode=true;
		nextStep.addEventListener(MouseEvent.CLICK, goToNextStep);
		addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
	}

	private function goToNextStep(e:MouseEvent):void {
		ReporterManager.interaction( { action: "click", actionObject: "goToNextStep" } );
		trace("Congratulations");
	}
	
	private function createGlowEffectForButn_Handlers(e:Event):void {		 
		btn_blinkCnt++
		if(btn_blinkCnt % 2==0){		
			glow.alpha = 1;			
			glowCnt++
			if (glowCnt == 5) {				 
				glowCnt = 0
				glow.alpha = 0;	
			}
			 
			nextStep.filters = [glow];
			
		} 
		if(btn_blinkCnt == 1000){
			btn_blinkCnt = 0
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
			glow.alpha = 0;		
			nextStep.filters = [glow];
		}
	}
	
	
	private function configRecordPanel():void{
		recordPanel.x = 510;
		recordPanel.y = 300;
		recordPanel.visible=true;
		recordPanel.show();
		recordPanel.scaleX = 1.4;
		recordPanel.scaleY = 1.4;
		
	}
	///end class
	}
}