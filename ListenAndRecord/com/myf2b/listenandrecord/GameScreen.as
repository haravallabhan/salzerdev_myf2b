﻿package com.myf2b.listenandrecord
{

	import com.myf2b.core.CoreSound;
	import com.myf2b.core.EventWithData;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.Session;
	import com.greensock.text.SplitTextField;
	import com.myf2b.listenandrecord.ui.overlay.UserPopup;
	import com.myf2b.listenandrecord.data.Config;
	import com.myf2b.listenandrecord.events.BookmarkerEvent;
	import com.myf2b.listenandrecord.manager.SaveManager;
	import com.myf2b.listenandrecord.manager.ReporterManager;
	//import com.myf2b.listenandrecord.manager.SoundManager;
	import com.myf2b.ui.*;
	import flash.text.TextField;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	

	public class GameScreen extends MovieClip {
		trace("GameScreen.. 1 ")
		private var _session:Session = Session.getInstance();
		
		private var speed:Number=1;
		private var groupNode;
		private var assetNode;
		private var _letterToDeal:String;
		private var level:Number=0;
		private var presetsXML:XML;
		private var instructionText:String;
		private var additionalText:String;
		public var monsterClip:MovieClip;
		private var _userPopup:UserPopup;
		private var push_arr:Array ;
		private var _letterXML:XMLList;
		//private var _btnArray : Array = new Array();
		private var _sync1 : TextFieldSoundSync;
		private var _sync2 : TextFieldSoundSync;
		private var isAutoPlay:Boolean = false;
		
		
		public function GameScreen() {
			presetsXML = ListenAndRecord.presetsXML;
			 
			instructionText = presetsXML.variables.instructionTxt;
			additionalText = presetsXML.variables.additionalTxt;	
			
			_letterToDeal = ConfigConstants.LETTER_TO_DEAL;
		//	trace("GameScreen= "+GameIntroScreen.deal_letter)
			
			init();
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			var soundsArr:Array = [ { key:"letterSound", url: _letterXML.letterSound },
									{key:"instructionAudio0", url: _letterXML.letterSound },
									{ key:"instructionAudio1", url: presetsXML.variables.additionalInstructionSound}];	
			//SoundManager.add(soundsArr, init);	
			
			
		}
		
		private function init():void {
			ReporterManager.init();
			topBar.cacheAsBitmap = true;
			
			topBar.instructionTxt.text = instructionText;
			topBar.additionalTxt.text=additionalText;
			topBar.soundBtn.buttonMode=topBar.backToMap.buttonMode=topBar.langBtn.buttonMode=topBar.userIcon.buttonMode=true;
			topBar.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);
			topBar.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			topBar.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);	
			loadXMLListForLetter();
			
			topBar.dealLetter_mc._txt.text = _letterToDeal;
			topBar.dealLetter_mc.buttonMode = true;
			topBar.dealLetter_mc._txt.mouseEnabled = false;			
			topBar.dealLetter_mc.addEventListener(MouseEvent.CLICK, playLetterSound);
			
			
			
			_sync1 = Ui.EnableTextSync(topBar.instructionTxt, presetsXML);
			_sync2 = Ui.EnableTextSync(topBar.additionalTxt, presetsXML);
			if(_session.role == "Teacher"){
				Config.USERPOPUP_TITLE=  presetsXML.variables.popupForTeacherTitle;
				Config.USERPOPUP_INFO =  presetsXML.variables.popupInfoForTeacher;
				Config.USERPOPUP_PARENT_INFO = presetsXML.variables.popupInfoForParent; 
				Config.IS_A_TEACHER = true;
				topBar.userIcon.teacherIcon.visible=true;
				topBar.userIcon.parentIcon.visible=false;
			}else if(_session.role == "Parent"){
				Config.USERPOPUP_TITLE=  presetsXML.variables.popupForParentTitle;
				Config.USERPOPUP_INFO =  presetsXML.variables.popupInfoForParent;
				Config.IS_A_TEACHER = false;
				topBar.userIcon.teacherIcon.visible=false;
				topBar.userIcon.parentIcon.visible=true;
			}
		
			Config.USERPOPUP_SUBTITLE = presetsXML.variables.popupSubTitle;
	
			_userPopup = new UserPopup(userPopup);		
			_userPopup.hide();		
			
			 
		}
		
		/**
		 * 
		 * 
		 */
		private function loadXMLListForLetter():void{
			_letterXML = presetsXML.deal.(@letter == _letterToDeal);
			
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function playLetterSound(e:MouseEvent):void {
			ReporterManager.interaction( { action: "click", actionObject: "playLetterSound" } );
			e.currentTarget.removeEventListener(MouseEvent.CLICK, playLetterSound);
			playSound();
		}
		
		/**
		 * 
		 * 
		 */
		private function playSound():void {
		 
			_letterXML = presetsXML.deal.(@letter == ConfigConstants.LETTER_TO_DEAL);
			topBar.dealLetter_mc.removeEventListener(MouseEvent.CLICK,playSound);
			var sound:CoreSound=new CoreSound(new URLRequest(_letterXML.letterSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				function onSoundComplete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					topBar.dealLetter_mc.addEventListener(MouseEvent.CLICK,playLetterSound);
				}
			}
		}
		
		private function playLetterAudio(e : Event):void {
			var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
			ch=_sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,playAdditionalInstructionSound);
			//_sync2.speakAndDo(playAdditionalInstructionSound);
		};		

		
		private function changeLang(e : MouseEvent ):void {
			if(topBar.langBtn.en.visible){
				topBar.langBtn.en.visible=false;			
				topBar.langBtn.esp.visible=true;
			}else{
				topBar.langBtn.en.visible=true;
				topBar.langBtn.esp.visible=false;
				
			}
			
		}
		
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
		}
		
		
		private function playAdditionalInstructionSound(e : Event ):void {
			ReporterManager.interaction( { action: "click", actionObject: "playAdditionalInstructionSound" } );
			topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
			_sync2.speakAndDo(playAddInstnSndEnd);
			
			//SoundManager.playSequence("instructionAudio"); 
			//_sync1.speak();
			//_sync2.speak();
		}
		private function playAddInstnSndEnd(e) {
			
			if (isAutoPlay == false) {
				isAutoPlay = true;
				trace(" Auto Play End")
				dispatchEvent(new Event("AUTO_PLAY_END"))
			}
		}
		
		/*private function _checkLanguage():void
		{
			if (_session.language == "en") {
				setButtons(1);
			}
			else if (_session.language == "es") {
				setButtons(2);
			}
			else {
				setButtons(1);
			}
		}
		
		private function setButtons( nr : Number ):void
		{
			for (var i:int = 0; i < _btnArray.length ; i++) 
			{
				(_btnArray[i] as MovieClip).gotoAndStop(nr);
			}
		}		*/
		
		private function showInfoForUser(e : MouseEvent ):void {
			_userPopup.show();

		}
	
		
		private function loadComplete():void{
			trace("Load Complete");
		}

		
		public function instruct(e:Event):void {
			//e.currentTarget.removeEventListener(Event.ADDED_TO_STAGE, instruct);
			Reporter.interaction("listen_and_record", ListenAndRecord._contentKey, ListenAndRecord.moduleSessionID);
			_sync1.speakAndDo(playLetterAudio);
		}
		
		private function soundClicked(e:MouseEvent):void {
			e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			Reporter.interaction("listen_and_record", ListenAndRecord._contentKey, ListenAndRecord.moduleSessionID);
			_sync1.speakAndDo(playLetterAudio);
		}
		
	}

}