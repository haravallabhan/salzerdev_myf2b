package com.myf2b.listenandrecord.ui.overlay
{
	import com.myf2b.listenandrecord.data.Config;
	import com.myf2b.listenandrecord.manager.tween.Ease;
	import com.myf2b.listenandrecord.manager.tween.TweenManager;
	import com.myf2b.listenandrecord.ui.assets.AssetExt;
	import flash.text.TextField;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import fl.controls.UIScrollBar;
	
	
	public class UserPopup extends AssetExt
	{	
		var sb:UIScrollBar = new UIScrollBar();
		
		public function UserPopup(mc:MovieClip) {
			super(mc);
			_mc.userPopupClose.buttonMode = true;
			_mc.userPopupClose.addEventListener(MouseEvent.CLICK, closePopup);
			_mc.userPopupTitle.text = Config.USERPOPUP_TITLE;//Config.USERPOPUP_PARENT
			_mc.userPopupInfo.text = Config.USERPOPUP_INFO;			
			assignScrollBar(_mc.userPopupInfo)
			if(Config.IS_A_TEACHER){}else{trace("Inside UserPopup : Parent");}
			
		}
		
		public function show():void {
			trace("Is this session belongs to a Teacher "+ Config.IS_A_TEACHER);
			TweenManager.tween(_mc, 0.5, {alpha:1, scaleX:1, scaleY:1}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		public function hide():void{
			TweenManager.tween(_mc, 0.5, {alpha:0, scaleX:0, scaleY:0}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		private function closePopup(e:MouseEvent):void {
			hide();	
			//_mc.visible = false;
		}
		function assignScrollBar(tf:TextField):void {
			//trace("textLen="+tf.text.length)
			sb = new UIScrollBar();
			sb.move(tf.x + tf.width, tf.y);
			sb.setSize(sb.width, tf.height);			
			
			sb.scrollTarget = tf;	
			if (tf.text.length>=420) {
				sb.visible = true;
			}else {
				sb.visible = false;
			}
			
			_mc.addChild(sb);            
		}
		
		
	}
}