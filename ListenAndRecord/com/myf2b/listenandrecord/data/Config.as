﻿package com.myf2b.listenandrecord.data 
{
	
	public class Config 
	{
		public static var GAME_XML_ID:String = "135";
		public static var GAME_KEY:String = "135";
		
		public static const GAME_MODULE_ID:String = "slime_game"
		public static var GAME_SESSION_ID:String = "";
		
		public static const GAME_AREA_WIDTH:Number = 768;
		public static const GAME_AREA_HEIGHT:Number = 544;
		
		public static var ITEMS_COUNT:uint = 0;
		
		public static var USERPOPUP_TITLE:String = "For Educators";
		
		//public static var USERPOPUP_TITLE_PARENT = "For Parents";
		
		public static var USERPOPUP_INFO:String = "This information is for teachers.";
		
		public static var USERPOPUP_SUBTITLE:String = "At Home";
		
		public static var USERPOPUP_PARENT_INFO:String = "This information is for parents.";
		
		public static var IS_A_TEACHER:Boolean = false;
		
		
	}
}