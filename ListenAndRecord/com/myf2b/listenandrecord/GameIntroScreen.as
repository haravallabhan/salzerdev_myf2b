﻿package com.myf2b.listenandrecord
{

	import flash.display.MovieClip;
	import flash.events.*;
 
import com.myf2b.core.CoreLoader;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.*;
	import com.myf2b.core.*;
	import com.greensock.*; 
	import com.greensock.plugins.*;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.system.Security;
    import flash.system.SecurityPanel;
	

	public class GameIntroScreen extends MovieClip {

		public var progressArray:Array=[];
		private var progressNr:Number=0;
		public var objectsArray:Array=[];
		private var assetsXML:XML;
		private var presetsXML:XML;
		private var initStartX:Number=90;
		private var initStartY:Number=225;
		private var startX:Number=90;
		private var startY:Number=225;
		private var columns:Number=7;
		private var spaceX:Number=30;
		private var spaceY:Number=9;
		private var objectWidth:Number=93;
		private var objectHeight:Number=91;
		public var selectedObject = undefined;
		public static var deal_letter = "";		
		private var sound:CoreSound;
		private var ch:SoundChannel;
		private var e_Time:Timer=new Timer(10);

		private var _bookmarkLoaded:Boolean = false;
		private var _bookmarkXML:XML;
		private var gameKey:String;
		private var speak_Word:String;
		 
		 
		 
		private var ths:*;
		private var stg:*;

		public function GameIntroScreen() {
			 
			
			 
			
			//_loadBookmarks();
				init();
		}

	 

		 
		private function init():void {
			 
			createObjects();
			 
		}
		
		private function createObjects():void{	  	 		 
		   
			if (this.getChildByName("introScreen") != null) {
					this.removeChild(ths.getChildByName("introScreen"))
				}
			 
		  var introScreen:gameIntroScreen = new gameIntroScreen()
		  this.addChild(introScreen)
		  introScreen.name = "introScreen";
		  
		  introScreen.addEventListener(MouseEvent.CLICK, onIntroScreen_Handler)
		  //introScreen.goBtn.addEventListener(MouseEvent.CLICK, goBtn_Handler)
		  //introScreen.goBtn.buttonMode = true;
		  introScreen.buttonMode = true;		  
		}
		
		private function onIntroScreen_Handler(e:MouseEvent):void {			
			if (String(e.target.name).split("_")[1] != null) {				
				selectedObject = String(e.target.name).split("_")[1];
				selectedObject = String(selectedObject).toLowerCase();
				trace(" selectedObject=== " + selectedObject);	
				deal_letter = String(selectedObject)
				this.removeChild(this.getChildByName("introScreen"));
				dispatchEvent(new Event("INTRO_SCREEN_GO_BTN_CLICKED"));				
			}		
		}
		
		  
	}

}