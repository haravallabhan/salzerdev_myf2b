﻿package com.myf2b.flashcard.manager.tween 
{
	import com.gskinner.motion.GTween;
	import com.gskinner.motion.easing.Back;
	import com.gskinner.motion.easing.Bounce;
	import com.gskinner.motion.easing.Circular;
	import com.gskinner.motion.easing.Cubic;
	import com.gskinner.motion.easing.Elastic;
	import com.gskinner.motion.easing.Exponential;
	import com.gskinner.motion.easing.Linear;
	import com.gskinner.motion.easing.Quintic;
	import com.gskinner.motion.easing.Sine;
	
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class TweenManager 
	{
		private static var _distance:Number;
		
		private static var _tween:GTween;
		private static var _easeObj:Object;
		
		//public static function tween( target:Object, duration:Number, params:Object, easeParams:Object = null, callbacksParams:Object = null ):void {
		public static function tween( target:Object, duration:Number, params:Object, easeParams:Object = null, callbacksParams:Object = null ) : GTween {
			
			if ( easeParams ) {
				/*
				if ( easeParams["ease"] == Linear ) {
				}
				*/
				_easeObj = {ease: easeParams["ease"][easeParams["easeType"]] };
			}
			else{
				_easeObj = {ease: Elastic.easeInOut };
				//_easeObj = {ease: Elastic.easeInOut };
				//_easeObj = {ease: Linear.easeNone };
			}
			
			_tween = new GTween( target, duration, params, _easeObj  );
			
			if ( callbacksParams ) {
				if ( callbacksParams.hasOwnProperty("onComplete") ) {
					_tween.onComplete = callbacksParams.onComplete;
				}
				if ( callbacksParams.hasOwnProperty("onChange") ) {
					_tween.onChange = callbacksParams.onChange;
				}
				if ( callbacksParams.hasOwnProperty("onInit") ) {
					_tween.onInit = callbacksParams.onInit;
				}
			}
			
			return _tween;
		}
		
		public static function stop():void{
			_tween.paused = true;
			
		}
		
	}
}