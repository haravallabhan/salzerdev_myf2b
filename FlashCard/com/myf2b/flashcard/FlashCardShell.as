﻿package com.myf2b.flashcard {
	

	/**
     * @company Footsteps 2 Brilliance
     * @author Hara J N
     */
 
    import com.myf2b.core.Core;
    import com.myf2b.core.ScreenManager;
    import com.myf2b.core.Session;
    import com.myf2b.flashcard.GameScreen;
    import com.myf2b.flashcard.GameSpace;
    import com.myf2b.flashcard.FlashCard
	
    
    import flash.display.MovieClip;
    import flash.events.Event;
   
 	public class FlashCardShell extends MovieClip {
		public static var mainScreenProxy:FlashCard;
		//public static var gameScreenProxy:GameSpace;
		public static var ths:*
		public static var stg:*
		public function FlashCardShell(id:String = "1")  {
            Core.useStaging = true;
			ths = this;
			stg = stage;
            Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			ScreenManager.getInstance().stage = stage;			 
			ScreenManager.getInstance().pushModule("com.myf2b.flashcard.FlashCard", ["135"]);			
			mainScreenProxy = new FlashCard(this,id)				
				
			//gameScreenProxy = new GameSpace(this)
			trace(" Flash Card")
			
        }
			
		public static function isThis():*{
			return ths
		}
		public static function isStage():*{
			return stg
		}
		
	}
	
}
