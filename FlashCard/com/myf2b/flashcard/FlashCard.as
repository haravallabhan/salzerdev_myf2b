﻿package com.myf2b.flashcard
{
	
	import com.avr.XmlLoader;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.Session;
	import com.myf2b.flashcard.FlashCardShell
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.utils.setTimeout;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;	
	import mx.events.MoveEvent;
	import com.myf2b.ui.*;
	import flash.filters.BitmapFilterQuality;
	
	/**
	 *
	 * @author haravallabhanjn
	 *
	 */
	public class FlashCard extends MovieClip
	{
		private var xmlLoader:XmlLoader;
		public var xml:XML;
		public static var presetsXML:XML;
		
		public static const BACK_TO_MAP:String = "back_to_map";
		public static var LETTER_TO_DEAL:String = "m";
		private var _letterXML:XMLList;
		 
		public static var LipLoaderArr = [];
		 
		public static var quesMarkLoaderArr = [];
	 
		
		
		//public static var mainScreenProxy:LetterToSoundMatch;
		
		public static var gameSpaceProxy:GameSpace;
		public static var gameScreenProxy:GameScreen;
		
		public var loader:CoreLoader = new CoreLoader();
		public static var liploader:CoreLoader = new CoreLoader();
	 
		
		
		public static var _contentKey:String = "1";
		private var _gameXMLURL:String;
		
		public static var moduleSessionID:String;
		private var ths:*;
		private var stg:*;
	
		
		private var letter_arr = [];
		private var startX:Number = 0;
		private var startY:Number = 0;
		private var isCorrectOrWrong:String = "";
		private var btn_blinkCnt = 0;
		private var getXmlLen:Number = 0;
		private var loadedArray = [];
		private var quesLoadedArray = [];
		
		//private var xPosArr = [570, 570, 80, 80, 80, 300, 300, 300];
	//	private var yPosArr = [195, 373, 205, 380, 550, 210, 380, 550];
	
		private var xPosArr = [ 80, 80, 80, 300, 300, 300];
		private var yPosArr = [ 205, 380, 550, 210, 380, 550];
	 
		
		private var quesMarkXpos:Number = 570;
		private var quesMarkYpos:Number = 555;
		private var counter:Number = 2;
		private var targetNum:Number = 0	 
		private var isHitting:Boolean = false
		
		
		////////////////////////////////////////
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		private var glowColor = 0x009EDB//0xCC3333;
		private var glow:GlowFilter = new GlowFilter();
		private var _imageContainer = [];
		private var coverMc_arr = [];
		var glow_str:String;
		public static var gameIntroProxy:GameIntroScreen
		//
		
	 
		
		/**
		 *
		 * @param contentKey
		 *
		 */
		
		public function FlashCard(aThs, contentKey:String = "1")
		{
			
			//trace(" LetterToSoundMatch ")
			ths = FlashCardShell.isThis();
			stg = FlashCardShell.isStage();
			//	ths = aThs
			
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
			
			
			addEventListener(Event.ADDED, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			_contentKey = contentKey;
			//moduleSessionID = Reporter.getNewModuleSessionID();//To DB	
			
			/////////////////////////////
			
			/////////////////////////////
		}
		
		/**
		 *
		 * @param e
		 *
		 */
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			_contentKey = null;
			SoundMixer.stopAll();
		}
		
		/**
		 *
		 * @param e
		 *
		 */
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED, init);
			stop();
			visible = false;
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined)
			{
				_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else
			{
				//	_gameXMLURL = Core.filesURL+"/listen_and_record/"+_contentKey+"/game.xml";
			}
			//	xmlLoader=new XmlLoader(_gameXMLURL,presetsXMLLoaded);
			
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/10/xml_feed", presetsXMLLoaded);//game.xml
		//	xmlLoader = new XmlLoader("game.xml", presetsXMLLoaded)
		
		}
		
		/**
		 *
		 *
		 */
		private function presetsXMLLoaded():void
		{
			presetsXML = XML(xmlLoader.data);
			setTimeout(showStuff, 100);
			gotoAndStop(2);
			
			gameIntroProxy = new GameIntroScreen();
			addChild(gameIntroProxy)			
			gameIntroProxy.addEventListener("INTRO_SCREEN_GO_BTN_CLICKED", endOfIntroScreen_Handler);
						
		}
		
		private function endOfIntroScreen_Handler(e:Event):void 
		{
			LETTER_TO_DEAL = gameIntroProxy.selectedObject;
			trace("LETTER_TO_DEAL= "+LETTER_TO_DEAL)
			gameScreenProxy = new GameScreen();			
			ths.dummyBg.visible = false;					
			loadXMLListForLetter();
			loadGraphicImages();
			loadMonsterImage()
			loadTargetLetterImage()
			loadGameQuestionImages()
			 
		}
		
		private function inits():void 
		{
			
/*			instructionText1 = presetsXML.variables.instructionTxt;	
			ths.topBar.instructionTxt.text = instructionText1;			
			_sync = Ui.EnableTextSync(ths.topBar.instructionTxt, presetsXML);
			*/
			 
			 
		}
		
		private function makeImagesContainer():void{
		}
		
		/**
		 *
		 *
		 */
		
		private function loadGraphicImages():void {	  	 			
		   var imgLoader:Loader = new Loader()
		   imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaded);	
		 
		   imgLoader.load(new URLRequest(_letterXML.gameImages.game_set.letterItem[counter]));		   
		}
		
		function loaded(e:Event):void{
			var itemWidth:uint = 150;			
			var itemHeight:uint = 140;
			var itemX: uint;
			var itemY: uint;
			var j = 0
		  loadedArray.push(e.target.content);		   
		   if (counter == 7)
		   { 	   
			   for (var i:uint = 0; i < loadedArray.length; i++) 
			   {	
				    itemX = xPosArr[i]//_letterXML.gameImages.game_set.letterItemLocationX[i];
				    itemY = yPosArr[i]//_letterXML.gameImages.game_set.letterItemLocationY[i];				   
					 				  			
					coverMc_arr[i] = new MovieClip()
					ths.addChild(coverMc_arr[i])					 
					coverMc_arr[i].addChild(loadedArray[i])
					
				   coverMc_arr[i].x = itemX;
				   coverMc_arr[i].y = itemY;	
				   
				   if (coverMc_arr[i].width > itemWidth || coverMc_arr[i].height > itemHeight) {					   
   						coverMc_arr[i].width = itemWidth;
						coverMc_arr[i].height = itemHeight;
					}else if (coverMc_arr[i].width < itemWidth / 2 || coverMc_arr[i].height < itemHeight / 2) {					   
   						coverMc_arr[i].width = itemWidth;
						coverMc_arr[i].height = itemHeight;
					}else{
					    coverMc_arr[i].x =  itemX + 10;
						coverMc_arr[i].y =  itemY + 10;						
				   }	
				   
				  /* loadedArray[i].posx = loadedArray[i].x;
				   loadedArray[i].posy = loadedArray[i].y;	*/
				   
				   j = (i+2)
				    
					
					
				  /* loadedArray[i].no = j;
				   loadedArray[i].addEventListener(MouseEvent.MOUSE_DOWN, onItemDown_Handler);
				   loadedArray[i].addEventListener(MouseEvent.CLICK, onItemClick_Handler);					  
				   loadedArray[i].buttonMode = true;*/
				   
				   coverMc_arr[i].mouseChildren = false;
				   coverMc_arr[i].no = j;
				   coverMc_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onItemDown_Handler);
				   coverMc_arr[i].addEventListener(MouseEvent.CLICK, onItemClick_Handler);					  
				   coverMc_arr[i].buttonMode = true;
				   
				   coverMc_arr[i].posx = coverMc_arr[i].x;
				   coverMc_arr[i].posy = coverMc_arr[i].y;	
					
				  
			   } 
			  //loadLipGraphic();	
			  
				var mc:MovieClip = new MovieClip()
				ths.addChild(mc)
				//ths.addChild(LipLoaderArr[0]);
				//mc.addChild(LipLoaderArr[0]);
				
				//LipLoaderArr[0].buttonMode = true;
				//LipLoaderArr[0].visible = true;
				var monsterCoverMc:MovieClip = new MovieClip();
				monsterCoverMc.graphics.beginFill(0xff00ff00,0);				
				monsterCoverMc.graphics.drawRect(570, 195, 130, 130);
				monsterCoverMc.graphics.endFill();
				ths.addChild(monsterCoverMc);		
				monsterCoverMc.buttonMode = true;
			//	LipLoaderArr[0].no = "0";
				mc.no = "0";	
				monsterCoverMc.no = "0";
				//trace("lip width " + LipLoaderArr[0].width +" height "+LipLoaderArr[0].height)					
				monsterCoverMc.addEventListener(MouseEvent.CLICK, onItemClick_Handler)	
				mc.buttonMode = true;
			  
		   }		   
		   else{
			   counter++;
			   loadGraphicImages()
		   }		   
		}		
		
		////////////////////////////////////	 
		
		private function loadMonsterImage():void {				
		   var imgLoader:CoreLoader = new CoreLoader()
		   imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, monsterImg_Loaded);
		   imgLoader.load(new URLRequest(_letterXML.gameImages.game_set.letterItem[0]));	 			
		}
		
		
		function monsterImg_Loaded(e:Event):void {			 
		  var tempArray:Array = []
		   tempArray.push(e.target.content);
		   var monst_itemWidth:uint = 150;
		   var monst_itemHeight:uint = 140;
		   
		   var itemX = 550;
		   var itemY = 195;
		  
		   tempArray[0].x = itemX;
		   tempArray[0].y = itemY;
		   
		   if (tempArray[0].width > monst_itemWidth || tempArray[0].height > monst_itemHeight) {					   
				tempArray[0].width = monst_itemWidth;
				tempArray[0].height = monst_itemHeight;				
			}else if (tempArray[0].width < monst_itemWidth / 2 || tempArray[0].height < monst_itemHeight / 2) {					   
				tempArray[0].width = monst_itemWidth;
				tempArray[0].height = monst_itemHeight;				 
			}else{
				tempArray[0].x =  itemX + 10;
				tempArray[0].y =  itemY + 10;								 
		   }	
		   
		 //  tempArray[0].x = 570
		  // tempArray[0].y = 195
		   
		   ths.addChild(tempArray[0]);
		  // trace("wid = "+tempArray[0])
		   tempArray[0].buttonMode = true;
		 //  gameScreenProxy.autoPlayInstruction(e);	 
		}
		//
		private function loadTargetLetterImage():void{		 
		   var imgLoader:CoreLoader = new CoreLoader()
		   imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, targetLetterImg_Loaded);
		   imgLoader.load(new URLRequest(_letterXML.gameImages.game_set.letterItem[1]));	 			
		}
		
		
		function targetLetterImg_Loaded(e:Event):void {
			var tempArray:Array = []
		   tempArray.push(e.target.content);	   
		//   tempArray[0].x = 560;//quesMarkXpos//presetsXML.variables.itemInQuestionLocationX;//0 + i * 100;
		   //tempArray[0].y = 390;//quesMarkXpos//presetsXML.variables.itemInQuestionLocationY;
		   
		   var letterImg_itemWidth:uint = 150;
		   var letterImg_itemHeight:uint = 140;		   
		   var itemX = 555;
		   var itemY = 390;		  
		   tempArray[0].x = itemX;
		   tempArray[0].y = itemY;
		   
		   if (tempArray[0].width > letterImg_itemWidth || tempArray[0].height > letterImg_itemHeight) {					   
				tempArray[0].width = letterImg_itemWidth;
				tempArray[0].height = letterImg_itemHeight;	
				
			}else if (tempArray[0].width < letterImg_itemWidth / 2 || tempArray[0].height < letterImg_itemHeight / 2) {					   
				tempArray[0].width = letterImg_itemWidth;
				tempArray[0].height = letterImg_itemHeight;	
			
			}else{
				tempArray[0].x =  itemX + 10;
				tempArray[0].y =  itemY + 10;	
				
		   }	
		   
		   
		   
		   ths.addChild(tempArray[0]);	   
		   tempArray[0].buttonMode = true;
		   
		   
		   
		   
		   
		   
		   var monsterCoverMc:MovieClip = new MovieClip();
			monsterCoverMc.graphics.beginFill(0xff00ff00,0);				
			monsterCoverMc.graphics.drawRect(570, 390, 130, 130);
			monsterCoverMc.graphics.endFill();
			ths.addChild(monsterCoverMc);		
			monsterCoverMc.buttonMode = true;
			monsterCoverMc.no = "1"
		   monsterCoverMc.addEventListener(MouseEvent.CLICK, onItemClick_Handler)
		 //  gameScreenProxy.autoPlayInstruction(e);	 
		}
		
		
		////////////////////////////////////
		
		private function loadGameQuestionImages():void{		 
		   var imgLoader:CoreLoader = new CoreLoader()
		   imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, gameQuestionloaded);
		   imgLoader.load(new URLRequest(presetsXML.variables.itemInQuestionImage));	 			
		}
		
		
		function gameQuestionloaded(e:Event):void{
		   quesLoadedArray.push(e.target.content);	   
		   quesLoadedArray[0].x = quesMarkXpos//presetsXML.variables.itemInQuestionLocationX;//0 + i * 100;
		   quesLoadedArray[0].y = quesMarkXpos//presetsXML.variables.itemInQuestionLocationY;				   
		   ths.addChild(quesLoadedArray[0]);	   	 
		}
		
		
		private function loadLipGraphic():void {
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL)			
			liploader.load(new URLRequest(_letterXML.gameImages.game_set.lipGraphic[0]));
			trace("deak= " + LETTER_TO_DEAL)
			trace("lip == "+_letterXML)
			//liploader.load(new URLRequest("http://salzer-ror.cloudapp.net/uploads/file_upload/attachment/9/Lip.swf"));
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, lipLoader_loadComplete);
			liploader.cacheAsBitmap = true;
		}
		
		private function lipLoader_loadComplete(e:Event):void{			
			if (LipLoaderArr == null)
			{
				LipLoaderArr = new Array();				
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			
			
			LipLoaderArr[0] = new MovieClip();			
			LipLoaderArr[0] = (e.target as LoaderInfo).content;
			LipLoaderArr[0].x = _letterXML.gameImages.game_set[0].lipLocationX;
			LipLoaderArr[0].y = _letterXML.gameImages.game_set[0].lipLocationY;
			trace("chk lip x = " + _letterXML.gameImages.game_set[0].lipLocationX)
			trace("chk lip x = " + _letterXML.gameImages.game_set[0].lipLocationY)
			
			
			/*var mc:MovieClip = new MovieClip()
			ths.addChild(mc)
			//ths.addChild(LipLoaderArr[0]);
			mc.addChild(LipLoaderArr[0]);
			
			LipLoaderArr[0].buttonMode = true;
			LipLoaderArr[0].visible = true;
			var monsterCoverMc:MovieClip = new MovieClip();
			monsterCoverMc.graphics.beginFill(0xff00ff00,0);				
			monsterCoverMc.graphics.drawRect(570, 195, 130, 130);
			monsterCoverMc.graphics.endFill();
			ths.addChild(monsterCoverMc);		
			monsterCoverMc.buttonMode = true;
		//	LipLoaderArr[0].no = "0";
			mc.no = "0";	
			monsterCoverMc.no = "0";
			//trace("lip width " + LipLoaderArr[0].width +" height "+LipLoaderArr[0].height)
			 	
			monsterCoverMc.addEventListener(MouseEvent.CLICK, onItemClick_Handler)	
			mc.buttonMode = true;*/
			
			
		}
		
		
		/////////////////////////////////////////////////////////////////////////////////////
		
		private function onItemClick_Handler (e:MouseEvent):void {		
			 
			targetNum = Number(String(e.currentTarget.no))	
			//targetNum = Number(String(e.target.no))
			playSoundInstructions(targetNum)			
		}
		
		private function onItemDown_Handler (e:MouseEvent):void {	
			 
			targetNum = Number(String(e.currentTarget.no))	
			//targetNum = Number(String(e.target.no))	
			e.currentTarget.startDrag()	
			
			//e.currentTarget.parent.parent.addChild(e.currentTarget.parent)	
			ths.setChildIndex(e.currentTarget, ths.numChildren-1)
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_DOWN, onItemUp_Handler)
			e.currentTarget.addEventListener(MouseEvent.MOUSE_UP, onItemUp_Handler)
		}
		
		private function onItemUp_Handler (e:MouseEvent):void {
			var mc:MovieClip = e.currentTarget as MovieClip;			 
			e.currentTarget.stopDrag();	
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_UP, onItemUp_Handler)
			 
			if (e.currentTarget.hitTestObject(quesLoadedArray[0])) {
				e.currentTarget.x = quesLoadedArray[0].x
				e.currentTarget.y = quesLoadedArray[0].y
				
				
				for (var i:uint = 0; i < coverMc_arr.length; i++) {
					if(e.currentTarget != coverMc_arr[i])
					{
						coverMc_arr[i].x = coverMc_arr[i].posx;
						coverMc_arr[i].y = coverMc_arr[i].posy;
					}
				}
								 
				if (quesLoadedArray[0].visible ==true) {
					  
					addEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)
					ths.nextArrow_mc.addEventListener(MouseEvent.CLICK, onNextArrow_Handler);
					ths.nextArrow_mc.buttonMode = true;
					ths.nextArrow_mc.visible = true;	
					isCorrectOrWrong = "correct";
				}
				quesLoadedArray[0].visible = false;				
				
			}else {
				 //trace(" not hitted")			 
				 e.currentTarget.x =  e.currentTarget.posx;
				 e.currentTarget.y =  e.currentTarget.posy;				
			 }		
					
		}
		//
		private function createGlowEffectForButn_Handlers(e):void {			 
			btn_blinkCnt++
			if(btn_blinkCnt %5==0){		
				glow.alpha = 1;			
				glowCnt++
				if (glowCnt == 3) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}				 
				ths.nextArrow_mc.filters = [glow];				
			} 
			if(btn_blinkCnt == 1000){
				btn_blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForButn_Handlers)				 
				glow.alpha = 0;		
				ths.nextArrow_mc.filters = [glow];				 
			}
		}
		//
		
		private function removeAllEvents():void 
		{
			ths.dummyBg.visible = true;
			ths.setChildIndex(ths.dummyBg, ths.numChildren-1)
			 
		}
		
		private function onNextArrow_Handler(e:MouseEvent):void 
		{
			trace(" onNextArrow_Handler ")
		}
		///////////////////////////////////////////////////////////////////////////
		
		private function playSoundInstructions(aUrlNo:*):void
		{
			
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL)			
			loader.removeEventListener(MouseEvent.CLICK, playSoundInstructions);		
			
			var sound:CoreSound = new CoreSound(new URLRequest(_letterXML.gameImages.game_set.letterSound[aUrlNo]));
			
			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			
			function onComplete(e:Event):void
			{
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch:SoundChannel = new SoundChannel();				
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);				
				function onSoundComplete(e:Event):void{
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					if (isCorrectOrWrong == "correct") {
						isCorrectOrWrong = "";
						playSuccessSound("correct")
					}
					
				}
			}
		}
		
		private function playSuccessSound(string:String):void {
			
			var sound:CoreSound = new CoreSound(new URLRequest(presetsXML.variables.successSounds.sound[0]));	 
			
			sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			
			function onComplete(e:Event):void
			{
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);
				var ch:SoundChannel = new SoundChannel();
				
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				function onSoundComplete(e:Event):void{
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				}
			}
			
		}
		
		private function enable_Listners_Handler(e:Event):void{
			trace("enable_Listners_Handler")
		
		}
		
		private function diable_Listners_Handler(e:Event):void
		{
			//monsterCoverMc.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler)	
		}
		
		private function loadXMLListForLetter():void
		{
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL)			
			getXmlLen = _letterXML.gameImages.game_set.letterItem.length()
			//trace("getXmlLen= " + getXmlLen)			
		}
		
		private function showStuff():void{
			visible = true;
		}
		
		public function backToGames():void
		{
			//trace("BackToGames called");
			ScreenManager.getInstance().pop();
			//dispatchEvent(new Event(BACK_TO_GAMES));
		}
	}
}

