package com.myf2b.slimegame.events
{
	public class SplashEvent
	{
		public static const START_LEVEL:String = "startLevel";
		public static const SHOW_TROPHY:String = "showTrophy";
		public static const BACK_TO_MAP:String = "backToMap";
		
	}
}