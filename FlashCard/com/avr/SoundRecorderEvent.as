﻿package com.avr
{
	import flash.events.Event;
	
	/**
	 * Events for sound recorder.
	 * @author Luke
	 */
	public class SoundRecorderEvent extends Event 
	{
		public static const NO_MICROPHONE:String = 'noMicrophone';
		public static const ENTER_RECORDING_STATE:String = 'enterRecordingState';
		public static const DENY:String = 'deny';
		public static const STOP:String = 'stop';
		
		public function SoundRecorderEvent(type:String) 
		{ 
			super(type);
		} 
	}
	
}