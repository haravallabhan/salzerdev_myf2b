﻿package com.myf2b.soundtolettermatch
{
	
	import com.myf2b.core.AssetManager;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Session;
	import com.myf2b.ui.*;
	import com.myf2b.soundtolettermatch.manager.tween.Ease;
	import com.myf2b.soundtolettermatch.manager.tween.TweenManager;
	import flash.text.TextField;
	
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import fl.controls.UIScrollBar;
	
	public class GameScreen extends MovieClip {
		private var speed:Number=1;
		private var groupNode;
		private var assetNode;
		private var _letterToDeal:String;
		private var level:Number=0;
		private var presetsXML:XML = SoundToLetterMatch.presetsXML;
		private var instructionText:String;
		public var monsterClip:MovieClip;
		private var USERPOPUP_TITLE = ""
		private var USERPOPUP_INFO = ""
		private var USERPOPUP_PARENT_INFO = ""
		private var IS_A_TEACHER:Boolean = false;
		private var USERPOPUP_SUBTITLE = ""
		var sb:UIScrollBar = new UIScrollBar();
		
		private var ths:*;
		private var stg:*;
		private var _sync: TextFieldSoundSync;
		private var userPopUp:userPopup;
		private var _letterXML:XMLList
		
		
		public function GameScreen() {
			
			ths = SoundToLetterMatchShell.isThis();
			stg = SoundToLetterMatchShell.isStage();
			
			_letterToDeal = SoundToLetterMatch.LETTER_TO_DEAL;
			 _letterXML = presetsXML.deal.(@letter == _letterToDeal);
			
			init();
			addEventListener(Event.REMOVED_FROM_STAGE,destroy);
			
			
		}
		
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
			//trace("GameScreen destroy");
			//stopGame();
			
		}
				
		private function init():void {
			//trace("init ")
			instructionText = presetsXML.variables.instructionTxt;
			ths.topBar.cacheAsBitmap = true;			
			ths.topBar.instructionTxt.text = instructionText;
			_sync = Ui.EnableTextSync(ths.topBar.instructionTxt, presetsXML); 				
			//trace("stage "+ths.topBar.instructionTxt.text);
			ths.topBar.soundBtn.buttonMode= ths.topBar.backToMap.buttonMode=true;
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);	
			
			//
			
		
			
			//User PopUp Tearcher or Parent//
			if(Session.getInstance().role == "Teacher"){
				USERPOPUP_TITLE = presetsXML.variables.popupForTeacherTitle 
				USERPOPUP_INFO =  presetsXML.variables.popupInfoForTeacher;
				USERPOPUP_PARENT_INFO = presetsXML.variables.popupInfoForParent; 
				IS_A_TEACHER = true;
				
			}else if(Session.getInstance().role == "Parent"){
				USERPOPUP_TITLE = presetsXML.variables.popupForParentTitle 
				USERPOPUP_INFO =  presetsXML.variables.popupInfoForParent;				
				IS_A_TEACHER = false;
			}
			
			USERPOPUP_SUBTITLE = presetsXML.variables.popupSubTitle;
			//trace("USERPOPUP_SUBTITLE="+USERPOPUP_SUBTITLE)
			
			//User PopUp Tearcher or Parent//
		 
			ths.topBar.userIcon.buttonMode=true;
			ths.topBar.soundBtn.buttonMode=true;
			ths.topBar.langBtn.buttonMode=true;
			
			
			if(IS_A_TEACHER){
				ths.topBar.userIcon.teacherIcon.visible = true;
				ths.topBar.userIcon.parentIcon.visible = false;
			}else{
				ths.topBar.userIcon.teacherIcon.visible = false;
				ths.topBar.userIcon.parentIcon.visible = true;
			}
			
			if(Session.getInstance().language == "en"){
				////trace("tpbar= "+ths.topBar.langBtn.en)
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;
			}else if(Session.getInstance().language == "esp"){
				ths.topBar.langBtn.en.visible=false;
				ths.topBar.langBtn.esp.visible=true;				
			}
			
			ths.topBar.langBtn.addEventListener(MouseEvent.CLICK, changeLang);
			ths.topBar.userIcon.addEventListener(MouseEvent.CLICK, showInfoForUser);
			// //Popup setting Language
			
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, onSpeaker);
		}
		private function changeLang(e : MouseEvent ):void {
			if(ths.topBar.langBtn.en.visible){
				ths.topBar.langBtn.en.visible=false;			
				ths.topBar.langBtn.esp.visible=true;
			}else{
				ths.topBar.langBtn.en.visible=true;
				ths.topBar.langBtn.esp.visible=false;				
			}
			
		}
		
		private function showInfoForUser(e : MouseEvent ):void {
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
				userPopUp = new userPopup();
				ths.addChild(userPopUp)
				userPopUp.name = "userPopUp"
				userPopUp.userPopupClose.addEventListener(MouseEvent.CLICK, closePopup);
				userPopUp.userPopupTitle.text = USERPOPUP_TITLE;//Config.USERPOPUP_PARENT
				userPopUp.userPopupInfo.text = USERPOPUP_INFO;			
			if(IS_A_TEACHER){
				userPopUp.popupDivider.visible=true;
				userPopUp.infoForParentTitle.visible = true;
				userPopUp.infoForParent.visible = true;
				userPopUp.infoForParentTitle.text = USERPOPUP_SUBTITLE;
				userPopUp.infoForParent.text = USERPOPUP_PARENT_INFO;
				trace("USERPOPUP_INFO="+USERPOPUP_INFO)
				assignScrollBar(userPopUp.userPopupInfo)
			}else{
				userPopUp.popupDivider.visible=false;
				userPopUp.infoForParentTitle.visible = false;
				userPopUp.infoForParent.visible = false;				
			}
			
			
			show();
		};
		
		private function onSpeaker(e:MouseEvent):void{
			//ReporterManager.interaction( { action: "click", actionObject: "gameInfoSpeaker" });
		//	SoundManager.play("gameInfo");
		}
		
		private function stopGame():void {
			removeAllInstances();
		}
		
		private function loadComplete():void{
			//trace("Load Complete");
		}
		
		public function startGame(node1,node2):void {
			//level=0;
			//groupNode=node1;
			//assetNode=node2;
			//visible=true;
			//startLevel();
			//levelOverScreen.setDetails(0,0,13,15,assetNode);
			//objectTimer.stop();
		}
		
		public function startLevel():void {
			//trace("start")
		}
		
		public function nextLevel():void {
			level++;
		}
		public function playAgain():void {
			
		}
		
		private function onObjectTimerComplete(e:TimerEvent):void {
			removeAllInstances();
		}
		
		private function removeAllInstances():void {
		}
		
		private function onObjectTimer(e:TimerEvent):void {
		}
		
		private function formatTime(val):String {
			return null;
		}
		
		private function soundClicked(e:MouseEvent):void {
			trace(" soundClicked ")
			e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			ths.dummyBg.visible = true;
			ths.setChildIndex(ths.dummyBg, ths.numChildren-1)
			//Reporter.interaction("listen_and_record", ListenAndRecord._contentKey, ListenAndRecord.moduleSessionID);
			//playSoundInstructions();
			_sync.speak(); //AndDo(activateListener);			
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
			ths.dummyBg.visible = false;
			

		}
		public function autoPlayInstruction(e:Event):void {
			
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, soundClicked);
			_sync.speakAndDo(playLetterAudio1);
		}
		private function playLetterAudio1(e:Event):void {
			dispatchEvent(new Event("AUTO_PLAY_INSTRUCTIONS_ENDS"))
			/*var ch:SoundChannel = new SoundChannel();
			var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.letterSound ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 dispatchEvent(new Event("AUTO_PLAY_INSTRUCTIONS_ENDS"))				 
			}*/
			 
		}
		
		
		private function activateListener(e:MouseEvent):void {
			ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
			ths.dummyBg.visible = false;
		}
		
		private function playSoundInstructions():void {
			//trace("RECORDING INSTRUCTIONS");
			ths.topBar.soundBtn.removeEventListener(MouseEvent.CLICK,soundClicked);
			var sound:CoreSound=new CoreSound(new URLRequest(presetsXML.variables.instructionSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					ths.topBar.soundBtn.addEventListener(MouseEvent.CLICK, soundClicked);
					ths.dummyBg.visible = false;
				}
			}
		}
		
		public function show():void {
			TweenManager.tween(userPopUp, 0.5, {alpha:1, scaleX:1, scaleY:1}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
		}
		
		public function hide():void{
			if(ths.getChildByName("userPopUp") !=null){
				ths.removeChild(ths.getChildByName("userPopUp"))
			}
			TweenManager.tween(userPopUp, 0.5, {alpha:0, scaleX:0, scaleY:0}, {ease:Ease.EXPONENTIAL, easeType:Ease.EASE_OUT });
			
		}
		
		private function closePopup(e:MouseEvent):void {			
			hide();	
			
		}
		function assignScrollBar(tf:TextField):void {
			trace("textLen="+tf.text.length)
			sb = new UIScrollBar();
			sb.move(tf.x + tf.width, tf.y);
			sb.setSize(sb.width, tf.height);			
			
			sb.scrollTarget = tf;	
			if (tf.text.length>=420) {
				sb.visible = true;
			}else {
				sb.visible = false;
			}
			
			userPopUp.addChild(sb);            
		}
		
		
	}
	
}

