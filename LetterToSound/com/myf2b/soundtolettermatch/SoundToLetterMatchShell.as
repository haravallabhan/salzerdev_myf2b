﻿package com.myf2b.soundtolettermatch {
	

	/**
     * @company Footsteps 2 Brilliance
     * @author Hara J N
     */
 
    import com.myf2b.core.Core;
    import com.myf2b.core.ScreenManager;
    import com.myf2b.core.Session;
    import com.myf2b.soundtolettermatch.GameScreen;
    import com.myf2b.soundtolettermatch.GameSpace;
    import com.myf2b.soundtolettermatch.SoundToLetterMatch;
    
    import flash.display.MovieClip;
    import flash.events.Event;
   
 	public class SoundToLetterMatchShell extends MovieClip {
		public static var mainScreenProxy:SoundToLetterMatch;
		//public static var gameScreenProxy:GameSpace;
		public static var ths:*
		public static var stg:*
		public static var Root:*
		public function SoundToLetterMatchShell(id:String = "1")  {
            Core.useStaging = true;
			ths = this;
			stg = stage;
			Root = MovieClip(root)
			
            Session.getInstance().language = "en";
			Session.getInstance().role = "Teacher";
			ScreenManager.getInstance().stage = stage;
			ScreenManager.getInstance().pushModule("com.myf2b.soundtolettermatch.SoundToLetterMatch", ["135"]);	
			
			mainScreenProxy = new SoundToLetterMatch(this,id)
				
				
			//gameScreenProxy = new GameSpace(this)
			
        }
			
		public static function isThis():*{
			return ths
		}
		public static function isStage():*{
			return stg
		}
		public static function isRoot():*{
			return Root
		}
		
	}
	
}
