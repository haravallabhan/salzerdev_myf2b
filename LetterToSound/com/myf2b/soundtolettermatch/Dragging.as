﻿package  com.myf2b.soundtolettermatch {
	
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.EventWithData;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import com.myf2b.soundtolettermatch.manager.SoundManager
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import fl.transitions.*;
	import fl.transitions.easing.*;
	import flash.filters.BitmapFilterQuality;
	import com.myf2b.ui.*;
	
	public class Dragging {
		private var _letterXML:XMLList;
		
		private var _getCurrentLipValue:Number = 0;
		private var drapLipLen:Number = 3;
		
		private var _dragAttempt:Number = 0;
		private var attemptCount:Number = 0;
		private var consAttempt:Number =-1; 
		private var resetLen:Number = 0
		private var _shootCount:Number = 1;	
		
		public var _isCorrectOrWrong:String = "";	
		public var isRoundInfor_str:String = "";
		
		private var soundsArr = [];
		private var _highLightMcArr = [];
		private var _isCorrectArr = [];
		private var wrongMc_arr = []
		
		private var isMonsterLipClicked:Boolean = false;
		private var isDragPlaying:Boolean = false;
		private var currentDealLetter:String = "";

		private var ths:*;
		private var stg:* ;	
		private var _levelStatus:levelStatus;	
		
		public function Dragging(aThs) {
			// constructor code
			ths = SoundToLetterMatchShell.isThis();
			stg = SoundToLetterMatchShell.isStage();
			 
			init()
			
			
			
		}
		
		
		private function init() {
			_createLetterSound()
		}
		
		private function _createLetterSound():void 
		{
			for (var i:uint = 0; i < 3; i++) {
				SoundToLetterMatch._drgObjLdr_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
				SoundToLetterMatch._drgObjLdr_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)
			}
		}
		
		private function playLetterSound(e:MouseEvent):void 
		{
			_getCurrentLipValue = Number(String(e.target.parent.no))	
			trace("deal " + e.target.parent.letterToDeal)
			currentDealLetter = String(e.target.parent.letterToDeal)
			trace("_getCurrentLipValue "+_getCurrentLipValue)
			_letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == String(e.target.parent.letterToDeal))				
			soundsArr.push( { key:"currItemAudio", url:_letterXML.letterSound } )			
			SoundManager.add(soundsArr, onGameSoundsComplete)
			SoundMixer.stopAll();
			SoundManager.play("currItemAudio", 1);		 			
		}
		
		private function onLipMouseDown_Handler(event):void {			
			isMonsterLipClicked = false;	
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}			
			event.target.parent.startDrag()
			event.target.parent.parent.parent.addChild(event.target.parent.parent)	
			ths.setChildIndex(event.target.parent,ths.numChildren-1)
				
			_getCurrentLipValue = Number(String(event.target.parent.no))	
			currentDealLetter = String(event.target.parent.letterToDeal)
			//
			stg.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
			stg.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler); 	
			//				
			event.target.parent.addEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
		}
		
		function onStageMouseUp_Handler (e) {		
			
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].stopDrag();
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].x = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posx;
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].y = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posy;		
		}
		
		function onStageMouseLeave_Handler (e) {
			//trace("out ")			 
			
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].stopDrag();
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].x = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posx;
			SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].y = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posy;
			
		}
		
		private function onLipMouseUp_Handler(event):void {
			
			event.target.parent.stopDrag();	
			event.target.parent.removeEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
			
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			isDragPlaying = true;
			//if(	_letterBoardArr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.monsterLoaderArr[0]) || _letterBoardArr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.blueBoardLoaderArr[0]) )
			if(	SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.monsterCoverMc))
			{					
				_dragAttempt++;		
				 trace(" hitted")
				if(SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].letterToDeal == SoundToLetterMatch.LETTER_TO_DEAL && isMonsterLipClicked == false){
								
					var xmlList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)											 
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].visible = false;								
					SoundToLetterMatch._handBrdLoded_arr[0].monsterHandBoard.gotoAndStop(SoundToLetterMatch.LETTER_TO_DEAL)						
					isDragPlaying = true;
					_levelStatus.gotoAndStop(_shootCount+1);					
					_isCorrectOrWrong = "correct";					
					attemptCount++;		
					 
					if(attemptCount == 1){
						_isCorrectArr.push(1)				
					}else if(attemptCount == 2){
						_isCorrectArr.push(2)
					}else if(attemptCount >= 3){
						_isCorrectArr.push(3)
					} 
					
					for ( var j = 0; j < _isCorrectArr.length;j++){
						if(_isCorrectArr[j] == 1){
							_levelStatus["star"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;		
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt++
						}else if(_isCorrectArr[j] == 2){
							_levelStatus["secondtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}else if(_isCorrectArr[j] >= 3){
							_levelStatus["thirdtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}
					}
					
					disableDrag()
					 				
					
				}else{				
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].x = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posx;
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].y = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posy;
					_levelStatus.gotoAndStop(_shootCount+1);
					_isCorrectOrWrong = "wrong";
					_levelStatus["wrong"+_shootCount].visible = true;
					_levelStatus["star"+_shootCount].visible= false;
					_levelStatus["emptyIcon"+_shootCount].visible= false;
					_levelStatus["secondtry"+_shootCount].visible= false;
					_levelStatus["thirdtry"+_shootCount].visible= false;
					
					wrongMc_arr[_getCurrentLipValue].visible = true;
					ths.setChildIndex(wrongMc_arr[_getCurrentLipValue], ths.numChildren - 1);
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].mouseEnabled = false;
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].mouseChildren = false;						
					attemptCount++;	
					trace(" reset ")
				}				
			}	
			 
			
			if(_isCorrectOrWrong !="correct"){
				SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].x = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posx;
				SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].y = SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].posy;
			}
			isMonsterLipClicked = false;
		};
		//
		public function monsterLipClicked(){
			
		 
			  
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			 
			 
			if(isMonsterLipClicked == true){
				isMonsterLipClicked = false;		 
				
				if(currentDealLetter == SoundToLetterMatch.LETTER_TO_DEAL){
					 
					var xmlList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)	
						
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].visible = false;										
					SoundToLetterMatch._handBrdLoded_arr[0].monsterHandBoard.gotoAndStop(SoundToLetterMatch.LETTER_TO_DEAL)
					_isCorrectOrWrong = "correct";										
					_levelStatus.gotoAndStop(_shootCount+1);	
					attemptCount++;								
					
					if(attemptCount == 1){
						_isCorrectArr.push(1)				
					}else if(attemptCount == 2){
						_isCorrectArr.push(2)
					}else if(attemptCount >= 3){
						_isCorrectArr.push(3)
					} 
					
					for ( var j = 0; j < _isCorrectArr.length;j++){						 
						if(_isCorrectArr[j] == 1){
							_levelStatus["star"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;		
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt++
						}else if(_isCorrectArr[j] == 2){
							_levelStatus["secondtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}else if(_isCorrectArr[j] >= 3){
							_levelStatus["thirdtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}
					}
					 		
					 					
					disableDrag()			
					
				}else{					 
					_isCorrectOrWrong = "wrong";
					_levelStatus.gotoAndStop(_shootCount+1);
					_levelStatus["wrong"+_shootCount].visible = true;
					_levelStatus["star"+_shootCount].visible= false;
					_levelStatus["emptyIcon"+_shootCount].visible= false;
					_levelStatus["secondtry"+_shootCount].visible= false;
					_levelStatus["thirdtry"+_shootCount].visible= false;					
					attemptCount++;					
					//wrongMc.visible = true;
					wrongMc_arr[_getCurrentLipValue].visible = true;
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].mouseEnabled = false;
					SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].mouseChildren = false;
					ths.setChildIndex(wrongMc_arr[_getCurrentLipValue], ths.numChildren - 1)						
				}
				SoundToLetterMatch._drgObjLdr_arr[_getCurrentLipValue].addEventListener(MouseEvent.CLICK,playLetterSound);
				
			}
		}
		//
		
		private function disableDrag(){			
			for(var i=0;i<drapLipLen;i++){
				SoundToLetterMatch._drgObjLdr_arr[i].removeEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)
				SoundToLetterMatch._drgObjLdr_arr[i].removeEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
				SoundToLetterMatch._drgObjLdr_arr[i].removeEventListener(MouseEvent.CLICK, playLetterSound)
			}
		}
		private function enableDrag(){
			for(var i=0;i<drapLipLen;i++){
				SoundToLetterMatch._drgObjLdr_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)
				SoundToLetterMatch._drgObjLdr_arr[i].addEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
				SoundToLetterMatch._drgObjLdr_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
			}
		}
		
		
		
		
		
		
		
		
		
		private function onGameSoundsComplete(e:Event):void {
			 	
		}
	 

	}
	
}
