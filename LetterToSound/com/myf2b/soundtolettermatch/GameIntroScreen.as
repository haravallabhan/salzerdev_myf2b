﻿package com.myf2b.soundtolettermatch
{

	import flash.display.MovieClip;
	import flash.events.*;
 
import com.myf2b.core.CoreLoader;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.*;
	import com.myf2b.core.*;
	import com.greensock.*; 
	import com.greensock.plugins.*;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.system.Security;
    import flash.system.SecurityPanel;
	

	public class GameIntroScreen extends MovieClip {

		public var progressArray:Array=[];
		private var progressNr:Number=0;
		public var objectsArray:Array=[];
		private var assetsXML:XML;
		private var presetsXML:XML;
		private var initStartX:Number=90;
		private var initStartY:Number=225;
		private var startX:Number=90;
		private var startY:Number=225;
		private var columns:Number=7;
		private var spaceX:Number=30;
		private var spaceY:Number=9;
		private var objectWidth:Number=93;
		private var objectHeight:Number=91;
		public var selectedObject=undefined;
		private var sound:CoreSound;
		private var ch:SoundChannel;
		private var e_Time:Timer=new Timer(10);

		private var _bookmarkLoaded:Boolean = false;
		private var _bookmarkXML:XML;
		private var gameKey:String;
		private var speak_Word:String;
		 
		 
		 
		private var ths:*;
		private var stg:*;

		public function GameIntroScreen() {
			
			Security.allowDomain("*")
			
			ths = SoundToLetterMatchShell.isThis();
			stg = SoundToLetterMatchShell.isStage();
			
			//_loadBookmarks();
				init();
		}

		private function _loadBookmarks():void {
			// Let the user know what we are doing
			var loadingMessage:TextField = new TextField();
			loadingMessage.text = "Loading your previous progress*.";
			var format1:TextFormat = new TextFormat();
		    format1.font="Arial";
		    format1.size=40;
		    loadingMessage.setTextFormat(format1);
			loadingMessage.autoSize = TextFieldAutoSize.CENTER;
			loadingMessage.x = stg.width/2-loadingMessage.width/2;
			loadingMessage.y = stg.height/2-loadingMessage.height/2;
			loadingMessage.name = "loadingMessage";
			
			trace("loadingMessage" + loadingMessage);
			ths.addChild(loadingMessage);
			// Load any bookmark data			
		
		}

		 
		private function init():void {
			 
			createObjects();
			 
		}
		
		private function createObjects():void{	  	 		 
		  
			if (ths.getChildByName("introScreen") != null) {
					ths.removeChild(ths.getChildByName("introScreen"))
				}
				
		  var introScreen:gameIntroScreen = new gameIntroScreen()
		  ths.addChild(introScreen)
		  introScreen.name = "introScreen";
		  introScreen.addEventListener(MouseEvent.CLICK, onIntroScreen_Handler)
		  // 
		  introScreen.buttonMode = true;		  
		}
		
		private function onIntroScreen_Handler(e:MouseEvent):void {			
			if (String(e.target.name).split("_")[1] != null) {				
				selectedObject = String(e.target.name).split("_")[1]	
				trace(" selectedObject=== " + selectedObject);		
				ths.removeChild(ths.getChildByName("introScreen"))
				dispatchEvent(new Event("INTRO_SCREEN_GO_BTN_CLICKED"))
			}		
		}
		
		private function goBtn_Handler(e) {			
			if (selectedObject != "" && selectedObject != undefined) {
				//trace("selectedObjectchk=== " + selectedObject)
				//trace(" Go BTn Clicked..")
				ths.removeChild(ths.getChildByName("introScreen"))
			//	ths.removeChild(ths.getChildByName("loadingMessage"))
				dispatchEvent(new Event("INTRO_SCREEN_GO_BTN_CLICKED"))
			}	
		}		 
	}

}