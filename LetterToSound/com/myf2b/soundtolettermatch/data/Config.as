package com.myf2b.lettertosoundmatch.data 
{
	/**
	 * ...
	 * @author Michal Chlebowski
	 */
	public class Config 
	{
		public static const SAVE_KEY:String = "Lettertosoundmatch";
		
		public static const SMALL_GRADE:Number = 0.75;
		public static const BIG_GRADE:Number = 0.8;
		
		public static var MASTERY_PERCENTS:Number;
		public static var DATA_URL:String;
		public static var DEFAULT_POINTS:uint;
		public static var HINT_POINTS:uint;
		public static var SEPARATOR:String;
		public static var SPLASH_GFX:String;
		public static var GAME_GFX:String;
		public static var CONFIG_XML:XMLList;
		
		public static var SETS_XML :XMLList;
		public static var BONUS_POINTS_TEXT:String;
		public static var BONUS_NO_HINT_TEXT:String;
		
		public static var LEVEL_FAILED_TITLE:String = "WHOOPS!";
		public static var LEVEL_SUCCEED_TITLE:String = "CONGRATULATIONS!";
		public static var RESULT_TEXT:String;
		public static var WHAT_NEXT_INFO:String;
		
		public static var GAME_XML_ID:String = "135";
		public static var GAME_KEY:String = "135";
		
		public static const GAME_MODULE_ID:String = "slime_game"
		public static var GAME_SESSION_ID:String = "";
		
		public static const GAME_AREA_WIDTH:Number = 768;
		public static const GAME_AREA_HEIGHT:Number = 544;
		
		public static var ITEMS_COUNT:uint = 0;
		
		public static var USERPOPUP_TITLE:String = "For Educators";
		
		//public static var USERPOPUP_TITLE_PARENT = "For Parents";
		
		public static var USERPOPUP_INFO:String = "This information is for teachers.";
		
		public static var USERPOPUP_SUBTITLE:String = "At Home";
		
		public static var USERPOPUP_PARENT_INFO:String = "This information is for parents.";
		
		public static var IS_A_TEACHER:Boolean = false;
		
		
	}
}