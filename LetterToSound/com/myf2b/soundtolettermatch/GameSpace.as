﻿package com.myf2b.soundtolettermatch
{
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreSound;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import flash.media.SoundLoaderContext;
	import flash.text.TextField;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import fl.transitions.*;
	import fl.transitions.easing.*;
	import flash.filters.BitmapFilterQuality;
	import com.myf2b.ui.*;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
 
	public class GameSpace extends MovieClip
	{
		
		private var _getCurrentLipValue:Number = 0;
		
		public static var _shootCount:Number = 1;		
		private var drapLipLen:Number  = 3;
		private var totalLen:Number = 3;
		private var totalLetterLen:Number = 0;
		private var _dragAttempt:Number = 0;
		private var attemptCount:Number = 0;
		private var consAttempt:Number =-1; 
		private var resetLen:Number = 0
		
		public var _isCorrectOrWrong:String = "";	
		public var isRoundInfor_str:String = "";
		private var _getDealTextValue:String = "";
		
		private var isMonsterLipClicked:Boolean = false;
		public var isDragPlaying:Boolean = false ;
		private var loadCnt2:Number = 0
		
		
		private var _letterXML:XMLList;
		
		private var _highLightMcArr = [];
		private var _letterArr = [] //= ["o","m","a"]
		private var _orderArr = [];
		private var _isCorrectArr = [];
		private var _letterBoardArr = [];
		private var _letter_1_BoardArr = [];
		private var _letter_2_BoardArr = [];
		private var _openTumblerdArr = [];
		
		private var tweenArr = []
		
		private var _randomLetters = []
		private var loader_Letter_arr = []
					
		private var _closeTumblerArr = []
		private var isStart_Game:Boolean = false;	
		
		private var _levelStatus:levelStatus;		
		private var wrongMc_arr = []
		var sound:CoreSound
		var bellSound:CoreSound 
		private var ch:SoundChannel
		var ch3:SoundChannel
		var mySound:Sound
		var myChannel:SoundChannel
		private var format:TextFormat;
		
		
		private var _sync1 : TextFieldSoundSync;
		private var _sync2 : TextFieldSoundSync;
		
		private var blinkCnt = 0;
		private var glowCnt = 0;
		private var blurx = 25;
		private var blury = 25;
		
		private var boardBlurX = 10;
		private var boardBlurY = 10;
		
		private var glowColor = 0x009EDB//0xCC3333;
		private var glow:GlowFilter = new GlowFilter();
		private var boardGlow:GlowFilter = new GlowFilter();
		
		private var glow_str:String;
		
		
		private var btn_blinkCnt:Number = 0;
		private var txtField_arr = [];
		
		var levelEndMc:mc_levelEnd
		
		
		var rand_item_arr = [];
		var iVal:Number = 0;
		var prevNum:Number;
		var getNum:Number;
		
		private var comb_arr = [012,  120, 201];
		
		 
		private var xarr = [10, 550, 750];
		private var yarr = [750, 10, 350];
		private var letterTextCont_arr = [];
		
		private var letterXpos1_arr = [125, 425, 725];
		
		private var letterXpos2_arr = [425, 725, 125];
		
		private var letterXpos3_arr = [725, 125, 425];
		
		private var letter_arr = ["a", "o", "m"]
		private var isletter1PosA:Boolean = false;
		private var isletter1PosB:Boolean = false;
		private var isletter1PosC:Boolean = false;
		private var isletter2PosA:Boolean = false;
		private var isletter2PosB:Boolean = false;
		private var isletter2PosC:Boolean = false;
		private var isInitMonsterEffect_bool:Boolean = false;
		
		
		
		private var ths:*;
		private var stg:* ;
		
		
		
		
		public function GameSpace()
		{
			//TODO: implement function
			//	super();
			ths = SoundToLetterMatchShell.isThis();
			stg = SoundToLetterMatchShell.isStage();
			
			
			//ths.addEventListener("TUMBER_SWAP_END",tumblerSwapping)
			 
			//-------------------Monster Glow Effect---------------------------///
			glow.color = glowColor//0xCC3333//0xffff00;
			glow.blurX = blurx;
			glow.blurY = blury;
			glow.quality = BitmapFilterQuality.MEDIUM;
			boardGlow.color = glowColor
			boardGlow.quality = BitmapFilterQuality.MEDIUM;
			//-----------------------------------------------------------------//
			
		//	
			addEventListener("END_AUTOPLAY_SOUND", endOfAutoPlay_Handler)
			addEventListener("GAME_INIT", startInitGame_Handler)	
			addEventListener("REMOVE_INIT_MONSTER_GLOW", removeInitMonsterGlow_Handler)
			addEventListener("END_OF_GLOW_EFFECT", endOfGlowEffect_Handler)						
			addEventListener("ENABLE_DRAG_LISTENERS", enableDrag_Handler)
			
			ths.tumberAnim_mc.gotoAndStop(1)	
			
			var newFont = new TimesRegular();
			format = new TextFormat();
			format.font = newFont.fontName;
			format.color = 0xFF00FF;
			format.size =  1;  
			format.align = "left";
			
			//addEventListener("START_GAME",onStart_Sound_Comp_Handler)
			getLevelStatus();
			createCrossMark()
			LoadCloseTumblerAnimation()
			
			ths.dummyBg.visible = true;
			ths.setChildIndex(ths.dummyBg, ths.numChildren-1)
			//ths.tumberAnim_mc.addFrameScript(70,tumblerSwapping)			
		}
		
		private function removeInitMonsterGlow_Handler(e:Event):void {
				
				blinkCnt = 0
				removeEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)
				glow.alpha = 0;		
				SoundToLetterMatch.monsterLoaderArr[0].filters = [glow];				
				dispatchEvent(new Event("END_OF_GLOW_EFFECT"))
			 
		}
		
		
		
		private function endOfAutoPlay_Handler(e:Event):void 
		{
			trace("END_AUTOPLAY_SOUND")
			SoundToLetterMatch.monsterCoverMc.mouseEnabled = true;				
			addEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)			
		}
		
		private function startInitGame_Handler(e:Event):void {
			trace(" roundStart ")
			//SoundMixer.stopAll();
			var ch:SoundChannel = new SoundChannel();
			 _letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)	
			var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.monsterHandBoardSound ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);				
				createRoundInformation("roundStart")
			}		
		}
		
		
		
		private function LoadCloseTumblerAnimation():void {
			var xList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)/**/		
			 
			for(var i=0;i<drapLipLen;i++){				 
				var liploader:Loader = new Loader();					
				liploader.load(new URLRequest(SoundToLetterMatch.presetsXML.variables.cupDown))				 
				liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, oncloseTumblerLoader_Complte(i))					
			}
		}
		
		function oncloseTumblerLoader_Complte(i):Function {			
			return function(e:Event):void {
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				//var image:DisplayObject = (e.target as LoaderInfo).content;
				_closeTumblerArr[i] = new MovieClip()
				_closeTumblerArr[i] =  ((e.target as LoaderInfo).content) as MovieClip ;
				_closeTumblerArr[i].x = 100+(i*300);
				_closeTumblerArr[i].y = 160;
				_closeTumblerArr[i].posx = _closeTumblerArr[i].x;
				_closeTumblerArr[i].posy = _closeTumblerArr[i].y;
				ths.addChild(_closeTumblerArr[i])
				//ths.setChildIndex(_closeTumblerArr[i], ths.numChildren-1)				
			}		
		}
		
		private function createSuffle_Tumbler():void 
		{
			//var xList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)
			var shuffle_str:String = SoundToLetterMatch.presetsXML.variables.lipAnimationSound
			mySound = new Sound();
			myChannel = new SoundChannel();
			mySound.load(new URLRequest(shuffle_str));//xList.letterAnimSound
			myChannel = mySound.play();
			myChannel.addEventListener(Event.SOUND_COMPLETE, onTumblerAnim_SoundComp_Handler)	
			for(var i=0;i<drapLipLen;i++){
				tweenArr[i] = new Tween(_closeTumblerArr[i], "x", Regular.easeInOut, xarr[i], yarr[i], .2, true)
				tweenArr[i].addEventListener(TweenEvent.MOTION_FINISH, handleFinish);
			}
		}
		
		
		private function handleFinish(e:Event):void {
			e.target.yoyo();	
		}
		
		private function onTumblerAnim_SoundComp_Handler(e:Event):void 
		{
			ths.dummyBg.visible = false;
			myChannel.removeEventListener(Event.SOUND_COMPLETE, onTumblerAnim_SoundComp_Handler)
			ths.openCont.visible = true;
			for(var i=0;i<drapLipLen;i++){
				tweenArr[i].stop()
				_closeTumblerArr[i].x = _closeTumblerArr[i].posx;
				_closeTumblerArr[i].y = _closeTumblerArr[i].posy;
				_closeTumblerArr[i].visible = false;				
			}			 
			trace("chkbool= "+isStart_Game)			
			if (isStart_Game == false) {
				isStart_Game = true;				 
				createRandomLetters()	
				loadLetterBoard()				
				loadOpenTumbler()					 
			}else{				 
				 ths.openCont.visible = true;
				 SoundToLetterMatch.monsterCoverMc.mouseEnabled = true;
				 resetLetterBoard();			 
			}	
			dispatchEvent(new Event("ENABLE_MONSTER_LISTENERS"))
		}
		
		
		private function createCrossMark():void 
		{
			for (var i = 0; i < 3;i++ ) {
				wrongMc_arr[i] = new wrong_mc()
				//ths.wrongContainer.addChild(wrongMc);
				ths.addChild(wrongMc_arr[i]);
				wrongMc_arr[i].name = "wrong_mc";
				wrongMc_arr[i].x = 140 + (i * 300);
				wrongMc_arr[i].y = 190;			
			}
			CrossMarkDisable()		
		}
		
		private function CrossMarkDisable():void 
		{
			for (var i = 0; i < 3; i++ ) {
				wrongMc_arr[i].visible = false;
			}			
		}
		
		private function CrossMarkEnable():void {
			for (var i = 0; i < 3; i++ ) {
				wrongMc_arr[i].visible = true;
			}			
		}
		
		
		private function onStart_Sound_Comp_Handler(e){
			trace("START_GAME")
			//ths.tumberAnim_mc.gotoAndPlay(2);			
			//ths.tumberAnim_mc.addFrameScript(ths.tumberAnim_mc.totalFrames-1,tumblerSwapping);				
			   
		}
		
		public function tumblerSwapping(){
			trace("swap Finished")				
			ths.tumberAnim_mc.gotoAndStop(1);			
			ths.tumberAnim_mc.addFrameScript(ths.tumberAnim_mc.totalFrames-1,null);		
			ths.tumberAnim_mc.visible = false;					
		}
		
		function loadOpenTumbler() {
			var xList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)/**/				 
			for(var i=0;i<drapLipLen;i++){				 
				var liploader:Loader = new Loader();					
				liploader.load(new URLRequest(SoundToLetterMatch.presetsXML.variables.cupUp))				 
				liploader.contentLoaderInfo.addEventListener(Event.COMPLETE, onOpenTumblerLoader_Complte(i))					
			}
		}
		
		function onOpenTumblerLoader_Complte(i):Function {			
			return function(e:Event):void {
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				//var image:DisplayObject = (e.target as LoaderInfo).content;
				_openTumblerdArr[i] = new MovieClip()
				_openTumblerdArr[i] =  ((e.target as LoaderInfo).content) as MovieClip ;
				_openTumblerdArr[i].x = 90+(i*300);
				//_openTumblerdArr[i].y = 90;
				_openTumblerdArr[i].posx = _openTumblerdArr[i].x;
				_openTumblerdArr[i].posy = _openTumblerdArr[i].y;
				ths.openCont.addChild(_openTumblerdArr[i])
				//ths.setChildIndex(_closeTumblerArr[i], ths.numChildren-1)				
			}		
		}	
		
		
 
		
		private function createRandomLetters() {
			 
			totalLetterLen = 0;
			totalLetterLen = SoundToLetterMatch.presetsXML.deal.length(); 		 
			for(var j=0;j<totalLetterLen;j++){
				_letterArr[j] =  SoundToLetterMatch.presetsXML.deal[j].@letter;
				_orderArr[j] =  SoundToLetterMatch.presetsXML.deal[j].@order;				
				//_randomLetters.push({_letter:SoundToLetterMatch.presetsXML.deal[j].@letter,_order:SoundToLetterMatch.presetsXML.deal[j].@order})
			}			
			//_randomLetters = _randomLetters.sort( randomize )				
			_randomLetters = getRandomNoRepeat();
			trace(" Initial --$ " + _randomLetters[0].letter)
			trace(" Initial --$ " + _randomLetters[1].letter)
			trace(" Initial --$ " + _randomLetters[2].letter)
			
			
		}
		
		
 
		private function loadLetterBoard() {		 		 
			var xList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[loadCnt2].letter)				 				
			var letterBoardloader:CoreLoader = new CoreLoader()
			letterBoardloader.contentLoaderInfo.addEventListener(Event.COMPLETE, _openItemloaded_Handler);
			letterBoardloader.load(new URLRequest(xList.monsterLetterBoard));
			trace("xList= "+xList.monsterLetterBoard)			 
		}
		
		function _openItemloaded_Handler(e:Event):void 
		{
			trace(" ======================== _openItemloaded_Handler ===========" )
			
			_letterBoardArr.push(e.target.content);
			
			if (loadCnt2 == 3 - 1)
			{	 
				for (var i:uint = 0; i < 3; i++) {
					
					letterTextCont_arr[i] = new MovieClip()
					ths.addChild(letterTextCont_arr[i])						 
					
					letterTextCont_arr[i].buttonMode = true;						 
					//ths.addChild(_letterBoardArr[i])				 
					
					letterTextCont_arr[i].addChild(_letterBoardArr[i])										
					 				
					letterTextCont_arr[i].buttonMode = true; 
					letterTextCont_arr[i].no = i.toString();
					
					txtField_arr[i] = createCustomTextField(letterTextCont_arr[i].x, letterTextCont_arr[i].y, letterTextCont_arr[i].width, letterTextCont_arr[i].height);
					txtField_arr[i].defaultTextFormat = format;					 
					txtField_arr[i].antiAliasType = AntiAliasType.ADVANCED;
					txtField_arr[i].embedFonts = true;
					txtField_arr[i].autoSize = TextFieldAutoSize.LEFT;					
					letterTextCont_arr[i].addChild(txtField_arr[i])
					txtField_arr[i].text = _randomLetters[i].letter + "_" + i.toString();
					
					letterTextCont_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)								
					letterTextCont_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
				}				
				setpositions()
			}else{
				loadCnt2++;
				loadLetterBoard()
			}
			
		}
		
	
		
		private function setpositions():void {	
		//	trace(" Get Positions and Random values ")
			if (_shootCount == 1) {
				var num1:Number = randomRange(0, 2)
			//	trace("random Num * "+num1)
				for (var i:uint = 0; i < 3; i++) {	
					if (num1 == 0) {
						letterTextCont_arr[i].x = letterXpos1_arr[i]//125+(i*300);	
						isletter1PosA = true;
					}else if(num1 == 1 ) {
						letterTextCont_arr[i].x = letterXpos2_arr[i]//125+(i*300);	
						isletter1PosB = true;
					}else if(num1 == 2 ) {
						letterTextCont_arr[i].x = letterXpos3_arr[i]//125+(i*300);	
						isletter1PosC = true;
					}										 
					letterTextCont_arr[i].y = 190;					
					letterTextCont_arr[i].posx = letterTextCont_arr[i].x; 
					letterTextCont_arr[i].posy = letterTextCont_arr[i].y;						
				}
			}
			
			if (_shootCount == 2) {				 
				
				for (var s:uint = 0; s < 3; s++) {							
					if (isletter1PosA) {
						letterTextCont_arr[s].x = letterXpos3_arr[s]
						isletter2PosA = true
					}
					if (isletter1PosB) {
						letterTextCont_arr[s].x = letterXpos1_arr[s]
						isletter2PosB = true
					}
					if (isletter1PosC) {
						letterTextCont_arr[s].x = letterXpos2_arr[s]
						isletter2PosC = true
					}					
										 
					letterTextCont_arr[s].y = 190;					
					letterTextCont_arr[s].posx = letterTextCont_arr[s].x;
					letterTextCont_arr[s].posy = letterTextCont_arr[s].y;						 
				}
			}
			if (_shootCount == 3) {
				
				for (var k:uint = 0; k < 3; k++) {						
					if (isletter2PosA) {
						letterTextCont_arr[k].x = letterXpos1_arr[k]	
					}
					if (isletter2PosB) {
						letterTextCont_arr[k].x = letterXpos2_arr[k]	
					}
					if (isletter2PosC) {
						letterTextCont_arr[k].x = letterXpos3_arr[k]	
					}
													
					letterTextCont_arr[k].y = 190;					
					letterTextCont_arr[k].posx = letterTextCont_arr[k].x;
					letterTextCont_arr[k].posy = letterTextCont_arr[k].y;				
					 
				}
			}
						
		}
		
		private function getLevelStatus(){
			_levelStatus = new levelStatus()
			ths.addChild(_levelStatus)
			_levelStatus.x = 510;
			_levelStatus.y = 730;
			var _levelInfoMc:mc_levelInfo = new mc_levelInfo()
			addChild(_levelInfoMc)
			_levelInfoMc.x = -250;
			_levelInfoMc.y = 1200;					
		}
		
		
		private function playLetterSound(event:MouseEvent):void {			 
		 _getDealTextValue = event.currentTarget.getChildAt(1).text;
		 
			if(event.type == "clicked"){
				event.currentTarget.stopDrag()
			}
			isMonsterLipClicked = true;	
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			var highlightMc:highlight_mc = new highlight_mc();
			ths.addChild(highlightMc);
			highlightMc.name = "hLight_mc";
			highlightMc.x = event.target.parent.x+88;
			highlightMc.y = event.target.parent.y+38;
			highlightMc.visible = false;	
			_getCurrentLipValue = Number(String(event.currentTarget.no))
			
			 
			for (var i = 0; i < 3; i++ ) {				 
				boardGlow.alpha = 0;
				letterTextCont_arr[i].filters = [boardGlow];
			}
			
			boardGlow.blurX = boardBlurX;
			boardGlow.blurY = boardBlurY;
			boardGlow.alpha = 1;
			letterTextCont_arr[_getCurrentLipValue].filters = [boardGlow];
			 
			if (ch) {
				ch.stop();
			}
			playSndInstructions(); 
		}
		
		
		
		private function onLipMouseDown_Handler(event):void {	
			 
			_getDealTextValue = event.currentTarget.getChildAt(1).text;
			isMonsterLipClicked = false;	
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}			
				
			
			event.currentTarget.startDrag()
			//event.target.parent.parent.parent.addChild(event.target.parent.parent)	
			ths.setChildIndex(event.currentTarget,ths.numChildren-1)
				
			_getCurrentLipValue = Number(String(event.currentTarget.no))	
			
			 for (var i = 0; i < 3; i++ ) {				 
				boardGlow.alpha = 0;
				letterTextCont_arr[i].filters = [boardGlow];
				}
			boardGlow.blurX = 0;
			boardGlow.blurY = 0;
			boardGlow.alpha = 0
			letterTextCont_arr[_getCurrentLipValue].filters = [boardGlow];
			
		//	_getCurrentLipValue = Number(String(event.currentTarget..getChildAt(1).text).split("_")[1])
			trace("_getCurrentLipValue  "+_getCurrentLipValue)			//
			stg.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
			stg.addEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler); 	
			//				
			event.currentTarget.addEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
			
		}
		
		function onStageMouseUp_Handler (e) {			
			letterTextCont_arr[_getCurrentLipValue].stopDrag();
			letterTextCont_arr[_getCurrentLipValue].x = letterTextCont_arr[_getCurrentLipValue].posx;
			letterTextCont_arr[_getCurrentLipValue].y = letterTextCont_arr[_getCurrentLipValue].posy;		
		}
		
		function onStageMouseLeave_Handler (e) {
			//trace("out ")			 			
			letterTextCont_arr[_getCurrentLipValue].stopDrag();
			letterTextCont_arr[_getCurrentLipValue].x = letterTextCont_arr[_getCurrentLipValue].posx;
			letterTextCont_arr[_getCurrentLipValue].y = letterTextCont_arr[_getCurrentLipValue].posy;			
		}
		
		public function monsterLipClicked() {				
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}	
			
		}
		
		
		private function onLipMouseUp_Handler(event):void {			
			event.currentTarget.stopDrag();	
			event.currentTarget.removeEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)			
			boardGlow.blurX = 0;
			boardGlow.blurY = 0;
			boardGlow.alpha = 0
			letterTextCont_arr[_getCurrentLipValue].filters = [boardGlow];
			
			if(ths.getChildByName("hLight_mc")!=null){
				ths.removeChild(ths.getChildByName("hLight_mc"))
			}
			isDragPlaying = true;
					
			//if(	_letterBoardArr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.monsterLoaderArr[0]) || _letterBoardArr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.blueBoardLoaderArr[0]) )
			if(	letterTextCont_arr[_getCurrentLipValue].hitTestObject(SoundToLetterMatch.monsterCoverMc))
			{					
				_dragAttempt++;		 
				 				
				if (_getDealTextValue.split("_")[0] == SoundToLetterMatch.LETTER_TO_DEAL && isMonsterLipClicked == false)
				{			
					trace("______________________ hitted ______________________")
					var xmlList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)											 
					letterTextCont_arr[_getCurrentLipValue].x = SoundToLetterMatch.blueBoardLoaderArr[0].x ;
					letterTextCont_arr[_getCurrentLipValue].y = SoundToLetterMatch.blueBoardLoaderArr[0].y ;
					stg.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp_Handler);
					stg.removeEventListener(Event.MOUSE_LEAVE, onStageMouseLeave_Handler);				 
					
					isDragPlaying = true;
					_levelStatus.gotoAndStop(_shootCount+1);					
					_isCorrectOrWrong = "correct";					
					attemptCount++;			
					ths.dummyBg.visible = true;
					ths.setChildIndex(ths.dummyBg, ths.numChildren - 1);
					SoundToLetterMatch.monsterCoverMc.mouseEnabled = false;
					
					if(attemptCount == 1){
						_isCorrectArr.push(1)				
					}else if(attemptCount == 2){
						_isCorrectArr.push(2)
					}else if(attemptCount >= 3){
						_isCorrectArr.push(3)
					} 
					
					for ( var j = 0; j < _isCorrectArr.length;j++){
						if(_isCorrectArr[j] == 1){
							_levelStatus["star"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;		
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt++
						}else if(_isCorrectArr[j] == 2){
							_levelStatus["secondtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}else if(_isCorrectArr[j] >= 3){
							_levelStatus["thirdtry"+(j+1)].visible= true;
							_levelStatus["emptyIcon"+(j+1)].visible= false;
							_levelStatus["wrong"+(j+1)].visible = false;
							consAttempt = -1
						}
					}
					disableDrag()
					
					if (ch) {
						ch.stop()
					}
					if (ch3) {
						ch3.stop()
					}
					
					playSuccessLetterSound()
					
					 
				}else {	
					/*SoundMixer.stopAll();
					if (SoundMixer.areSoundsInaccessible() == false) {						 						
						dispatchEvent(new Event("ENABLE_MONSTER_LISTENERS"))
						dispatchEvent(new Event("ENABLE_DRAG_LISTENERS"))
					}*/
					disableDrag()
					
					isDragPlaying = false;
					letterTextCont_arr[_getCurrentLipValue].x = letterTextCont_arr[_getCurrentLipValue].posx;
					letterTextCont_arr[_getCurrentLipValue].y = letterTextCont_arr[_getCurrentLipValue].posy;
					_levelStatus.gotoAndStop(_shootCount+1);
					_isCorrectOrWrong = "wrong";
					_levelStatus["wrong"+_shootCount].visible = true;
					_levelStatus["star"+_shootCount].visible= false;
					_levelStatus["emptyIcon"+_shootCount].visible= false;
					_levelStatus["secondtry"+_shootCount].visible= false;
					_levelStatus["thirdtry"+_shootCount].visible= false;
					
					 
					wrongMc_arr[_getCurrentLipValue].visible = true;					 
					for (var i = 0; i < 3;i++ ) {
						if (_getDealTextValue.split("_")[0] != SoundToLetterMatch.LETTER_TO_DEAL) {						
							wrongMc_arr[i].x = 20 + letterTextCont_arr[i].x;
							wrongMc_arr[i].y = 10 + letterTextCont_arr[i].y;							
						}
					}
					 
					ths.setChildIndex(wrongMc_arr[_getCurrentLipValue], ths.numChildren - 1);
					letterTextCont_arr[_getCurrentLipValue].mouseEnabled = false;
					letterTextCont_arr[_getCurrentLipValue].mouseChildren = false;					
					
					attemptCount++;
					//SoundMixer.stopAll();
					if (ch) {
						ch.stop()
					}	
					playSuccessOrTryAgainSound("wrong")
				}				
			}			
			
			if(_isCorrectOrWrong !="correct"){
				letterTextCont_arr[_getCurrentLipValue].x = letterTextCont_arr[_getCurrentLipValue].posx;
				letterTextCont_arr[_getCurrentLipValue].y = letterTextCont_arr[_getCurrentLipValue].posy;
			}
			isMonsterLipClicked = false;
		};
		
		
		//
		private function enableDrag_Handler(e:Event):void 
		{
			trace("enableDrag_Handler")
			enableDrag()
		}
		//
		 
		
		//
		
		
		private function playSndInstructions(aIntroSound:String = ""):void {			 
			var mybuffer:SoundLoaderContext = new SoundLoaderContext(1000,true);
			 if (aIntroSound=="introsnd") {
				 _letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)					 
			 }else {				  
				//_letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[_getCurrentLipValue].letter)	
				_letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == _getDealTextValue.split("_")[0])	
				isDragPlaying = true;
			 }
			  
			
			if(_letterXML.letterSound !=""){
				sound =new CoreSound(new URLRequest(_letterXML.letterSound),mybuffer);
				sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
				sound.addEventListener(ProgressEvent.PROGRESS, onLoadProgress)
				sound.addEventListener(IOErrorEvent.IO_ERROR, onIOError); 
			}
			 
			
			function onComplete(e:Event):void {	
				
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);				
				ch = new SoundChannel();
				ch = sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {				
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);					
					ths.dummyBg.visible = false;
					 
					if (aIntroSound == "") {						
						//letterTextCont_arr[_getCurrentLipValue].addEventListener(MouseEvent.CLICK, playLetterSound);						 
					}else {
						trace("Game Starts/")	
						 
					}			
					 
				}
			}
		}
		private function onLoadProgress(event:ProgressEvent):void 
		{ 
			var loadedPct:uint =         Math.round(100 * (event.bytesLoaded / event.bytesTotal)); 
			trace("The sound is " + loadedPct + "% loaded."); 
		} 
		//
		private function onIOError(event:IOErrorEvent) 
		{ 
			trace("The sound could not be loaded: " + event.text); 
		}
		
	
		
		private function onMonsterGlowEffect_Handler(e:Event):void 
		{
			blinkCnt++
			if(blinkCnt %5==0){		
				glow.alpha = 1;			
				glowCnt++
				if(glowCnt==3){
					glowCnt = 0
					glow.alpha = 0;	
				}
				SoundToLetterMatch.monsterLoaderArr[0].filters = [glow];				 
			} 
			
			if (blinkCnt == 50) {
				trace(" cghj ebdbbd "+isInitMonsterEffect_bool)						
				if (isInitMonsterEffect_bool == true) {
					blinkCnt = 0
					removeEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)
					glow.alpha = 0;		
					SoundToLetterMatch.monsterLoaderArr[0].filters = [glow];	
					dispatchEvent(new Event("END_OF_GLOW_EFFECT"))
				}
				isInitMonsterEffect_bool = true;
			}
			
		}
		
		
		private function onLetterSound_Comp_Handler(event:Event):void {	
			trace(" onLetterSound_Comp_Handler ")
			ths.dummyBg.visible = true;
			ths.setChildIndex(ths.dummyBg, ths.numChildren - 1);
			
		}
		
		private function playSuccessLetterSound():void 
		{	_letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)	
			var _sound:CoreSound  =new CoreSound(new URLRequest(_letterXML.monsterHandBoardSound));
			_sound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			function onComplete(e:Event):void {			
				e.currentTarget.removeEventListener(Event.COMPLETE, onComplete);			
				var ch2:SoundChannel = new SoundChannel();							
				ch2 = _sound.play();				
				ch2.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
				function onSoundComplete(e:Event):void {	
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);	
					playSuccessOrTryAgainSound("correct")
				}
			}
		}
		
		private function playSuccessOrTryAgainSound(_isCorrectOrWrong:String)
		{
			//disableDrag()
			var _successSndArr = [];
			var _failureSndArr = [];		
			var getRandomSound:Number = 0;
			var getFailRandSound:Number = 0;
			var i = 0;
			var fcorr_wrng:String = "";
			var _Sounds:CoreSound 
			var mybuffer:SoundLoaderContext = new SoundLoaderContext(1000,true);
						
			SoundToLetterMatch.monsterCoverMc.mouseEnabled = false;
			
			if (_isCorrectOrWrong == "correct") {	
				
				fcorr_wrng = "correct"
				for( i=0;i<SoundToLetterMatch.presetsXML.variables.successSounds.sound.length();i++){
					_successSndArr[i] = SoundToLetterMatch.presetsXML.variables.successSounds.sound[i]					
				}
				getRandomSound = randomRangeA(0,_successSndArr.length-1)				
				_Sounds = new CoreSound(new URLRequest(_successSndArr[getRandomSound]),mybuffer);
				_Sounds.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
				_Sounds.addEventListener(IOErrorEvent.IO_ERROR, onIOError_Handler); 
				_Sounds.addEventListener(ProgressEvent.PROGRESS, onLoadProgress_Handler)
				
			}else if (_isCorrectOrWrong == "wrong") {		
				fcorr_wrng = "wrong"
				for( i=0;i<SoundToLetterMatch.presetsXML.variables.failSounds.sound.length();i++){
					_failureSndArr[i] = SoundToLetterMatch.presetsXML.variables.failSounds.sound[i]						 
				}
				getFailRandSound = randomRangeA(0, _failureSndArr.length - 1)	
				trace("getFailRandSound = "+getFailRandSound)
				_Sounds = new CoreSound(new URLRequest(_failureSndArr[getFailRandSound]),mybuffer);
				_Sounds.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
				_Sounds.addEventListener(IOErrorEvent.IO_ERROR, onIOError_Handler); 
				_Sounds.addEventListener(ProgressEvent.PROGRESS, onLoadProgress_Handler)
			}			
			
			function onComplete(e:Event):void {				
				
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);				
				ch3 = new SoundChannel();			
				ch3 = _Sounds.play();				
				ch3.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {	
					if(ths.getChildByName("hLight_mc")!=null){
						ths.removeChild(ths.getChildByName("hLight_mc"))
					}		
					ths.dummyBg.visible = false;					
					isMonsterLipClicked = false;
					if (fcorr_wrng == "correct") {
						fcorr_wrng = "";
						 trace(" GO TO NEXT LEVEL>>> ")
						 nextLevel()
					}else if (fcorr_wrng == "wrong") {
						fcorr_wrng = "";
						 trace(" TRY ONE MORE>>> ")
						 dispatchEvent(new Event("ENABLE_MONSTER_LISTENERS"))						  
							dispatchEvent(new Event("ENABLE_DRAG_LISTENERS"))
						 enableDrag();
					}
										
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);						
				}
				
			}		
		}
		
		private function onLoadProgress_Handler(event:ProgressEvent):void 
		{
			var loadedPct:uint =         Math.round(100 * (event.bytesLoaded / event.bytesTotal)); 
				trace("The sound is " + loadedPct + "% loaded."); 
		}
		
		private function onIOError_Handler(event:IOErrorEvent):void 
		{
			trace("The sound could not be loaded: " + event.text);
		}
		
		
		private function disableDrag(){
			
			for(var i=0;i<drapLipLen;i++){
				letterTextCont_arr[i].removeEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)
				//letterTextCont_arr[i].removeEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
				letterTextCont_arr[i].removeEventListener(MouseEvent.CLICK, playLetterSound)
			}
		}
		public function enableDrag(){
			for(var i=0;i<drapLipLen;i++){
				letterTextCont_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)
				//letterTextCont_arr[i].addEventListener(MouseEvent.MOUSE_UP, onLipMouseUp_Handler)
				letterTextCont_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
			}
		}
		
		private function nextLevel(){			
					
			if(_isCorrectOrWrong == "correct"){
				_shootCount++;	
				_dragAttempt = 0;	
				attemptCount =0;
				isMonsterLipClicked = false;
				enableDrag();				
				_isCorrectOrWrong = "gamestarts";
				_randomLetters = []
				//_randomLetters = _randomLetters.sort( randomize );	
				 isInitMonsterEffect_bool = true;
				 
				//_randomLetters = getRandomNoRepeat();
				 
				//trace("_randomLetters- = " + _randomLetters[0].letter)
				//trace("_randomLetters- = " + _randomLetters[1].letter)
				//trace("_randomLetters- = " + _randomLetters[2].letter)
				
				
				ths.round_mc._txt.text = String(Number(_shootCount))
				 SoundToLetterMatch.monsterCoverMc.mouseEnabled = false;
				for (var k = 0; k < drapLipLen; k++) {	
					//letterTextCont_arr[k].x = letterTextCont_arr[k].posx;
				//letterTextCont_arr[k].y = letterTextCont_arr[k].posy;					
					letterTextCont_arr[k].mouseChildren = true;
					letterTextCont_arr[k].mouseEnabled = true;	
				 
					glow.alpha = 0
					letterTextCont_arr[k].filters = [glow];
				}
				isDragPlaying = false;
				enableDrag();
				if(_shootCount>totalLen){
					//trace("Activity Ends/")
					disableDrag();					 
					showResultPopUpAtEnd();
					resetLen = totalLen;
				}else {					
					createRoundInformation("roundPlaying")
				}
				 
			}else{				
				enableDrag()
				_isCorrectOrWrong = "gamestarts";	
				isDragPlaying = false;
				for(var i=0;i<drapLipLen;i++){				
					letterTextCont_arr[i].x = letterTextCont_arr[i].posx;
					letterTextCont_arr[i].y = letterTextCont_arr[i].posy;					 
				}		
				_getCurrentLipValue = 0;
			}			
		}
		
		
		
		private function showResultPopUpAtEnd():void {				
			var resultCnt = 0;
			var resultFailTxt:String;
			var resultSuccessTxt:String;		
			
			if (ths.getChildByName("levelMc") != null) {
				ths.removeChild(ths.getChildByName("levelMc"));
			}
			
			levelEndMc = new mc_levelEnd();
			ths.addChild(levelEndMc);
			levelEndMc.name = "levelMc";
			
			resultSuccessTxt = SoundToLetterMatch.presetsXML.variables.round_complete.resultTextSuccess			 
			levelEndMc.successContainer.SuccessTxt.text  = resultSuccessTxt;
			_sync1 = Ui.EnableTextSync(levelEndMc.successContainer.SuccessTxt, SoundToLetterMatch.presetsXML); 
			//
			resultFailTxt = SoundToLetterMatch.presetsXML.variables.round_complete.resultTextFail				 
			levelEndMc.failContainer.FailTxt.text  = resultFailTxt;
			_sync2 = Ui.EnableTextSync(levelEndMc.failContainer.FailTxt, SoundToLetterMatch.presetsXML); 			
			levelEndMc.levelStatus.gotoAndStop(_isCorrectArr.length + 1);			
			ths.tumberAnim_mc.gotoAndStop(1);
			
			
			for ( var j = 0; j < _isCorrectArr.length;j++){
				if(_isCorrectArr[j] == 1){
					levelEndMc.levelStatus["star"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;	
					resultCnt++
				}else if(_isCorrectArr[j] == 2){
					levelEndMc.levelStatus["secondtry"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;
				}else if(_isCorrectArr[j] >= 3){
					levelEndMc.levelStatus["thirdtry"+(j+1)].visible= true;
					levelEndMc.levelStatus["emptyIcon"+(j+1)].visible= false;					
				}
			}
			
			
			var resultTxt:String
			var myPattern:RegExp
			if(resultCnt == 2 || resultCnt == 3){
				
				levelEndMc.titleTf.text = SoundToLetterMatch.presetsXML.variables.round_complete.levelSucceedTitle;			
				levelEndMc.levelStatus["emptyIcon"+3].visible = false;
				levelEndMc.restart_btn.visible = false;
				
				levelEndMc.failContainer.visible = false
				levelEndMc.successContainer.visible = true;				
				glow_str = "nextbtn";					
				ths.test_mc.gotoAndPlay(3)
				ths.test_mc.addFrameScript(ths.test_mc.totalFrames-1,successTextSync)
				
			}
			
			if(resultCnt == 1 || resultCnt == 0 ){
				
				levelEndMc.titleTf.text = SoundToLetterMatch.presetsXML.variables.round_complete.levelFailedTitle;	
				levelEndMc.restart_btn.visible = true;
				glow_str = "restartbtn"
				levelEndMc.failContainer.visible = true;
				levelEndMc.successContainer.visible = false;				
				ths.test_mc.gotoAndPlay(3)
				ths.test_mc.addFrameScript(ths.test_mc.totalFrames-1,failTextSync)
			//	levelEndMc.nextInfoTxt.text = SoundToLetterMatch.presetsXML.variables.round_complete.whatNextFailInfo;
			}			
					
			_isCorrectArr = null;
			_isCorrectArr = new Array()
				
			SoundToLetterMatch.blueBoardLoaderArr[0].monsterHandBoard.gotoAndStop(1)
			ths.round_mc._txt.text = String(Number(0))
			ths.dummyBg.visible = false;		
			
			for(var i=0;i<drapLipLen;i++){				 
				_closeTumblerArr[i].visible = false;
				_closeTumblerArr[i].x = _closeTumblerArr[i].posx
				_closeTumblerArr[i].y = _closeTumblerArr[i].posy
				letterTextCont_arr[i].visible = false;			
				wrongMc_arr[i].visible = false;
			}
			SoundToLetterMatch.monsterLoaderArr[0].visible = false;
			ths.openCont.visible = false;		
			isletter1PosC = false;
			isletter1PosA = false;
			isletter1PosB = false;
			
			isletter2PosC = false;
			isletter2PosA = false;
			isletter2PosB = false;
			
				
		}
		
		private function successTextSync() {
			trace(" successTextSync ")
			ths.test_mc.addFrameScript(ths.test_mc.totalFrames-1,null)
			_sync1.speakAndDo(celebrateCompletion);
			addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers)
			levelEndMc.nxtBtn.buttonMode = true;	
			levelEndMc.nxtBtn.addEventListener(MouseEvent.CLICK,onNextBtn_Handler)
		}
		
		
		private function failTextSync() {
			ths.test_mc.addFrameScript(ths.test_mc.totalFrames-1,null)
			trace(" failTextSync ")
			_sync2.speakAndDo(failSyncSoundCompletion)
			addEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers)
			levelEndMc.restart_btn.addEventListener(MouseEvent.CLICK, onReset_Handler)
			levelEndMc.restart_btn.buttonMode = true;
			
		}
		
		
		
		
		private function celebrateCompletion(e):void {				
			var ch:SoundChannel = new SoundChannel();			
			var _sound:CoreSound = new CoreSound( new URLRequest(  SoundToLetterMatch.presetsXML.variables.masteredSound  ) );				
			ch = _sound.play();
			ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
			function onSoundComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
				 trace(" celebrateCompletion Sound Completed ");	
				
			}
		}
		
		private function failSyncSoundCompletion(e):void {
				
		}
			
		private function onNextBtn_Handler(e:MouseEvent):void 
		{
			trace(" Next Btn Handler")
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers)
			SoundMixer.stopAll();
			glow.alpha = 0;
			levelEndMc.nxtBtn.filters = [glow];
		}
		
		
		
		
		private function createGlowEffectForBtn_Handlers(e):void 
		{
			btn_blinkCnt++
			if(btn_blinkCnt %5==0){		
				glow.alpha = 1;			
				glowCnt++
				if (glowCnt == 3) {				 
					glowCnt = 0
					glow.alpha = 0;	
				}
				 
				if (glow_str == "restartbtn") {
					levelEndMc.restart_btn.filters = [glow];
				}else if(glow_str == "nextbtn") {
					levelEndMc.nxtBtn.filters = [glow];
				}else {
					SoundToLetterMatch.monsterLoaderArr[0].filters = [glow];
				}
			} 
			
			if(btn_blinkCnt == 50){
				btn_blinkCnt = 0
							 
				glow.alpha = 0;		
				if (glow_str == "restartbtn") {
					levelEndMc.restart_btn.filters = [glow];
				}else if(glow_str == "nextbtn") {
					levelEndMc.nxtBtn.filters = [glow];
				}else {
					SoundToLetterMatch.monsterLoaderArr[0].filters = [glow];
				}
				removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers)	
				// dispatchEvent(new Event("END_OF_GLOW_EFFECT"))
			}
		}
		
		
		private function onReset_Handler (event:MouseEvent):void {
			
			event.target.parent.visible = false;			
			_isCorrectArr = null;		
			_isCorrectArr = new Array()
			removeEventListener(Event.ENTER_FRAME, createGlowEffectForBtn_Handlers)
			SoundMixer.stopAll();
			glow.alpha = 0;	
			levelEndMc.restart_btn.filters = [glow];
			
			for ( var j = 0; j < resetLen;j++){
				_levelStatus["emptyIcon"+(j+1)].visible= true;
				_levelStatus["wrong"+(j+1)].visible = false;
				_levelStatus["star"+(j+1)].visible= false;				
				_levelStatus["secondtry"+(j+1)].visible= false;
				_levelStatus["thirdtry"+(j+1)].visible= false;
			}
					
			for(var i=0;i<drapLipLen;i++){				
				letterTextCont_arr[i].x = letterTextCont_arr[i].posx;
				letterTextCont_arr[i].y = letterTextCont_arr[i].posy;
				letterTextCont_arr[i].visible = true;
				letterTextCont_arr[i].mouseChildren = true;
				letterTextCont_arr[i].mouseEnabled = true;
				_closeTumblerArr[i].visible = true	;
				
				boardGlow.blurX = 0;
				boardGlow.blurY = 0;
				boardGlow.alpha = 0
				letterTextCont_arr[i].filters = [boardGlow];
			}	
					
			//_randomLetters = _randomLetters.sort( randomize ) ;
			_shootCount = 1;
			_randomLetters = getRandomNoRepeat();
			
			ths.tumberAnim_mc.gotoAndStop(1);
			ths.tumberAnim_mc.visible = true;					
			ths.openCont.visible = false;	
			SoundToLetterMatch.monsterLoaderArr[0].visible = true;
			for(var ss=0;ss<drapLipLen;ss++){	
				letterTextCont_arr[ss].visible = false;				 
			}
					
			_getCurrentLipValue = 0;
				
			_dragAttempt = 0;	
			attemptCount =0;
			_isCorrectOrWrong = "gamestarts";
			ths.round_mc._txt.text = String(Number(_shootCount));
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount))
			consAttempt = -1;
			resetLen =0;
			enableDrag();	
			CrossMarkDisable()
			isStart_Game = false;			
			SoundToLetterMatch.blueBoardLoaderArr[0].monsterHandBoard.gotoAndStop(1);	
			_letter_1_BoardArr = []
			_letter_2_BoardArr = []
			_letterBoardArr = []
			loadCnt2 = 0;
			loader_Letter_arr = [];
			ths.dummyBg.visible = false;
			isletter1PosC = false;
			isletter1PosA = false;
			isletter1PosB = false;
			
			isletter2PosC = false;
			isletter2PosA = false;
			isletter2PosB = false;
			createRoundInformation("roundReset")
			
		}
		
		
		public function tumblerSwappingReset() {	
			
		//	ths.tumberAnim_mc.addFrameScript(ths.tumberAnim_mc.totalFrames-1,null);		
			//ths.tumberAnim_mc.visible = false;	
		//	ths.openCont.visible = true;	
		
		}
		
		private function createRoundInformation(aParam:String){			
			ths.roundFly_mc.gotoAndPlay(2)				
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)				
			isRoundInfor_str = aParam					
			ths.roundFly_mc.roundAnim_mc._txt.text = String(Number(_shootCount))
			ths.roundFly_mc.visible = true;		
			ths.roundFly_mc.addFrameScript(5-1,roundFlyDepth);
			ths.roundFly_mc.addFrameScript(11-1,roundFlyAnimation);					
		}
		
		private function roundFlyDepth(){			
			ths.roundFly_mc.addFrameScript(5-1,null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)
		}
				
		private function roundFlyAnimation(){			
			ths.roundFly_mc.addFrameScript(11-1,null);
			ths.setChildIndex(ths.roundFly_mc, ths.numChildren-1)			
			ths.roundFly_mc.stop()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.gotoAndStop(1)
			startBellSound()			 
		}
		private function startBellSound():void 
		{	
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndPlay(2)
			
			var flyBell_str:String = SoundToLetterMatch.presetsXML.variables.flybellAnimationSound;	
			SoundMixer.bufferTime = 1000;
			trace("flyBell_str&="+flyBell_str)
			bellSound = new CoreSound(new URLRequest(flyBell_str));
			bellSound.addEventListener(Event.COMPLETE, onComplete, false, 0, true);	
			
			function onComplete(e:Event):void {
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);
				var ch:SoundChannel = new SoundChannel();				
				ch = bellSound.play();
				
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
				function onSoundComplete(e:Event):void {					 			
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);
					roundInformation()
				}
			}
			
		}
			
		public function roundInformation(){				
			ths.roundFly_mc.roundAnim_mc.bell_mc.gotoAndStop(1);			
			//ths.roundFly_mc.roundAnim_mc.bell_mc.addFrameScript(ths.roundFly_mc.roundAnim_mc.bell_mc.totalFrames-1,null);					
			ths.roundFly_mc.play()
			ths.roundFly_mc.roundAnim_mc.flyHand_mc.play()
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames-1,roundFlyFreezeAnimation);			
		}
		
		
		private function roundFlyFreezeAnimation(){
			ths.roundFly_mc.addFrameScript(ths.roundFly_mc.totalFrames-1,null);
			ths.roundFly_mc.gotoAndStop(1);			
			
			if (isRoundInfor_str == "roundStart") {

				trace("START THE GAME")
				createSuffle_Tumbler()								
			}else if(isRoundInfor_str == "roundPlaying"){				
				CrossMarkDisable()
				SoundToLetterMatch.blueBoardLoaderArr[0].monsterHandBoard.gotoAndStop(1)							
				ths.openCont.visible = false;				
				for(var i=0;i<drapLipLen;i++){				 
					_closeTumblerArr[i].visible = true;
				}
				addEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)	
				 var ch:SoundChannel = new SoundChannel();
				 _letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)	
				var _sound:CoreSound = new CoreSound( new URLRequest(  _letterXML.monsterHandBoardSound  ) );				
				ch = _sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);				
				function onSoundComplete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);				
					 
				}
				for(var s=0;s<drapLipLen;s++){	
					letterTextCont_arr[s].visible = false;					
				};			
				
			}else if (isRoundInfor_str == "roundReset") {
				trace(" After round reset/")				 
				addEventListener(Event.ENTER_FRAME, onMonsterGlowEffect_Handler)	
				 var ch1:SoundChannel = new SoundChannel();
				 _letterXML = SoundToLetterMatch.presetsXML.deal.(@letter == SoundToLetterMatch.LETTER_TO_DEAL)	
				var _sound1:CoreSound = new CoreSound( new URLRequest(  _letterXML.monsterHandBoardSound  ) );				
				ch1 = _sound1.play();
				ch1.addEventListener(Event.SOUND_COMPLETE,onSound1Complete);				
				function onSound1Complete(e:Event):void {
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSound1Complete);					 
				}
			}
		}
		
		private function endOfGlowEffect_Handler(e:Event):void {
			trace("endOfGlowEffect_Handler")
			 if (isRoundInfor_str == "roundPlaying" || isRoundInfor_str == "roundReset") {				 
					createSuffle_Tumbler()	
			 }
			 
			
		}
		
		
		private function resetLetterBoard() {			
			trace(" come arround letter playing.../")
			 
			 
			 _randomLetters = getRandomNoRepeat();
			 
			//setpositions()			
		
			
			for(var i=0;i<drapLipLen;i++){		
				letterTextCont_arr[i].visible = true;
				letterTextCont_arr[i].no = i.toString()			
			}
			
			for each(var mc:MovieClip in letterTextCont_arr){
				mc.parent.removeChild(mc);
			}			
		  
		 
			loadCnt2 = 0			
			if (_shootCount == 2) {
				loader_Letter_arr = []
				var xList:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[0].letter)
				var xList1:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[1].letter)
				var xList2:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[2].letter)			
				loader_Letter_arr.push(xList.monsterLetterBoard, xList1.monsterLetterBoard, xList2.monsterLetterBoard)
				loadLetter_1_Board()	
				trace(" Second../")
			}else if (_shootCount == 3) {
				trace(" Third../")
				loader_Letter_arr = []
				var xList_1:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[0].letter)
				var xList_2:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[1].letter)
				var xList_3:XMLList = SoundToLetterMatch.presetsXML.deal.(@letter == _randomLetters[2].letter)			
				loader_Letter_arr.push(xList_1.monsterLetterBoard, xList_2.monsterLetterBoard, xList_3.monsterLetterBoard)				
				loadLetter_2_Board()
			}
			 
		}
		private function loadLetter_1_Board() {				 
			var letterBoardloader:CoreLoader = new CoreLoader()				
			letterBoardloader.contentLoaderInfo.addEventListener(Event.COMPLETE, _openItemloaded_1_Handler);				
			letterBoardloader.load(new URLRequest(loader_Letter_arr[loadCnt2]));				
		}
		private function loadLetter_2_Board() {				 
			var letterBoardloader:CoreLoader = new CoreLoader()				
			letterBoardloader.contentLoaderInfo.addEventListener(Event.COMPLETE, _openItemloaded_2_Handler);				
			letterBoardloader.load(new URLRequest(loader_Letter_arr[loadCnt2]));				
		}
		
		
		
		
		//
		function _openItemloaded_1_Handler(e:Event):void 		{
			//trace(" ======================== _openItemloaded_1_Handler ===========" +e.target.content) 				
			_letter_1_BoardArr.push(e.target.content);
			//trace("get board values $$$ " + _letter_1_BoardArr)
			//trace("connnt = "+letterTextCont_arr)
			if (loadCnt2 == 3 - 1)
			{	 
				for (var i:uint = 0; i < 3; i++) {
					
					letterTextCont_arr[i] = new MovieClip()
					ths.addChild(letterTextCont_arr[i])						 
					
					letterTextCont_arr[i].buttonMode = true;						 
					//ths.addChild(_letterBoardArr[i])				 
					
					letterTextCont_arr[i].addChild(_letter_1_BoardArr[i])										
					 				
					letterTextCont_arr[i].buttonMode = true; 
					letterTextCont_arr[i].no = i.toString();
					
					txtField_arr[i] = createCustomTextField(letterTextCont_arr[i].x, letterTextCont_arr[i].y, letterTextCont_arr[i].width, letterTextCont_arr[i].height);
					txtField_arr[i].defaultTextFormat = format;					 
					txtField_arr[i].antiAliasType = AntiAliasType.ADVANCED;
					txtField_arr[i].embedFonts = true;
					txtField_arr[i].autoSize = TextFieldAutoSize.LEFT;					
					letterTextCont_arr[i].addChild(txtField_arr[i])
					txtField_arr[i].text = _randomLetters[i].letter + "_" + i.toString();
					
					letterTextCont_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)								
					letterTextCont_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
				}				
				setpositions()
			}else{
				loadCnt2++;
				loadLetter_1_Board()
			}
			
		}		
		//
		function _openItemloaded_2_Handler(e:Event):void 		{
	//		trace(" ======================== _openItemloaded_1_Handler ===========" +e.target.content) 				
			_letter_2_BoardArr.push(e.target.content);
		//	trace("get board values $$$ " + _letter_2_BoardArr)
			//trace("connnt = "+letterTextCont_arr)
			if (loadCnt2 == 3 - 1)
			{	 
				for (var i:uint = 0; i < 3; i++) {
					
					letterTextCont_arr[i] = new MovieClip()
					ths.addChild(letterTextCont_arr[i])						 
					
					letterTextCont_arr[i].buttonMode = true;						 
					//ths.addChild(_letterBoardArr[i])				 
					
					letterTextCont_arr[i].addChild(_letter_2_BoardArr[i])										
					 				
					letterTextCont_arr[i].buttonMode = true; 
					letterTextCont_arr[i].no = i.toString();
					
					txtField_arr[i] = createCustomTextField(letterTextCont_arr[i].x, letterTextCont_arr[i].y, letterTextCont_arr[i].width, letterTextCont_arr[i].height);
					txtField_arr[i].defaultTextFormat = format;					 
					txtField_arr[i].antiAliasType = AntiAliasType.ADVANCED;
					txtField_arr[i].embedFonts = true;
					txtField_arr[i].autoSize = TextFieldAutoSize.LEFT;					
					letterTextCont_arr[i].addChild(txtField_arr[i])
					txtField_arr[i].text = _randomLetters[i].letter + "_" + i.toString();
					
					letterTextCont_arr[i].addEventListener(MouseEvent.MOUSE_DOWN, onLipMouseDown_Handler)								
					letterTextCont_arr[i].addEventListener(MouseEvent.CLICK, playLetterSound)
				}				
				setpositions()
			}else{
				loadCnt2++;
				loadLetter_2_Board()
			}
			
		}
		//
		
		
		/*public function getRandomNoRepeat():Array
		{			
			var return_arr = [] 
			var letterArray:Array 
			trace("combination -- < "+_letterArr)
			letterArray = RandomLetters.stringShuffule(_letterArr, _orderArr, SoundToLetterMatch.LETTER_TO_DEAL, 3)		
			
			for (var s:int = 0; s < 3; s++)  {
				//trace("getFrom Random LEtter "+letterArray[s].letter)
				return_arr[s] = letterArray[s]				 
			}			
			return letterArray
			//return shuffledLetters 
		}*/
		
		public function getRandomNoRepeat():Array
		{
			trace("repeat --$ " + _letterArr)
			trace("repeatOrd --$ " + _orderArr)
			
			var letterArray:Array = RandomLetters.stringShuffule(_letterArr, _orderArr, SoundToLetterMatch.LETTER_TO_DEAL, 3)
			var arr = []
			
			arr.push(letterArray[0], letterArray[1], letterArray[2])
			
			
			var shuffledLetters:Array = new Array(letterArray.length);				
			var randomPos:Number = 0;
			for (var i:int = 0; i < shuffledLetters.length; i++) //use shuffledLetters.length because splice() will change letters.length
			{
				randomPos = int(Math.random() * letterArray.length);
				shuffledLetters[i] = letterArray.splice(randomPos, 1)[0];    //note this the other way around to the naive approach	
				
			}
			for (var k = 0; k < 3;k++ ) {
				trace(" Random Letter *&* "+arr[k].letter )
			}
			 
			
			return arr//shuffledLetters 
		}
		
		
		
		//
		function randomNumber(low:Number=NaN, high:Number=NaN):Number
		{
			var low:Number = low;
			var high:Number = high;

			if (isNaN(low))
			{
				throw new Error("low must be defined");
			}
			if (isNaN(high))
			{
				throw new Error("high must be defined");
			}

			return Math.round(Math.random() * (high - low)) + low;
		}
		
		
		private function randomRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (1 + maxNum - minNum)) + minNum);
		}
		private function randomRangeA(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (1 + maxNum - minNum)) + minNum);
		}
		
		private function randomize ( a : *, b : * ) : int {
			return ( Math.random() > .5 ) ? 1 : -1;
		}
		//
		private function createCustomTextField(x:Number, y:Number, width:Number, height:Number):TextField
		{
			var result:TextField = new TextField();
			result.x = x;
			result.y = y;
			result.width = width;
			result.height = height;
			result.multiline = true
			result.wordWrap = true;				 
			//addChild(result);			
			return result;
		}
	}
}