﻿package com.myf2b.soundtolettermatch
{

	import flash.display.MovieClip;
	import flash.events.*;

	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.filters.GlowFilter;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.*;
	import com.myf2b.core.*;
	import com.greensock.*; 
	import com.greensock.plugins.*;

	public class Intro1 extends MovieClip {

		public var progressArray:Array=[];
		private var progressNr:Number=0;
		public var objectsArray:Array=[];
		private var assetsXML:XML;
		private var presetsXML:XML;
		private var initStartX:Number=90;
		private var initStartY:Number=225;
		private var startX:Number=90;
		private var startY:Number=225;
		private var columns:Number=7;
		private var spaceX:Number=30;
		private var spaceY:Number=9;
		private var objectWidth:Number=93;
		private var objectHeight:Number=91;
		public var selectedObject=undefined;
		private var sound:CoreSound;
		private var ch:SoundChannel;
		private var e_Time:Timer=new Timer(10);

		private var _bookmarkLoaded:Boolean = false;
		private var _bookmarkXML:XML;
		private var gameKey:String;
		private var speak_Word:String;

		public function Intro1() {
			_loadBookmarks();
		}

		private function _loadBookmarks():void {
			// Let the user know what we are doing
			var loadingMessage:TextField = new TextField();
			loadingMessage.text = "Loading your previous progress*.";
			var format1:TextFormat = new TextFormat();
		    format1.font="Arial";
		    format1.size=40;
		    loadingMessage.setTextFormat(format1);
			loadingMessage.autoSize = TextFieldAutoSize.CENTER;
			loadingMessage.x = this.width/2-loadingMessage.width/2;
			loadingMessage.y = this.height/2-loadingMessage.height/2;
			loadingMessage.name = "loadingMessage";
			
			trace("loadingMessage" + loadingMessage);
			addChild(loadingMessage);

			// Load any bookmark data			
			var bookmarker:Bookmarker = new Bookmarker("see_it_catch_it", SeeItCatchIt._contentKey);
			bookmarker.addEventListener(Event.COMPLETE, _onBookmarkLoaded);
			bookmarker.addEventListener(IOErrorEvent.IO_ERROR, _onBookmarkLoaded);
			bookmarker.load();
		}

		private function _onBookmarkLoaded(e:Event):void {
			e.target.removeEventListener(Event.COMPLETE, _onBookmarkLoaded);
			e.target.removeEventListener(IOErrorEvent.IO_ERROR, _onBookmarkLoaded);

			// Remove the loading message
			removeChild(getChildByName("loadingMessage"));

			// If the bookmark data is not null use it
			if (e.target.data != null) {
				_bookmarkXML = e.target.data;
			} 
			// Otherwise create a new empty _bookmarkXML
			else {
				_bookmarkXML =	<data>
									<progress/>
								</data>;
			}
			trace(_bookmarkXML);

			_bookmarkLoaded = true;

			init();
		}

		private function init():void {
			presetsXML=SeeItCatchIt.presetsXML;
			assetsXML=SeeItCatchIt.assetsXML;

			createObjects();
			goBtn.addEventListener(MouseEvent.CLICK,goClicked);
			mapBottomInstructions.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);
			sunBtn.addEventListener(MouseEvent.CLICK,sunClicked);

			goBtn.buttonMode=mapBottomInstructions.soundBtn.buttonMode=sunBtn.buttonMode=true;

			// Report the word exposure for showing the screen
			Reporter.wordExposure("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID, mapBottomInstructions.t.text);
			Reporter.wordExposure("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID, lettersMastered.t1.text);
			Reporter.wordExposure("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID, "go");
		}

		private function sunClicked(e:MouseEvent):void {
			Reporter.interaction("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID);

			if (gameComplete()) {
				SeeItCatchIt.winnerScreenProxy.init();
			}
		}
		private function soundClicked(e:MouseEvent):void {
			Reporter.interaction("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID);

			// Report exposure to the word
			Reporter.wordExposure("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID, mapBottomInstructions.t.text);

			loadSound();
		}
		private function loadSound():void {
			sound=new CoreSound(new URLRequest(presetsXML.variables.helpSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);

			function onComplete(e:Event):void {
				if (ch!=null) {
					ch.stop();
				}
				ch=sound.play();
			}

		}
		private function gameComplete():Boolean {
			var temp:Boolean=true;
			for (var i=0; i<progressArray.length; i++) {
				if (progressArray[i]==0) {
					temp=false;
					break;
				}
			}
			return temp;
		}
		private function goClicked(e:MouseEvent):void {
			
			
			
			
			Reporter.interaction("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID);

			//trace(gameComplete());
			/*if (gameComplete()) {
				SeeItCatchIt.winnerScreenProxy.init();
			} else {*/
				if (selectedObject==undefined) { // find first that isn't done
					for (var i=0; i<progressArray.length; i++) {
						if (progressArray[i]==0) {
							selectedObject=objectsArray[i];
							break;
						}
					}
				}
				
				if (selectedObject==undefined) { // everything is done so we choose first one
					//SeeItCatchIt.winnerScreenProxy.visible=true;
					selectedObject = objectsArray[0];
				}
				
				SeeItCatchIt.intro1Proxy.visible=false;
				SeeItCatchIt.intro2Proxy.visible=true;
				SeeItCatchIt.intro2Proxy.selectedObject= selectedObject;
				/* else {
					SeeItCatchIt.intro1Proxy.visible=false;
					SeeItCatchIt.intro2Proxy.visible=true;
					SeeItCatchIt.intro2Proxy.selectedObject=selectedObject;
				}*/
			//}
			
			trace("contentKey" + SeeItCatchIt._contentKey);
			trace("Reporter" + Reporter);
			trace("getNewModuleSessionID" + Reporter.getNewModuleSessionID());
			
			trace("***go for it!***");
		}
		private function createObjects():void {
			
			
			for (var i=0; i<presetsXML.GROUP.length(); i++) {
				
				var object:MapObject=new MapObject;
				object.i=i;
				object.assetNode=getItemNode(presetsXML.GROUP[i].item);
				addChild(object);
				object.x=startX;
				object.y=startY;
				
				trace("Object Creating in stage");
				
				if ((((i+1)/columns)==int(((i+1)/columns)))) {
					startY+=objectHeight+spaceY;
					startX=initStartX;
				} else {
					startX+=objectWidth+spaceX;
				}
				object.addEventListener(MouseEvent.CLICK,objectMovieClick);
				object.buttonMode=true;
				
				// Check the _bookmarkXML for previous progress and load it.
				if (_bookmarkXML.progress.item.(@num == i)[0]) {
					progressArray.push(1);
					selectedObject = object; // Temporarily set this because loadCompleteGraphic expects it to be set
					loadCompleteGraphic();
					selectedObject = undefined; // Unset it now
				} 
				else {
					progressArray.push(0);
				}
				
				objectsArray.push(object);
			}
			SeeItCatchIt.intro1Proxy.lettersMastered.t0.text=String(_bookmarkXML.progress.item.length());

			//progressArray[progressArray.length-2] = 0;
			//progressArray[progressArray.length-1]=0;
		}
		private function getItemNode(val):XML {
			var node:XML;
			for (var i=0; i<presetsXML.assets.id.length(); i++) {

				if (presetsXML.assets.id[i].name.toString()==val.toString()) {
					node=XML(presetsXML.assets.id[i]);
				}
			}

			return node;
		}
		public function completeItem():void {
			SeeItCatchIt.winnerScreenProxy.visible=false;
			SeeItCatchIt.gameScreenProxy.visible=false;
			SeeItCatchIt.mainScreenProxy.BG.visible=true;
			SeeItCatchIt.introScreenProxy.intro1.visible=true;
			SeeItCatchIt.introScreenProxy.intro2.visible=false;

			loadCompleteGraphic();

			progressArray[selectedObject.i]=1;
			// Save progress to _bookmarkXML
			if (!_bookmarkXML.progress.item.(@num == selectedObject.i)[0]) {
				_bookmarkXML.progress.appendChild(<item num={selectedObject.i}/>);
				var bookmarker:Bookmarker = new Bookmarker("see_it_catch_it", SeeItCatchIt._contentKey);
				bookmarker.save(_bookmarkXML);
			}

			selectedObject=undefined;
			progressNr=getProgressNr();
			SeeItCatchIt.intro1Proxy.lettersMastered.t0.text=progressNr.toString();

			showExplosionTimer();
			loadCongratulatorySound();
			glowSun();
		}
		private function glowSun():void {
			if (gameComplete()) {
				var glowFilter:GlowFilter=new GlowFilter(presetsXML.variables.glow,1,10,10,20,2,false,false);
				sunBtn.filters=new Array(glowFilter);
			}
		}
		private function getProgressNr():Number {
			var nr:Number=0;
			for (var i=0; i<progressArray.length; i++) {
				if (progressArray[i]==1) {
					nr++;
				}
			}
			return nr;
		}
		private function loadCompleteGraphic() {

			var del=selectedObject.getChildAt(selectedObject.numChildren-1);
			if (del.toString()=="[object CoreLoader]"&&selectedObject.numChildren>1) {
				selectedObject.removeChild(del);
			}

			var loader:CoreLoader = new CoreLoader();
			loader.load(new URLRequest(selectedObject.assetNode.imageCompleted));
			selectedObject.addChild(loader);

			selectedObject.filters=null;

			//selectedObject.buttonMode=false;
			//selectedObject.removeEventListener(MouseEvent.CLICK,objectMovieClick);
		}
		private function playItemSound():void {
//			goBtn.removeEventListener(MouseEvent.CLICK,goClicked);
			var sound:CoreSound=new CoreSound(new URLRequest(selectedObject.assetNode.audio));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);

			function onComplete(e:Event):void {
				e.target.removeEventListener(Event.COMPLETE,onComplete);

				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,playItemSound2);
			}
		}
		private function playItemSound2(e:Event):void {
			var sound:CoreSound=new CoreSound(new URLRequest(selectedObject.assetNode.audioSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);

			function onComplete(e:Event):void {
				e.target.removeEventListener(Event.COMPLETE,onComplete);

				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,function () {goBtn.addEventListener(MouseEvent.CLICK,goClicked);});
			}

		}
		private function objectMovieClick(e:MouseEvent):void {
			Reporter.interaction("see_it_catch_it", SeeItCatchIt._contentKey, SeeItCatchIt.moduleSessionID);

			if (selectedObject!=undefined) {
				selectedObject.filters=null;
			}

			selectedObject=e.currentTarget;
			playItemSound();
			var glowFilter:GlowFilter=new GlowFilter(presetsXML.variables.glow,1,10,10,20,2,false,false);
			selectedObject.filters=new Array(glowFilter);
		}
		private function loadCongratulatorySound():void {
			var i:Number=int(Math.random()*presetsXML.variables.congratulation_sound.length());
			var sound:CoreSound=new CoreSound(new URLRequest(presetsXML.variables.congratulation_sound[i]));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);

			function onComplete(e:Event):void {
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,hideExplosionTimer);
			}
		}
		private function showExplosion(e:TimerEvent):void {
			/*
			var randX:int=Math.round(Math.random()*500)+200;
			var randY:int=Math.round(Math.random()*400)+150;
			showParticle(6,randX,randY);
			var mc_Explo:MovieClip = new Explode();
			mc_Explo.x=randX;
			mc_Explo.y=randY;
			parent.addChild(mc_Explo);
			*/

		}

		private function showExplosionTimer():void {
//			goBtn.removeEventListener(MouseEvent.CLICK,goClicked);
			mapBottomInstructions.soundBtn.removeEventListener(MouseEvent.CLICK,soundClicked);
			sunBtn.removeEventListener(MouseEvent.CLICK,sunClicked);
			
			e_Time.addEventListener(TimerEvent.TIMER,showExplosion);
			e_Time.start();
		}
		private function hideExplosionTimer(e:Event):void {
			e_Time.stop();
			e_Time.removeEventListener(TimerEvent.TIMER,showExplosion);
			
			goBtn.addEventListener(MouseEvent.CLICK,goClicked);
			mapBottomInstructions.soundBtn.addEventListener(MouseEvent.CLICK,soundClicked);
			sunBtn.addEventListener(MouseEvent.CLICK,sunClicked);

		}
		private function showParticle(numP:int,targetX:int,targetY:int):void {
			for (var i:int =0; i<numP; i++) {
				var part_Star:MovieClip=new Particle(targetX,targetY);
				addChild(part_Star);
			}

		}


	}

}