package com.myf2b.soundtolettermatch 
{
	
	import com.avr.XmlLoader;
	import com.myf2b.core.Core;
	import com.myf2b.core.CoreLoader;
	import com.myf2b.core.CoreSound;
	import com.myf2b.core.Reporter;
	import com.myf2b.core.ScreenManager;
	import com.myf2b.core.Session;
	import com.myf2b.soundtolettermatch.SoundToLetterMatchShell;
	
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.utils.setTimeout;
	 
	
	import mx.events.MoveEvent;
		
	
	/**
	 * 
	 * @author haravallabhanjn
	 * 
	 */
	public class SoundToLetterMatch extends MovieClip {
		private var xmlLoader:XmlLoader;
		public var xml:XML;
		public static var presetsXML:XML;
		
		public static const BACK_TO_MAP:String = "back_to_map";
		public static var LETTER_TO_DEAL:String="m";
		private var _letterXML:XMLList;
				
		
	 

		//public static var mainScreenProxy:LetterToSoundMatch;
		
		public static var gameSpaceProxy:GameSpace;
		public static var gameScreenProxy:GameScreen;
		public var loader:CoreLoader = new CoreLoader();
		
		public static var liploader:Loader = new Loader();
		public static var LipLoaderArr = [];
		
		public var blueBrd_ldr:Loader = new Loader()
		public static var blueBoardLoaderArr = []
		
		public static var monsterLoaderArr = [];
		public static var monsterMC:MovieClip;		 
		public static var monsterCoverMc:MovieClip;
		private var getMonsterWidth:Number = 0;
		private var getMonsterHeight:Number = 0;
		
		public static var _contentKey:String = "1";
		private var _gameXMLURL:String;
		private var getIndex:*
		
		public static var moduleSessionID:String;
		private var ths:*;
		private var stg:* ;
		private var _root:* ;
		private var getMonsterWidth1:Number = 0;
		private var getMonsterHeight1:Number = 0;
		 
		
		/**
		 * 
		 * @param contentKey
		 * 
		 */
		
	public function SoundToLetterMatch(aThs,contentKey:String="1") {
		
			//trace(" SoundToLetterMatch ")
			ths = SoundToLetterMatchShell.isThis();
			stg = SoundToLetterMatchShell.isStage();
			_root = SoundToLetterMatchShell.isRoot()
			
		//	ths = aThs
			
			addEventListener(Event.ADDED,init);
			addEventListener(Event.REMOVED_FROM_STAGE,destroy);
			_contentKey = contentKey;
			//moduleSessionID = Reporter.getNewModuleSessionID();//To DB	
			
			
					 
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function destroy(e:Event):void {
			removeEventListener(Event.REMOVED_FROM_STAGE,destroy);
			//trace("Listen and Record destroy");
			//gameScreenProxy = null;
			//mainScreenProxy = null;
			//gameSpaceProxy = null;
			_contentKey = null;
			SoundMixer.stopAll();
		}
		
		/**
		 * 
		 * @param e
		 * 
		 */
		private function init(e:Event):void {
			removeEventListener(Event.ADDED,init);
			stop();
			visible=false;
			// If we got a URL from the HTML use it
			if (this.loaderInfo.parameters.gameXMLURL != undefined) {
				//_gameXMLURL = this.loaderInfo.parameters.gameXMLURL;
			}
			// Otherwise build the URL for the CDN
			else {
				//_gameXMLURL = Core.filesURL+"/listen_and_record/"+_contentKey+"/game.xml";
			}
			
			 
			xmlLoader = new XmlLoader("http://stage2.myf2b.com/xmls/9/xml_feed",presetsXMLLoaded);//game.xml
		//	xmlLoader = new XmlLoader("game.xml",presetsXMLLoaded);//			
		}
		
		/**
		 * 
		 * 
		 */
		private function presetsXMLLoaded():void {
		//	presetsXML = XML(e.target.data);
			presetsXML = XML(xmlLoader.data);
			//trace("init # "+presetsXML)
			 
			setTimeout(showStuff,100);
			gotoAndStop(2);
			 
			//gameScreenProxy = gameScreen;
			gameScreenProxy = new GameScreen()		
			loadXMLListForLetter();
			loadMonsterGraphic();
					 
			//mainScreenProxy = this;			
		}
		
		private function loadMonsterGraphic():void{		
			//loader.load(new URLRequest("http://stage2.myf2b.com/uploads/file_upload/attachment/130/SndToLetterMatchMonster.swf"));
			loader.load(new URLRequest(_letterXML.monsterGraphic));			
			loader.cacheAsBitmap = true;			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loadComplete(0))			 
		}
		
		private function loadComplete(i):Function{			
			return function(e:Event):void {
				if(monsterLoaderArr == null){
					monsterLoaderArr = new Array()
				}
				EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
				monsterLoaderArr[0] = new MovieClip();				
				monsterLoaderArr[0] =  (e.target as LoaderInfo).content;			
				ths.addChild(monsterLoaderArr[0])				 
				monsterLoaderArr[0].x = _letterXML.monsterLocationX;
				monsterLoaderArr[0].y = _letterXML.monsterLocationY;				
				monsterLoaderArr[0].buttonMode = true;		
 				getMonsterWidth = monsterLoaderArr[0].width;
				getMonsterHeight = monsterLoaderArr[0].height;
				monsterMC = SoundToLetterMatch.monsterLoaderArr[0].getChildAt(0);				
				ths.monMc.buttonMode = true;				
				loadMonsterHandBlueBoard();
				loadLipGraphic();
			}		
		}
		

		private function loadLipGraphic():void{
			trace("_letterXML.lipGraphic *(* "+_letterXML.lipGraphic)
			liploader.load(new URLRequest(_letterXML.lipGraphic))		
			liploader.contentLoaderInfo.addEventListener(Event.COMPLETE,lipLoader_loadComplete);
			liploader.cacheAsBitmap = true;		
		}
		
		private function lipLoader_loadComplete(e:Event):void{
			
			if(LipLoaderArr == null){
				LipLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			
			LipLoaderArr[0] = new MovieClip();			
			LipLoaderArr[0] =  (e.target as LoaderInfo).content
			
			LipLoaderArr[0].x = _letterXML.lipLocationX;
			LipLoaderArr[0].y = _letterXML.lipLocationY;				
			ths.addChild(LipLoaderArr[0])		 
			LipLoaderArr[0].buttonMode = true;		
			LipLoaderArr[0].visible = true;
			if(LipLoaderArr[0].mouth_mc){
				LipLoaderArr[0].mouth_mc.stop()	
			}	
			 
			LipLoaderArr[0].addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
		}		
	
		//
		private function loadMonsterHandBlueBoard():void 
		{
			blueBrd_ldr.load(new URLRequest(_letterXML.monsterHandBoardGraphic))			 
			blueBrd_ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, monsterBlueBoard_loadComplete);
			
			blueBrd_ldr.cacheAsBitmap = true;	
			
		}
		
		private function monsterBlueBoard_loadComplete(e:Event):void 
		{
			if(blueBoardLoaderArr == null){
				blueBoardLoaderArr = new Array()
			}
			EventDispatcher(e.target).removeEventListener(e.type, arguments.callee);
			
			blueBoardLoaderArr[0] = new MovieClip();			
			blueBoardLoaderArr[0] =  (e.target as LoaderInfo).content
			blueBoardLoaderArr[0].x = _letterXML.monsterHandBoardLocationX;
			blueBoardLoaderArr[0].y = _letterXML.monsterHandBoardLocationY;				
			ths.addChild(blueBoardLoaderArr[0])			 
			blueBoardLoaderArr[0].buttonMode = true;		
			blueBoardLoaderArr[0].visible = true;				
			getMonsterWidth1 =  blueBoardLoaderArr[0].width
			getMonsterHeight1 = blueBoardLoaderArr[0].height
			loadMonsterCover()
		}
		
		 
		//
		private function loadMonsterCover():void 
		{
			monsterCoverMc = new MovieClip();
			//monsterCoverMc.graphics.lineStyle(0,0xffffff);
			monsterCoverMc.graphics.beginFill(0xff00ff00,0);				
			monsterCoverMc.graphics.drawRect(0, 0, getMonsterWidth+getMonsterWidth1-80, getMonsterHeight+50);
			monsterCoverMc.graphics.endFill();
			ths.addChild(monsterCoverMc);			 
			monsterCoverMc.x = _letterXML.monsterHandBoardLocationX;
			monsterCoverMc.y = _letterXML.monsterHandBoardLocationY;
			monsterCoverMc.buttonMode = true;
			monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)			
			//_root.setChildIndex(monsterCoverMc as MovieClip,  getIndex+1);
			////////////////////////////////////////////////////////////////////////////////////
			gameSpaceProxy = new GameSpace()
			
			gameSpaceProxy.addEventListener("ENABLE_LISTENERS", enable_Listners_Handler)	
			
			gameScreenProxy.autoPlayInstruction(null)
			gameScreenProxy.addEventListener("AUTO_PLAY_INSTRUCTIONS_ENDS", autoPlay_Handler)
		}
		
		private function autoPlay_Handler(e:Event):void 
		{
			 
			gameSpaceProxy.dispatchEvent(new Event("END_AUTOPLAY_SOUND"))
			
		}
		
		private function onMonsterCover_Handler(e:MouseEvent):void {
			trace("Monstercover clicked")
			//e.currentTarget.removeEventListener(MouseEvent.CLICK, onMonsterCover_Handler);
			gameSpaceProxy.monsterLipClicked(e.target.name)
			trace("gameSpaceProxy.isDragPlaying & "+gameSpaceProxy.isDragPlaying)
			if (gameSpaceProxy.isDragPlaying) {
				gameSpaceProxy.isDragPlaying = false;
				gameSpaceProxy.dispatchEvent(new Event("LETTER_SOUND_COMPETE"));
			}else {				
				playSoundInstructions();				
			}
		}
		

		private function playSoundInstructions():void {			 
			loader.removeEventListener(MouseEvent.CLICK,playSoundInstructions);
			var sound:CoreSound=new CoreSound(new URLRequest(_letterXML.monsterHandBoardSound));
			sound.addEventListener(Event.COMPLETE,onComplete,false,0,true);
			
			function onComplete(e:Event):void {				
				e.currentTarget.removeEventListener(Event.COMPLETE,onComplete);				
				var ch:SoundChannel = new SoundChannel();
				ch=sound.play();
				ch.addEventListener(Event.SOUND_COMPLETE,onSoundComplete);
				
				function onSoundComplete(e:Event):void {					
					e.currentTarget.removeEventListener(Event.SOUND_COMPLETE, onSoundComplete);			 
					monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)
					
					if(gameSpaceProxy._isCorrectOrWrong == "wrong"){
						gameSpaceProxy.dispatchEvent(new Event("LETTER_SOUND_COMPETE"));
					}
				}
			}
		}
				
	 	private function enable_Listners_Handler(e:Event):void {
			//trace("enable_Listners_Handler")
			monsterCoverMc.addEventListener(MouseEvent.CLICK, onMonsterCover_Handler)			 
		}
		
		
		private function loadXMLListForLetter():void{
			_letterXML = presetsXML.deal.(@letter == LETTER_TO_DEAL);			
		}
		
		private function showStuff():void {
			visible=true;
		}
		
		public function backToGames():void {
			//trace("BackToGames called");
			ScreenManager.getInstance().pop();
			//dispatchEvent(new Event(BACK_TO_GAMES));
		}	
		
		public static function monster_MC(){
			return monsterMC
		}
		 
		
	}
}



